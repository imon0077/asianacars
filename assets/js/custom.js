(function($) {
  "use strict";
//Start Menu JS Function Taksi Theme
	// Superfish Menu Hover Effect
/* 	jQuery(function(){
		jQuery('ul.sf-menu').superfish({
			hoverClass:    'sfHover',         
		    pathClass:     'overideThisToUse',
		    pathLevels:    1,    
		    delay:         200, 
		    animation:     {opacity:'show', height:'show'}, 
		    speed:         'normal',   
		    autoArrows:    false, 
		    dropShadows:   true, 
		    disableHI:     false, 
		    easing:        "easeOutQuad",
		    onInit:        function(){},
		    onBeforeShow:  function(){},
		    onShow:        function(){},
		    onHide:        function(){}
		});
	});

    // Mean Menu
	jQuery(document).ready(function () {
		jQuery('.navigation nav').meanmenu();
	}); */
//End Menu JS Function Taksi Theme
 

//End Menu JS Function Taksi Theme
	$(window).on('load', function () {
        var waypointCounter = 0;
        var waypointIDCounter = 0;
        var waypoint_del_id = 0

        //$('.waypoint_geocode').val('');
        $('.waypoint_geocode').each(function (i,val){
            if ($(this).val().length > 0) {
                waypointCounter = waypointCounter + 1;
            }
        })
        $('#way_point_view').html(waypointCounter);


        //$('.selectpicker').selectpicker({});
        $('.selectpicker').selectpicker({size:10});
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            $('.selectpicker').selectpicker('mobile');
        }

        //$('.selectpicker_opt').selectpicker({});
        //$('.selectpicker').selectpicker('hide');

        $('#start').geocomplete().bind("geocode:result", function(event, result){
            //console.log(result.address_components[result.address_components.length-1].long_name)
            /*
            var postcode = '';

            if(result.address_components[result.address_components.length-1].types == 'postal_code'){
                postcode = result.address_components[result.address_components.length-1].long_name;
            }
            else postcode = '';
             $('#pickup_postcode').val(postcode);
            */
        });

        $('#end').geocomplete().bind("geocode:result", function(event, result){
            //console.log(result.address_components[result.address_components.length-1].long_name)
            /*
            var postcode = '';

            if(result.address_components[result.address_components.length-1].types == 'postal_code'){
                postcode = result.address_components[result.address_components.length-1].long_name;
            }
            else postcode = '';
             $('#dropoff_postcode').val(postcode);
            */
        });


        $('.waypoint_geocode').geocomplete()
            .bind("geocode:result", function(event, result){

                //console.log(result.address_components[result.address_components.length-1].long_name)
                /*
                var postcode = '';
                if(result.address_components[result.address_components.length-1].types == 'postal_code'){
                    postcode = result.address_components[result.address_components.length-1].long_name;
                }
                else postcode = '';

                $('#'+this.id+'_postcode').val(postcode);
                */

                //=== counter hishab
                //waypointCounter = waypointCounter + 1;
                //$('#way_point_view').html(waypointCounter);

            });

        /*
        $('#waypoint_1__del').click(function (){
            waypointCounter = waypointCounter - 1;
            $('#waypoint_1').val('')
            $('#way_point_view').html(waypointCounter);
        });

        $('#waypoint_2__del').click(function (){
            waypointCounter = waypointCounter - 1;
            $('#waypoint_2').val('')
            $('#way_point_view').html(waypointCounter);

        });

        $('#waypoint_3__del').click(function (){
            waypointCounter = waypointCounter - 1;
            $('#waypoint_3').val('')
            $('#way_point_view').html(waypointCounter);
        });

        $('#waypoint_4__del').click(function (){
            waypointCounter = waypointCounter - 1;
            $('#waypoint_4').val('')
            $('#way_point_view').html(waypointCounter);
        });
        */

        $('#reset_index').click(function(){
            waypointCounter = 0;
            $('#way_point_view').html(waypointCounter);
            $("input[type=text]").val("");
        });

        $('#add_waypoints').click(function (){
            waypointCounter = 0;
            $('.waypoint_geocode').each(function (i,val){
                if ($(this).val().length > 0) {
                    waypointCounter = waypointCounter + 1;
                }
            })
            $('#way_point_view').html(waypointCounter);
        });

        $('.waypoint_del').click(function (){
            waypoint_del_id = this.id.split('__');
            waypoint_del_id = waypoint_del_id[0];
            $('#'+waypoint_del_id).val("");
        });

        $('.bookingOption').click(function (){
                var curClick = this.id;
                $('.bookingOption').each(function (){
                    if(curClick != this.id) {
                        $('#'+this.id).removeAttr('checked');
                    }
                });
                //$('.meet').removeAttr('checked');
        });

        $('.meet').click(function (){
            var curClick = this.id;
            $('.meet').each(function (){
                if(curClick != this.id) {
                    $('#'+this.id).removeAttr('checked');
                }
            });
        });


        /*
        $("#my_waypoints").geocomplete()
            .bind("geocode:result", function(event, result){
                waypointCounter = waypointCounter + 1;
                waypointIDCounter = waypointIDCounter + 1;
                $('#way_point_view').append('<div class="waypoints_added" id="waypoint_'+waypointIDCounter+'"><span>Waypoint '+waypointCounter+': </span><span>'+result.formatted_address+'</span><span><a href="javascript:void(0);" class="waypoint_del" id="waypoint_'+waypointIDCounter+'__del">X</a></span></div>');
                $("#my_waypoints").val('');

                $('#waypoint_'+waypointCounter+'__del').click(function (){
                    waypoint_del_id = this.id.split('__');
                    waypoint_del_id = waypoint_del_id[0];
                    $('#'+waypoint_del_id).remove();
                    waypointCounter = waypointCounter - 1;
                });
            })
            .bind("geocode:error", function(event, status){
                $.log("ERROR: " + status);
            })
            .bind("geocode:multiple", function(event, results){
                $.log("Multiple: " + results.length + " results found");
            });

        */
        //$("#add_way_point").click(function(){
        //    $("#my_waypoints").trigger("geocode");
        //});
        //

        // $('.selectpicker').selectpicker('hide');
	});
//End Menu JS Function Taksi Theme



/* ==============================================
Preloader
=============================================== */

jQuery(window).load(function(){
    jQuery("#preloader").delay(500).fadeOut(1000);
		jQuery(".preload-gif").addClass('fadeOutUp');
});

})(jQuery);
/* ===================== */
/* ==============================================
Parallax
=============================================== */
	
	try {
		$(window).stellar({ 
		horizontalScrolling: false,

		});
	} catch(err) {

		}	

/* ==============================================*/




/* ==============================================
Animated Custom
=============================================== */
	try {		
		if ($(".animated")[0]) {
            $('.animated').css('opacity', '1');
			}
			$('.triggerAnimation').waypoint(function() {
            var animation = $(this).attr('data-animate');
            $(this).css('opacity', '');
            $(this).addClass("animated " + animation);

			},
                {
                    offset: '80%',
                    triggerOnce: true
                }
			);
	} catch(err) {

		}


/* ==============================================
text-rotator
=============================================== */

jQuery(document).ready(function($) {

    $("#carSelection").click(function() {

        $("input#orderId").val();
        $("input#pageId").val();

        var orderId = $("input#orderId").val();
        var pageId = $("input#pageId").val();

        $('#clickedPage').val('carSelection');


        if(pageId == 1){
            $("#editLocationForm").submit();
            return true;
        }
        else{
            $("#editCustomerForm").submit();
            return true;
            //window.location("editOrderCarDeails/"+orderId);
        }

    });

    $("#customerDeails").click(function() {

        $("input#pageId").val();
        var pageId = $("input#pageId").val();

        $('.clickedPage').val('customerDeails');
//        var clickedPage = $("input#clickedPage").val();
//        console.log(pageId);
//        console.log(clickedPage);
        if(pageId == 1){
            $("#editLocationForm").submit();
            return true;
        }
        else{
            $.bootstrapGrowl('Please click in Update Now button!', {
                type: 'danger',
                width: 'auto', align: 'center', allow_dismiss: true
            });
//            $(".editCarsForm").submit();
//            return true;
        }

    });

    $("#locations").click(function() {

        $("input#pageId").val();
        var pageId = $("input#pageId").val();
        $('#clickedPage').val('locations');
        console.log(pageId);
        if(pageId == 3){
            $("#editCustomerForm").submit();
            return true;
        }
        else{
            $(".editCarsForm").submit();
            return true;
        }

    });

//    $("#updateNow").click(function() {
//
//        $('#updateFlag').val('1');
//        var updateFlag = $("input#updateFlag").val();
//        console.log(updateFlag);
//
//    });



    $(".updateNow").click(function() {
        var thisForm = $(this).closest('form');
        //var carId = $("input#carId").val();
        var carId = thisForm.find(".carId").val();
        console.log(carId);
        $('#updateFlag_'+carId).val('1');
        var updateFlag = $("input#updateFlag_"+carId).val();
        //console.log(updateFlag);

    });

    $(".next").click(function() {
        var thisForm = $(this).closest('form');
        var carId = thisForm.find(".carId").val();
        console.log(carId);
        $('#updateFlag_'+carId).val('2');
        var updateFlag = $("input#updateFlag_"+carId).val();
        console.log(updateFlag);

    });

    $(".singleTrip").click(function() {
        var thisForm = $(this).closest('form');
        thisForm.find(".next").hide();
        thisForm.find(".updateNow").show();
        thisForm.find(".tourType").val('0');
        var tourType = thisForm.find(".tourType").val();
        console.log(tourType);
    });

    $(".roundTrip").click(function() {
        var thisForm = $(this).closest('form');
        thisForm.find(".updateNow").hide();
        thisForm.find(".next").show();
        thisForm.find(".tourType").val('1');

        var tourType = thisForm.find(".tourType").val();
        console.log(tourType);
    });

//    $('.next').hide();




//end
});












