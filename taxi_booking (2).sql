-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 06, 2016 at 11:24 AM
-- Server version: 5.6.23
-- PHP Version: 5.6.8RC1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `taxi_booking`
--

-- --------------------------------------------------------

--
-- Table structure for table `airports`
--

CREATE TABLE IF NOT EXISTS `airports` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `booking_time`
--

CREATE TABLE IF NOT EXISTS `booking_time` (
  `id` int(10) unsigned NOT NULL,
  `minimum_booking_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `booking_time`
--

INSERT INTO `booking_time` (`id`, `minimum_booking_time`, `created_at`, `updated_at`) VALUES
(1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `car_category`
--

CREATE TABLE IF NOT EXISTS `car_category` (
  `id` int(10) unsigned NOT NULL,
  `cat_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `max_person` int(11) NOT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_ratio` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `max_suitcases` int(11) NOT NULL,
  `max_carry_on` int(11) NOT NULL,
  `car_pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `car_category`
--

INSERT INTO `car_category` (`id`, `cat_name`, `max_person`, `details`, `price_ratio`, `created_at`, `updated_at`, `max_suitcases`, `max_carry_on`, `car_pic`) VALUES
(1, 'Saloon', 4, 'Its a mini car', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 6, 'saloon.jpg'),
(2, 'Estate', 3, 'Its a large car', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 5, 'estate.jpg'),
(3, 'MPV', 3, 'Its a large car', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 5, 'mpv.jpg'),
(4, 'Executive', 3, 'Its a large car', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 5, 'executive.jpg'),
(5, '8 Seater Minibus', 3, 'Its a large car', 50, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 5, 'miniBus.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `car_details`
--

CREATE TABLE IF NOT EXISTS `car_details` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `total_seats` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `car_details`
--

INSERT INTO `car_details` (`id`, `name`, `category`, `total_seats`, `driver_id`, `created_at`, `updated_at`) VALUES
(1, 'Saloon', 1, 6, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Estate', 2, 3, 9, '0000-00-00 00:00:00', '2015-10-03 23:10:45'),
(3, 'MPV', 3, 6, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '8 Seaters Mini Bus', 5, 12, 2, '2015-08-04 17:10:46', '2015-08-04 17:10:46'),
(6, 'Axio', 2, 5, 2, '2016-01-10 05:36:21', '2016-01-10 05:36:21');

-- --------------------------------------------------------

--
-- Table structure for table `cruiseports`
--

CREATE TABLE IF NOT EXISTS `cruiseports` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE IF NOT EXISTS `drivers` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` int(11) NOT NULL,
  `residence_no` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`id`, `user_id`, `first_name`, `last_name`, `contact_name`, `address`, `gender`, `city`, `street`, `postcode`, `residence_no`, `email`, `phone_number`, `created_at`, `updated_at`) VALUES
(2, 2, 'Mr. ABCEF', 'Khondokar DOT DOT', 'XYZ ABvdsfds', 'abcdefg', 'Female', 'Ctg', 'CTG', 4000, 234, 'me@imonislam.com', '01673346504', '0000-00-00 00:00:00', '2015-10-04 00:37:55'),
(9, 3, 'imon', 'islam', 'imon', 'ctg', 'Male', 'Chittagong', 'LA', 4000, 0, 'me@imonislam.com', '343443', '2015-09-02 20:43:45', '2015-09-02 20:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_06_28_225802_create_users_table', 1),
('2015_06_28_233453_create_pricelist_table', 1),
('2015_06_29_002414_update_pricelist', 1),
('2015_06_29_015002_create_orders_table', 1),
('2015_06_29_030635_create_passengers_table', 1),
('2015_06_29_033558_create_car_details_table', 1),
('2015_06_29_034412_create_car_category_table', 1),
('2015_06_29_034606_create_drivers_table', 1),
('2015_06_29_040600_create_pickup_from_airport_table', 1),
('2015_06_30_222412_create_airports_table', 1),
('2015_06_30_222647_create_cruiseports_table', 1),
('2015_07_01_020337_add_col_to_orders', 1),
('2015_07_01_024252_add_col_to_car_category', 1),
('2015_07_01_025134_modify_car_category', 1),
('2015_07_08_025331_create_roles_table', 1),
('2015_07_08_025414_create_role_user_table', 1),
('2015_08_13_041106_add_payment_status_to_orders', 1),
('2015_08_15_213752_modify_tbl_orders', 1),
('2015_08_22_060334_cost_ratio', 1),
('2015_06_28_225802_create_users_table', 1),
('2015_06_28_233453_create_pricelist_table', 1),
('2015_06_29_002414_update_pricelist', 1),
('2015_06_29_015002_create_orders_table', 1),
('2015_06_29_030635_create_passengers_table', 1),
('2015_06_29_033558_create_car_details_table', 1),
('2015_06_29_034412_create_car_category_table', 1),
('2015_06_29_034606_create_drivers_table', 1),
('2015_06_29_040600_create_pickup_from_airport_table', 1),
('2015_06_30_222412_create_airports_table', 1),
('2015_06_30_222647_create_cruiseports_table', 1),
('2015_07_01_020337_add_col_to_orders', 1),
('2015_07_01_024252_add_col_to_car_category', 1),
('2015_07_01_025134_modify_car_category', 1),
('2015_07_08_025331_create_roles_table', 1),
('2015_07_08_025414_create_role_user_table', 1),
('2015_08_13_041106_add_payment_status_to_orders', 1),
('2015_08_15_213752_modify_tbl_orders', 1),
('2015_08_22_060334_cost_ratio', 1),
('2015_06_28_225802_create_users_table', 1),
('2015_06_28_233453_create_pricelist_table', 2),
('2015_06_29_002414_update_pricelist', 3),
('2015_06_29_015002_create_orders_table', 4),
('2015_06_29_030635_create_passengers_table', 5),
('2015_06_29_033558_create_car_details_table', 6),
('2015_06_29_034412_create_car_category_table', 7),
('2015_06_29_034606_create_drivers_table', 8),
('2015_06_29_040600_create_pickup_from_airport_table', 9),
('2015_06_30_222412_create_airports_table', 10),
('2015_06_30_222647_create_cruiseports_table', 10),
('2015_07_01_020337_add_col_to_orders', 11),
('2015_07_01_024252_add_col_to_car_category', 12),
('2015_07_01_025134_modify_car_category', 13),
('2015_07_08_025331_create_roles_table', 14),
('2015_07_08_025414_create_role_user_table', 14),
('2015_08_24_024237_modify_table_passengers', 15),
('2015_08_24_030833_modify_table_pickup_from_airport', 15),
('2015_09_01_224454_add_address_field_to_orders', 16),
('2015_09_03_023933_modify_tbl_drivers', 17),
('2015_10_03_211500_modify_orders_table', 18),
('2015_10_24_232041_create_tbl_setting', 19),
('2015_10_24_233751_add_row_into_tbl_setting', 20),
('2015_10_27_092934_add_col_to_users_tbl', 21),
('2015_10_27_093025_modify_setting_tbl', 21),
('2015_10_27_093728_modify_users_tbl', 22),
('2015_10_27_110054_modify_orders_table_2', 23),
('2015_11_09_111246_add_col_into_tbl_orders', 24),
('2015_11_09_112203_add_editedBy_col_into_tbl_orders', 25),
('2015_12_08_094622_create_lookup_table', 26),
('2015_12_08_110112_modify_lookup_table', 27),
('2015_12_08_120836_modify_lookup_table2', 28),
('2015_12_08_122740_modify_lookup_table3', 29),
('2015_12_22_061300_create_table_special_route', 30),
('2016_01_10_064231_set_booking_time', 31),
('2016_01_11_062946_create_table_routes', 31);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `pickup_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pickup address through gmap',
  `pickup_address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pickup_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'meeting point (e.g. house/flat no)',
  `dropoff_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'dropoff address through gmap',
  `dropoff_address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dropoff_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'dropoff details (e.g. house/flat no)',
  `no_of_person` int(11) NOT NULL,
  `no_of_child` int(11) NOT NULL,
  `no_of_infant` int(11) NOT NULL,
  `no_of_booster` int(11) NOT NULL,
  `car_details` int(11) NOT NULL,
  `total_bill` double(11,2) NOT NULL,
  `total_distance` double(11,2) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `via_points` longtext COLLATE utf8_unicode_ci NOT NULL,
  `picking_category` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'Like Instant, Prebook, Airport',
  `return_booking` int(11) NOT NULL COMMENT '0 = No, 1 = Yes',
  `status` int(11) NOT NULL COMMENT '0 = Pending, 1 = Approve, 2 = Reject',
  `pay_via` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_customer_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_charge_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` int(11) NOT NULL COMMENT '0 = Unpaid; 1 = Paid; 2 = Partially paid',
  `date_time` int(11) NOT NULL,
  `return_date_time` int(11) NOT NULL,
  `special_date` tinyint(4) NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `editedBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `route1_lat_lon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route1_distance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route1_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route1_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route2_lat_lon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route2_distance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route2_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route2_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suitcases` int(11) NOT NULL,
  `carry_on` int(11) NOT NULL,
  `meetngreet` tinyint(1) NOT NULL DEFAULT '0',
  `pickup_address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pickup_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dropoff_address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dropoff_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `pickup_address`, `pickup_address1`, `pickup_details`, `dropoff_address`, `dropoff_address1`, `dropoff_details`, `no_of_person`, `no_of_child`, `no_of_infant`, `no_of_booster`, `car_details`, `total_bill`, `total_distance`, `driver_id`, `total_time`, `via_points`, `picking_category`, `return_booking`, `status`, `pay_via`, `card_customer_id`, `card_charge_id`, `payment_status`, `date_time`, `return_date_time`, `special_date`, `comments`, `createdBy`, `editedBy`, `created_at`, `updated_at`, `route1_lat_lon`, `route1_distance`, `route1_time`, `route1_address`, `route2_lat_lon`, `route2_distance`, `route2_time`, `route2_address`, `suitcases`, `carry_on`, `meetngreet`, `pickup_address2`, `pickup_postcode`, `dropoff_address2`, `dropoff_postcode`) VALUES
(1, 1, 'London, United Kingdom', 'ctg', '', 'London Heathrow Terminal 5, Longford, United Kingdom', 'dewanat', '', 0, 0, 0, 0, 0, 1000.00, 0.00, 0, '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, '', '', '', '2015-07-27 22:30:05', '2015-11-04 00:31:23', '', '', '', '', '', '', '', '', 0, 0, 0, '', '0000', 'bari', 'bari new'),
(2, 2, 'St Louis, Mo', '', '', 'Oklahoma City, Ok', '', '', 0, 0, 0, 0, 1, 300.00, 498.78, 0, '', '', '', 0, 1, 'Card payment via stripe with SSL', 'cus_7V4DLVTXmCYvJQ', 'ch_17G458CF5OBk5IcOCmwxWV6Y', 1, 0, 0, 0, '', '', '1', '2015-08-26 22:50:01', '2015-12-09 03:17:55', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(3, 1, 'st louis, mo', '', '', 'oklahoma city, ok', '', '', 0, 0, 0, 0, 1, 4015.00, 803.00, 0, '', '', '', 0, 1, NULL, NULL, NULL, 1, 2015, 0, 0, '', '', '', '2015-08-26 22:30:05', '2015-08-26 22:30:05', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(4, 1, 'st louis, mo', '', '', 'oklahoma city, ok', '', '', 0, 0, 0, 0, 1, 4015.00, 803.00, 2, '', '', '', 0, 1, NULL, NULL, NULL, 1, 2015, 0, 0, '', '', '', '2015-07-27 22:30:05', '2015-07-27 22:30:05', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(5, 3, 'st louis, mo', '', '', 'oklahoma city, ok', '', '', 0, 0, 0, 0, 1, 2757.00, 802.00, 0, '', '', '', 0, 0, NULL, NULL, NULL, 0, 2015, 0, 0, '', '', '', '2015-08-09 16:04:48', '2015-08-27 16:04:48', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(6, 3, 'London+Borough+of+Southwark,+United+Kingdom', '', '', 'Cardiff,+United+Kingdom', '', '', 0, 0, 0, 0, 2, 1233.00, 383.00, 2, '', 'Heathrow+Terminal+5,+Longford,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, NULL, NULL, NULL, 0, 1440867600, 1441008000, 0, '', '', '', '2015-08-25 17:10:53', '2015-08-25 17:10:53', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(7, 4, 'London,+United+Kingdom', '', '', 'Barming,+Maidstone+District,+United+Kingdom', '', '', 0, 0, 0, 0, 2, 283.00, 48.00, 2, '', '', '', 1, 0, NULL, NULL, NULL, 0, 1440748800, 1440921600, 0, '', '', '', '2015-08-25 23:50:15', '2015-08-25 23:50:15', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(8, 3, 'London, United Kingdom', '', '', 'Heathrow Terminal 5, Longford, United Kingdom', '', '', 0, 0, 0, 0, 1, 104.00, 19.00, 1, '', '', '', 1, 0, NULL, NULL, NULL, 0, 0, 1441094400, 0, '', '', '', '2015-08-29 22:30:41', '2015-08-29 22:30:41', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(9, 3, 'London, United Kingdom', '', '', 'Heathrow Terminal 5, Longford, United Kingdom', '', '', 0, 0, 0, 0, 1, 104.00, 19.00, 1, '', '', '', 1, 0, NULL, NULL, NULL, 0, 0, 1441267200, 0, '', '', '', '2015-08-29 22:43:19', '2015-08-29 22:43:19', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(10, 4, 'London, United Kingdom', '', '', 'Barmouth, United Kingdom', '', '', 0, 0, 0, 0, 2, 1440.00, 244.00, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom', '', 1, 0, NULL, NULL, NULL, 0, 0, 1441267200, 0, '', '', '', '2015-08-30 17:05:39', '2015-08-30 17:05:39', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(11, 4, 'London, United Kingdom', '', '', 'London Borough of Hillingdon, United Kingdom', '', '', 0, 0, 0, 0, 2, 3189.00, 540.00, 2, '', 'Barming,+Maidstone+District,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, NULL, NULL, NULL, 0, 0, 1441180800, 0, '', '', '', '2015-08-30 18:06:47', '2015-08-30 18:06:47', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(12, 4, 'London, United Kingdom', '', '', 'London Borough of Hillingdon, United Kingdom', '', '', 0, 0, 0, 0, 2, 3189.00, 540.00, 2, '', 'Barming,+Maidstone+District,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, NULL, NULL, NULL, 0, 1440867600, 1441180800, 0, '', '', '', '2015-08-30 18:07:29', '2015-08-30 18:07:29', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(13, 1, 'London, United Kingdom', '', '', 'London Borough of Hillingdon, United Kingdom', '', '', 2, 2, 0, 0, 2, 3189.00, 540.00, 2, '', 'Barming,+Maidstone+District,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, NULL, NULL, NULL, 0, 1440867600, 1440835200, 0, '', '', '', '2015-08-30 18:09:42', '2015-08-30 18:09:42', '', '', '', '', '', '', '', '', 2, 0, 0, '', '', '', ''),
(14, 4, 'London, United Kingdom', '', '', 'London Borough of Hillingdon, United Kingdom', '', '', 2, 2, 0, 0, 2, 3189.00, 540.00, 2, '', 'Barming,+Maidstone+District,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, NULL, NULL, NULL, 0, 1440867600, 1441008000, 0, '', '', '', '2015-08-30 19:29:40', '2015-08-30 19:29:40', '', '', '', '', '', '', '', '', 2, 0, 0, '', '', '', ''),
(15, 4, 'London, United Kingdom', '', '', 'London Borough of Hillingdon, United Kingdom', '', '', 2, 2, 0, 0, 2, 3189.00, 540.00, 2, '', 'Barming,+Maidstone+District,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, NULL, NULL, NULL, 0, 1440867600, 1441267200, 0, '', '', '', '2015-08-30 19:31:56', '2015-08-30 19:31:56', '', '', '', '', '', '', '', '', 2, 0, 0, '', '', '', ''),
(16, 5, 'London, United Kingdom', '', 'House number : 06, Rd : 3, abcd area Postcode : 4000', 'Barmouth, United Kingdom', '', 'House : 50, Rd : 04, O. R. Nizam R/A. Postcode : 4000', 2, 2, 0, 0, 2, 1385.00, 234.00, 2, '', '', '', 1, 0, NULL, NULL, NULL, 0, 0, 1441267200, 0, '', '', '', '2015-08-30 20:53:55', '2015-08-30 20:53:55', '', '', '', '', '', '', '', '', 2, 0, 0, '', '', '', ''),
(17, 4, 'Barmouth, United Kingdom', 'Rd: 01, H : 50', '', 'Barming, London, United Kingdom', 'Rd : 05, H : 334', '', 2, 3, 0, 0, 2, 905.00, 281.00, 2, '', '', '', 1, 0, NULL, NULL, NULL, 0, 0, 1444291200, 0, 'testing fdfd', '', '', '2015-09-01 17:20:44', '2015-09-01 17:20:44', '', '', '', '', '', '', '', '', 3, 0, 0, '', '4000', '', '4001'),
(18, 4, 'London, United Kingdom', '', '', 'Durham, United Kingdom', '', '', 2, 2, 0, 0, 1, 1408.00, 262.00, 1, '', '', '', 1, 0, NULL, NULL, NULL, 0, 1440867600, 1444291200, 0, 'fsdfdsfsd', '', '', '2015-09-02 15:46:52', '2015-09-02 15:46:52', '', '', '', '', '', '', '', '', 2, 0, 0, '', '', '', ''),
(19, 1, 'London, United Kingdom', 'House number : 06, Rd : 3', '', 'Barmouth, United Kingdom', 'House : 50, Rd : 04', '', 2, 2, 0, 0, 1, 1258.00, 234.00, 2, '', '', '', 1, 1, NULL, NULL, NULL, 0, 1441903500, 1442140200, 0, 'Test', '', '', '2015-09-05 22:02:38', '2015-10-04 21:22:12', '', '', '', '', '', '', '', '', 2, 0, 0, 'Besides Bata show room', '4100', 'O. R. Nizam R/A.', '4000'),
(20, 4, 'London, United Kingdom', 'sdfsdf', '', 'Barmouth, United Kingdom', 'sfsdfsjd', '', 4, 2, 0, 0, 1, 1266.00, 234.00, 2, '', '', '', 1, 4, NULL, NULL, NULL, 0, 1440867600, 0, 0, 'test', '', '', '2015-09-13 16:01:09', '2015-10-05 23:09:22', '', '', '', '', '', '', '', '', 2, 0, 0, 'sdfdsf', '435', 'sdfsdjfl', '4343'),
(21, 6, 'London, United Kingdom', 'sdfsd', '', 'Barmouth, United Kingdom', 'dfdsf', '', 3, 4, 0, 0, 1, 1266.00, 234.00, 1, '', '', '', 1, 0, NULL, NULL, NULL, 0, 1442217600, 1442217600, 0, 'sddsf', '', '', '2015-09-13 17:14:40', '2015-09-13 17:14:40', '', '', '', '', '', '', '', '', 4, 0, 0, 'dfsd', 'sfsd', 'fsdf', 'sdfsd'),
(22, 7, 'London, United Kingdom', 'sfsdf', '', 'Barmouth, United Kingdom', 'sdfsdf', '', 44, 3, 0, 0, 1, 1258.00, 234.00, 1, '', '', '', 1, 0, NULL, NULL, NULL, 0, 1442995200, 1443081600, 0, '', '', '', '2015-09-15 20:15:19', '2015-09-15 20:15:19', '', '', '', '', '', '', '', '', 33, 0, 0, '', 'sdfsdf', 'sdfdsf', 'dfds'),
(23, 8, 'London, United Kingdom', 'sdfdsf', '', 'Barmouth, United Kingdom', 'sdfds', '', 2, 4, 0, 0, 1, 1266.00, 234.00, 1, '', '', '', 1, 0, NULL, NULL, NULL, 0, 1443168000, 1443427200, 0, 'dsds', '', '', '2015-09-20 18:10:28', '2015-09-20 18:10:28', '', '', '', '', '', '', '', '', 3, 0, 0, 'sdfsdf', 'sdfds', 'sdfds', 'sdfsdf'),
(24, 9, 'London Borough of Hillingdon, United Kingdom', '', '', 'Barmouth, United Kingdom', '', '', 0, 0, 0, 0, 1, 287.00, 220.34, 1, '', '', '', 0, 0, NULL, NULL, NULL, 0, 1445328000, 0, 0, '', '', '', '2015-10-19 23:45:44', '2015-10-19 23:45:44', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(25, 9, 'London Borough of Hillingdon, United Kingdom', '', '', 'Barmouth, United Kingdom', '', '', 0, 0, 0, 0, 1, 287.00, 220.34, 1, '', '', '', 0, 0, NULL, NULL, NULL, 0, 1445328000, 0, 0, '', '', '', '2015-10-19 23:51:43', '2015-10-19 23:51:43', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(26, 1, 'London, United Kingdom', 'dfsdf', '', 'Barming, Maidstone District, United Kingdom', 'dfsf', '', 2, 2, 0, 0, 1, 119.15, 37.58, 1, '', '', '', 1, 0, 'Cash', 'cus_7FK6zTNXEO5dvy', 'ch_170pSMCF5OBk5IcOBeYP9MCe', 0, 1446166800, 1446253200, 0, 'dsfds', '', '', '2015-10-28 02:38:54', '2015-10-28 02:38:54', '', '', '', '', '', '', '', '', 2, 0, 8, 'dfdsf', 'sdfsd', 'dfsfd', 'ssfdsdf'),
(27, 1, 'London, United Kingdom', 'dfsdf test', '', 'Barming, Maidstone District, United Kingdom', 'dfsf test', '', 4, 4, 0, 0, 2, 122.85, 37.58, 2, '', '', '', 1, 0, 'Cash', 'cus_7FK6zTNXEO5dvy', 'ch_170pSMCF5OBk5IcOBeYP9MCe', 0, 1441152000, 0, 0, 'Test comment  test testing', '', '', '2015-10-28 02:41:54', '2015-11-09 03:57:36', '', '', '', '', '', '', '', '', 4, 0, 8, 'GEC test', 'Agrabad test', 'dfsfd test', 'ssfdsdf test'),
(28, 1, 'London, United Kingdom', 'dfsdf', '', 'Barming, Maidstone District, United Kingdom', 'dfsf', '', 2, 2, 0, 0, 1, 119.15, 37.58, 2, '', '', '', 1, 0, 'Cash', 'cus_7FK6zTNXEO5dvy', 'ch_170pSMCF5OBk5IcOBeYP9MCe', 0, 1446166800, 1446253200, 0, 'dsfds', '', '', '2015-10-28 02:46:58', '2015-10-28 02:46:58', '', '', '', '', '', '', '', '', 2, 0, 8, 'dfdsf', 'sdfsd', 'dfsfd', 'ssfdsdf'),
(29, 1, 'London, United Kingdom', 'dfsdf', '', 'Barming, Maidstone District, United Kingdom', 'dfsf', '', 2, 2, 0, 0, 1, 119.15, 37.58, 2, '', '', '', 1, 0, 'Cash', 'cus_7FK6zTNXEO5dvy', 'ch_170pSMCF5OBk5IcOBeYP9MCe', 0, 1446166800, 1446253200, 0, 'dsfds', '', '', '2015-10-28 02:54:07', '2015-10-28 02:54:07', '', '', '', '', '', '', '', '', 2, 0, 8, 'dfdsf', 'sdfsd', 'dfsfd', 'ssfdsdf'),
(30, 3, 'London, United Kingdom', 'sdfsd', '', 'London Borough of Hillingdon, United Kingdom', 'dfsdfsd', '', 2, 2, 0, 0, 2, 44.00, 17.18, 9, '', '', '', 0, 0, 'Cash', NULL, NULL, 0, 0, 0, 0, 'dfdsf', '', '', '2015-10-28 06:40:49', '2015-10-28 06:40:49', '', '', '', '', '', '', '', '', 2, 0, 0, 'dfdsf', 'safdaf', 'sdfds', 'dsfdsf'),
(31, 3, 'St Louis, Mo', 'sdfsd', '', 'Oklahoma City, Ok', 'dfsdfsd', '', 2, 2, 0, 0, 2, 1071.00, 498.78, 9, '', '', '', 0, 0, 'Card payment via stripe with SSL', 'cus_7V25SitTgNmBnR', 'ch_17G21DCF5OBk5IcOFLoWJdGY', 0, 0, 0, 0, 'dfdsf', '', '1', '2015-10-28 06:44:40', '2015-12-09 01:06:07', '', '', '', '', '', '', '', '', 2, 0, 0, 'dfdsf', 'safdaf', 'sdfds', 'dsfdsf'),
(32, 2, 'London Borough of Hillingdon, United Kingdom', 'Bata goli', '', 'Barmouth, United Kingdom', 'ABCD', '', 3, 3, 0, 0, 0, 120.00, 0.00, 9, '', '', '', 0, 0, 'Cash', NULL, NULL, 0, 0, 0, 0, '', '', '', '2015-10-28 06:52:44', '2015-11-03 07:09:28', '', '', '', '', '', '', '', '', 0, 0, 0, '', '4100', 'Dewanhat', '4000'),
(34, 11, 'London, United Kingdom', 'gec', '', 'Barming, Maidstone District, United Kingdom', 'Agrabad', '', 1, 1, 0, 0, 3, 2687.30, 845.35, 2, '', 'Glasgow,+United+Kingdom', '', 1, 0, 'Cash', NULL, NULL, 0, 1448643600, 1449074700, 0, 'This is test comment', '', '', '2015-11-09 04:28:12', '2015-11-09 04:28:12', '', '', '', '', '', '', '', '', 1, 0, 8, 'near gec', '4200', 'After Agrabad', '4000'),
(35, 11, 'London, United Kingdom', 'gec', '', 'Barming, Maidstone District, United Kingdom', 'Agrabad', '', 2, 2, 0, 0, 3, 1374.00, 845.35, 2, '', 'Glasgow,+United+Kingdom', '', 0, 0, 'Cash', NULL, NULL, 0, 1447317900, 0, 0, 'This is test comment', '', '1', '2015-11-09 05:09:53', '2015-11-10 03:27:08', '', '', '', '', '', '', '', '', 2, 0, 8, 'near gec', '4200', 'After Agrabad', '4000'),
(36, 12, 'London, United Kingdom', 'test test', '', 'Barming, Maidstone District, United Kingdom', 'Agrabad', '', 3, 5, 0, 0, 2, 2367.50, 846.00, 9, '', 'Glasgow,+United+Kingdom|Glasgow+Central+Station,+Gordon+Street,+Glasgow,+United+Kingdom', '', 1, 0, 'Cash', NULL, NULL, 0, 1448122500, 0, 0, 'fsdfsdfds test', '1', '1', '2015-11-10 03:34:46', '2015-11-16 02:57:24', '', '', '', '', '', '', '', '', 5, 0, 8, 'gec', '4000', 'bata', 'nasirbad'),
(37, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Ml8zTAIMkz2Cu', 'ch_1781bzCF5OBk5IcOhjp6YHa3', 0, 1448003700, 1448695800, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-16 23:02:37', '2015-11-16 23:02:37', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(38, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Ml8zTAIMkz2Cu', 'ch_1781bzCF5OBk5IcOhjp6YHa3', 0, 1448003700, 1448695800, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-16 23:18:34', '2015-11-16 23:18:34', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(39, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Ml8zTAIMkz2Cu', 'ch_1781bzCF5OBk5IcOhjp6YHa3', 0, 1448003700, 1448695800, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-16 23:58:27', '2015-11-16 23:58:27', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(40, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Ml8zTAIMkz2Cu', 'ch_1781bzCF5OBk5IcOhjp6YHa3', 0, 1448003700, 1448695800, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:08:04', '2015-11-17 00:08:04', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(41, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Ml8zTAIMkz2Cu', 'ch_1781bzCF5OBk5IcOhjp6YHa3', 0, 1448003700, 1448695800, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:11:20', '2015-11-17 00:11:20', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(42, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Ml8zTAIMkz2Cu', 'ch_1781bzCF5OBk5IcOhjp6YHa3', 0, 1448003700, 1448695800, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:13:09', '2015-11-17 00:13:09', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(43, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Ml8zTAIMkz2Cu', 'ch_1781bzCF5OBk5IcOhjp6YHa3', 0, 1448003700, 1448695800, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:18:49', '2015-11-17 00:18:49', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(44, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:21:21', '2015-11-17 00:21:21', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(45, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:24:06', '2015-11-17 00:24:06', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(46, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:24:22', '2015-11-17 00:24:22', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(47, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:24:54', '2015-11-17 00:24:54', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(48, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:27:16', '2015-11-17 00:27:16', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(49, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:27:45', '2015-11-17 00:27:45', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(50, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:33:00', '2015-11-17 00:33:00', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(51, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:33:18', '2015-11-17 00:33:18', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(52, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:41:30', '2015-11-17 00:41:30', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(53, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:45:01', '2015-11-17 00:45:01', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(54, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:45:27', '2015-11-17 00:45:27', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(55, 13, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 2, 271.25, 94.36, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7MmOIiKXHtAnRq', 'ch_1782qCCF5OBk5IcONaSeC7dB', 0, 1448046900, 1448703000, 0, 'Testing comments', 'OnlineBooking', '', '2015-11-17 00:46:18', '2015-11-17 00:46:18', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(56, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:07:07', '2015-11-17 03:07:07', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(57, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:08:09', '2015-11-17 03:08:09', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(58, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:16:08', '2015-11-17 03:16:08', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(59, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:17:13', '2015-11-17 03:17:13', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(60, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:17:31', '2015-11-17 03:17:31', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(61, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:18:43', '2015-11-17 03:18:43', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(62, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:20:29', '2015-11-17 03:20:29', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(63, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:20:47', '2015-11-17 03:20:47', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(64, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:21:40', '2015-11-17 03:21:40', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(65, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:22:12', '2015-11-17 03:22:12', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(66, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:23:00', '2015-11-17 03:23:00', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(67, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 5, 366.80, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1449287100, 0, 'Testing Comment', 'OnlineBooking', '', '2015-11-17 03:23:26', '2015-11-17 03:23:26', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(68, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 4, 4, 0, 0, 3, 341.25, 89.35, 2, '', 'London Borough of Hillingdon, United Kingdom|London Borough of Southwark, United Kingdom', '', 1, 0, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 1448897400, 0, 'Testing Comment', 'OnlineBooking', '1', '2015-11-17 03:26:46', '2015-11-30 23:26:56', '', '', '', '', '', '', '', '', 4, 0, 8, 'Test address 2 Test', '4100', 'Test address 2 dropoff Testing', '4000'),
(69, 14, 'River Thames, United Kingdom', '', '', 'Barming, Maidstone District, United Kingdom', '', '', 0, 0, 0, 0, 2, 754.00, 94.36, 2, '', 'London Borough Of Hillingdon, United Kingdom|London Borough Of Southwark, United Kingdom|Liverpool, United Kingdom', '', 0, 5, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 0, 0, '', 'OnlineBooking', '1', '2015-11-17 03:27:27', '2015-11-26 03:07:54', '', '', '', '', '', '', '', '', 0, 0, 8, '', '', '', ''),
(70, 14, 'London, United Kingdom', 'Test address 1', '', 'Barming, Maidstone District, United Kingdom', 'Test address 1 dropoff', '', 2, 4, 0, 0, 1, 0.00, 94.36, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom|London+Borough+of+Islington,+United+Kingdom|London+Borough+of+Camden,+United+Kingdom', '', 0, 1, 'Cash', 'cus_7Mp4Fa6o99GA6l', 'ch_1785QcCF5OBk5IcOrfkODYXK', 0, 1448040600, 0, 0, 'Testing Comment', 'OnlineBooking', '1', '2015-11-17 03:28:53', '2015-11-19 08:00:24', '', '', '', '', '', '', '', '', 2, 0, 8, 'Test address 2', '4100', 'Test address 2 dropoff', '4000'),
(71, 15, 'London, United Kingdom', 'sdfdsf', '', 'Glasgow, United Kingdom', 'sdfsdf', '', 2, 2, 0, 0, 2, 598.00, 412.28, 9, '', '', '', 0, 0, 'Card payment via stripe with SSL', NULL, NULL, 0, 1451108700, 0, 0, 'TEST', '1', '', '2015-12-02 00:41:00', '2015-12-02 00:41:00', '', '', '', '', '', '', '', '', 2, 0, 8, 'sdfdsf', 'sdfsdf', 'sdfdsf', 'sdfsdf'),
(72, 9, 'London, United Kingdom', '', '', 'Glasgow, United Kingdom', '', '', 0, 0, 0, 0, 2, 950.00, 412.28, 9, '', '', '', 0, 0, 'Card payment via stripe with SSL', 'cus_7V6PhEGTGiiY3I', 'ch_17G6CWCF5OBk5IcOOv7cZ84p', 0, 1451005200, 0, 1, '', '1', '', '2015-12-09 05:33:41', '2015-12-09 05:33:41', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(73, 9, 'London, United Kingdom', '', '', 'Birmingham, United Kingdom', '', '', 0, 0, 0, 0, 2, 368.75, 129.31, 9, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Hillingdon,+United+Kingdom', '', 1, 0, 'Card payment via stripe with SSL', 'cus_7VOLpc5PRFhjSP', 'ch_17GNYkCF5OBk5IcOBs4BBu7Z', 0, 1449709200, 1449709200, 0, '', '1', '', '2015-12-10 00:05:48', '2015-12-10 00:05:48', '', '', '', '', '', '', '', '', 0, 0, 8, '', '', '', ''),
(74, 1, 'London, United Kingdom', 'Test address 1', '', 'Glasgow, United Kingdom', 'Test address 1 dropoff', '', 0, 1, 0, 0, 2, 1156.55, 411.75, 9, '', '', '', 1, 0, 'Card payment via stripe with SSL', 'cus_7VTvr7U1LdxZSF', 'ch_17GSxDCF5OBk5IcOnNGrT5fE', 0, 1449882000, 1451091600, 1, 'Test', '1', '1', '2015-12-10 05:13:15', '2016-01-03 00:43:47', '', '', '', '', '', '', '', '', 1, 0, 8, 'Test address 2 Test', '4000', 'O. R. Nizam R/A.', '4000'),
(75, 16, '4K Lomond Street, Helensburgh, United Kingdom', 'Boomerang Street', '', 'London Borough of Southwark, United Kingdom', '44, Baker Street', '', 1, 0, 0, 0, 1, 566.00, 434.65, 2, '', '', '', 0, 0, 'cash', '', '', 0, 1453942800, 0, 0, '', '1', '', '2016-01-06 05:03:08', '2016-01-06 05:03:08', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(76, 16, '4K Lomond Street, Helensburgh, United Kingdom', 'Boomerang Street', '', 'London Borough of Southwark, United Kingdom', '44, Baker Street', '', 1, 0, 0, 0, 1, 566.00, 434.65, 2, '', '', '', 0, 0, 'cash', '', '', 0, 1453942800, 0, 0, '', '1', '', '2016-01-06 05:13:19', '2016-01-06 05:13:19', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', ''),
(77, 16, 'Gatwick Airport', 'Boomerang Street', '', 'Luton Airport', '44, Baker Street', '', 1, 1, 0, 0, 1, 20.00, 0.00, 2, '', '', '', 0, 0, 'paypal', '', '', 0, 1453964400, 0, 0, '', 'OnlineBooking', '', '2016-01-26 22:51:22', '2016-01-26 22:51:22', '', '', '', '', '', '', '', '', 1, 0, 0, '', '', '', ''),
(78, 16, 'Gatwick Airport', 'Boomerang Street', '', 'Luton Airport', '44, Baker Street', '', 1, 1, 0, 0, 1, 20.00, 0.00, 2, '', '', '', 0, 0, 'paypal', '697TLGD5853K8', 'PAY-2PR3371954185144SK2UE3VI', 1, 1453964400, 0, 0, '', 'OnlineBooking', '', '2016-01-26 22:55:42', '2016-01-26 23:00:37', '', '', '', '', '', '', '', '', 1, 0, 0, '', '', '', ''),
(79, 16, 'Gatwick Airport', 'Boomerang Street', '', 'Luton Airport', '44, Baker Street', '', 1, 1, 0, 0, 1, 20.00, 0.00, 2, '', '', '', 0, 0, 'paypal', '', '', 0, 1454115600, 0, 0, '', 'OnlineBooking', '', '2016-01-28 05:29:29', '2016-01-28 05:29:29', '', '', '', '', '', '', '', '', 1, 0, 0, '', '', '', ''),
(80, 16, 'Gatwick Airport', 'Boomerang Street', '', 'Luton Airport', '44, Baker Street', '', 1, 1, 0, 0, 1, 20.00, 0.00, 2, '', '', '', 0, 0, 'paypal', '', '', 0, 1454202000, 0, 0, '', 'OnlineBooking', '', '2016-01-30 00:22:19', '2016-01-30 00:22:19', '', '', '', '', '', '', '', '', 1, 0, 0, '', '', '', ''),
(81, 16, 'Gatwick Airport', 'Boomerang Street', '', 'Luton Airport', '44, Baker Street', '', 1, 1, 0, 0, 1, 20.00, 0.00, 2, '', '', '', 0, 0, 'paypal', '', '', 0, 1454202000, 0, 0, '', 'OnlineBooking', '', '2016-01-30 00:47:59', '2016-01-30 00:47:59', '', '', '', '', '', '', '', '', 1, 0, 0, '', '', '', ''),
(82, 16, 'Gatwick Airport', 'Boomerang Street', '', 'Luton Airport', '44, Baker Street', '', 1, 1, 0, 0, 1, 20.00, 0.00, 2, '', '', '', 0, 0, 'paypal', '', '', 0, 1454202000, 0, 0, '', 'OnlineBooking', '', '2016-01-30 00:52:15', '2016-01-30 00:52:15', '', '', '', '', '', '', '', '', 1, 0, 0, '', '', '', ''),
(83, 16, 'Gatwick Airport', 'Boomerang Street', '', 'Luton Airport', '44, Baker Street', '', 1, 1, 0, 0, 1, 20.00, 0.00, 2, '', '', '', 0, 0, 'paypal', '', '', 0, 1454202000, 0, 0, '', 'OnlineBooking', '', '2016-01-30 01:01:42', '2016-01-30 01:01:42', '', '', '', '', '', '', '', '', 1, 0, 0, '', '', '', ''),
(84, 16, 'Gatwick Airport', 'Boomerang Street', '', 'Luton Airport', '44, Baker Street', '', 1, 1, 0, 0, 1, 20.00, 0.00, 2, '', '', '', 0, 0, 'paypal', '', '', 0, 1454202000, 0, 0, '', 'OnlineBooking', '', '2016-01-30 01:16:09', '2016-01-30 01:16:09', '', '', '', '', '', '', '', '', 1, 0, 0, '', '', '', ''),
(85, 16, 'Gatwick Airport', 'Boomerang Street', '', 'Luton Airport', 'Anywhere', '', 1, 1, 0, 0, 1, 20.00, 0.00, 2, '', '', '', 0, 0, 'paypal', '', '', 0, 1454374800, 0, 0, '', 'OnlineBooking', '', '2016-01-30 23:39:26', '2016-01-30 23:39:26', '', '', '', '', '', '', '', '', 1, 0, 0, '', '', '', ''),
(87, 16, 'Airport Way, Staines, Greater London, United Kingdom', 'Boomerang Street', '', 'Heathrow Airport Terminal 3, Longford, United Kingdom', '44, Baker Street', '', 1, 1, 0, 0, 1, 16.00, 5.66, 2, '', '', '', 0, 0, 'paypal', '697TLGD5853K8', 'PAY-6D801508XY943862RK3GAMYQ', 1, 1457571600, 0, 0, '', 'OnlineBooking', '', '2016-02-23 01:13:13', '2016-02-23 01:13:13', '', '', '', '', '', '', '', '', 1, 0, 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `order_edit_history`
--

CREATE TABLE IF NOT EXISTS `order_edit_history` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(11) NOT NULL,
  `adjusted_bill` double(11,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_edit_history`
--

INSERT INTO `order_edit_history` (`id`, `order_id`, `adjusted_bill`, `created_at`, `updated_at`) VALUES
(1, 3, 649.00, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 31, 0.00, '2015-12-08 07:07:43', '2015-12-08 07:07:43'),
(3, 31, 0.00, '2015-12-09 01:06:07', '2015-12-09 01:06:07'),
(4, 2, 351.00, '2015-12-09 01:08:11', '2015-12-09 01:08:11'),
(5, 2, -700.00, '2015-12-09 03:17:55', '2015-12-09 03:17:55'),
(6, 73, 368.75, '2015-12-10 00:05:48', '2015-12-10 00:05:48'),
(7, 74, 1733.75, '2015-12-10 05:13:15', '2015-12-10 05:13:15'),
(8, 74, -233.75, '2015-12-10 05:51:24', '2015-12-10 05:51:24'),
(9, 75, 566.00, '2016-01-06 05:03:08', '2016-01-06 05:03:08'),
(10, 76, 566.00, '2016-01-06 05:13:19', '2016-01-06 05:13:19');

-- --------------------------------------------------------

--
-- Table structure for table `passengers`
--

CREATE TABLE IF NOT EXISTS `passengers` (
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` int(11) NOT NULL,
  `residence_no` int(11) NOT NULL,
  `fax_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `passengers`
--

INSERT INTO `passengers` (`first_name`, `last_name`, `contact_name`, `address`, `gender`, `city`, `street`, `postcode`, `residence_no`, `fax_number`, `email`, `phone_number`, `created_at`, `updated_at`, `id`) VALUES
('Imon I', 'Islam I', '', '', '', '', '', 0, 0, '', 'Imon0077@gmail.com', '+8801616010004', '0000-00-00 00:00:00', '2015-12-10 05:51:24', 1),
('Imon', 'Islam', '', '', '', '', '', 0, 0, '', 'Imon0077@yahoo.com', '44444444', '0000-00-00 00:00:00', '2015-12-07 04:41:20', 2),
('Md.', 'Anisuzzaman', '', '', '', '', '', 0, 0, '', 'Tasneemanis4@gmail.com', '01672053707', '0000-00-00 00:00:00', '2015-12-08 07:06:05', 3),
('Mr.', 'Islam', '', '', '', '', '', 0, 0, '', 'me@imonfislam.com', '01616010000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4),
('Mr.', 'Arif', '', '', '', '', '', 0, 0, '', 'imoni@ris10.com', '43434', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5),
('dssdf', 'sdfds', '', '', '', '', '', 0, 0, '', 'sdfsf', 'sfsd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6),
('dsfsdf', 'dfsdf', '', '', '', '', '', 0, 0, '', 'dsd', 'fsd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
('dsfds', 'dfds', '', '', '', '', '', 0, 0, '', 'dfsd', 'dfdsf', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8),
('', '', '', '', '', '', '', 0, 0, '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9),
('Imon test', 'Testing', '', '', '', '', '', 0, 0, '', 'meao@imonislam.com', '5454554', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10),
('Md. Kamrul', 'Islam', '', '', '', '', '', 0, 0, '', 'kamalbd0077@gmail.com', '016734343', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11),
('Anisur', 'Rahman', '', '', '', '', '', 0, 0, '', 'tasneemanis43@gmail.com', '01672053707', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12),
('Imon', 'Islam', '', '', '', '', '', 0, 0, '', 'imon@imonislam.com', '01616010000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13),
('Muhammed Test Testing 333', 'Anisuzzaman Test Test333', '', '', '', '', '', 0, 0, '', 'Anis@imonislam.com', '01672053707333', '0000-00-00 00:00:00', '2015-11-24 04:06:41', 14),
('dsfdsf', 'afdsf', '', '', '', '', '', 0, 0, '', 'me@imonislam.com', '33333333', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15),
('Muhammad Mahbubur', 'Khan', '', '', '', '', '', 0, 0, '', 'mahbub.e.khuda@gmail.com', '9883838', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16);

-- --------------------------------------------------------

--
-- Table structure for table `pickup_from_airport`
--

CREATE TABLE IF NOT EXISTS `pickup_from_airport` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(11) NOT NULL,
  `flight_arrival_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `airline_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `flight_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `departure_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pickup_after` int(11) NOT NULL COMMENT 'When should the driver pick you up? (Minutes after landing)',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pickup_from_airport`
--

INSERT INTO `pickup_from_airport` (`id`, `order_id`, `flight_arrival_time`, `airline_name`, `flight_no`, `departure_city`, `display_name`, `pickup_after`, `created_at`, `updated_at`) VALUES
(1, 6, '12', 'Air Arabia', '12', 'Chittagong', '', 1, '2015-08-25 17:10:53', '2015-08-25 17:10:53'),
(2, 7, '0', '', '', '', '', 0, '2015-08-25 23:50:15', '2015-08-25 23:50:15'),
(3, 8, '0', '', '', '', '', 0, '2015-08-29 22:30:42', '2015-08-29 22:30:42'),
(4, 9, '0', '', '', '', '', 0, '2015-08-29 22:43:19', '2015-08-29 22:43:19'),
(5, 10, '0', '', '', '', '', 0, '2015-08-30 17:05:39', '2015-08-30 17:05:39'),
(6, 11, '0', '', '', '', '', 0, '2015-08-30 18:06:47', '2015-08-30 18:06:47'),
(7, 12, '0', '', '', '', '', 0, '2015-08-30 18:07:29', '2015-08-30 18:07:29'),
(8, 13, '0', '', '', '', '', 0, '2015-08-30 18:09:42', '2015-08-30 18:09:42'),
(9, 14, '0', '', '', '', '', 0, '2015-08-30 19:29:40', '2015-08-30 19:29:40'),
(10, 15, '2:00', 'Air Bangla', '', '', '', 0, '2015-08-30 19:31:56', '2015-08-30 19:31:56'),
(11, 16, '0', '', '', '', '', 0, '2015-08-30 20:53:55', '2015-08-30 20:53:55'),
(12, 17, '0', '', '', '', '', 0, '2015-09-01 17:20:44', '2015-09-01 17:20:44'),
(13, 18, '0', '', '', '', '', 0, '2015-09-02 15:46:52', '2015-09-02 15:46:52'),
(14, 19, '0', '', '', '', '', 0, '2015-09-05 22:02:38', '2015-09-05 22:02:38'),
(15, 20, '0', 'sdfdsf', 'dfds', 'sdsf', '', 4, '2015-09-13 16:01:09', '2015-09-13 16:01:09'),
(16, 21, '0', '', '', '', '', 0, '2015-09-13 17:14:40', '2015-09-13 17:14:40'),
(17, 22, '0', '', '', '', '', 0, '2015-09-15 20:15:19', '2015-09-15 20:15:19'),
(18, 23, '3244', 'dsfds', '33', 'sdfsd', '', 15, '2015-09-20 18:10:28', '2015-09-20 18:10:28'),
(19, 24, '', '', '', '', '', 0, '2015-10-19 23:45:44', '2015-10-19 23:45:44'),
(20, 25, '', '', '', '', '', 0, '2015-10-19 23:51:43', '2015-10-19 23:51:43'),
(21, 26, '2', 'adasdsad', '22', 'dasdasd', '', 2, '2015-10-28 02:38:54', '2015-10-28 02:38:54'),
(22, 27, '22', 'Bangladesh Biman test', '101', 'Saudi Arabia test', '', 5, '2015-10-28 02:41:54', '2015-11-08 23:58:29'),
(23, 28, '2', 'adasdsad', '22', 'dasdasd', '', 2, '2015-10-28 02:46:58', '2015-10-28 02:46:58'),
(24, 29, '2', 'adasdsad', '22', 'dasdasd', '', 2, '2015-10-28 02:54:07', '2015-10-28 02:54:07'),
(25, 30, '23', 'dfdfsdf', '23', 'ctg', '', 2, '2015-10-28 06:40:49', '2015-10-28 06:40:49'),
(26, 31, '23', 'dfdfsdf', '23', 'ctg', '', 2, '2015-10-28 06:44:41', '2015-10-28 06:44:41'),
(27, 32, '4', 'Bangladesh Biman test', '101', 'Saudi Arabia test', '', 10, '2015-10-28 06:52:44', '2015-11-08 05:13:53'),
(28, 33, '03:30', 'Air Bangla', '420', 'CTG', '', 5, '2015-11-09 04:17:55', '2015-11-09 04:17:55'),
(29, 34, '04:00', 'Amaar Airline', '112', 'Mollapara', '', 8, '2015-11-09 04:28:12', '2015-11-09 04:28:12'),
(30, 35, '04:00', 'Amaar Airline', '112', 'Mollapara', '', 8, '2015-11-09 05:09:53', '2015-11-09 05:09:53'),
(31, 36, '03:30', 'Bangladesh Biman test', '420', 'Chittagong', '', 5, '2015-11-10 03:34:46', '2015-11-10 03:34:46'),
(32, 37, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-16 23:02:37', '2015-11-16 23:02:37'),
(33, 38, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-16 23:18:34', '2015-11-16 23:18:34'),
(34, 39, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-16 23:58:27', '2015-11-16 23:58:27'),
(35, 40, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:08:05', '2015-11-17 00:08:05'),
(36, 41, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:11:20', '2015-11-17 00:11:20'),
(37, 42, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:13:09', '2015-11-17 00:13:09'),
(38, 43, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:18:49', '2015-11-17 00:18:49'),
(39, 44, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:21:21', '2015-11-17 00:21:21'),
(40, 45, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:24:06', '2015-11-17 00:24:06'),
(41, 46, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:24:22', '2015-11-17 00:24:22'),
(42, 47, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:24:54', '2015-11-17 00:24:54'),
(43, 48, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:27:16', '2015-11-17 00:27:16'),
(44, 49, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:27:46', '2015-11-17 00:27:46'),
(45, 50, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:33:00', '2015-11-17 00:33:00'),
(46, 51, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:33:18', '2015-11-17 00:33:18'),
(47, 52, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:41:30', '2015-11-17 00:41:30'),
(48, 53, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:45:01', '2015-11-17 00:45:01'),
(49, 54, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:45:27', '2015-11-17 00:45:27'),
(50, 55, '7:10 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 5, '2015-11-17 00:46:18', '2015-11-17 00:46:18'),
(51, 56, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:07:07', '2015-11-17 03:07:07'),
(52, 57, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:08:09', '2015-11-17 03:08:09'),
(53, 58, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:16:08', '2015-11-17 03:16:08'),
(54, 59, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:17:13', '2015-11-17 03:17:13'),
(55, 60, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:17:31', '2015-11-17 03:17:31'),
(56, 61, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:18:44', '2015-11-17 03:18:44'),
(57, 62, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:20:29', '2015-11-17 03:20:29'),
(58, 63, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:20:47', '2015-11-17 03:20:47'),
(59, 64, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:21:40', '2015-11-17 03:21:40'),
(60, 65, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:22:12', '2015-11-17 03:22:12'),
(61, 66, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:23:00', '2015-11-17 03:23:00'),
(62, 67, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:23:26', '2015-11-17 03:23:26'),
(63, 68, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:26:46', '2015-11-17 03:26:46'),
(64, 69, '', '', '', '', '', 0, '2015-11-17 03:27:27', '2015-11-26 03:07:54'),
(65, 70, '07:15 pm', 'Air Arabia', 'FR2369', 'Ostrava', '', 8, '2015-11-17 03:28:53', '2015-11-17 03:28:53'),
(66, 71, '2', 'Bangladesh Biman test', '222', 'ctg', '', 8, '2015-12-02 00:41:00', '2015-12-02 00:41:00'),
(67, 72, '', '', '', '', '', 0, '2015-12-03 01:16:47', '2015-12-03 01:16:47'),
(68, 73, '', '', '', '', '', 0, '2015-12-03 06:12:03', '2015-12-03 06:12:03'),
(69, 2, '12', 'Air Arabia', '12', 'Chittagong', '', 1, '2015-08-25 17:10:53', '2015-08-25 17:10:53'),
(70, 72, '', '', '', '', '', 0, '2015-12-09 05:33:42', '2015-12-09 05:33:42'),
(71, 73, '', '', '', '', '', 0, '2015-12-10 00:05:48', '2015-12-10 00:05:48'),
(72, 74, '', '', '', '', '', 0, '2015-12-10 05:13:15', '2015-12-10 05:13:15'),
(73, 75, '', '', '', '', '', 2, '2016-01-06 05:03:08', '2016-01-06 05:03:08'),
(74, 76, '', '', '', '', '', 2, '2016-01-06 05:13:19', '2016-01-06 05:13:19'),
(75, 77, '', '', '', '', '', 1, '2016-01-26 22:51:22', '2016-01-26 22:51:22'),
(76, 78, '', '', '', '', '', 1, '2016-01-26 22:55:42', '2016-01-26 22:55:42'),
(77, 79, '', '', '', '', '', 1, '2016-01-28 05:29:29', '2016-01-28 05:29:29'),
(78, 80, '', '', '', '', '', 0, '2016-01-30 00:22:19', '2016-01-30 00:22:19'),
(79, 81, '', '', '', '', '', 0, '2016-01-30 00:47:59', '2016-01-30 00:47:59'),
(80, 82, '', '', '', '', '', 0, '2016-01-30 00:52:15', '2016-01-30 00:52:15'),
(81, 83, '', '', '', '', '', 0, '2016-01-30 01:01:42', '2016-01-30 01:01:42'),
(82, 84, '', '', '', '', '', 0, '2016-01-30 01:16:09', '2016-01-30 01:16:09'),
(83, 85, '', '', '', '', '', 1, '2016-01-30 23:39:26', '2016-01-30 23:39:26'),
(84, 86, '', '', '', '', '', 1, '2016-01-31 00:17:14', '2016-01-31 00:17:14'),
(85, 86, '', '', '', '', '', 1, '2016-02-23 00:10:17', '2016-02-23 00:10:17'),
(86, 87, '', '', '', '', '', 1, '2016-02-23 01:13:13', '2016-02-23 01:13:13');

-- --------------------------------------------------------

--
-- Table structure for table `pricelist`
--

CREATE TABLE IF NOT EXISTS `pricelist` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_price` int(10) DEFAULT NULL COMMENT 'set unit price for per km, per person, child, infant, booster etc',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pricelist`
--

INSERT INTO `pricelist` (`id`, `name`, `unit_price`, `created_at`, `updated_at`) VALUES
(1, 'Per km distance', 10, '0000-00-00 00:00:00', '2015-07-08 21:10:58'),
(2, 'Child seats (9-18kg, 1-3 years)', 5, '0000-00-00 00:00:00', '2015-07-08 21:11:17'),
(3, 'Infant seats (10-13kg, up to 1 year)', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Booster seats (15-25kg, 3-8 years)', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Stop on the way(via)', 3, '0000-00-00 00:00:00', '2015-07-08 21:11:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE IF NOT EXISTS `routes` (
  `id` int(10) unsigned NOT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`id`, `route`, `description`, `created_at`, `updated_at`) VALUES
(2, 'Gatwick–Folkestone', 'This is homepage description for Gatwick – Folkestone route. This is homepage description for Gatwick – Folkestone route. This is homepage description for Gatwick – Folkestone route. This is homepage description for Gatwick – Folkestone route. This is homepage description for Gatwick – Folkestone route. ', '2016-01-11 01:01:06', '2016-01-11 01:01:06');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(10) unsigned NOT NULL,
  `property` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `text_identifier` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `property`, `value`, `text_identifier`, `created_at`, `updated_at`) VALUES
(1, 'payment_method', 'card,paypal', '', '0000-00-00 00:00:00', '2015-12-30 05:56:43');

-- --------------------------------------------------------

--
-- Table structure for table `special_route`
--

CREATE TABLE IF NOT EXISTS `special_route` (
  `id` int(10) unsigned NOT NULL,
  `pickup_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dropoff_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `distance` int(11) NOT NULL,
  `price` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `single_1` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `round_1` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `single_2` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `round_2` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `single_3` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `round_3` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `single_5` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `round_5` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `single_6` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `round_6` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `special_route`
--

INSERT INTO `special_route` (`id`, `pickup_address`, `dropoff_address`, `distance`, `price`, `single_1`, `round_1`, `single_2`, `round_2`, `single_3`, `round_3`, `single_5`, `round_5`, `single_6`, `round_6`, `created_at`, `updated_at`) VALUES
(12, 'Gatwick Airport', 'Luton Airport', 0, NULL, '20', '50', '30', '40', '50', '60', '70', '80', '55', '99', '2016-01-25 06:13:10', '2016-01-25 06:13:10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `full_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '0', 'imon0077@gmail.com', 1, '$2y$10$o5qSYRQ/R4hd/tm2/js5lOKA6tVMCx1ggjSs5vGdXP/zcitKbKSU6', 'waonEAEuzMVnnnwHrvwHtachoQkAGs5NbwA6aZJPe1jIC9jCO0dY7oLpwR0J', '0000-00-00 00:00:00', '2015-12-27 00:20:52'),
(2, '0', 'abc@gmail.comm', 1, '$2y$10$GIRme40.GDM4iUMcsTbsAOW/EpjNfiES4Y/LDVsOy3rZ/UUckW0im', 'TfVmvgSxji9YrKd2j0GaTjjkaBMfduP0Acbys4MiKvpvrE5FgqEfaix9yhgS', '0000-00-00 00:00:00', '2015-10-04 00:37:55'),
(3, '0', 'me@imonislam.com', 2, '$2y$10$4g1f0lDSMBqbdWPaOCjS.OPsYc.8GqCdKVdAmCJtYfDnrrxwLOGre', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `airports`
--
ALTER TABLE `airports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_time`
--
ALTER TABLE `booking_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_category`
--
ALTER TABLE `car_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_details`
--
ALTER TABLE `car_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cruiseports`
--
ALTER TABLE `cruiseports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_edit_history`
--
ALTER TABLE `order_edit_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passengers`
--
ALTER TABLE `passengers`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `passengers_email_unique` (`email`);

--
-- Indexes for table `pickup_from_airport`
--
ALTER TABLE `pickup_from_airport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricelist`
--
ALTER TABLE `pricelist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `special_route`
--
ALTER TABLE `special_route`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `airports`
--
ALTER TABLE `airports`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `booking_time`
--
ALTER TABLE `booking_time`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `car_category`
--
ALTER TABLE `car_category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `car_details`
--
ALTER TABLE `car_details`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cruiseports`
--
ALTER TABLE `cruiseports`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `order_edit_history`
--
ALTER TABLE `order_edit_history`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `passengers`
--
ALTER TABLE `passengers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `pickup_from_airport`
--
ALTER TABLE `pickup_from_airport`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `pricelist`
--
ALTER TABLE `pricelist`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `special_route`
--
ALTER TABLE `special_route`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
