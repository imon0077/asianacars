@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Edit Profile</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'editprofilesave', 'class' => 'form-horizontal form-bordered','id'=> 'editProfileSave' ]) !!}
    <input class="form-control" value="{!! $profiledetails->id !!}" name='id' id="id" type="hidden">
    @if(Auth::user()->role == 1)
    <div class="form-group field">
        <label class="col-md-2 control-label" for="first_name">Full Name</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->full_name !!}" name='full_name' id="full_name" type="text">
        </div>

        <label class="col-md-3 control-label" for="last_name">Email</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->email !!}" name='email' id="email" type="text">
        </div>
    </div>
    @else
    <div class="form-group field">
        <label class="col-md-2 control-label" for="first_name">First Name</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->first_name !!}" name='first_name' id="first_name" type="text">
        </div>

        <label class="col-md-3 control-label" for="last_name">Last Name</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->last_name !!}" name='last_name' id="last_name" type="text">
        </div>
    </div>
    @endif

    @if(Auth::user()->role == 2)
    <div class="form-group field">
        <label class="col-md-2 control-label" for="contact_name">Contact Name</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->contact_name !!}" name='contact_name' id="contact_name" type="text">
        </div>

        <label class="col-md-3 control-label" for="phn_number">Phone Number</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->phone_number !!}" name='phn_number' id="phn_number" type="text">
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="email">email</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->email !!}" name='email' id="email" type="text">
        </div>

    </div>
    @endif

    <div class="form-group field">
        <label class="col-md-2 control-label" for="password">Password</label>
        <div class="col-md-3">
            <input class="form-control"  name='password' id="password" type="password">
        </div>

        <label class="col-md-3 control-label" for="password_confirmation">Confirm Password</label>
        <div class="col-md-3">
            <input class="form-control" name='password_confirmation' id="password_confirmation" type="password">
        </div>
    </div>


    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Save Changes</button>

        </div>
    </div>

    <!--  end  -->



     {!! Form::close() !!}
</section>
</div>
</div>

<!-- end: page -->
@stop
@section('javascript')
$(document).ready(function(){


$("#editProfileSave").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {
first_name: "required",
last_name: "required",
pharmacy_name: "required",
contact_name: {
required: true
},
phn_number: {
required: true,
number: true
},
fax_number: {

number: true
},
email: {
required: true,
email: true
},
npi_number: {

number: true
},
dea_number: {

number: true
}
},
messages:{
first_name: {
    required:"First Name field could not remain blank"
    },
last_name: {
    required:"Last Name field could not remain blank"
    },
pharmacy_name: {
    required:"Pharmacy Name field could not remain blank"
    },
contact_name: {
    required:"Contact Name field could not remain blank",
regexp:"Input text only"
    },
phn_number: {
    required:"The phone number is required"
    },
fax_number: {
    required:"The fax number is required"
    },
email: {
    required:"The email is required",
    email:"Email does not seems to valid"
    },
npi_number: {
    required:"The NPI Number is required"
    },
dea_number: {
    required:"The DEA Number is required"
    }
}
});

$('input[name="first_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});
$('input[name="last_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});

$('input[name="contact_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});


});
@stop
