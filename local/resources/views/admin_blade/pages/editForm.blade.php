@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Edit Booking | Modify Quote</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    @include('admin_blade.pages.tabs')


    {!! Form::open(['url' => 'editOrderSubmit', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'editLocationForm'] ) !!}
    <input type="hidden" name="order_id" value="<?=$order->id?>">
    <input type="hidden" name="car_id" value="<?=$order->car_details?>">
    <input type="hidden" name="tourType" value="<?=$order->return_booking?>">
    <input type="hidden" name="meetngreet" value="<?=$order->meetngreet?>">
    <input type="hidden" name="pageId" id="pageId" d value="1">
    <input type="hidden" name="clickedPage" class="clickedPage"  />
<?php //echo $viapointsCount;die();?>
    <div class="form-group field">
        <label class="col-md-3 control-label" for="pickup">Pickup Address</label>
        <div class="col-md-7">
            <input class="form-control" id="start" name="pickup" value="<?= ($sessData1stStage == NULL) ? ucwords($order->pickup_address) : $sessData1stStage['pickup'] ?>" type="text">
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 1</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_1" name="waypoint_1" value="<?php if($sessData1stStage['waypoint_1'] != ""){echo $sessData1stStage['waypoint_1'];}else{if($viapointsCount > 0)echo ucwords($via_points_seperate[0]);else echo "";}?>" type="text"  >

        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 2</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_2" name="waypoint_2" value="<?php if($sessData1stStage['waypoint_2'] != ""){echo $sessData1stStage['waypoint_2'];}else{if($viapointsCount > 1)echo ucwords($via_points_seperate[1]);else echo "";}?>" type="text"  >

        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 3</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_3" name="waypoint_3" value="<?php if($sessData1stStage['waypoint_3'] != ""){echo $sessData1stStage['waypoint_3'];}else{if($viapointsCount > 2)echo ucwords($via_points_seperate[2]);else echo "";}?>" type="text"  >

        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 4</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_4" name="waypoint_4" value="<?php if($sessData1stStage['waypoint_4'] != ""){echo $sessData1stStage['waypoint_4'];}else{if($viapointsCount > 3)echo ucwords($via_points_seperate[3]);else echo "";}?>" type="text"  >

        </div>
    </div>


    <div class="form-group field">
        <label class="col-md-3 control-label" for="dropoff">Dropoff Address</label>
        <div class="col-md-7">
            <input class="form-control" id="end" name="dropoff" value="<?= ($sessData1stStage == NULL) ? ucwords($order->dropoff_address) : $sessData1stStage['dropoff'] ?>" type="text"  >

        </div>
    </div>



<!--    <div class="form-group">-->
<!--        <label class="col-md-3 control-label">&nbsp;</label>-->
<!--        <div class="col-md-6">-->
<!--            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Next</button>-->
<!---->
<!--        </div>-->
<!--    </div>-->

    <!--  end  -->
     {!! Form::close() !!}
</section>
</div>
</div>
<!-- end: page -->


@stop

