@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Add Driver</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'AddDriverSave', 'class' => 'form-horizontal form-bordered', 'id' => 'driverSave'] ) !!}

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">First Name</label>
        <div class="col-md-3">
            <input class="form-control" name='first_name' type="text"  >
        </div>

        <label class="col-md-3 control-label" for="">Last Name</label>
        <div class="col-md-3">
            <input class="form-control" name='last_name' type="text"  >
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">Contact Name</label>
        <div class="col-md-3">
            <input class="form-control" name='contact_name' type="text"  >
        </div>

        <label class="col-md-3 control-label" for="">Gender</label>
        <div class="col-md-3">
            <select data-plugin-selectTwo class="form-control populate" name="gender">
                <optgroup label="Gender">
                    <option value="">Select...</option>
                    <option value="Male"> Male </option>
                    <option value="Female"> Female </option>
                </optgroup>
            </select>
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">Address</label>
        <div class="col-md-3">
            <textarea class="form-control" rows="3" id="textareaDefault" name="address"></textarea>
        </div>

        <label class="col-md-3 control-label" for="">City</label>
        <div class="col-md-3">
            <input class="form-control" name='city' type="text">
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">Street</label>
        <div class="col-md-3">
            <input class="form-control" name='street' type="text">
        </div>

        <label class="col-md-3 control-label" for="">Post Code</label>
        <div class="col-md-3">
            <input class="form-control" name='postcode' type="text">
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">Email</label>
        <div class="col-md-3">
            <input class="form-control" name='email' type="text">
        </div>

        <label class="col-md-3 control-label" for="">Phone Number</label>
        <div class="col-md-3">
            <input class="form-control" name='phone' type="text">
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">Password</label>
        <div class="col-md-3">
            <input class="form-control" name='password' id="password" type="password" >
        </div>

        <label class="col-md-3 control-label" for="">Password Confirmation</label>
        <div class="col-md-3">
            <input class="form-control" name='password_confirmation' id="password_confirmation" type="password" >
        </div>
    </div>


    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Add Driver</button>

        </div>
    </div>

    <!--  end  -->
     {!! Form::close() !!}
</section>
</div>
</div>

<!-- end: page -->
@stop
@section('javascript')

$("#driverSave").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {

first_name: {
required: true
},
last_name: {
required: true
},
user_name: {
required: true
},
phone: {
number: true
},
email: {
required: true,
email:true
},
password: {
required: true,
rangelength: [6, 32]
},
password_confirmation: {
required: true,
equalTo: "#password"
}
},
messages:{
first_name: {
required:"Dont forget to input your First Name."
},
last_name: {
required:"Dont forget to input your Last Name."
},
email: {
required:"Please input your Email address.",
email:"Your Email does not seems to valid"
},
password: {
required:"Password field could not remain blank."

},
password_confirmation: {
required:"Password Confirmation field is required.",
equalTo:"Password does not matched"
}
}
});

$('input[name="first_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});
$('input[name="last_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});
@stop