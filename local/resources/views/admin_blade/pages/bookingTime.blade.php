@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Set Minimum Booking Time</h2>

</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
    <div class="col-xs-12">
        <section class="panel">

            <div class="panel-body">

                {!! Form::open(['url' => 'bookingTimesave', 'class' => 'form-horizontal form-bordered']) !!}

                <div class="form-group">
                    <label class="col-md-3 control-label" for="">Minimum Booking Time</label>
                    <div class="col-md-6">
                        <input class="form-control" name='id' value="{!! $minimumBookingtime->id !!}" id="" type="hidden">
                        <input class="form-control" name='booking_time' value="{!! $minimumBookingtime->minimum_booking_time !!}" id="" type="text" >
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">&nbsp;</label>
                    <div class="col-md-6">
                        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Update</button>

                    </div>
                </div>

                {!! Form::close() !!}
        </section>
    </div>
</div>

<!-- end: page -->
@stop