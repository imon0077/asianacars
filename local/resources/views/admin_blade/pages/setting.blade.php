@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Profile</h2>

</header>
@include('flash::message')
@include('admin_blade.common.error-message')
<!-- start: page -->
<div class="row">
    <div class="col-lg-8">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">

                            <div id="collapse1One" class="accordion-body collapse in">
                                <!-- -->
                                <div class="panel-body">
                                    <h4>Payment Method</h4>
                                    {!! Form::open(['url'=>'setting', 'method'=>'post', ]) !!}
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <!--<div class="form-group"><input type="checkbox" class="" name="pm[]" value="cash" id="pm1" @if(in_array('cash',$pm)) checked @endif > <label for="pm1">Cash</label></div>-->

                                            <div class="form-group"><input type="checkbox" class="" name="pm[]" value="card" id="pm2" @if(in_array('card',$pm)) checked @endif > <label for="pm2">Card</label></div>

                                            <div class="form-group"><input type="checkbox" class="" name="pm[]" value="paypal" id="pm3" @if(in_array('paypal',$pm)) checked @endif > <label for="pm3">Paypal</label></div>

                                            <div class="form-group"><input type="checkbox" class="" name="pm[]" value="phone_call" id="pm5" @if(in_array('phone_call',$pm)) checked @endif > <label for="pm5">Pay via phone call</label></div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">&nbsp;</label>
                                        <div class="col-md-6">
                                            <button type="submit" name="update_setting" class="btn btn-success">Update Settings</button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}


                                </div>
                                <!-- -->
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- end: page -->
@stop

