@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Add Car</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'AddCarSave', 'class' => 'form-horizontal form-bordered'] ) !!}

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">Car Name</label>
        <div class="col-md-3">
            <input class="form-control" name='name' type="text"  >
        </div>

        <label class="col-md-3 control-label" for="">Total Seats</label>
        <div class="col-md-3">
            <input class="form-control" name='tot_seats' type="text"  >
        </div>
    </div>


    <div class="form-group">
        <label class="col-md-2 control-label" for="">Car Category</label>
        <div class="col-md-3">
            <select data-plugin-selectTwo class="form-control populate" name="cars_cat">
                <optgroup label="Cars Category">
                    <option value="">Select...</option>
                    @foreach($CarsCat as $CarsCats)
                    <option value="{{ $CarsCats->id }}"> {{ $CarsCats->cat_name }} </option>
                    @endforeach
                </optgroup>
            </select>
        </div>

        <label class="col-md-3 control-label" for="">Drivers</label>
        <div class="col-md-3">
            <select data-plugin-selectTwo class="form-control populate" name="drivers">
                <optgroup label="Drivers">
                    <option value="">Select...</option>
                    @foreach($Driver as $Drivers)
                    <option value="{{ $Drivers->id }}"> {{ $Drivers->first_name }} {{ $Drivers->last_name }} </option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Add Car</button>

        </div>
    </div>

    <!--  end  -->
     {!! Form::close() !!}
</section>
</div>
</div>

<!-- end: page -->
@stop