@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Order Details of "<span class="text-danger">{!! ucwords($order->first_name) !!} {!! ucwords($order->last_name) !!}</span>"  </h2>

</header>
@include('flash::message')
@include('admin_blade.common.error-message')
<!-- start: page -->
<div class="row">
    <div class="col-xs-12">

        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Order Details </h2>
            </header>
            <div class="panel-body">

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Order No</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->id !!}</label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Pickup Address</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ucwords($order->pickup_address) !!}</label>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Pickup Address1</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ucwords($order->pickup_address1) !!}</label>

                    </div>
                </div>

                @if($order->pickup_address2)
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Pickup Address2</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ucwords($order->pickup_address2) !!}</label>
                    </div>
                </div>
                @endif

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Pickup Postcode</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ucwords($order->pickup_postcode) !!}</label>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Dropoff Address</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ucwords($order->dropoff_address) !!}</label>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Dropoff Address1</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ucwords($order->dropoff_address1) !!}</label>

                    </div>
                </div>
                @if($order->dropoff_address2)
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Dropoff Address2</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ucwords($order->dropoff_address2) !!}</label>

                    </div>
                </div>
                @endif

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Dropoff Postcode</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ucwords($order->dropoff_postcode) !!}</label>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Total Distance</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ceil($order->total_distance) !!} miles</label>

                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Meet & Greet</label>
                    <div class="col-md-6">
                        <label class="control-label">{!! ((int)$order->meetngreet > 0) ? '<span class="badge green">Yes</span>&nbsp; (£'.number_format($order->meetngreet,2).' included)':'No' !!}</label>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Total Bill</label>
                    <div class="col-md-6">
                          <label class="control-label">£{!! number_format($order->total_bill,2) !!}</label>
                          @if($order->special_date == 1)<label class="control-label">(Additional charge included due to special date.)</label>@endif

                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Order Date</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->created_at !!}</label>
                    </div>
                </div>

                <hr/>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Payment Method</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ucfirst(str_replace('_',' ',$order->pay_via)) !!}</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Card Customer ID (Ex: card id reference used for Stripe/Paypal)</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->card_customer_id !!}</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Card Charge ID (Ex: charge/transaction id in Stripe/Paypal)</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->card_charge_id !!}</label>
                    </div>
                </div>

                <hr/>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Order Status</label>
                    <div class="col-md-6">
                          <label class="control-label">
<!--                            @if($order->status == 0)Pending @elseif($order->status == 1) Approved @else Rejected @endif-->

                            @if(Auth::user()->role == 1)

                            {!! Form::open(array('url'=>'changeStatusFromAdmin', 'class'=>'form-bordered', 'id'=>'update_form')); !!}
                            <input type="hidden" name="order_id" value="{{$order->id}}" />
                            <select name="status_change" id="status" class="input-sm">

                                <option value="0" @if($order->status == 0 ) selected @endif >
                                Pending
                                </option>
                                <option value="1" @if($order->status == 1 ) selected @endif >
                                Approved
                                </option>
                                <option value="2" @if($order->status == 2 ) selected @endif >
                                Rejected
                                </option>

                                <option value="3" @if($order->status == 3 ) selected @endif >
                                Confirm LGW North
                                </option>
                                <option value="4" @if($order->status == 4 ) selected @endif >
                                Confirm LGW South
                                </option>
                                <option value="5" @if($order->status == 5 ) selected @endif >
                                Confirm LHR T3 M&G
                                </option>
                                <option value="6" @if($order->status == 6 ) selected @endif >
                                Confirm M&G
                                </option>
                                <option value="7" @if($order->status == 7 ) selected @endif >
                                Confirm Non Airport
                                </option>

                            </select>
                            @else
                                @if($order->status == 0)Pending
                                @elseif($order->status == 1) Approved
                                @elseif($order->status == 3) Confirm LGW North
                                @elseif($order->status == 4) Confirm LGW South
                                @elseif($order->status == 5) Confirm LHR T3 M&G
                                @elseif($order->status == 6) Confirm M&G
                                @elseif($order->status == 7) Confirm Non Airport
                                @else Rejected
                                @endif
                            @endif

                        </label>
                    </div>
                </div>


                @if(Auth::user()->role == 1)

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Driver</label>
                    <div class="col-md-6">
                          <label class="control-label">

                            <select name="driver" id="driver" class="input-sm">

                                @foreach($driverlist as $dList)
                                <option value="{{$dList->id}}" @if($order->driver_id == $dList->id ) selected @endif >{{$dList->first_name}} {{$dList->last_name}} - {{$dList->phone_number}}</option>
                                @endforeach

                            </select>

                            <button type="submit" class="btn btn-primary btn-xs"> Update</button>
                            {!! Form::close() !!}

                        </label>
                    </div>
                </div>

                @endif

                @if(Auth::user()->role == 2)
                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Payment Status</label>
                    <div class="col-md-6">
                          <label class="control-label">
<!--                            @if($order->payment_status == 0)Unpaid @elseif($order->payment_status == 1) Paid @else Partially Paid @endif-->

                            {!! Form::open(array('url'=>'PaymentStatus', 'class'=>'form-bordered')); !!}
                            <input type="hidden" name="order_id" value="{{$order->id}}" />
                            <select name="PaymentStatus" class="input-sm">

                                <option value="0" @if($order->payment_status == 0 ) selected @endif >
                                Unpaid
                                </option>
                                <option value="1" @if($order->payment_status == 1 ) selected @endif >
                                Paid
                                </option>

                            </select>

                            <button type="submit" class="btn btn-primary btn-xs"> Update</button>
                            {!! Form::close() !!}

                        </label>
                    </div>
                </div>
                @endif
            </div>
        </section>
    </div>

    <!-- Passengers details -->
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Passengers Details</h2>
            </header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Name</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ucwords($order->first_name) !!} {!! ucwords($order->last_name) !!}</label>

                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Email</label>
                    <div class="col-md-6">
                          <label class="control-label">
                            {!! $order->email !!}
                        </label>
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Mobile</label>
                    <div class="col-md-6">
                          <label class="control-label">
                            {!! $order->phone_number !!}
                        </label>
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Number of Person</label>
                    <div class="col-md-6">
                        <label class="control-label">
                            {!! $order->no_of_person !!} Nos.
                        </label>
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Number of Child</label>
                    <div class="col-md-6">
                        <label class="control-label">
                            {!! $order->no_of_child !!} Nos.
                        </label>
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">No of Suitcase</label>
                    <div class="col-md-6">
                        <label class="control-label">
                            {!! $order->suitcases !!} Nos.
                        </label>
                    </div>
                </div>


            </div>
        </section>
        </div>
    <!-- /Passenger details -->

    <!-- waypoint details -->
    <div class="col-xs-12">
        @if($via_points)
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Waypoint Details</h2>
            </header>
            <div class="panel-body">
                @foreach($via_points_seperate as $k=>$v)
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Way Point {{ $k+1 }}</label>
                    <div class="col-md-6">
                        <label class="control-label">{{ str_replace('+', ' ', $v) }}</label>

                    </div>
                </div>
                @endforeach
            </div>
        </section>
        @endif
    </div>
    <!-- /waypoint details  -->

    <!-- Journey Date & time details -->
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Journey Date &amp; Time</h2>
            </header>
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Journey Date</label>
                    <div class="col-md-6">
                        <label class="control-label">{{ date('d-m-Y', $order->date_time) }}</label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Journey Time</label>
                    <div class="col-md-6">
                        <label class="control-label">{{ date('H:i', $order->date_time) }}</label>
                    </div>
                </div>

            </div>
        </section>
    </div>
    <!-- /Journey Date & time details  -->

    @if($order->return_booking == '1')
    <!-- Return Date & time details -->
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Return Date &amp; Time</h2>
            </header>
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Journey Date</label>
                    <div class="col-md-6">
                        <label class="control-label">{{ date('d-m-Y', $order->return_date_time) }}</label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Journey Time</label>
                    <div class="col-md-6">
                        <label class="control-label">{{ date('H:i', $order->return_date_time) }}</label>
                    </div>
                </div>

            </div>
        </section>
    </div>
    <!-- /Return Date & time details  -->
    @endif

    <!-- Airport details -->
    <div class="col-xs-12">
        @if($order->airline_name)
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Airport Details</h2>
            </header>
            <div class="panel-body">
                @if($order->flight_arrival_time != '0')
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Flight Arrival Time</label>
                    <div class="col-md-6">
                        <label class="control-label">{!! $order->flight_arrival_time !!}</label>
                    </div>
                </div>
                @endif

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Airline Name</label>
                    <div class="col-md-6">
                        <label class="control-label">{!! $order->airline_name !!}</label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Flight Number</label>
                    <div class="col-md-6">
                        <label class="control-label">{!! $order->flight_no !!}</label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Departure City</label>
                    <div class="col-md-6">
                        <label class="control-label">{!! $order->departure_city !!}</label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Pickup After Landing</label>
                    <div class="col-md-6">
                        <label class="control-label">{!! $order->pickup_after !!} Mins.</label>
                    </div>
                </div>

            </div>
        </section>
        @endif
    </div>
    <!-- /Airport details  -->

    <!-- Car details -->
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Car Details</h2>
            </header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Car Name</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->carname !!} </label>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Total Seats</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->total_seats !!} </label>

                    </div>
                </div>

            </div>
        </section>
    </div>

@if($order->status == 1)
    <!-- Driver details -->
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Driver Details</h2>
            </header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Name</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->dFname !!} {!! $order->dLname !!} </label>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Email</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->dEmail !!} </label>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Mobile</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->dPhone !!} </label>

                    </div>
                </div>

            </div>
        </section>
    </div>
@endif

<!--    -->
    <div class="form-group  ">
        <label class="col-md-3 control-label" for="inputDefault"></label>

        <a href="{{ URL::previous() }}" class="mb-xs mt-xs mr-xs btn btn-warning saveco"> Back</a>

    </div>
</div>
<!-- end: page -->

<!-- end: page -->
@stop