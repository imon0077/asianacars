@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Order list</h2>

</header>
@include('flash::message')
@include('admin_blade.common.error-message')
<!-- start: page -->
<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">

                            <div id="collapse1One" class="accordion-body collapse in">

                                <!-- -->
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-default" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
                                        <thead>
                                        <tr>
                                            <th style="display: none">testing column(dont remove)</th>
                                            <th>Order ID</th>
                                            <th>Passenger Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Status</th>
                                            <th>Order Date</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($orderlist as $OrderLists)

                                        <tr class="gradeX @if($OrderLists->status == 0)warning @elseif($OrderLists->status == 2) @else success @endif">
                                            <td style="display: none">testing column(dont remove)</td>
                                            <td>{!! $OrderLists->id !!}</td>
                                            <td>
                                                {!! $OrderLists->first_name !!} {!! $OrderLists->last_name !!}
                                            </td>

                                            <td>{!! $OrderLists->email !!} </td>
                                            <td>{!! $OrderLists->phone_number !!}</td>
                                            <td>
                                                @if($OrderLists->status == 0)Pending @elseif($OrderLists->status == 2) Rejected @else Approved @endif
                                            </td>
                                            <td>{!! $OrderLists->created_at !!}</td>


                                            <td class="actions">

                                                <a href="orderDetails/{!! $OrderLists->id !!}">
                                                    <span class="btn btn-xs btn btn-success text-xs">
                                                    <i class="fa fa-list"></i> View Details
                                                    </span>
                                                </a>
                                                <a href="editOrder/{!! $OrderLists->id !!}">
                                                    <span class="btn btn-xs btn btn-success text-xs">
                                                    <i class="fa fa-list"></i> Edit Order
                                                    </span>
                                                </a>

                                                <button  data-href="delOrder/{!! $OrderLists->id !!}" class="btn btn-danger orderDelete" title="Delete"><i class="fa fa-trash-o"></i></button>


                                            </td>
                                        </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>

<!-- Modal Bootstrap -->


<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-warning text-center" id="myModalLabel">Confirm Order Delete</h4>
            </div>
            <div class="modal-body">
                <p>Are you really trying to Delete?</p>
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-warning successDelete">Confirm</a>
                <button type="button" class="btn btn-default closeCancell" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- end: page -->
@stop
@section('javascript')

$('.orderDelete').click(function(){
var dataHref = $(this).attr('data-href');
$('.successDelete').attr('href',dataHref);
$('#modalDelete').modal('show');

});
@stop