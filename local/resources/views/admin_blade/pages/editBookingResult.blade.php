@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Booking Result | Modify Quote</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    @include('admin_blade.pages.tabs')

    <div class="row show-grid">
        <div class="col-md-12 panel-heading bg-primary"></div>

        <div class="col-md-3">
            <h3 class="text-semibold text-left">Pickup Address</h3>

            <hr class="mt-md mb-md">

            <p class="text-left"><?= ($sessData1stStage == NULL) ? $order->pickup_address : $sessData1stStage['pickup'] ?></p>
        </div>

        <div class="col-md-3">
            <h3 class="text-semibold text-left">Dropoff Address</h3>

            <hr class="mt-md mb-md">

            <p class="text-left"><?= ($sessData1stStage == NULL) ? $order->dropoff_address : $sessData1stStage['dropoff'] ?></p>
        </div>

        <div class="col-md-2">
            <h3 class="text-semibold text-left">Est. Distance</h3>

            <hr class="mt-md mb-md">

            <p class="text-left"><?= ($sessDatatotalDistance == NULL) ? $order->total_distance : $sessDatatotalDistance ?></p>
        </div>

        <div class="col-md-4">

        </div>
    </div>
    <hr class="mt-md mb-md">



    <div class="row">
        <div class="col-md-6 col-lg-12 col-xl-6">
        <section class="panel panel-featured-left">
                <div class="panel-body">
                    <div class="widget-summary">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-primary">
                                <i class="fa fa-car"></i>
                            </div>
                        </div>
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="amount">Cars List

                                    </h4>
                                <div class="info">
                                    <strong class="title">Choose your desired car</strong>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

  @foreach($carList as $key => $cardetails)
    {!! Form::open(['url' => 'editOrderCarDeailsSubmit', 'class' => 'form-horizontal editCarsForm'] ) !!}
    <div class="row show-grid panel-body mt-md mb-md">

        <div class="col-md-2">
            <img src="{!! asset('assets/images/cars/') !!}/{!! $cardetails->car_pic !!}" alt="car image" width="132px" />
        </div>

        <div class="col-md-4">
            <h4 class="text-semibold text-left">{{ $cardetails->name }}</h4>
            <p class="text-left text-danger text-semibold">5% Discount Included on Return Journey</p>
        </div>

        <div class="col-md-3">

            <input type="hidden" name="order_id" id="orderId" value="{{$order->id}}">
            <input type="hidden" name="userId" value="{{$order->user_id}}">

            <input type="hidden" name="carname" class="carId" value="{{$cardetails->id}}"  />
            <input type="hidden" name="carfullname" value="{{$cardetails->name}}"  />
            <input type="hidden" name="car_pic" value="{{$cardetails->car_pic}}"  />
            <input type="hidden" name="tourType" class="tourType" />
            <input type="hidden" name="distances" value="{{$sessDatatotalDistance}}"  />
            <input type="hidden" name="pageId" id="pageId" value="2">
            <input type="hidden" name="clickedPage" class="clickedPage"  />
            <input type="hidden" name="updateFlag" id="updateFlag_{!! $cardetails->id !!}"  />


            <input type="hidden" name="singleTrip" value="{{ $routesArray['carPrices'][$cardetails->id]['price'] }}"  />
            <input type="hidden" name="roundTrip" value="{{ $routesArray['carPrices'][$cardetails->id]['priceReturn'] }}"  />
            <h5>
                <input type="radio" class="singleTrip bookingOption" id="single_{!! $cardetails->id !!}" name="totalprice" required="required" value="{{ $routesArray['carPrices'][$cardetails->id]['price'] }}" @if($sessData2ndStage != NULL) @if($cardetails->id == $sessData2ndStage['carname'] && $sessData2ndStage['tourType'] == 0) checked @endif @else @if($cardetails->id == $order->car_details && $order->return_booking == 0) checked @endif @endif />
                <label for="single_{!! $cardetails->id !!}">Single</label>
                <strong>{{ $routesArray['carPrices'][$cardetails->id]['currencySymbol'] }}{{ $routesArray['carPrices'][$cardetails->id]['priceFormatted'] }}
                </strong>
            </h5>
            <h5>
                <input type="radio" class="roundTrip bookingOption" id="round_{!! $cardetails->id !!}" name="totalprice" value="{{ $routesArray['carPrices'][$cardetails->id]['priceReturn'] }}" @if($sessData2ndStage != NULL) @if($cardetails->id == $sessData2ndStage['carname'] && $sessData2ndStage['tourType'] == 1) checked @endif @else @if($cardetails->id == $order->car_details && $order->return_booking == 1) checked @endif @endif />
                <label for="round_{!! $cardetails->id !!}">Return</label>
                <strong>{{ $routesArray['carPrices'][$cardetails->id]['currencySymbol'] }}{{ $routesArray['carPrices'][$cardetails->id]['priceReturnFormatted'] }}
                </strong>
            </h5>
        </div>

        <div class="col-md-3">
            <p class="text-left">
                <input type="checkbox" class="meet" id="meet_{!! $cardetails->id !!}" name="meetngreet" value="6.00" @if($sessData2ndStage != NULL) @if($cardetails->id == $sessData2ndStage['carname'] && $sessMeetngreet == '8') checked @endif @else @if($cardetails->id == $order->car_details && $order->meetngreet == '8') checked @endif @endif />
                <label for="meet_{!! $cardetails->id !!}">Meet & Greet</label>
                <strong>{{ $routesArray['carPrices'][$cardetails->id]['currencySymbol'] }}6.00
                </strong>
            </p>

                <button type="submit" id="resultsave" class="btn btn-primary btn-block resultsave updateNow">Update Now</button>
                <button type="submit" id="resultsave" class="btn btn-primary btn-block resultsave next" style="display: none;">Next</button>

        </div>
    </div>
    {!! Form::close() !!}
  @endforeach








</div>
    <!--  end  -->

</section>
</div>
</div>
<!-- end: page -->




@stop