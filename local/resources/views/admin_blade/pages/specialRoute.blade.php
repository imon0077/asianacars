@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Add Special Routes</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'addSpecialRoute', 'method'=>'post', 'class' => 'form-horizontal adminAddorder'] ) !!}

    <div class="form-group field">
        <label class="col-md-3 control-label" for="pickup">Pickup Address</label>
        <div class="col-md-7">
            <input class="form-control" id="start" name="pickup" value="" type="text">
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-3 control-label" for="dropoff">Dropoff Address</label>
        <div class="col-md-7">
            <input class="form-control" id="end" name="dropoff" value="" type="text"  >
        </div>
    </div>


    <div class="form-group field">
        <label class="col-md-3 control-label" for="distance">Distance</label>
        <div class="col-md-7">
            <input class="form-control" id="distance" name="distance" value="" type="text"  >
        </div>
    </div>


    <!--div class="form-group field">
        <label class="col-md-3 control-label" for="price">Price</label>
        <div class="col-md-7">
            <input class="form-control" id="price" name="price" value="" type="text"  >
        </div>
    </div-->

    @foreach($cars as $car)
        <div class="form-group field">
            <label class="col-md-3 control-label" for="price">{!! $car->name !!} One way</label>
            <div class="col-md-7">
                <input class="form-control" id="single_{!! $car->id !!}" name="single_{!! $car->id !!}" value="" type="text"  >
            </div>
        </div>
        <div class="form-group field">
            <label class="col-md-3 control-label" for="price">{!! $car->name !!} Round way</label>
            <div class="col-md-7">
                <input class="form-control" id="round_{!! $car->id !!}" name="round_{!! $car->id !!}" value="" type="text"  >
            </div>
        </div>
    @endforeach

    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary adminQuotenow">Quote Now</button>
        </div>
    </div>

    <!--  end  -->
    {!! Form::close() !!}
</section>
</div>
</div>
<!-- end: page -->




@stop