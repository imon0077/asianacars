@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Add Special Routes</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'addRouteDescription', 'method'=>'post', 'class' => 'form-horizontal adminAddorder'] ) !!}

    <div class="form-group field">
        <label class="col-md-3 control-label" for="distance">Route</label>
        <div class="col-md-7">
            <input class="form-control" id="distance" name="route" value="" type="text" required="required" >
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-3 control-label" for="distance">Description</label>
        <div class="col-md-7">
            <textarea class="form-control" id="distance" name="description" required="required"></textarea>
        </div>
    </div>


    <!--div class="form-group field">
        <label class="col-md-3 control-label" for="price">Price</label>
        <div class="col-md-7">
            <input class="form-control" id="price" name="price" value="" type="text"  >
        </div>
    </div-->
    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Submit</button>
        </div>
    </div>

    <!--  end  -->
    {!! Form::close() !!}
</section>
</div>
</div>
<!-- end: page -->




@stop