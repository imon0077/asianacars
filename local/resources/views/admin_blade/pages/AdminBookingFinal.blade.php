@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Booking Details | Instant Quote</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

{!! Form::open(['url' => 'AdminBookingSubmit', 'id' => 'booking_submit', 'class' => 'form-horizontal'] ) !!}

<input type="hidden" name="distances" value="{{$distances}}"  />
<input type="hidden" name="pickup" value="{{$pickup}}"  />
<input type="hidden" name="dropoff" value="{{$dropoff}}"  />
<input type="hidden" name="via_points" value="{{$via_points}}"  />
<input type="hidden" name="carname" value="{{$carname}}"  />
<input type="hidden" name="tourType" id="tourType" value="{{$tripType}}"  />
<input type="hidden" name="totalpricespecialdate" id="totalPriceSpecialDate" value="{{ $totalSpecialPrice }}"  />
<input type="hidden" name="special_date" id="special_date" value="0"  />
<input type="hidden" name="totalpriceregular" id="totalpriceregular" value="{{ $totalprice }}"  />
<!--<input type="hidden" name="totalprice" id="totalprice" value="{{ $totalprice }}"  />-->
<input type="hidden" id="meetngreet" name="meetngreet" value="{{ $meetngreet }}"  />
<input type="hidden" id="stripeCustomerID" name="stripeCustomerID" value=""  />
<input type="hidden" id="stripeChargeID" name="stripeChargeID" value=""  />
<input type="hidden" id="baseURL" name="baseURL" value="{!!url()!!}" />
<input type="hidden" id="stripePublishableKey" name="stripePublishableKey" value="{{$stripe['publishable_key']}}"  />
<input type="hidden" id="siteName" name="siteName" value="Asiana Cars Ltd."  />

<!--Custmers Details-->
<div class="col-lg-12">
    <section class="panel panel-transparent">
        <div class="panel-body">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Custmers Details</h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="fname">First Name</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='firstname' id="firstname" type="text"  >
                                    </div>

                                    <label class="col-md-2 control-label" for="lname">Last Name</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='lastname' id="lastname" type="text"  >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="email">Email Address</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='email' type="text" id="email" >
                                    </div>

                                    <label class="col-md-2 control-label" for="mobile">Mobile</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='mobile' type="text" id="mobile" >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="comments">Comments</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control" rows="3" id="textareaDefault" name="comments" ></textarea>
                                    </div>

                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </section>
</div>
<!-- /Customer details-->


<!--Pickup Details-->
<div class="col-lg-12">
    <section class="panel panel-transparent">
        <div class="panel-body">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Pickup Address</h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="pickup">pickup_address</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='pickup' id="pickup" value="{{ str_replace('+', ' ', $pickup) }}" disabled="disabled" type="text"  >
                                    </div>

                                    <label class="col-md-2 control-label" for="pickup_address1">pickup_address1</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='pickup_address1' id="pickup_address1" type="text"  >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="pickup_address2">pickup_address2</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='pickup_address2' id="pickup_address2" type="text"  >
                                    </div>

                                    <label class="col-md-2 control-label" for="pickup_postcode">pickup_postcode</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='pickup_postcode' id="pickup_postcode" type="text"  >
                                    </div>
                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </section>
</div>
<!-- /Pickup details-->


<!--Dropoff Details-->
<div class="col-lg-12">
    <section class="panel panel-transparent">
        <div class="panel-body">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Dropoff Address</h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="dropoff">dropoff_address</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='dropoff' id="dropoff" value="{{ str_replace('+', ' ', $dropoff) }}" disabled="disabled" type="text"  >
                                    </div>

                                    <label class="col-md-2 control-label" for="dropoff_address1">dropoff_address1</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='dropoff_address1' id="dropoff_address1" type="text"  >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="dropoff_address2">dropoff_address2</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='dropoff_address2' id="dropoff_address2" type="text"  >
                                    </div>

                                    <label class="col-md-2 control-label" for="dropoff_postcode">dropoff_postcode</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='dropoff_postcode' id="dropoff_postcode" type="text"  >
                                    </div>
                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </section>
</div>
<!-- /Dropoff details-->

<!--Journey Date & Time Details-->
<div class="col-lg-12">
    <section class="panel panel-transparent">
        <div class="panel-body">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Journey Date & Time</h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="tourdate">Journey Date</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</span>
                                            <input class="form-control" data-plugin-datepicker name='tourdate' id="tourdate" type="text"  >
                                        </div>

                                    </div>

                                    <div class="col-md-2">
                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-clock-o"></i>
														</span>
                                            <select data-plugin-selectTwo class="form-control populate" name="tourtime" id="tourtime">
                                                <optgroup label="Hour">
                                                    <option class="time1" value="01"> 01 hr</option>
                                                    <option class="time1" value="02"> 02 hr</option>
                                                    <option class="time1" value="03"> 03 hr</option>
                                                    <option class="time1" value="04"> 04 hr</option>
                                                    <option class="time1" value="05"> 05 hr</option>
                                                    <option class="time1" value="06"> 06 hr</option>
                                                    <option class="time1" value="07"> 07 hr</option>
                                                    <option class="time1" value="08"> 08 hr</option>
                                                    <option class="time1" value="09"> 09 hr</option>
                                                    <option class="time1" value="10"> 10 hr</option>
                                                    <option class="time1" value="11"> 11 hr</option>
                                                    <option class="time1" value="12"> 12 hr</option>
                                                    <option class="time1" value="13"> 13 hr</option>
                                                    <option class="time1" value="14"> 14 hr</option>
                                                    <option class="time1" value="15"> 15 hr</option>
                                                    <option class="time1" value="16"> 16 hr</option>
                                                    <option class="time1" value="17"> 17 hr</option>
                                                    <option class="time1" value="18"> 18 hr</option>
                                                    <option class="time1" value="19"> 19 hr</option>
                                                    <option class="time1" value="20"> 20 hr</option>
                                                    <option class="time1" value="21"> 21 hr</option>
                                                    <option class="time1" value="22"> 22 hr</option>
                                                    <option class="time1" value="23"> 23 hr</option>
                                                    <option class="time1" value="00"> 00 hr</option>
                                                </optgroup>
                                            </select>
                                        </div>


                                    </div>

                                    <div class="col-md-2">

                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-clock-o"></i>
														</span>
                                            <select data-plugin-selectTwo class="form-control populate" name="tourmin" id="tourmin">
                                                <optgroup label="Min">
                                                    <option class="time1" value="00"> 00 min</option>
                                                    <option class="time1" value="15"> 15 min</option>
                                                    <option class="time1" value="30"> 30 min</option>
                                                    <option class="time1" value="45"> 45 min</option>
                                                </optgroup>
                                            </select>
                                        </div>


                                    </div>

                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </section>
</div>
<!-- /Journey Date & Time details-->


@if($tripType == 'round')
<!--Return Date & Time Details-->
<div class="col-lg-12">
    <section class="panel panel-transparent">
        <div class="panel-body">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Return Date & Time</h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="re-tourdate">Return Date</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</span>
                                            <input class="form-control" data-plugin-datepicker name='returnDate' id="re-tourdate" type="text" >
                                        </div>

                                    </div>

                                    <div class="col-md-2">
                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-clock-o"></i>
														</span>
                                            <select data-plugin-selectTwo class="form-control populate" name="returnTime" id="re-tourtime">
                                                <optgroup label="Hour">
                                                    @for($i=1; $i<24; $i++)
                                                    @if($i < 10)
                                                    <option class="time1" value="0{{ $i }}"> 0{{ $i }} hr</option>
                                                    @else
                                                    <option class="time1" value="{{ $i }}"> {{ $i }} hr</option>
                                                    @endif
                                                    @endfor
                                                    <option class="time1" value="00"> 00 hr</option>
                                                </optgroup>
                                            </select>
                                        </div>


                                    </div>

                                    <div class="col-md-2">

                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-clock-o"></i>
														</span>
                                            <select data-plugin-selectTwo class="form-control populate" name="returnMin" id="re-tourmin">
                                                <optgroup label="Min">
                                                    @for($i=0; $i<60; $i+=15)
                                                    @if($i < 10)
                                                    <option class="time1" value="0{{ $i }}"> 0{{ $i }} min</option>
                                                    @else
                                                    <option class="time1" value="{{ $i }}"> {{ $i }} hr</option>
                                                    @endif
                                                    @endfor
                                                </optgroup>
                                            </select>
                                        </div>


                                    </div>

                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </section>
</div>
<!-- /Return Date & Time details-->
@endif


<!--Airport Details-->
<div class="col-lg-12">
    <section class="panel panel-transparent">
        <div class="panel-body">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Airport Details<span style="font-size: 13px">(optional)</span></h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="arrival_time">Flight Arrival Time</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='arrival_time' type="text" id="arrival_time" >
                                    </div>

                                    <label class="col-md-2 control-label" for="airline_name">Airline Name</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='airline_name' id="airline_name" type="text"  >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="flight_number">Flight Number</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='flight_number' type="text" id="flight_number" >
                                    </div>

                                    <label class="col-md-2 control-label" for="departure_city">Departure Cty</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='departure_city' id="departure_city" type="text"  >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="pickup_after">When should the driver pick you up?"</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='pickup_after' id="pickup_after" type="number" max="10" min="0" required >
                                    </div>

                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </section>
</div>
<!-- /Airport Details-->

<!--Passenger Details-->
<div class="col-lg-12">
    <section class="panel panel-transparent">
        <div class="panel-body">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Passenger Details<span style="font-size: 13px"> (optional)</span></h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="no_of_person">Number of Person</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name="no_of_person" max="10" min="0" id="no_of_person" type="number" >
                                    </div>

                                    <label class="col-md-2 control-label" for="no_of_suitcase">Number of Suitcase</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control" name="no_of_suitcase" max="10" min="0" id="no_of_suitcase" required>
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="no_of_child">Number of Child</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control" name="no_of_child" max="10" min="0" id="no_of_child" required>
                                    </div>
                                </div>


                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </section>
</div>
<!-- /Passenger Details-->






<!-- start: page -->
<div class="row">

    <div class="col-md-6 col-lg-6 col-xl-3">
        <section class="panel">
            <header class="panel-heading bg-white">
                <div class="center">
                    <img class="minicar4" src="{!! asset('assets/images/cars/') !!}/{!! $car_pic !!}" alt="mini car"/>
                </div>
            </header>
            <header class="panel-heading bg-primary text-center">
                {{$carfullname}}
            </header>
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">From</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">{{ str_replace('+', ' ', $pickup) }}</div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Destination</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">{{ str_replace('+', ' ', $dropoff) }}</div>
                    </div>
                </div>

                @if($via_points)
                @foreach($via_points_seperate as $k=>$v)
                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Way Point {{ $k+1 }}</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">{{ str_replace('+', ' ', $v) }}</div>
                    </div>
                </div>
                @endforeach
                @endif

                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Booking Type</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">{!! ucfirst($tripType) !!} Trip</div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Distance</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">{{ ceil($distances) }} miles</div>
                    </div>
                </div>

                @if($meetngreet)
                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Meet & Greet</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">£ {{$meetngreet}} (Included)</div>
                    </div>
                </div>
                @endif




<br/>
<!--                <h3 class="text-semibold mt-none text-center">Simple Block Title</h3>-->
<!--                <p class="text-center">Nullam quiris risus eget urna mollis ornare vel eu leo. Soccis natoque penatibus et magnis dis parturient montes. Soccis natoque penatibus et magnis dis parturient montes.</p>-->
            </div>

<!--            <header class="panel-heading bg-primary text-center">-->
<!--                <div class="row">-->
<!--                    <div class="col-md-6 text-right">-->
<!--                        <div class="h4 text-bold mb-none mt-lg">Total Fare</div>-->
<!--                    </div>-->
<!--                    <div class="col-md-6 text-left">-->
<!--                        <div class="h4 text-bold mb-none mt-lg" id="price_show">&pound; {{number_format($totalprice,2)}}</div>-->
<!--                    </div>-->
<!--                    <p class="text-center">(Toll, Congestion Charge Not Included.)</p>-->
<!--                    <p class="specialcharge" style="display: none; text-align: center; margin-top: 2px;">(Special pricing has been applied due to the date.)</p>-->
<!--                </div>-->
<!--            </header>-->


            <header class="panel-heading bg-primary text-center">
                <div class="row">
                    <div class="col-md-6 text-right">
                        <div class="h3 text-bold mb-none mt-lg">Total Fare (&pound;)</div>
                    </div>
                    <div class="col-md-6 text-left">
                        <div class="mt-lg">
                            <div class="col-sm-6">
                                <input type="text" name="totalprice" id="totalprice" class="form-control input-sm mb-md" value="{{$totalprice}}">
                            </div>
                        </div>
                    </div>
                    <p class="col-md-11 text-center">(Toll, Congestion Charge Not Included.)</p>
                </div>
            </header>

        </section>
    </div>

    <div class="col-md-6 col-lg-6 col-xl-3">
        <section class="panel">
            <header class="panel-heading bg-primary text-center">
                Payment Method
            </header>
            <div class="panel-body text-center">


                <div class="row">
                    <div class="col-sm-offset-0 col-sm-12">


                        <div class="form-group">
                            <div class="form-group">
                                <input class="" type="radio" id="cash" name="payment_method" value="cash" />
                                <label style="display: initial; font-weight: normal; margin-bottom: 0;" for="cash">&nbsp;&nbsp;Cash</label>
                            </div>

                            @foreach($pm as $payment_method)
                            <?php $payment_method_label = str_replace('_',' ',$payment_method); ?>

                            <div class="form-group">
                                <input class="" type="radio" id="{!! $payment_method !!}" name="payment_method" value="{!! $payment_method !!}" />
                                <label style="display: initial; font-weight: normal; margin-bottom: 0;" for="{!! $payment_method !!}">&nbsp;&nbsp;{!! ucwords($payment_method_label) !!}</label>
                                @if($payment_method == 'secured_card_payment') <img src="{{ asset('assets/images/big-rapidsslsiteseal.jpg') }}" style="height: 20px;" alt="Secured By RapidSSL"/> @endif
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>


<!--                <div class="row">-->
<!--                    <div class="col-md-8">-->
<!--                        <div class="h5 text-bold mb-none mt-lg text-left">-->
<!--                            <input class="" type="radio" id="Cash" name="payment_method" value="Cash" />-->
<!--                            <label style="display: initial; font-weight: normal; margin-bottom: 0;" for="Cash">&nbsp;&nbsp;Cash</label>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->


            </div>
        </section>
    </div>


    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary adminBookingsubmit">Pay for booking</button>

        </div>
    </div>

{!! Form::close() !!}

</div>
<!-- end: page -->

<!-- Special Modal -->

<div class="modal fade" id="specialPrice" tabindex="-1" role="dialog" aria-labelledby="Special Price" aria-hidden="true">

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-danger text-center">Special Charge!</h4>
            </div>
            <div class="modal-body" style="text-align:center; min-height: 150px; height: 150px; font-size: 25px;" id="">
                There will be an additional charge of <span id="additionalPrice" style="color: red; font-weight: bolder;"></span> GBP for the journey on the selected date.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
            </div>
        </div>

    </div>

</div>

<!-- /Special Modal -->

<!-- Stripe Payment Status Modal -->

<div class="modal fade" id="payment_with_stripe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-danger text-center">Payment Status</h4>
            </div>
            <div class="modal-body" style="text-align:center; min-height: 150px; height: 150px;" id="card_payment_status">

            </div>

        </div>

    </div>

</div>

<!-- /Stripe Payment Status Modal -->



@stop