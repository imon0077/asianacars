@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Special Route list</h2>

</header>
@include('flash::message')
@include('admin_blade.common.error-message')
<!-- start: page -->
<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">

                            <div id="collapse1One" class="accordion-body collapse in">

                                <!-- -->
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-default" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
                                        <thead>
                                        <tr>
                                            <th style="display: none">testing column(dont remove)</th>
                                            <th>#ID</th>
                                            <th>Pickup Address</th>
                                            <th>Dropoff Address</th>
                                            <th>Distance</th>
                                            @foreach($cars as $car)
                                                <th>{!! $car->name !!} One way</th>
                                                <th>{!! $car->name !!} Round way</th>
                                            @endforeach
                                            <th>Created at</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($special_routes as $sr)

                                        <tr class="gradeX">
                                            <td style="display: none">testing column(dont remove)</td>
                                            <td>{!! $sr->id !!}</td>
                                            <td>{!! $sr->pickup_address !!} </td>
                                            <td> {!! $sr->dropoff_address !!}</td>
                                            <td> {!! $sr->distance !!}</td>
                                            @foreach($cars as $car)
                                                <td><?php $a='single_'.$car->id; echo $sr->$a ?></td>
                                                <td><?php $a='round_'.$car->id; echo $sr->$a ?></td>
                                            @endforeach
                                            <td>{!! $sr->created_at !!}</td>

                                            <td class="actions">
                                                <a href="edit_sr/{!! $sr->id !!}" class="on-default btn edit-row" title="Edit"><i class="fa fa-pencil"></i></a>
                                                <button  data-href="del_sr/{!! $sr->id !!}" class="btn patientDelete" title="Delete"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>

<!-- Modal Bootstrap -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-warning text-center" id="myModalLabel">Confirm Special Route Delete</h4>
            </div>
            <div class="modal-body">
                <p>Are you really trying to Delete?</p>
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-warning successDelete">Confirm</a>
                <button type="button" class="btn btn-default closeCancell" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- end: page -->
@stop

@section('javascript')
$('.patientDelete').click(function(){
var dataHref = $(this).attr('data-href');
$('.successDelete').attr('href',dataHref);
$('#modalDelete').modal('show');
});
@stop