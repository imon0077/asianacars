@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Edit Order of "<span class="text-danger">{!! ucwords($order->first_name) !!} {!! ucwords($order->last_name) !!}</span>"  </h2>

</header>
@include('flash::message')
@include('admin_blade.common.error-message')
<!-- start: page -->
<div class="row">
    <div class="col-xs-12">

        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Order Details </h2>
            </header>
            <div class="panel-body">
            {!! Form::open(['url' => 'editOrderSave', 'class' => 'form-horizontal'] ) !!}
                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Order No</label>
                    <div class="col-md-6">
                              <input class="form-control" name='orderId' value="{!! $order->id !!}" readonly type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Pickup Address</label>
                    <div class="col-md-6">
                              <input class="form-control" id="start" name='pickup_address' value="{!! ucwords($order->pickup_address) !!}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Pickup Address1</label>
                    <div class="col-md-6">
                        <input class="form-control" name='pickup_address1' value="{!! ucwords($order->pickup_address1) !!}" type="text">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Pickup Address2</label>
                    <div class="col-md-6">
                        <input class="form-control" name='pickup_address1' value="{!! ucwords($order->pickup_address2) !!}" type="text">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Pickup Postcode</label>
                    <div class="col-md-6">
                        <input class="form-control" name='pickup_postcode' value="{!! ucwords($order->pickup_postcode) !!}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Dropoff Address</label>
                    <div class="col-md-6">
                        <input class="form-control" id="end" name='dropoff_address' value="{!! ucwords($order->dropoff_address) !!}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Dropoff Address1</label>
                    <div class="col-md-6">
                        <input class="form-control" name='dropoff_address1' value="{!! ucwords($order->dropoff_address1) !!}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Dropoff Address2</label>
                    <div class="col-md-6">
                        <input class="form-control" name='dropoff_address2' value="{!! ucwords($order->dropoff_address2) !!}" type="text">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Dropoff Postcode</label>
                    <div class="col-md-6">
                        <input class="form-control" name='dropoff_postcode' value="{!! ucwords($order->dropoff_postcode) !!}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Total Distance</label>
                    <div class="col-md-6">
                        <input class="form-control" name='total_distance' value="{!! ceil($order->total_distance) !!}" type="text">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Meet & Greet</label>
                    <div class="col-md-6">
                        <label class="control-label">{!! ((int)$order->meetngreet > 0) ? '<span class="badge green">Yes</span>&nbsp; (£'.number_format($order->meetngreet,2).' included)':'No' !!}</label>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Total Bill(£)</label>
                    <div class="col-md-6">
                        <input class="form-control" name='total_bill' value="{!! number_format($order->total_bill,2) !!}" type="text">
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Order Date</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->created_at !!}</label>
                    </div>
                </div>

                <hr/>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Payment Method</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! ucfirst(str_replace('_',' ',$order->pay_via)) !!}</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Card Customer ID (Ex: card id reference used for Stripe/Paypal)</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->card_customer_id !!}</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Card Charge ID (Ex: charge/transaction id in Stripe/Paypal)</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->card_charge_id !!}</label>
                    </div>
                </div>

                <hr/>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Order Status</label>
                    <div class="col-md-6">
                          <label class="control-label">
<!--                            @if($order->status == 0)Pending @elseif($order->status == 1) Approved @else Rejected @endif-->

                            @if(Auth::user()->role == 1)



                        </label>
                    </div>
                </div>

                @endif

                @if(Auth::user()->role == 2)
                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Payment Status</label>
                    <div class="col-md-6">
                          <label class="control-label">
<!--                            @if($order->payment_status == 0)Unpaid @elseif($order->payment_status == 1) Paid @else Partially Paid @endif-->



                        </label>
                    </div>
                </div>
                @endif
            </div>
        </section>
    </div>

    <!-- Passengers details -->
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Passengers Details</h2>
            </header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Name</label>
                    <div class="col-md-6">
                        <input class="form-control" name='name' value="{!! ucwords($order->first_name) !!} {!! ucwords($order->last_name) !!}" type="text">
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Email</label>
                    <div class="col-md-6">
                        <input class="form-control" name='email' value="{!! ucwords($order->email) !!}" type="text">
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Mobile</label>
                    <div class="col-md-6">
                        <input class="form-control" name='phone_number' value="{!! ucwords($order->phone_number) !!}" type="text">
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Number of Person</label>
                    <div class="col-md-6">
                        <input class="form-control" name='no_of_person' value="{!! ucwords($order->no_of_person) !!}" type="text">
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">Number of Child</label>
                    <div class="col-md-6">
                        <input class="form-control" name='no_of_child' value="{!! ucwords($order->no_of_child) !!}" type="text">
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label bold" for="inputDefault">No of Suitcase</label>
                    <div class="col-md-6">
                       <input class="form-control" name='suitcases' value="{!! ucwords($order->suitcases) !!}" type="text">
                    </div>
                </div>


            </div>
        </section>
        </div>
    <!-- /Passenger details -->

    <!-- waypoint details -->
    <div class="col-xs-12">
        @if($via_points)
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Waypoint Details</h2>
            </header>
            <div class="panel-body">
                @foreach($via_points_seperate as $k=>$v)
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Way Point {{ $k+1 }}</label>
                    <div class="col-md-6">
                        <input class="form-control" name='waypoints' value="{{ str_replace('+', ' ', $v) }}" type="text">
                    </div>
                </div>
                @endforeach
            </div>
        </section>
        @endif
    </div>
    <!-- /waypoint details  -->

    <!-- Journey Date & time details -->
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Journey Date &amp; Time</h2>
            </header>
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Journey Date</label>
                    <div class="col-md-6">
                        <input class="form-control" name='tour_time' value="{{ date('m-d-Y', $order->date_time) }}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Journey Time</label>
                    <div class="col-md-6">
                        <input class="form-control" name='tour_time' value="{{ date('H:i', $order->date_time) }}" type="text">
                    </div>
                </div>

            </div>
        </section>
    </div>
    <!-- /Journey Date & time details  -->

    @if($order->return_booking == '1')
    <!-- Return Date & time details -->
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Return Date &amp; Time</h2>
            </header>
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Journey Date</label>
                    <div class="col-md-6">
                        <input class="form-control" name='return_date' value="{{ date('m-d-Y', $order->return_date_time) }}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Journey Time</label>
                    <div class="col-md-6">
                        <input class="form-control" name='return_time' value="{{ date('H:i', $order->return_date_time) }}" type="text">
                    </div>
                </div>

            </div>
        </section>
    </div>
    <!-- /Return Date & time details  -->
    @endif

    <!-- Airport details -->
    <div class="col-xs-12">
        @if($order->airline_name)
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Airport Details</h2>
            </header>
            <div class="panel-body">
                @if($order->flight_arrival_time != '0')
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Flight Arrival Time</label>
                    <div class="col-md-6">
                        <input class="form-control" name='flight_arrival_time' value="{!! $order->flight_arrival_time !!}" type="text">
                    </div>
                </div>
                @endif

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Airline Name</label>
                    <div class="col-md-6">
                        <input class="form-control" name='airline_name' value="{!! $order->airline_name !!}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Flight Number</label>
                    <div class="col-md-6">
                        <input class="form-control" name='flight_no' value="{!! $order->flight_no !!}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Departure City</label>
                    <div class="col-md-6">
                        <input class="form-control" name='departure_city' value="{!! $order->departure_city !!}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Pickup After Landing</label>
                    <div class="col-md-6">
                        <input class="form-control" name='pickup_after' value="{!! $order->pickup_after !!}" type="text">
                    </div>
                </div>

            </div>
        </section>
        @endif
    </div>
    <!-- /Airport details  -->

    <!-- Car details -->
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Car Details</h2>
            </header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Car Name</label>
                    <div class="col-md-6">
                        <input class="form-control" name='carname' value="{!! $order->carname !!}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Total Seats</label>
                    <div class="col-md-6">
                        <input class="form-control" name='total_seats' value="{!! $order->total_seats !!}" type="text">
                    </div>
                </div>

            </div>
        </section>
    </div>

@if($order->status == 1)
    <!-- Driver details -->
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Driver Details</h2>
            </header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Name</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->dFname !!} {!! $order->dLname !!} </label>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Email</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->dEmail !!} </label>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label bold" for="inputDefault">Mobile</label>
                    <div class="col-md-6">
                          <label class="control-label">{!! $order->dPhone !!} </label>

                    </div>
                </div>

            </div>
        </section>
    </div>
@endif

<!--    -->
    <div class="form-group  ">
        <label class="col-md-3 control-label" for="inputDefault"></label>

        <a href="{{ URL::previous() }}" class="mb-xs mt-xs mr-xs btn btn-warning saveco"> Back</a>
        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Save Changes</button>

    </div>
</div>
<!-- end: page -->
{!! Form::close() !!}
<!-- end: page -->
@stop