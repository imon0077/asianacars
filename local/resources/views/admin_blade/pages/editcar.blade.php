@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Edit Car</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'editCarSave', 'class' => 'form-horizontal form-bordered','id'=> 'editCarSave' ]) !!}
    <input class="form-control" value="{!! $cardetails->id !!}" name='id' id="id" type="hidden">

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">Car Name</label>
        <div class="col-md-3">
            <input class="form-control" name='name' value="{{ $cardetails->name }}" type="text"  >
        </div>

        <label class="col-md-3 control-label" for="">Total Seats</label>
        <div class="col-md-3">
            <input class="form-control" name='tot_seats' value="{{ $cardetails->total_seats }}" type="text"  >
        </div>
    </div>


    <div class="form-group">
        <label class="col-md-2 control-label" for="">Car Category</label>
        <div class="col-md-3">
            <select data-plugin-selectTwo class="form-control populate" name="cars_cat">
                <optgroup label="Cars Category">
                    <option value="">Select...</option>
                    @foreach($CarsCat as $CarsCats)
                    <option value="{{ $CarsCats->id }}" @if($cardetails->category == $CarsCats->id ) selected @endif> {{ $CarsCats->cat_name }} </option>
                    @endforeach
                </optgroup>
            </select>
        </div>

        <label class="col-md-3 control-label" for="">Drivers</label>
        <div class="col-md-3">
            <select data-plugin-selectTwo class="form-control populate" name="drivers">
                <optgroup label="Drivers">
                    <option value="">Select...</option>
                    @foreach($Driver as $dList)
                    <option value="{{$dList->id}}" @if($cardetails->driver_id == $dList->id ) selected @endif >{{$dList->first_name}} {{$dList->last_name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Save Changes</button>

        </div>
    </div>

    <!--  end  -->



     {!! Form::close() !!}
</section>
</div>
</div>

<!-- end: page -->
@stop
