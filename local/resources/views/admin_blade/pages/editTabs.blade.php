<div class="wizard-tabs">
    <ul class="wizard-steps">
        <li class="active">
            <a href="javascript:void(0)" id="editLoc" class="text-center">
                <span class="badge hidden-xs">1</span>
                Locations
            </a>
        </li>
        <li class="">
            <a href="javascript:void(0)" id="editCar" class="text-center">
                <span class="badge hidden-xs">2</span>
                Car Selections
            </a>
        </li>
        <li class="">
            <a href="javascript:void(0)" id="editCustomer" class="text-center">
                <span class="badge hidden-xs">3</span>
                Customer Details
            </a>
        </li>
    </ul>
</div>
<!--{!!url()!!}/editOrderCarDeails/{{ $order->id }}-->