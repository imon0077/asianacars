            <!--Custmers Details-->
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Customers Details</h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="fname">First Name</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='firstname' type="text" value="<?= ($sessData3rdStage['firstname'] == NULL) ? ucwords($order->first_name) : $sessData3rdStage['firstname'] ?>" >
                                    </div>

                                    <label class="col-md-2 control-label" for="lname">Last Name</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='lastname' type="text" value="<?= ($sessData3rdStage['lastname'] == NULL) ? ucwords($order->last_name) : $sessData3rdStage['lastname'] ?>"  >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="email">Email Address</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='email' type="text" value="<?= ($sessData3rdStage['email'] == NULL) ? ucwords($order->email) : $sessData3rdStage['email'] ?>"  >
                                    </div>

                                    <label class="col-md-2 control-label" for="mobile">Mobile</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='mobile' type="text" value="<?= ($sessData3rdStage['mobile'] == NULL) ? $order->phone_number : $sessData3rdStage['mobile'] ?>" >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="comments">Comments</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control" rows="3" id="textareaDefault" name="comments"><?= ($sessData3rdStage['comments'] == NULL) ? $order->comments : $sessData3rdStage['comments'] ?></textarea>
                                    </div>

                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>
            <!-- /Customer details-->

            <!--Pickup Details-->
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Pickup Address</h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="pickup_address">pickup_address</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name="pickup_address" id="pickup_address"  value="<?= ($sessData1stStage['pickup'] == NULL) ? ucwords($order->pickup_address) : $sessData1stStage['pickup'] ?>" disabled="disabled" type="text"  >
                                    </div>

                                    <label class="col-md-2 control-label" for="pickup_address1">pickup_address1</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='pickup_address1' id="pickup_address1" type="text" value="<?= ($sessData3rdStage['pickup_address1'] == NULL) ? str_replace('+', ' ', $order->pickup_address1) : $sessData3rdStage['pickup_address1'] ?>"  >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="pickup_address2">pickup_address2</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='pickup_address2' id="pickup_address2" type="text" value="<?= ($sessData3rdStage['pickup_address2'] == NULL) ? str_replace('+', ' ', $order->pickup_address2) : $sessData3rdStage['pickup_address2'] ?>"  >
                                    </div>

                                    <label class="col-md-2 control-label" for="pickup_postcode">pickup_postcode</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='pickup_postcode' id="pickup_postcode" type="text" value="<?= ($sessData3rdStage['pickup_postcode'] == NULL) ? str_replace('+', ' ', $order->pickup_postcode) : $sessData3rdStage['pickup_postcode'] ?>" >
                                    </div>
                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>
            <!-- /Pickup details-->


<!--Dropoff Details-->
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Dropoff Address</h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="dropoff_address">dropoff_address</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name="dropoff_address" id="dropoff_address" value="<?= ($sessData1stStage == NULL) ? ucwords($order->dropoff_address) : $sessData1stStage['dropoff'] ?>" disabled="disabled" type="text"  >
                                    </div>

                                    <label class="col-md-2 control-label" for="dropoff_address1">dropoff_address1</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='dropoff_address1' id="dropoff_address1" type="text" value="<?= ($sessData3rdStage['dropoff_address1'] == NULL) ? str_replace('+', ' ', $order->dropoff_address1) : $sessData3rdStage['dropoff_address1'] ?>" >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="dropoff_address2">dropoff_address2</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='dropoff_address2' id="dropoff_address2" type="text" value="<?= ($sessData3rdStage['dropoff_address2'] == NULL) ? str_replace('+', ' ', $order->dropoff_address2) : $sessData3rdStage['dropoff_address2'] ?>" >
                                    </div>

                                    <label class="col-md-2 control-label" for="dropoff_postcode">dropoff_postcode</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='dropoff_postcode' id="dropoff_postcode" type="text" value="<?= ($sessData3rdStage['dropoff_postcode'] == NULL) ? str_replace('+', ' ', $order->dropoff_postcode) : $sessData3rdStage['dropoff_postcode'] ?>" >
                                    </div>
                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>
<!-- /Dropoff details-->

<!--Journey Date & Time Details-->
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Journey Date & Time</h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="tourdate">Journey Date
                                        {{-- {{ date('d/m/Y', $order->date_time) }} {{ $sessData3rdStage['tourdate'] }} --}}
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</span>
                                            <input class="form-control" data-plugin-datepicker name='tourdate' id="tourdate" type="text" value="<?= ($sessData3rdStage['tourdate'] == NULL) ? date('d/m/Y', $order->date_time) : $sessData3rdStage['tourdate'] ?>" >
                                            <input class="form-control" name='special_date' id="special_date" type="hidden" value="<?= ($sessData3rdStage['special_date'] == NULL) ? $order->special_date : $sessData3rdStage['special_date'] ?>" >
                                        </div>

                                    </div>

                                    <?php
                                    //echo $sessData3rdStage['tourtime'];die();
                                        $journeyHour = date('H', $order->date_time);
                                        $journeyMin = date('i', $order->date_time);
                                        if($sessData3rdStage['tourtime'] != "")
                                            $tourTime = $sessData3rdStage['tourtime'];
                                        else
                                            $tourTime = $journeyHour;

                                        if($sessData3rdStage['tourmin'] != "")
                                            $tourMin = $sessData3rdStage['tourmin'];
                                        else
                                            $tourMin = $journeyMin;
                                    ?>

                                    <div class="col-md-2">
                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-clock-o"></i>
														</span>
                                            <select data-plugin-selectTwo class="form-control populate" name="tourtime" id="tourtime">
                                                <optgroup label="Hour">
                                                    @for($i=1; $i<24; $i++)
                                                        @if($i < 10)
                                                        <option class="time1" value="0{{ $i }}" @if($tourTime == '0'.$i) selected @endif> 0{{ $i }} hr</option>
                                                        @else
                                                        <option class="time1" value="{{ $i }}" @if($tourTime == $i) selected @endif> {{ $i }} hr</option>
                                                        @endif
                                                    @endfor
                                                    <option class="time1" value="00"> 00 hr</option>
                                                </optgroup>
                                            </select>
                                        </div>


                                    </div>

                                    <div class="col-md-2">

                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-clock-o"></i>
														</span>
                                            <select data-plugin-selectTwo class="form-control populate" name="tourmin" id="tourmin">
                                                <optgroup label="Min">
                                                    @for($i=0; $i<60; $i+=15)
                                                        @if($i < 10)
                                                        <option class="time1" value="0{{ $i }}" @if($tourMin == '0'.$i) selected @endif> 0{{ $i }} min</option>
                                                        @else
                                                        <option class="time1" value="{{ $i }}" @if($tourMin == $i) selected @endif> {{ $i }} hr</option>
                                                        @endif
                                                    @endfor
                                                </optgroup>
                                            </select>
                                        </div>


                                    </div>

                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>
<!-- /Journey Date & Time details-->
<!--    --><?php
//        if($sessData2ndStage['tourType'] != ""){
//            $returnBooking = $sessData2ndStage['tourType'];
//        }
//        else
//            $returnBooking = $order->return_booking;
//
//    ?>
<!--@if($returnBooking == 1)-->

<!--Return Date & Time Details-->
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Return Date & Time</h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">
                            <?php
                                $returnDateTime = $order->return_date_time;
                                if($returnDateTime == 0){
                                    $returnDate = '';
                                    $returnHour = '';
                                    $returnMin = '';
                                }
                                else{
                                    $returnDate = date('d/m/Y', $order->return_date_time);
                                    $returnHour = date('H', $order->return_date_time);
                                    $returnMin = date('i', $order->return_date_time);
                                }

                                if($sessReturnTime != "")
                                    $returnTime = $sessReturnTime;
                                else
                                    $returnTime = $returnHour;

                                if($sessReturnMin != "")
                                    $returnMinvalue = $sessReturnMin;
                                else
                                    $returnMinvalue = $returnMin;

                            ?>
                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="re-tourdate">Return Date </label>
                                    <div class="col-md-4">
                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</span>

                                            <input class="form-control" data-plugin-datepicker name='returnDate' id="re-tourdate" required="required" type="text" value="<?= ($sessReturnDate == '') ? $returnDate : $sessReturnDate ?>" >
                                        </div>


                                    </div>

                                    <div class="col-md-2">
                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-clock-o"></i>
														</span>
                                            <select data-plugin-selectTwo class="form-control populate" name="returnTime" id="re-tourtime">
                                                <optgroup label="Hour">
                                                    @for($i=1; $i<24; $i++)
                                                    @if($i < 10)
                                                    <option class="time1" value="0{{ $i }}" @if($returnTime == '0'.$i) selected @endif > 0{{ $i }} hr</option>
                                                    @else
                                                    <option class="time1" value="{{ $i }}" @if($returnTime == $i) selected @endif> {{ $i }} hr</option>
                                                    @endif
                                                    @endfor
                                                    <option class="time1" value="00"> 00 hr</option>
                                                </optgroup>
                                            </select>
                                        </div>


                                    </div>

                                    <div class="col-md-2">

                                        <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-clock-o"></i>
														</span>
                                            <select data-plugin-selectTwo class="form-control populate" name="returnMin" id="re-tourmin">
                                                <optgroup label="Min">
                                                    @for($i=0; $i<60; $i+=15)
                                                    @if($i < 10)
                                                    <option class="time1" value="0{{ $i }}" @if($returnMinvalue == '0'.$i) selected @endif> 0{{ $i }} min</option>
                                                    @else
                                                    <option class="time1" value="{{ $i }}" @if($returnMinvalue == $i) selected @endif> {{ $i }} hr</option>
                                                    @endif
                                                    @endfor
                                                </optgroup>
                                            </select>
                                        </div>


                                    </div>

                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>
<!-- /Return Date & Time details-->

<!--@endif-->

<!--Airport Details-->
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Airport Details<span style="font-size: 13px">(optional)</span></h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="arrival_time">Flight Arrival Time</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='arrival_time' type="text" id="arrival_time" value="<?= ($sessData3rdStage['arrival_time'] == NULL) ? $order->flight_arrival_time : $sessData3rdStage['arrival_time'] ?>" >
                                    </div>

                                    <label class="col-md-2 control-label" for="airline_name">Airline Name</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='airline_name' id="airline_name" type="text" value="<?= ($sessData3rdStage['airline_name'] == NULL) ? $order->airline_name : $sessData3rdStage['airline_name'] ?>" >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="flight_number">Flight Number</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='flight_number' type="text" id="flight_number" value="<?= ($sessData3rdStage['flight_number'] == NULL) ? $order->flight_no : $sessData3rdStage['flight_number'] ?>" >
                                    </div>

                                    <label class="col-md-2 control-label" for="departure_city">Departure Cty</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='departure_city' id="departure_city" type="text" value="<?= ($sessData3rdStage['departure_city'] == NULL) ? $order->departure_city : $sessData3rdStage['departure_city'] ?>" >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="pickup_after">When should the driver pick you up?"</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name='pickup_after' id="pickup_after" type="number" max="10" min="0" required value="<?= ($sessData3rdStage['pickup_after'] == NULL) ? $order->pickup_after : $sessData3rdStage['pickup_after'] ?>" >
                                    </div>

                                </div>

                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>
<!-- /Airport Details-->

<!--Passenger Details-->
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Passenger Details<span style="font-size: 13px"> (optional)</span></h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="no_of_person">Number of Person</label>
                                    <div class="col-md-4">
                                        <input class="form-control" name="no_of_person" max="10" min="0" id="no_of_person" type="number" value="<?= ($sessData3rdStage['no_of_person'] == NULL) ? $order->no_of_person : $sessData3rdStage['no_of_person'] ?>" >
                                    </div>

                                    <label class="col-md-2 control-label" for="no_of_suitcase">Number of Suitcase</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control" name="no_of_suitcase" max="10" min="0" id="no_of_suitcase" required value="<?= ($sessData3rdStage['no_of_suitcase'] == NULL) ? $order->suitcases : $sessData3rdStage['no_of_suitcase'] ?>" >
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-2 control-label" for="no_of_child">Number of Child</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control" name="no_of_child" max="10" min="0" id="no_of_child" required value="<?= ($sessData3rdStage['no_of_child'] == NULL) ? $order->no_of_child : $sessData3rdStage['no_of_child'] ?>" >
                                    </div>
                                </div>


                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>
<!-- /Passenger Details-->


<!-- start: page -->
            <header class="panel-heading bg-white">
                <div class="center">
                    {{--<img class="minicar4" src="{!! asset('assets/images/cars/') !!}/{!! $carList->car_pic !!}" alt="mini car"/>--}}
                    <img class="minicar4" src="{!! asset('assets/images/cars/') !!}/<?= ($sessData2ndStage['car_pic'] == NULL) ? $carList->car_pic : $sessData2ndStage['car_pic'] ?>" alt="Car"/>
                </div>
            </header>
            <header class="panel-heading bg-primary text-center">
                <?= ($sessData2ndStage['carfullname'] == NULL) ? ucwords($order->carname) : $sessData2ndStage['carfullname'] ?>
                {{--$order->carname--}}
            </header>
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">From</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">
                            <?= ($sessData1stStage['pickup'] == NULL) ? ucwords($order->pickup_address) : $sessData1stStage['pickup'] ?>
                            {{-- str_replace('+', ' ', $order->pickup_address) --}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Destination</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">
                            <?= ($sessData1stStage == NULL) ? ucwords($order->dropoff_address) : $sessData1stStage['dropoff'] ?>
                            {{-- str_replace('+', ' ', $order->dropoff_address) --}}
                        </div>
                    </div>
                </div>




            @if($sessData1stStage['waypoint_1'] != NULL)
                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Way Point 1</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">
                            <?= ($sessData1stStage['waypoint_1'] == NULL) ? '' : $sessData1stStage['waypoint_1'] ?>
                        </div>
                    </div>
                </div>
            @endif

            @if($sessData1stStage['waypoint_2'] != NULL)
                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Way Point 2</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">
                            <?= ($sessData1stStage['waypoint_2'] == NULL) ? '' : $sessData1stStage['waypoint_2'] ?>
                        </div>
                    </div>
                </div>
            @endif

            @if($sessData1stStage['waypoint_3'] != NULL)
                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Way Point 3</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">
                            <?= ($sessData1stStage['waypoint_3'] == NULL) ? '' : $sessData1stStage['waypoint_3'] ?>
                        </div>
                    </div>
                </div>
            @endif

            @if($sessData1stStage['waypoint_4'] != NULL)
                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Way Point 4</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">
                            <?= ($sessData1stStage['waypoint_4'] == NULL) ? '' : $sessData1stStage['waypoint_4'] ?>
                        </div>
                    </div>
                </div>
            @endif


            @if($sessData1stStage == NULL)
                @if($order->via_points)
                    @foreach($via_points_seperate as $k=>$v)
                    <div class="row">
                        <div class="col-md-4">
                            <div class="h5 text-bold mb-none mt-lg">Way Point {{ $k+1 }}</div>
                        </div>
                        <div class="col-md-8">
                            <div class="h5 text-bold mb-none mt-lg">{{ str_replace('+', ' ', $v) }}</div>
                        </div>
                    </div>
                    @endforeach
                @endif
            @endif


                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Booking Type</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">@if($returnBooking == 1) Round @else Single @endif Trip</div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Distance</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">
                            {{--{{ ceil($sessDatatotalDistance) }} miles--}}
                       <?= ($sessDatatotalDistance == NULL) ? ceil($order->total_distance) : ceil($sessDatatotalDistance) ?> miles
                        </div>
                    </div>
                </div>


               {{-- @if($sessData2ndStage['meetngreet'])
                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Meet & Greet</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">£ {{$sessData2ndStage['meetngreet']}} (Included)</div>
                    </div>
                </div>
                @endif --}}

                @if($sessMeetngreet == '8')
                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Meet & Greet</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">
                            £ 8<?php //(isset($order->meetngreet)) ? $order->meetngreet : $sessMeetngreet ?> (Included)
                        </div>
                    </div>
                </div>
                @endif

                @if($sessMeetngreet == 'N' AND $order->meetngreet == '8')
                <div class="row">
                    <div class="col-md-4">
                        <div class="h5 text-bold mb-none mt-lg">Meet & Greet</div>
                    </div>
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg">
                            £ 8 (Included)
                        </div>
                    </div>
                </div>
                @endif

                <input type="hidden" name="meetngreet" id="meetngreet" value="<?=($sessMeetngreet)? ($sessMeetngreet=='N')? $order->meetngreet:$sessMeetngreet :$order->meetngreet?>">



<br/>

            </div>

            <header class="panel-heading bg-primary text-center">
                <div class="row">
                    <div class="col-md-6 text-right">
                        <div class="h4 text-bold mb-none mt-lg">Total Fare</div>
                    </div>
                    <div class="col-md-6 text-left">
                        <div class="h4 text-bold mb-none mt-lg" id="price_show">&pound;
                            <?= ($sessDatatotalPrice == NULL) ? number_format($order->total_bill,2) : $sessDatatotalPrice ?>
                        </div>
                        <input type="hidden" name="totalprice" id="totalprice" value="<?= ($sessDatatotalPrice == NULL) ? number_format($order->total_bill,2) : $sessDatatotalPrice ?>">
                    </div>
                    <p class="text-center">(Toll, Congestion Charge Not Included.)</p>
                </div>
            </header>

    <div class="col-md-6 col-lg-6 col-xl-3">
        <section class="panel">
            <header class="panel-heading bg-primary text-center">
                Payment Method
            </header>
            <div class="panel-body text-center">





                <div class="row">
                    <div class="col-md-8">
                        <div class="h5 text-bold mb-none mt-lg text-left">
                            <input class="" type="radio" id="Cash" name="payment_method" value="Cash" checked/>
                            <label style="display: initial; font-weight: normal; margin-bottom: 0;" for="Cash">&nbsp;&nbsp;Cash</label>
                        </div>
                    </div>
                </div>


            </div>
        </section>
    </div>


    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <button type="submit" id="updateNow" class="mb-xs mt-xs mr-xs btn btn-primary">Update Now</button>

        </div>
    </div>
<!-- end: page -->
