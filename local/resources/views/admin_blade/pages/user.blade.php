@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Dashboard | Driver</h2>

</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">

                            <div id="collapse1One" class="accordion-body collapse in">

                                <!-- -->
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-default" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
                                        <thead>
                                        <tr>
                                            <th style="display: none">testing column(dont remove)</th>
                                            <th>Order ID</th>
                                            <th>Passenger Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Driver Name</th>
                                            <th>Order Date</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($approved_orderlist as $approved_order)

                                        <tr class="gradeX">
                                            <td style="display: none">testing column(dont remove)</td>
                                            <td>{!! $approved_order->id !!}</td>
                                            <td>{!! $approved_order->first_name !!} {!! $approved_order->last_name !!}</td>
                                            <td>{!! $approved_order->email !!} </td>
                                            <td>{!! $approved_order->phone_number !!} </td>
                                            <td>
                                                @if($approved_order->driver_id == 0)
                                                Not assigned
                                                @else
                                                {!! $approved_order->Dfirst_name !!} {!! $approved_order->Dlast_name !!}
                                                @endif
                                            </td>
                                            <td>{!! $approved_order->created_at !!}</td>

                                            <td class="actions">

                                                <a href="orderDetails/{!! $approved_order->id !!}">
                                                    <span class="btn btn-xs btn btn-success text-xs">
                                                    <i class="fa fa-list"></i> View Details
                                                    </span>
                                                </a>

                                            </td>
                                        </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- end: page -->
@stop
