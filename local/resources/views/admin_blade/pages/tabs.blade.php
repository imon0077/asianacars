<div class="wizard-tabs">
    <ul class="wizard-steps">
        <li class="@if(Request::segment(1)== 'editOrder') active @endif">
            <a href="javascript:void(0)" id="locations" class="text-center">
                <span class="badge hidden-xs">1</span>
                Locations
            </a>
        </li>
        <li class="@if(Request::segment(1)== 'editOrderCarDeails') active @endif">
            <a href="javascript:void(0)" id="carSelection" class="text-center">
                <span class="badge hidden-xs">2</span>
                Car Selections
            </a>
        </li>
        <li class="@if(Request::segment(1)== 'editOrderCustomerDeails') active @endif">
            <a href="javascript:void(0)" id="customerDeails" class="text-center">
                <span class="badge hidden-xs">3</span>
                Customer Details
            </a>
        </li>
    </ul>
</div>
<!--{!!url()!!}/editOrderCarDeails/{{ $order->id }}-->