@extends('admin_blade.layout.master-nobar')

@section('content')
<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">
        <a href="" class="logo pull-left">
            <img src="{{asset('admin_assets/images/logo.png')}}" height="76" alt="Taxi" />
        </a>

        <div class="panel panel-sign">
            <div class="panel-title-sign mt-xl text-right">
                <h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign In</h2>
            </div>
            <div class="panel-body">
                    
                @include('admin_blade.common.error-message')

                {!! Form::open(['url' => '/auth/login','id'=>'userLogin']) !!}
                    <div class="form-group mb-lg">
                        <label>Email</label>
                        <div class="input-group input-group-icon">
                            <input name="username" type="text" class="form-control input-lg" tabindex="1" autofocus="" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
                        </div>  
                    </div>

                    <div class="form-group mb-lg">
                        <div class="clearfix">
                            <label class="pull-left">Password</label>
                            <a href="recover-password" class="pull-right">Lost Password?</a>
                        </div>
                        <div class="input-group input-group-icon">
                            <input name="password" type="password" class="form-control input-lg" tabindex="2" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="checkbox-custom checkbox-default">
                                <input id="RememberMe" name="rememberme" type="checkbox"/>
                                <label for="RememberMe">Remember Me</label>
                            </div>
                        </div>
                        <div class="col-sm-4 text-right">
                            <button type="submit" class="btn btn-primary hidden-xs" tabindex="3">Sign In</button>
                            <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>
        </div>

        <p class="text-center text-muted mt-md mb-md">ASIANA Cars Ltd &copy; Copyright 2015. All Rights Reserved.</p>
    </div>

</section>



<!-- end: page -->
@stop

@section('javascript')

$("#userLogin").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {

username: {
required: true
},
password: {
required: true,
rangelength: [1, 32]
}
},
messages:{
username: {
required:"Dont forget to input your User Name."
},
password: {
required:"Password field could not remain blank.",
rangelength:"Password length should be between 1 to 32"
}
}
});


@stop