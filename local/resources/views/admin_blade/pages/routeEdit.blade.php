@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Route Edit</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'editRouteDescription', 'method'=>'post', 'class' => 'form-horizontal adminAddorder'] ) !!}

    <div class="form-group field">
        <label class="col-md-3 control-label" for="pickup">Route</label>
        <div class="col-md-7">
            <input class="form-control" id="start" name="route" value="{!! $sr->route !!}" type="text">
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-3 control-label" for="dropoff">Description</label>
        <div class="col-md-7">
            <textarea class="form-control" id="end" name="description">{!! $sr->description !!}</textarea>
        </div>
    </div>


    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <input type="hidden" name="route_id" value="{!! $sr->id !!}">
            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Update</button>
        </div>
    </div>

    <!--  end  -->
    {!! Form::close() !!}
</section>
</div>
</div>
<!-- end: page -->




@stop