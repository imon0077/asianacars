@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Profile</h2>

</header>
@include('flash::message')
@include('admin_blade.common.error-message')
<!-- start: page -->
<div class="row">
    <div class="col-lg-8">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">

                            <div id="collapse1One" class="accordion-body collapse in">
                                <!-- -->
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-default" >
                                        <tbody>
                                        @if(Auth::user()->role == 1)
                                        <tr>
                                            <th>Full name</th>
                                            <td>{!! $profile->full_name !!} </td>
                                        </tr>
                                        @else
                                        <tr>
                                            <th>First name</th>
                                            <td>{!! $profile->first_name !!} </td>
                                        </tr>

                                        <tr>
                                            <th>Last name</th>
                                            <td>{!! $profile->last_name !!} </td>
                                        </tr>

                                        <tr>
                                            <th>Contact name</th>
                                            <td>{!! $profile->contact_name !!} </td>
                                        </tr>
                                        @endif

                                        <tr>
                                            <th>Email</th>
                                            <td>{!! $profile->email !!} </td>
                                        </tr>


                                        </tbody>
                                    </table>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">&nbsp;</label>
                                        <div class="col-md-6">
                                            <a href="editprofile/{!! $profile->id !!}" class="mb-xs mt-xs mr-xs btn btn-success">Edit Profile</a>
                                        </div>
                                    </div>


                                </div>
                                <!-- -->
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- end: page -->
@stop

