@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Edit Pricelist</h2>

</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
    <div class="col-xs-12">
        <section class="panel">

            <div class="panel-body">

                {!! Form::open(['url' => 'edit_pricelist_save', 'class' => 'form-horizontal form-bordered']) !!}

                <div class="form-group">
                    <label class="col-md-3 control-label" for="">Particulars</label>
                    <div class="col-md-6">
                        <input class="form-control" name='id' value="{!! $pricelistupdate->id !!}" id="" type="hidden">
                        <input class="form-control" name='name' value="{!! $pricelistupdate->name !!}" id="" type="text" disabled="disabled">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="">Unit Price</label>
                    <div class="col-md-6">
                        <input class="form-control" name='unit_price' value="{!! $pricelistupdate->unit_price !!}" type="text">
                    </div>
                </div>



                <div class="form-group">
                    <label class="col-md-3 control-label">&nbsp;</label>
                    <div class="col-md-6">
                        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Update Price</button>

                    </div>
                </div>

                {!! Form::close() !!}
        </section>
    </div>
</div>

<!-- end: page -->
@stop