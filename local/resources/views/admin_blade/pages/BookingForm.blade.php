@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Add new booking | Instant Quote</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'AdminBookingResult', 'class' => 'form-horizontal adminAddorder'] ) !!}

    <div class="form-group field">
        <label class="col-md-3 control-label" for="pickup">Pickup Address</label>
        <div class="col-md-7">
            <input class="form-control" id="start" name="pickup" value="<?=($sessData==NULL) ? '':$sessData['pickup'] ?>" type="text">
<!--            <br/>-->
<!--            <div style="text-align: right">-->
                   <!-- Button trigger modal -->
<!--                <span style="font-weight: bold" id="way_point_view">0</span> way point(s) is added-->
<!--                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal">-->
<!--                    Add/Remove waypoints-->
<!--                </button>-->
<!--            </div>-->

            <!-- Start: Waypoints modal -->
            <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add up to 4 waypoints</h4>
                        </div>
                        <div class="modal-body">
                            <div class="container">
                                <div class="row">
                                    <div class="custom-select-box">
                                        <div class="row">
                                            <input type="text" id="waypoint_1" name="waypoint_1" class="col-md-4 waypoint_geocode" />
                                            <a href="javascript:void(0);" class="waypoint_del" id="waypoint_1__del" style="font-size: 15px;">Clear</a>
                                        </div>
                                    </div>
                                    <div class="custom-select-box tec-domain-cat4">
                                        <div class="row">
                                            <input type="text" id="waypoint_2" name="waypoint_2" class="col-md-4 waypoint_geocode" />
                                            <a href="javascript:void(0);" class="waypoint_del" id="waypoint_2__del" style="font-size: 15px;">Clear</a>
                                        </div>
                                    </div>
                                    <div class="custom-select-box tec-domain-cat4">
                                        <div class="row">
                                            <input type="text" id="waypoint_3" name="waypoint_3" class="col-md-4 waypoint_geocode" />
                                            <a href="javascript:void(0);" class="waypoint_del" id="waypoint_3__del" style="font-size: 15px;">Clear</a>
                                        </div>
                                    </div>
                                    <div class="custom-select-box tec-domain-cat4">
                                        <div class="row">
                                            <input type="text" id="waypoint_4" name="waypoint_4" class="col-md-4 waypoint_geocode" />
                                            <a href="javascript:void(0);" class="waypoint_del" id="waypoint_4__del" style="font-size: 15px;">Clear</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="add_waypoints" data-dismiss="modal">Done</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: Waypoints modal -->

        </div>
    </div>


    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 1</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_1" name="waypoint_1" value="<?=($sessData==NULL) ? '':$sessData['waypoint_1'] ?>" type="text"  >
<!--            <input type="text" id="waypoint_4" name="waypoint_4" class="col-md-4 waypoint_geocode" />-->
<!--            <a href="javascript:void(0);" class="waypoint_del" id="waypoint_1__del" style="font-size: 15px;">Clear</a>-->
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 2</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_2" name="waypoint_2" value="<?=($sessData==NULL) ? '':$sessData['waypoint_2'] ?>" type="text"  >
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 3</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_3" name="waypoint_3" value="<?=($sessData==NULL) ? '':$sessData['waypoint_3'] ?>" type="text"  >
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 4</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_4" name="waypoint_4" value="<?=($sessData==NULL) ? '':$sessData['waypoint_4'] ?>" type="text"  >
        </div>
    </div>


    <div class="form-group field">
        <label class="col-md-3 control-label" for="dropoff">Dropoff Address</label>
        <div class="col-md-7">
            <input class="form-control" id="end" name="dropoff" value="<?=($sessData==NULL) ? '':$sessData['dropoff'] ?>" type="text"  >
        </div>
    </div>



    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary adminQuotenow">Quote Now</button>

        </div>
    </div>

    <!--  end  -->
     {!! Form::close() !!}
</section>
</div>
</div>
<!-- end: page -->




@stop