@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Edit Booking | Modify Quote</h2>
</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

    <div class="panel-body">

    @include('admin_blade.pages.editTabs')
    <div id="firstStep">
    {!! Form::open(['url' => 'editOrderSubmit', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'editLocationForm'] ) !!}
    <input type="hidden" name="order_id" value="<?=$order->id?>">
    <input type="hidden" name="car_id" value="<?=$order->car_details?>">
    <input type="hidden" name="tourType" value="<?=$order->return_booking?>">
    <input type="hidden" name="pageId" id="pageId" d value="1">
    <input type="hidden" name="clickedPage" class="clickedPage"  />
    <div class="form-group field">
        <label class="col-md-3 control-label" for="pickup">Pickup Address</label>
        <div class="col-md-7">
            <input class="form-control" id="start" name="pickup" value="<?= ucwords($order->pickup_address) ?>" type="text">
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 1</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_1" name="waypoint_1" value="<?php if($viapointsCount > 0)echo ucwords($via_points_seperate[0]);else echo "";?>" type="text"  >

        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 2</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_2" name="waypoint_2" value="<?php if($viapointsCount > 1)echo ucwords($via_points_seperate[1]);else echo "";?>" type="text"  >

        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 3</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_3" name="waypoint_3" value="<?php if($viapointsCount > 2)echo ucwords($via_points_seperate[2]);else echo "";?>" type="text"  >

        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-6 control-label" for="dropoff">Waypoint 4</label>
        <div class="col-md-4">
            <input class="form-control waypoint_geocode" id="waypoint_4" name="waypoint_4" value="<?php if($viapointsCount > 3)echo ucwords($via_points_seperate[3]);else echo "";?>" type="text"  >

        </div>
    </div>


    <div class="form-group field">
        <label class="col-md-3 control-label" for="dropoff">Dropoff Address</label>
        <div class="col-md-7">
            <input class="form-control" id="end" name="dropoff" value="<?= ucwords($order->dropoff_address) ?>" type="text"  >

        </div>
    </div>

    {!! Form::close() !!}
    </div>
</section>
</div>
</div>
<!-- end: page -->


@stop

