@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Dashboard | Admin</h2>

</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="col-md-6 col-lg-12 col-xl-6">
    <div class="row">
        <div class="col-md-12 col-lg-6 col-xl-6">
            <section class="panel panel-featured-left panel-featured-primary">
                <div class="panel-body">
                    <div class="widget-summary">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-primary">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                        </div>
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title">Orders in last month</h4>
                                <div class="info">
                                    <strong class="amount">{{ $last_month_orders_count }}  </strong>

                                </div>
                            </div>
                            <div class="summary-footer">
<!--                                <a class="text-muted text-uppercase">(view all)</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-6">
            <section class="panel panel-featured-left panel-featured-secondary">
                <div class="panel-body">
                    <div class="widget-summary">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-secondary">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                        </div>
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title">Order completed in current month</h4>
                                <div class="info">
                                    <strong class="amount">{{ $completed_this_month_count }}</strong>
                                </div>
                            </div>
                            <div class="summary-footer">
<!--                                <a class="text-muted text-uppercase">(withdraw)</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-6">
            <section class="panel panel-featured-left panel-featured-tertiary">
                <div class="panel-body">
                    <div class="widget-summary">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-tertiary">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                        </div>
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title">Today's total order received</h4>
                                <div class="info">
                                    <strong class="amount">{{ $todays_recieved_count }}</strong>
                                </div>
                            </div>
                            <div class="summary-footer">
<!--                                <a class="text-muted text-uppercase">(statement)</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-6">
            <section class="panel panel-featured-left panel-featured-quartenary">
                <div class="panel-body">
                    <div class="widget-summary">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-quartenary">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                        </div>
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title">Orders confirmed today</h4>
                                <div class="info">
                                    <strong class="amount">{{ $todays_confirmed_count }}</strong>
                                </div>
                            </div>
                            <div class="summary-footer">
<!--                                <a class="text-muted text-uppercase">(report)</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!--recent booking request-->
<div class="col-lg-12">
    <section class="panel panel-transparent">
        <div class="panel-body">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Recent booking request </h2>
            </header>
            <section class="panel panel-group">
                <div id="accordion">
                    <div class="panel panel-accordion panel-accordion-first">

                        <div id="collapse1One" class="accordion-body collapse in">

                            <!-- -->
                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none" id="datatable-default" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
                                    <thead>
                                    <tr>
                                        <th style="display: none">testing column(dont remove)</th>
                                        <th>Order ID</th>
                                        <th>Passenger Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Order Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orderlist as $OrderLists)

                                    <tr class="gradeX">
                                        <td style="display: none">testing column(dont remove)</td>
                                        <td>{!! $OrderLists->id !!}</td>
                                        <td>
                                            {!! $OrderLists->first_name !!} {!! $OrderLists->last_name !!}
                                        </td>

                                        <td>{!! $OrderLists->email !!} </td>
                                        <td>
                                            {!! $OrderLists->phone_number !!}
                                        </td>
                                        <td>{!!  $OrderLists->created_at !!}</td>


                                        <td class="actions">

                                            <a href="orderDetails/{!! $OrderLists->id !!}">
                                                    <span class="btn btn-xs btn btn-success text-xs">
                                                    <i class="fa fa-list"></i> View Details
                                                    </span>
                                            </a>

                                        </td>
                                    </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- -->

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </section>
</div>
<!-- /recent booking request-->


<!--Approved booking request-->
<!--<div class="col-lg-12">-->
<!--    <section class="panel panel-transparent">-->
<!--        <div class="panel-body">-->
<!--            <header class="panel-heading">-->
<!--                <div class="panel-actions">-->
<!--                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>-->
<!--                </div>-->
<!---->
<!--                <h2 class="panel-title">Approved booking request </h2>-->
<!--            </header>-->
<!--            <section class="panel panel-group">-->
<!--                <div id="accordion">-->
<!--                    <div class="panel panel-accordion panel-accordion-first">-->
<!---->
<!--                        <div id="collapse1One" class="accordion-body collapse in">-->
<!---->
<!--                            <div class="panel-body">-->
<!--                                <table class="table table-bordered table-striped mb-none" id="datatable-default" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">-->
<!--                                    <thead>-->
<!--                                    <tr>-->
<!--                                        <th style="display: none">testing column(dont remove)</th>-->
<!--                                        <th>Order ID</th>-->
<!--                                        <th>Passenger Name</th>-->
<!--                                        <th>Email</th>-->
<!--                                        <th>Mobile</th>-->
<!--                                        <th>Driver Name</th>-->
<!--                                        <th>Order Date</th>-->
<!--                                        <th>Actions</th>-->
<!--                                    </tr>-->
<!--                                    </thead>-->
<!--                                    <tbody>-->
<!--                                    @foreach($approved_orderlist as $approved_order)-->
<!---->
<!--                                    <tr class="gradeX">-->
<!--                                        <td style="display: none">testing column(dont remove)</td>-->
<!--                                        <td>{!! $approved_order->id !!}</td>-->
<!--                                        <td>{!! $approved_order->first_name !!} {!! $approved_order->last_name !!}</td>-->
<!--                                        <td>{!! $approved_order->email !!} </td>-->
<!--                                        <td>{!! $approved_order->phone_number !!} </td>-->
<!--                                        <td>-->
<!--                                            @if($approved_order->driver_id == 0)-->
<!--                                                Not assigned-->
<!--                                            @else-->
<!--                                            {!! $approved_order->Dfirst_name !!} {!! $approved_order->Dlast_name !!}-->
<!--                                            @endif-->
<!--                                        </td>-->
<!--                                        <td>{!! $approved_order->created_at !!}</td>-->
<!---->
<!--                                        <td class="actions">-->
<!---->
<!--                                            <a href="orderDetails/{!! $approved_order->id !!}">-->
<!--                                                    <span class="btn btn-xs btn btn-success text-xs">-->
<!--                                                    <i class="fa fa-list"></i> View Details-->
<!--                                                    </span>-->
<!--                                            </a>-->
<!---->
<!--                                        </td>-->
<!--                                    </tr>-->
<!---->
<!--                                    @endforeach-->
<!--                                    </tbody>-->
<!--                                </table>-->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </section>-->
<!---->
<!--        </div>-->
<!--    </section>-->
<!--</div>-->
<!-- /Approved booking request-->


</div>


<!-- end: page -->
@stop