@extends('admin_blade.layout.master')

@section('content')
<header class="page-header">
    <h2>Set Pricelist</h2>

</header>

@include('flash::message')
@include('admin_blade.common.error-message')

<!-- start: page -->
<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">

                            <div id="collapse1One" class="accordion-body collapse in">

                                <!-- -->
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-default" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Particulars</th>
                                            <th>Unit Price (USD)</th>
                                            <th>Updated Date</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($Pricelist as $Price)
                                        <tr class="gradeX">
                                            <td>{!! $Price->id !!}</td>
                                            <td>{!! $Price->name !!} </td>
                                            <td>{!! $Price->unit_price !!}</td>
                                            <td>{!! $Price->updated_at !!}</td>
                                            <td class="actions">
                                                <a href="edit_pricelist/{!! $Price->id !!}" title="Edit">
                                                    <span class="btn-xs btn btn-success">
                                                    <i class="fa fa-pencil"></i> Edit
                                                    </span>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- end: page -->
@stop