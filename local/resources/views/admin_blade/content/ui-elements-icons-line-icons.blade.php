@extends('porto.layout.master')



@section('content')
<header class="page-header">
    <h2>Line Icons</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>UI Elements</span></li>
            <li><span>Icons</span></li>
            <li><span>Line</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
        </div>

        <h2 class="panel-title">162 icons</h2>
    </header>
    <div class="panel-body">
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-user"></i> licon-user</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-user-female"></i> licon-user-female</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-users"></i> licon-users</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-user-follow"></i> licon-user-follow</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-user-following"></i> licon-user-following</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-user-unfollow"></i> licon-user-unfollow</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-trophy"></i> licon-trophy</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-speedometer"></i> licon-speedometer</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-social-youtube"></i> licon-social-youtube</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-social-twitter"></i> licon-social-twitter</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-social-tumblr"></i> licon-social-tumblr</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-social-facebook"></i> licon-social-facebook</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-social-dropbox"></i> licon-social-dropbox</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-social-dribbble"></i> licon-social-dribbble</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-shield"></i> licon-shield</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-screen-tablet"></i> licon-screen-tablet</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-screen-smartphone"></i> licon-screen-smartphone</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-screen-desktop"></i> licon-screen-desktop</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-plane"></i> licon-plane</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-notebook"></i> licon-notebook</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-moustache"></i> licon-moustache</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-mouse"></i> licon-mouse</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-magnet"></i> licon-magnet</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-magic-wand"></i> licon-magic-wand</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-hourglass"></i> licon-hourglass</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-graduation"></i> licon-graduation</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-ghost"></i> licon-ghost</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-game-controller"></i> licon-game-controller</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-fire"></i> licon-fire</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-eyeglasses"></i> licon-eyeglasses</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-envelope-open"></i> licon-envelope-open</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-envelope-letter"></i> licon-envelope-letter</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-energy"></i> licon-energy</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-emoticon-smile"></i> licon-emoticon-smile</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-disc"></i> licon-disc</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-cursor-move"></i> licon-cursor-move</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-crop"></i> licon-crop</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-credit-card"></i> licon-credit-card</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-chemistry"></i> licon-chemistry</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-bell"></i> licon-bell</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-badge"></i> licon-badge</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-anchor"></i> licon-anchor</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-action-redo"></i> licon-action-redo</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-action-undo"></i> licon-action-undo</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-bag"></i> licon-bag</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-basket"></i> licon-basket</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-basket-loaded"></i> licon-basket-loaded</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-book-open"></i> licon-book-open</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-briefcase"></i> licon-briefcase</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-bubbles"></i> licon-bubbles</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-calculator"></i> licon-calculator</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-call-end"></i> licon-call-end</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-call-in"></i> licon-call-in</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-call-out"></i> licon-call-out</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-compass"></i> licon-compass</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-cup"></i> licon-cup</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-diamond"></i> licon-diamond</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-direction"></i> licon-direction</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-directions"></i> licon-directions</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-docs"></i> licon-docs</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-drawer"></i> licon-drawer</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-drop"></i> licon-drop</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-earphones"></i> licon-earphones</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-earphones-alt"></i> licon-earphones-alt</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-feed"></i> licon-feed</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-film"></i> licon-film</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-folder-alt"></i> licon-folder-alt</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-frame"></i> licon-frame</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-globe"></i> licon-globe</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-globe-alt"></i> licon-globe-alt</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-handbag"></i> licon-handbag</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-layers"></i> licon-layers</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-map"></i> licon-map</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-picture"></i> licon-picture</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-pin"></i> licon-pin</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-playlist"></i> licon-playlist</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-present"></i> licon-present</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-printer"></i> licon-printer</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-puzzle"></i> licon-puzzle</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-speech"></i> licon-speech</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-vector"></i> licon-vector</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-wallet"></i> licon-wallet</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-arrow-down"></i> licon-arrow-down</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-arrow-left"></i> licon-arrow-left</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-arrow-right"></i> licon-arrow-right</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-arrow-up"></i> licon-arrow-up</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-bar-chart"></i> licon-bar-chart</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-bulb"></i> licon-bulb</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-calendar"></i> licon-calendar</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-control-end"></i> licon-control-end</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-control-forward"></i> licon-control-forward</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-control-pause"></i> licon-control-pause</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-control-play"></i> licon-control-play</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-control-rewind"></i> licon-control-rewind</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-control-start"></i> licon-control-start</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-cursor"></i> licon-cursor</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-dislike"></i> licon-dislike</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-equalizer"></i> licon-equalizer</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-graph"></i> licon-graph</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-grid"></i> licon-grid</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-home"></i> licon-home</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-like"></i> licon-like</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-list"></i> licon-list</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-login"></i> licon-login</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-logout"></i> licon-logout</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-loop"></i> licon-loop</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-microphone"></i> licon-microphone</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-music-tone"></i> licon-music-tone</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-music-tone-alt"> licon-music</i>-tone-alt</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-note"></i> licon-note</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-pencil"></i> licon-pencil</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-pie-chart"></i> licon-pie-chart</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-question"></i> licon-question</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-rocket"></i> licon-rocket</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-share"></i> licon-share</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-share-alt"></i> licon-share-alt</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-shuffle"></i> licon-shuffle</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-size-actual"></i> licon-size-actual</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-size-fullscreen"></i> licon-size-fullscreen</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-support"></i> licon-support</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-tag"></i> licon-tag</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-trash"></i> licon-trash</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-umbrella"></i> licon-umbrella</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-wrench"></i> licon-wrench</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-ban"></i> licon-ban</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-bubble"></i> licon-bubble</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-camcorder"></i> licon-camcorder</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-camera"></i> licon-camera</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-check"></i> licon-check</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-clock"></i> licon-clock</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-close"></i> licon-close</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-cloud-download"></i> licon-cloud-download</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-cloud-upload"></i> licon-cloud-upload</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-doc"></i> licon-doc</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-envelope"></i> licon-envelope</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-eye"></i> licon-eye</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-flag"></i> licon-flag</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-folder"></i> licon-folder</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-heart"></i> licon-heart</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-info"></i> licon-info</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-key"></i> licon-key</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-link"></i> licon-link</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-lock"></i> licon-lock</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-lock-open"></i> licon-lock-open</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-magnifier"></i> licon-magnifier</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-magnifier-add"></i> licon-magnifier-add</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-magnifier-remove"></i> licon-magnifier-remove</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-paper-clip"></i> licon-paper-clip</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-paper-plane"></i> licon-paper-plane</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-plus"></i> licon-plus</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-pointer"></i> licon-pointer</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-power"></i> licon-power</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-refresh"></i> licon-refresh</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-reload"></i> licon-reload</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-settings"></i> licon-settings</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-star"></i> licon-star</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-symbol-female"></i> licon-symbol-female</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-symbol-male"></i> licon-symbol-male</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-target"></i> licon-target</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-volume-1"></i> licon-volume-1</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-volume-2"></i> licon-volume-2</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="licon-volume-off"></i> licon-volume-off</div>
    </div>
</section>
<!-- end: page -->
@stop