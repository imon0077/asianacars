@extends('porto.layout.master')



@section('content')
<header class="page-header">
    <h2>Vector Maps</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Maps</span></li>
            <li><span>Vector</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">World</h2>
            </header>
            <div class="panel-body">
                <div id="vector-map-world" style="height: 500px; width: 100%;" data-vector-map data-plugin-options='{ "map": "world_en" }'></div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">North America</h2>
            </header>
            <div class="panel-body">
                <div id="vector-map-north-america" style="height: 500px; width: 100%;" data-vector-map data-plugin-options='{ "map": "north-america_en" }'></div>
            </div>
        </section>
    </div>

    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Europe</h2>
            </header>
            <div class="panel-body">
                <div id="vector-map-europe" style="height: 500px; width: 100%;" data-vector-map data-plugin-options='{ "map": "europe_en" }'></div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Asia</h2>
            </header>
            <div class="panel-body">
                <div id="vector-map-asia" style="height: 500px; width: 100%;" data-vector-map data-plugin-options='{ "map": "asia_en" }'></div>
            </div>
        </section>
    </div>

    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">South America</h2>
            </header>
            <div class="panel-body">
                <div id="vector-map-south-america" style="height: 500px; width: 100%;" data-vector-map data-plugin-options='{ "map": "south-america_en" }'></div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Africa</h2>
            </header>
            <div class="panel-body">
                <div id="vector-map-asia" style="height: 500px; width: 100%;" data-vector-map data-plugin-options='{ "map": "africa_en" }'></div>
            </div>
        </section>
    </div>

    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Oceania</h2>
            </header>
            <div class="panel-body">
                <div id="vector-map-australia" style="height: 500px; width: 100%;" data-vector-map data-plugin-options='{ "map": "australia_en" }'></div>
            </div>
        </section>
    </div>
</div>
<!-- end: page -->
@stop