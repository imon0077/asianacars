<!doctype html>
<html class="fixed">
@include('admin_blade.common.head')
<body>

    @yield('content')

    @include('admin_blade.common.js')

	<script type="text/javascript">
	$(document).ready(function(){
   		@yield('javascript')
   	})
   	</script>
</body>
</html>