<!doctype html>
<html class="fixed">
@include('admin_blade.common.head')
<body>
<section class="body">

<!-- start: header -->
@include('admin_blade.common.header')
<!-- end: header -->

<div class="inner-wrapper">
<!-- start: sidebar -->
    @include('admin_blade.common.sidebar')
<!-- end: sidebar -->

<section role="main" class="content-body">

    @yield('content')

</section>
</div>
    @include('admin_blade.common.right-sidebar')
</section>

@include('admin_blade.common.js')

<script type="text/javascript">
$(document).ready(function(){
		@yield('javascript')
})
</script>

</body>
</html>