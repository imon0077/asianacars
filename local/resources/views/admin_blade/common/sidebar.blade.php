<aside id="sidebar-left" class="sidebar-left">

<div class="sidebar-header">
    <div class="sidebar-title">
        Navigation
    </div>
    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html"
         data-fire-event="sidebar-left-toggle">
        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
    </div>
</div>

<div class="nano">
<div class="nano-content">
<nav id="menu" class="nav-main" role="navigation">
<ul class="nav nav-main">


<li class="@if(Request::segment(1)== 'dashboard') nav-active @endif">
    <a href="{!!url()!!}/dashboard">
        <i class="fa fa-home" aria-hidden="true"></i>
        <span>Dashboard</span>
    </a>

</li>

<li class="@if(Request::segment(1)== 'orderlist') nav-active @endif">
    <a href="{!!url()!!}/orderlist">
        <i class="fa fa-file-text" aria-hidden="true"></i>
        <span>Order list</span>
    </a>
</li>

<li class="@if(Request::segment(1)== 'conf_orderlist') nav-active @endif">
    <a href="{!!url()!!}/conf_orderlist">
        <i class="fa fa-file-text" aria-hidden="true"></i>
        <span>Confirmed Order list</span>
    </a>
</li>

@if(Auth::user()->role == 1)
<li class="@if(Request::segment(1)== 'Createorder') nav-active @endif">
    <a href="{!!url()!!}/Createorder">
        <i class="fa fa-plus" aria-hidden="true"></i>
        <span>Add Order</span>
    </a>
</li>

<li class="@if(Request::segment(1)== 'AddCar') nav-active @endif">
    <a href="{!!url()!!}/AddCar">
        <i class="fa fa-plus" aria-hidden="true"></i>
        <span>Add Car</span>
    </a>
</li>

<li class="@if(Request::segment(1)== 'AddDriver') nav-active @endif">
    <a href="{!!url()!!}/AddDriver">
        <i class="fa fa-plus" aria-hidden="true"></i>
        <span>Add Driver</span>
    </a>
</li>

<li class="@if(Request::segment(1)== 'special_route') nav-active @endif">
    <a href="{!!url()!!}/special_route">
        <i class="fa fa-plus" aria-hidden="true"></i>
        <span>Add Special Route</span>
    </a>
</li>

<li class="@if(Request::segment(1)== 'route_description') nav-active @endif">
    <a href="{!!url()!!}/route_description">
        <i class="fa fa-plus" aria-hidden="true"></i>
        <span>Add Route Description</span>
    </a>
</li>

<li class="@if(Request::segment(1)== 'bookingTime') nav-active @endif">
    <a href="{!!url()!!}/bookingTime/1">
        <i class="fa fa-plus" aria-hidden="true"></i>
        <span>Set Booking Time</span>
    </a>
</li>

<li class="@if(Request::segment(1)== 'carlist') nav-active @endif">
    <a href="{!!url()!!}/carlist">
        <i class="fa fa-users" aria-hidden="true"></i>
        <span>Car List</span>
    </a>
</li>

<li class="@if(Request::segment(1)== 'driverlist') nav-active @endif">
    <a href="{!!url()!!}/driverlist">
        <i class="fa fa-users" aria-hidden="true"></i>
        <span>Driver List</span>
    </a>
</li>

<li class="@if(Request::segment(1)== 'special_route_list') nav-active @endif">
    <a href="{!!url()!!}/special_route_list">
        <i class="fa fa-file-text" aria-hidden="true"></i>
        <span>Special Route List</span>
    </a>
</li>

<li class="@if(Request::segment(1)== 'route_list') nav-active @endif">
    <a href="{!!url()!!}/route_list">
        <i class="fa fa-file-text" aria-hidden="true"></i>
        <span>Route List</span>
    </a>
</li>

<!--<li class="@if(Request::segment(1)== 'userlist') nav-active @endif">-->
<!--    <a href="{!!url()!!}/userlist">-->
<!--        <i class="fa fa-users" aria-hidden="true"></i>-->
<!--        <span>User List</span>-->
<!--    </a>-->
<!--</li>-->
@endif

<li class="@if(Request::segment(1)== 'profile') nav-active @endif">
    <a href="{!!url()!!}/profile">
        <i class="fa fa-user" aria-hidden="true"></i>
        <span>Profile</span>
    </a>
</li>

<li class="@if(Request::segment(1)== 'setting') nav-active @endif">
    <a href="{!!url()!!}/setting">
        <i class="fa fa-gear" aria-hidden="true"></i>
        <span>Setting</span>
    </a>
</li>

<li>
    <a href="{!!url()!!}/auth/logout">
        <i class="fa fa-power-off" aria-hidden="true"></i>
        <span>Logout</span>
    </a>
</li>

</div>

</aside>
