
<header class="header">
    <div class="logo-container">
        <a href="../" class="logo">
            <img src="{{asset('admin_assets/images/logo_dashboard.png')}}" height="50" alt="Taxi" />
        </a>
        <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <!-- start: search & user box -->
    <div class="header-right load_nt">

        <span class="separator"></span>

        <ul class="notifications">

            @if(Auth::user()->hasRole('1'))
            <li>
                <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                    <i class="fa fa-tasks"></i>
                    <span class="badge"></span>
                </a>

                <div class="dropdown-menu notification-menu large">
                    <div class="notification-title">
                        <span class="pull-right label label-default"></span>
                        Notifications
                    </div>

                </div>

            </li>
            @endif
        </ul>

        <span class="separator"></span>

        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="{{asset('admin_assets/images/!logged-user.jpg')}}" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
                </figure>
                <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
            <span class="name">
                {!! Auth::user()->email !!}
            </span>
            <span class="role">
                @if(Auth::user()->role == 1)
                Admin
                @else
                Driver
                @endif
            </span>
                </div>

                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu dropdown-menu-logout">
                <ul class="list-unstyled">
                    <li class="divider"></li>

                    <li>
                        <a role="menuitem" tabindex="-1" href="{{ url('/auth/logout') }}"><i class="fa fa-power-off"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end: search & user box -->
</header>