<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>@if(Auth::guest()) Admin Panel | Taxi Booking @else Dashboard | Taxi Booking @endif</title>
    <meta name="keywords" content="Taxi Booking" />
    <meta name="description" content="Taxi Booking">
    <meta name="author" content="ris10.com">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('admin_assets/vendor/bootstrap/css/bootstrap.css') }}" />

    <link rel="stylesheet" href="{{ asset('admin_assets/vendor/font-awesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('admin_assets/vendor/magnific-popup/magnific-popup.css') }}" />
    <link rel="stylesheet" href="{{ asset('admin_assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

    <!-- Specific Page Vendor CSS -->
    @if (Request::segment(1) == 'AddCar' or Request::segment(1) == 'AddDriver' or Request::segment(1) == 'dashboard'  or Request::segment(1) == 'edit_order')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/select2/select2.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/dropzone/css/basic.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/dropzone/css/dropzone.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/summernote/summernote.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/summernote/summernote-bs3.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/codemirror/lib/codemirror.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/codemirror/theme/monokai.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css") }}' />
    @elseif (Request::segment(1) == 'forms-basic')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css") }}' />
    @elseif (Request::segment(1) == 'forms-code-editor')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/codemirror/lib/codemirror.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/codemirror/theme/monokai.css") }}' />
    @elseif (Request::segment(1) == 'forms-wizard' OR Request::segment(1) == 'ui-elements-modals' OR Request::segment(1) == 'ui-elements-notifications')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/pnotify/pnotify.custom.css") }}' />
    @elseif (Request::segment(1) == 'mailbox-compose' OR Request::segment(1) == 'mailbox-email' OR Request::segment(1) == 'mailbox-folder')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/summernote/summernote.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/summernote/summernote-bs3.css") }}' />
    @elseif (Request::segment(1) == 'contact')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/select2/select2.css") }}' />
    @elseif (Request::segment(1) == 'contact')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/jqvmap/jqvmap.css") }}' />
    @elseif (Request::segment(1) == 'pages-calendar')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/fullcalendar/fullcalendar.css") }}' />
        <link rel="stylesheet"  media="print" href='{{ asset("admin_assets/vendor/fullcalendar/fullcalendar.print.css") }}' />
    @elseif (Request::segment(1) == 'pages-invoice-print')
        <link rel="stylesheet" href='{{ asset("asset/stylesheets/invoice-print.css") }}' />
    @elseif (Request::segment(1) == 'pages-media-gallery')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/isotope/jquery.isotope.css") }}' />
    @elseif (Request::segment(1) == 'tables-advanced' OR Request::segment(1) == 'tables-ajax' OR Request::segment(1) == 'tables-editable')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/select2/select2.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/jquery-datatables-bs3/assets/css/datatables.css") }}' />
    @elseif (Request::segment(1) == 'carlist' or Request::segment(1) == 'orderlist' or Request::segment(1) == 'Doctors'  or Request::segment(1) == 'newDoctors')
    <link rel="stylesheet" href='{{ asset("admin_assets/vendor/select2/select2.css") }}' />
    <link rel="stylesheet" href='{{ asset("admin_assets/vendor/jquery-datatables-bs3/assets/css/datatables.css") }}' />
    @elseif (Request::segment(1) == 'ui-elements-animations-hover')
        <style>
            [class^="hvr-"] {
                /*display: inline-block;*/
                /*vertical-align: middle;*/
                background: #e1e1e1;
                color: #666;
                cursor: pointer;
                line-height: 1.2em;
                margin: .4em;
                padding: 1em;
                text-decoration: none;
                /* Prevent highlight colour when element is tapped */
                -webkit-tap-highlight-color: rgba(0,0,0,0);
            }
        </style>
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/hover-css/hover.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/font-awesome/css/font-awesome.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/magnific-popup/magnific-popup.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/bootstrap-datepicker/css/datepicker3.css") }}' />
    @elseif (Request::segment(1) == 'ui-elements-blocks' OR Request::segment(1) == 'ui-elements-widgets')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/morris/morris.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/owl-carousel/owl.carousel.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/owl-carousel/owl.theme.css") }}' />
    @elseif (Request::segment(1) == 'ui-elements-carousels')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/owl-carousel/owl.carousel.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/owl-carousel/owl.theme.css") }}' />
    @elseif (Request::segment(1) == 'ui-elements-charts')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/morris/morris.css") }}' />
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/chartist/chartist.css") }}' />
    @elseif (Request::segment(1) == 'ui-elements-icons-elusive')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/elusive-icons/css/elusive-webfont.css") }}' />
    @elseif (Request::segment(1) == 'ui-elements-icons-line-icons')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/lineicons/css/lineicons.css") }}' />
    @elseif (Request::segment(1) == 'ui-elements-icons-meteocons')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/meteocons/css/meteocons.css") }}' />
    @elseif (Request::segment(1) == 'ui-elements-portlets' OR Request::segment(1) == 'ui-elements-sliders')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css") }}' />
    @elseif (Request::segment(1) == 'ui-elements-tree-view')
        <link rel="stylesheet" href='{{ asset("admin_assets/vendor/jstree/themes/default/style.css") }}' />
    @else
        <link rel="stylesheet" href="{{ asset('admin_assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />
        <link rel="stylesheet" href="{{ asset('admin_assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
        <link rel="stylesheet" href="{{ asset('admin_assets/vendor/morris/morris.css') }}" />
    @endif

<!-- hover effect   -->
    <style>
        [class^="hvr-"] {
            display: inline-block;
            /*vertical-align: middle;*/
            background: none;
            color: #666;
            cursor: pointer;
            line-height: 1.2em;
            margin: .4em;
            padding: 1em;
            text-decoration: none;
            /* Prevent highlight colour when element is tapped */
            -webkit-tap-highlight-color: rgba(0,0,0,0);
        }
    </style>
    <link rel="stylesheet" href='{{ asset("admin_assets/vendor/hover-css/hover.css") }}' />
    <link rel="stylesheet" href='{{ asset("admin_assets/vendor/font-awesome/css/font-awesome.css") }}' />
    <link rel="stylesheet" href='{{ asset("admin_assets/vendor/magnific-popup/magnific-popup.css") }}' />
    <link rel="stylesheet" href='{{ asset("admin_assets/vendor/bootstrap-datepicker/css/datepicker3.css") }}' />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('admin_assets/stylesheets/theme.css') }}" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('admin_assets/stylesheets/skins/default.css') }}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset('admin_assets/stylesheets/theme-custom.css') }}">

    <!-- Head Libs -->
    <script src="{{ asset('admin_assets/vendor/modernizr/modernizr.js') }}"></script>
</head>

