<script src="{{ asset('assets/js/menu/jquery.min.js') }}"></script>

<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/menu/modernizr.custom.js') }}"></script>

<script src="{{ asset('assets/js/custom.js') }}"></script>
<script src="https://checkout.stripe.com/checkout.js"></script>

{{--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>--}}
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<script src="{{ asset('assets/js/geocomplete/jquery.geocomplete.js') }}"></script>
{{--<script src="{{ asset('assets/js/g-map/map.js') }}"></script>--}}
<script src="{{ asset('assets/js/bootstrap-select.js') }}"></script>


<script src="{{ asset('assets/js/waypoints.min.js') }}"></script>
<script src="{{ asset('assets/js/wow.min.js') }}"></script>


<!-- Vendor -->
<script src="{{ asset('admin_assets/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('admin_assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
<script src="{{ asset('admin_assets/vendor/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('admin_assets/vendor/nanoscroller/nanoscroller.js') }}"></script>
<script src="{{ asset('admin_assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('admin_assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ asset('admin_assets/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>
<script src="{{ asset('admin_assets/vendor/intercooler/intercooler-0.4.8.js') }}"></script>



<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    })
</script>

<!-- Specific Page Vendor -->
<?php if(Request::segment(1) == 'extra-ajax-made-easy'){?>
    <script src="{{ asset('admin_assets/vendor/jquery-mockjax/jquery.mockjax.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'AddCar' or Request::segment(1) == 'AddDriver' or Request::segment(1) == 'CreateDoctors' or Request::segment(1) == 'dashboard' or Request::segment(1) == 'edit_order'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-maskedinput/jquery.maskedinput.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/fuelux/js/spinner.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/lib/codemirror.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/addon/selection/active-line.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/addon/edit/matchbrackets.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/mode/javascript/javascript.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/mode/xml/xml.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/mode/css/css.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/summernote/summernote.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/ios7-switch/ios7-switch.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-autosize/jquery.autosize.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'forms-basic'){
?>
    <script src="{{ asset('admin_assets/vendor/jquery-autosize/jquery.autosize.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'patientlist' or Request::segment(1) == 'orderlist'or Request::segment(1) == 'Doctors'  or Request::segment(1) == 'newDoctors' or Request::segment(1) == 'dashboard'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
    <script src="{{ asset('admin_assets/javascripts/tables/examples.datatables.default.js') }}"></script>
<!--    <script src="{{ asset('assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>-->
<!--    <script src="{{ asset('assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>-->
    <script src="{{ asset('admin_assets/javascripts/theme.js') }}"></script>

    <script src="{{ asset('admin_assets/javascripts/tables/examples.datatables.editable.js') }}"></script>

<?php
}
elseif(Request::segment(1) == 'forms-code-editor'){
    ?>
    <script src="{{ asset('admin_assets/vendor/codemirror/lib/codemirror.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/addon/selection/active-line.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/addon/edit/matchbrackets.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/mode/javascript/javascript.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/mode/xml/xml.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/codemirror/mode/css/css.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'forms-validation'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'forms-wizard'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/pnotify/pnotify.custom.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'mailbox-compose' OR Request::segment(1) == 'mailbox-email' OR Request::segment(1) == 'mailbox-folder'){
    ?>
    <script src="{{ asset('admin_assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/summernote/summernote.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'contact'){
    ?>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="{{ asset('admin_assets/vendor/gmaps/gmaps.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'contacts'){
    ?>
    <script src="{{ asset('admin_assets/vendor/select2/select2.js') }}"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="{{ asset('admin_assets/javascripts/maps/snazzy.themes.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'maps-vector'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jqvmap/jquery.vmap.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/data/jquery.vmap.sampledata.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-calendar'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/fullcalendar/fullcalendar.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'pages-lock-screen-example'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-idletimer/dist/idle-timer.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-session-timeout'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-idletimer/dist/idle-timer.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-media-gallery'){
    ?>
    <script src="{{ asset('admin_assets/vendor/isotope/jquery.isotope.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-timeline'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="{{ asset('admin_assets/vendor/gmaps/gmaps.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-user-profile'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-autosize/jquery.autosize.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'tables-advanced'){
    ?>
    <script src="{{ asset('admin_assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'tables-editable' OR Request::segment(1) == 'tables-ajax' ){
    ?>
    <script src="{{ asset('admin_assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'ui-elements-animations-appear' OR Request::segment(1) == 'ui-elements-animations-hover'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-blocks' OR Request::segment(1) == 'ui-elements-widgets'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/raphael/raphael.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/morris/morris.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/owl-carousel/owl.carousel.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'ui-elements-carousels'){
    ?>
    <script src="{{ asset('admin_assets/vendor/owl-carousel/owl.carousel.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-charts'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/raphael/raphael.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/morris/morris.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/gauge/gauge.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/snap-svg/snap.svg.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/liquid-meter/liquid.meter.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/chartist/chartist.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-lightbox'){
    ?>
    <script src="{{ asset('admin_assets/vendor/pnotify/pnotify.custom.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-loading-progress'){
    ?>
    <script src="{{ asset('admin_assets/vendor/nprogress/nprogress.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-modals'){
    ?>
    <script src="{{ asset('admin_assets/vendor/pnotify/pnotify.custom.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-nestable'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-nestable/jquery.nestable.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-notifications'){
    ?>
    <script src="{{ asset('admin_assets/vendor/pnotify/pnotify.custom.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-portlets'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/store-js/store.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-sliders'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-tree-view'){
    ?>
    <script src="{{ asset('admin_assets/vendor/jstree/jstree.js') }}"></script>
<?php
}else{
    ?>
    <script src="{{ asset('admin_assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/raphael/raphael.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/morris/morris.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/gauge/gauge.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/snap-svg/snap.svg.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/liquid-meter/liquid.meter.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/jquery.vmap.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/data/jquery.vmap.sampledata.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js') }}"></script>
    <script src="{{ asset('admin_assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js') }}"></script>
<?php } ?>



<!-- Theme Base, Components and Settings -->
<script src="{{ asset('admin_assets/javascripts/theme.js') }}"></script>

<!-- Theme Custom -->
<script src="{{ asset('admin_assets/javascripts/theme.custom.js') }}"></script>

<!-- Theme Initialization Files -->
<script src="{{ asset('admin_assets/javascripts/theme.init.js') }}"></script>

<!--
        Examples - Actually there's just ajax mockups in the file below
        All mockups are for demo purposes only. You don't need to include this in your application.
    -->

<?php if(Request::segment(1) == 'extra-ajax-made-easy'){?>
    <script src="{{ asset('admin_assets/javascripts/extra/examples.ajax.made.easy.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'forms-validation'){
?>
    <script src="{{ asset('admin_assets/javascripts/forms/examples.validation.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'forms-wizard'){
?>
    <script src="{{ asset('admin_assets/javascripts/forms/examples.wizard.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'create-patient'){
?>
    <script src="{{ asset('admin_assets/javascripts/forms/examples.advanced.form.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'contact'){
?>
    <script src="{{ asset('admin_assets/javascripts/maps/examples.gmap.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'contact'){
?>
    <script src="{{ asset('admin_assets/javascripts/maps/examples.map.builder.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'maps-vector'){
?>
    <script src="{{ asset('admin_assets/javascripts/maps/examples.vector.maps.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-calendar'){
?>
    <script src="{{ asset('admin_assets/javascripts/pages/examples.calendar.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-lock-screen-example'){
?>
    <script src="{{ asset('admin_assets/javascripts/pages/examples.lockscreen.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-session-timeout'){
?>
    <script src="{{ asset('admin_assets/javascripts/pages/examples.lockscreen.js') }}"></script>
    <script src="{{ asset('admin_assets/javascripts/pages/examples.session.timeout.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-media-gallery'){
?>
    <script src="{{ asset('admin_assets/javascripts/pages/examples.mediagallery.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-timeline'){
?>
    <script src="{{ asset('admin_assets/javascripts/pages/examples.timeline.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'tables-advanced'){
?>
    <script src="{{ asset('admin_assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('admin_assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('admin_assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'tables-editable'){
?>
    <script src="{{ asset('admin_assets/javascripts/tables/examples.datatables.editable.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'tables-ajax'){
?>
    <script src="{{ asset('admin_assets/javascripts/tables/examples.datatables.ajax.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-blocks' OR Request::segment(1) == 'ui-elements-widgets'){
?>
    <script src="{{ asset('admin_assets/javascripts/ui-elements/examples.widgets.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-charts'){
?>
    <style>
        #ChartistCSSAnimation .ct-series.ct-series-a .ct-line {
            fill: none;
            stroke-width: 4px;
            stroke-dasharray: 5px;
            -webkit-animation: dashoffset 1s linear infinite;
            -moz-animation: dashoffset 1s linear infinite;
            animation: dashoffset 1s linear infinite;
        }

        #ChartistCSSAnimation .ct-series.ct-series-b .ct-point {
            -webkit-animation: bouncing-stroke 0.5s ease infinite;
            -moz-animation: bouncing-stroke 0.5s ease infinite;
            animation: bouncing-stroke 0.5s ease infinite;
        }

        #ChartistCSSAnimation .ct-series.ct-series-b .ct-line {
            fill: none;
            stroke-width: 3px;
        }

        #ChartistCSSAnimation .ct-series.ct-series-c .ct-point {
            -webkit-animation: exploding-stroke 1s ease-out infinite;
            -moz-animation: exploding-stroke 1s ease-out infinite;
            animation: exploding-stroke 1s ease-out infinite;
        }

        #ChartistCSSAnimation .ct-series.ct-series-c .ct-line {
            fill: none;
            stroke-width: 2px;
            stroke-dasharray: 40px 3px;
        }

        @-webkit-keyframes dashoffset {
            0% {
                stroke-dashoffset: 0px;
            }

            100% {
                stroke-dashoffset: -20px;
            };
        }

        @-moz-keyframes dashoffset {
            0% {
                stroke-dashoffset: 0px;
            }

            100% {
                stroke-dashoffset: -20px;
            };
        }

        @keyframes dashoffset {
            0% {
                stroke-dashoffset: 0px;
            }

            100% {
                stroke-dashoffset: -20px;
            };
        }

        @-webkit-keyframes bouncing-stroke {
            0% {
                stroke-width: 5px;
            }

            50% {
                stroke-width: 10px;
            }

            100% {
                stroke-width: 5px;
            };
        }

        @-moz-keyframes bouncing-stroke {
            0% {
                stroke-width: 5px;
            }

            50% {
                stroke-width: 10px;
            }

            100% {
                stroke-width: 5px;
            };
        }

        @keyframes bouncing-stroke {
            0% {
                stroke-width: 5px;
            }

            50% {
                stroke-width: 10px;
            }

            100% {
                stroke-width: 5px;
            };
        }

        @-webkit-keyframes exploding-stroke {
            0% {
                stroke-width: 2px;
                opacity: 1;
            }

            100% {
                stroke-width: 20px;
                opacity: 0;
            };
        }

        @-moz-keyframes exploding-stroke {
            0% {
                stroke-width: 2px;
                opacity: 1;
            }

            100% {
                stroke-width: 20px;
                opacity: 0;
            };
        }

        @keyframes exploding-stroke {
            0% {
                stroke-width: 2px;
                opacity: 1;
            }

            100% {
                stroke-width: 20px;
                opacity: 0;
            };
        }
    </style>
    <script src="{{ asset('admin_assets/javascripts/ui-elements/examples.charts.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-grid-system'){
?>
    <style>
        .show-grid-block {
            background-color: #EEE;
            border: 1px solid #FFF;
            display: block;
            line-height: 40px;
            min-height: 40px;
            text-align: center;
        }

        html.dark .show-grid-block {
            background-color: #282d36;
            border-color: #282d36;
        }
    </style>
<?php
}elseif(Request::segment(1) == 'ui-elements-icons-elusive' OR Request::segment(1) == 'ui-elements-icons-font-awesome' OR Request::segment(1) == 'ui-elements-icons-glyphicons' OR Request::segment(1) == 'ui-elements-icons-line-icons' OR Request::segment(1) == 'ui-elements-icons-meteocons'){
?>
    <style>
        .icons-demo-page .demo-icon-hover {
            cursor: pointer;
            font-size: 15px;
        }

        .icons-demo-page .demo-icon-hover:hover {
            color: #111;
        }

        .icons-demo-page .demo-icon-hover i {
            min-width: 40px;
            padding-right: 15px;
        }

        html.dark .icons-demo-page .demo-icon-hover:hover {
            color: #FEFEFE;
        }
    </style>
<?php
}elseif(Request::segment(1) == 'ui-elements-lightbox'){
?>
    <script src="{{ asset('admin_assets/javascripts/ui-elements/examples.lightbox.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-loading-overlay'){
?>
    <script src="{{ asset('admin_assets/javascripts/ui-elements/examples.loading.overlay.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-loading-progress'){
?>
    <script src="{{ asset('admin_assets/javascripts/ui-elements/examples.loading.progress.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-modals'){
?>
    <script src="{{ asset('admin_assets/javascripts/ui-elements/examples.modals.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-nestable'){
?>
    <script src="{{ asset('admin_assets/javascripts/ui-elements/examples.nestable.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-notifications'){
?>
    <script src="{{ asset('admin_assets/javascripts/ui-elements/examples.notifications.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-portlets'){
?>
    <script src="{{ asset('admin_assets/javascripts/ui-elements/examples.portlets.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-tree-view'){
?>
    <script src="{{ asset('admin_assets/javascripts/ui-elements/examples.treeview.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-sliders'){
?>
    <script>
        (function() {
            $('#listenSlider').change(function() {
                $('.output b').text( this.value );
            });

            $('#listenSlider2').change(function() {
                var min = parseInt(this.value.split('/')[0], 10);
                var max = parseInt(this.value.split('/')[1], 10);

                $('.output2 b.min').text( min );
                $('.output2 b.max').text( max );
            });
        })();
    </script>
<?php
}else{
?>
    <!--script src="{{ asset('admin_assets/javascripts/dashboard/examples.dashboard.js') }}"></script-->
<?php } ?>
@if(Request::segment(1)=='editprofile' OR Request::segment(1)=='login' OR Request::segment(1)=='orderDetails' OR Request::segment(1)=='edit_patient' OR Request::segment(1)=='editdoctor' OR Request::segment(1)=='' OR Request::segment(1)=='signup')
<script src="{{ asset('admin_assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
@endif
@if(Request::segment(1)=='dashboard')
<script src="{{ asset('admin_assets/vendor/pnotify/pnotify.custom.js') }}"></script>
@endif


<script>
    jQuery(document).ready(function($){

        $(".adminQuotenow").click(function() {
            $("#error").hide();

            //required:
            var pickup = $("input#start").val();
            var dropoff = $("input#end").val();

            if (pickup == '' || pickup == null) {
                $("input#start").focus();
                $("input#start").addClass('has-error');
                alert("Please enter the required fields!");
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if (dropoff == '' || dropoff == null) {
                $("input#end").focus();
                $("input#end").addClass('has-error');
                alert("Please enter the required fields!");
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if (dropoff == pickup) {
                $("input#end").focus();
                $("input#end").addClass('has-error');
                alert("Pickup & Dropoff address never be same");
                $.bootstrapGrowl('Pickup & Dropoff address never be same', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else{
                $(".adminAddorder").submit();
                return true;
            }

        });

        $('.resultsave').click(function(){
            var thisForm = $(this).closest('form');
            //var tripType = $("input[type='radio'][name='totalprice']:checked").val();
            var tripType = thisForm.find("input[type='radio'][name='totalprice']:checked").length;
            console.log(tripType);
            if(tripType == '0' || tripType == '' || tripType == null || tripType == 'undefined'){
                alert("Please select a trip!");
                $.bootstrapGrowl('Please select a trip!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
            }
            else{
                thisForm.submit();
            }
        });


        //js validation end

//=== stripe
        var handler = StripeCheckout.configure({
            key: $('#stripePublishableKey').val(), //'pk_test_5ELZbytCg4I78QOqQPZdcnPj',
            image: $("#baseURL").val()+'/assets/images/logo.png',
            locale: 'auto',
            token: function(token) {
                //alert(token.id)
                // Use the token to create the charge with a server-side script.
                // You can access the token ID with `token.id`
                $('#card_payment_status').html("Please wait while processing....");
                $('#payment_with_stripe').modal();
                $.ajax({
                    type     : "POST",
                    // url      : $(this).attr('action') + '/store',
                    url      : $("#baseURL").val()+"/stripeCharge",
                    data     : "tokenID="+token.id+"&tokenEmail="+token.email+'&description='+siteName+' ### '+description+'&currency=GBP&amount='+totalPrice,
                    cache    : false,

                    success  : function(data) {

                        //console.log(data);
                        //=== check for any error
                        if(data == 'error_card' || data == 'error_info'){
                            if(data == 'error_card') $('#card_payment_status').html("<b style='color: red'>Unfortunately your card has been declined.</b>");
                            if(data == 'error_info') $('#card_payment_status').html("<b style='color: red'>Unfortunately your card has been declined or information is incorrect..</b>");
                        }
                        else {
                            //=== show success payment method
                            $('#card_payment_status').html("<b style='color:green'>Payment done, please wait though...</b>");

                            //===get stripe data
                            var payData = data.split('^^__^^');
                            var stripeCustomerID = payData[0];
                            var stripeChargeID = payData[1];

                            //===set stripe data as hidden form input
                            $('#stripeCustomerID').val(stripeCustomerID);
                            $('#stripeChargeID').val(stripeChargeID);

                            //=== submit form
                            $("#editCustomerForm").submit();

                        }
                    }
                })
            }
        });

        // Close Checkout on page navigation
        $(window).on('popstate', function() {
            handler.close();
        });


        <!--   final booking edit     -->
        var totalPrice;
        var description;
        var siteName;

        $("#updateNow").on('click',function(e) {
            //$(".adminBookingsubmit").click(function() {

            $('#updateFlag').val('1');
            var updateFlag = $("input#updateFlag").val();
            console.log(updateFlag);

            //required:
            var firstname = $("input#firstname").val();
            var lastname = $("input#lastname").val();
            var email = $("input#email").val();
            var mobile = $("input#mobile").val();
            var reTourdate = $("input#re-tourdate").val();
            var redate = moment(reTourdate, 'DD/MM/YYYY').format('MM/DD/YYYY');
            var reTourtime = $("select#re-tourtime").val();
            var reTourmin = $("select#re-tourmin").val();
            var tourType = $("input#tourType").val();
            //console.log(firstname);
            var no_of_person = $("input#no_of_person").val();
            var no_of_suitcase = $("input#no_of_suitcase").val();
            var no_of_child = $("input#no_of_child").val();

            var pickup_address1 = $("input#pickup_address1").val();
            var pickup_postcode = $("input#pickup_postcode").val();
            var dropoff_address1 = $("input#dropoff_address1").val();
            var dropoff_postcode = $("input#dropoff_postcode").val();

            var tourdate = $("input#tourdate").val();
            var date = moment(tourdate, 'DD/MM/YYYY').format('MM/DD/YYYY');
            var tourtime = $("select#tourtime").val();
            var tourmin = $("select#tourmin").val();
            var startTime = date+' '+tourtime+':'+tourmin;
            var returnDateTime = redate+' '+reTourtime+':'+reTourmin;

            var thisForm = $(this).closest('form');
            var payment_method = $("input[type='radio'][name='payment_method']:checked").val();

            var currentTime = new Date();
            var orderTime = moment(currentTime).add(8,'h').format('MM/DD/YYYY HH:mm');

            console.log(payment_method);

            if(payment_method == '' || payment_method == null || payment_method == 'undefined'){
                alert("Payment method required");
            }
            else{
                // Open Checkout with further options
                totalPrice = $('#totalprice').val() * 100;
                description = 'Trip for '+$('#firstname').val() +' '+ $('#lastname').val();
                siteName = $('#siteName').val();

                handler.open({
                    name: siteName,
                    description: description,
                    currency: "gbp",
                    amount: totalPrice
                });
                e.preventDefault();
                return true;
            }

        });
        <!--   /final booking edit   -->
    });
</script>


@if(Request::segment(1) == 'AdminBookingFinal')
<script>
    var specialDate = ['25/12','26/12','31/12','01/01'];
    jQuery(document).ready(function($){


        //=== stripe
        var handler = StripeCheckout.configure({
            key: $('#stripePublishableKey').val(), //'pk_test_5ELZbytCg4I78QOqQPZdcnPj',
            image: $("#baseURL").val()+'/assets/images/logo.png',
            locale: 'auto',
            token: function(token) {
                //alert(token.id)
                // Use the token to create the charge with a server-side script.
                // You can access the token ID with `token.id`
                $('#card_payment_status').html("Please wait while processing....");
                $('#payment_with_stripe').modal();
                $.ajax({
                    type     : "POST",
                    // url      : $(this).attr('action') + '/store',
                    url      : $("#baseURL").val()+"/stripeCharge",
                    data     : "tokenID="+token.id+"&tokenEmail="+token.email+'&description='+siteName+' ### '+description+'&currency=GBP&amount='+totalPrice,
                    cache    : false,

                    success  : function(data) {

                        //$('#payment_with_stripe').modal();
                        //$("#booking_submit").submit();

                        //console.log(data);
                        //=== check for any error
                        if(data == 'error_card' || data == 'error_info'){
                            if(data == 'error_card') $('#card_payment_status').html("<b style='color: red'>Unfortunately your card has been declined.</b>");
                            if(data == 'error_info') $('#card_payment_status').html("<b style='color: red'>Unfortunately your card has been declined or information is incorrect..</b>");
                        }
                        else {
                            //=== show success payment method
                            $('#card_payment_status').html("<b style='color:green'>Payment done, please wait though...</b>");

                            //===get stripe data
                            var payData = data.split('^^__^^');
                            var stripeCustomerID = payData[0];
                            var stripeChargeID = payData[1];

                            //===set stripe data as hidden form input
                            $('#stripeCustomerID').val(stripeCustomerID);
                            $('#stripeChargeID').val(stripeChargeID);

                            //=== submit form
                            $("#booking_submit").submit();

                        }
                    }
                })
            }
        });

//        $('#booking_submit_save').on('click', function(e) {
//            // Open Checkout with further options
//            handler.open({
//                name: 'Asiana Cars Ltd.',
//                description: '2 widgets',
//                currency: "gbp",
//                amount: 2000
//            });
//            e.preventDefault();
//        });

        // Close Checkout on page navigation
        $(window).on('popstate', function() {
            handler.close();
        });


        <!--   final booking     -->
        var totalPrice;
        var description;
        var siteName;

        $(".adminBookingsubmit").on('click',function(e) {
        //$(".adminBookingsubmit").click(function() {

            //required:
            var firstname = $("input#firstname").val();
            var lastname = $("input#lastname").val();
            var email = $("input#email").val();
            var mobile = $("input#mobile").val();
            var reTourdate = $("input#re-tourdate").val();
            var redate = moment(reTourdate, 'DD/MM/YYYY').format('MM/DD/YYYY');
            var reTourtime = $("select#re-tourtime").val();
            var reTourmin = $("select#re-tourmin").val();
            var tourType = $("input#tourType").val();
            //console.log(firstname);
            var no_of_person = $("input#no_of_person").val();
            var no_of_suitcase = $("input#no_of_suitcase").val();
            var no_of_child = $("input#no_of_child").val();

            var pickup_address1 = $("input#pickup_address1").val();
            var pickup_postcode = $("input#pickup_postcode").val();
            var dropoff_address1 = $("input#dropoff_address1").val();
            var dropoff_postcode = $("input#dropoff_postcode").val();

            var tourdate = $("input#tourdate").val();
            var date = moment(tourdate, 'DD/MM/YYYY').format('MM/DD/YYYY');
            var tourtime = $("select#tourtime").val();
            var tourmin = $("select#tourmin").val();
            var startTime = date+' '+tourtime+':'+tourmin;
            var returnDateTime = redate+' '+reTourtime+':'+reTourmin;

            var thisForm = $(this).closest('form');
            var payment_method = $("input[type='radio'][name='payment_method']:checked").val();

            var currentTime = new Date();
            var orderTime = moment(currentTime).add(8,'h').format('MM/DD/YYYY HH:mm');

            console.log(payment_method);


            if (firstname == '' || firstname == null) {
                $("input#firstname").focus();
                $("input#firstname").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if (lastname == '' || lastname == null) {
                $("input#lastname").focus();
                $("input#lastname").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if (email == '' || email == null) {
                $("input#email").focus();
                $("input#email").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if (mobile == '' || mobile == null) {
                $("input#mobile").focus();
                $("input#mobile").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if (pickup_address1 == '' || pickup_address1 == null) {
                $("input#pickup_address1").focus();
                $("input#pickup_address1").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            /*
             else if (pickup_postcode == '' || pickup_postcode == null) {
             $("input#pickup_postcode").focus();
             $("input#pickup_postcode").addClass('has-error');
             $.bootstrapGrowl('Please enter the required fields!', {
             type: 'danger',
             width: 'auto', align: 'center', allow_dismiss: true
             });
             return false;
             }
             */
            else if (dropoff_address1 == '' || dropoff_address1 == null) {
                $("input#dropoff_address1").focus();
                $("input#dropoff_address1").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            /*
             else if (dropoff_postcode == '' || dropoff_postcode == null) {
             $("input#dropoff_postcode").focus();
             $("input#dropoff_postcode").addClass('has-error');
             $.bootstrapGrowl('Please enter the required fields!', {
             type: 'danger',
             width: 'auto', align: 'center', allow_dismiss: true
             });
             return false;
             }
             */
            else if (tourdate == '' || tourdate == null || tourtime == '' || tourtime == null) {
                $("input#tourdate").focus();
                $("input#tourdate").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields datetime!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if (tourType == 'round' && (reTourdate == '' || reTourdate == null || reTourtime == '' || reTourtime == null)) {
                $("input#re-tourdate").focus();
                $("input#re-tourdate").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if(moment(orderTime).isAfter(startTime)){
                $("input#tourdate").addClass('has-error');
                $("input#tourdate").focus();
                alert("Please Allow 8 Hour(s) For Online Booking, Please Call For Urgent Booking.");
                $.bootstrapGrowl('Please Allow 8 Hour(s) For Online Booking, Please Call For Urgent Booking.', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if(tourType == 'round' && moment(startTime).isAfter(returnDateTime)){
                $("input#re-tourdate").addClass('has-error');
                $("input#re-tourdate").focus();
                alert("Return schedule should be after Start schedule.");
                $.bootstrapGrowl('Return schedule should be after Start schedule.', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if (no_of_person == '' || no_of_person == null) {
                $("input#no_of_person").focus();
                $("input#no_of_person").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if (no_of_suitcase == '' || no_of_suitcase == null) {
                $("input#no_of_suitcase").focus();
                $("input#no_of_suitcase").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if (no_of_child == '' || no_of_child == null) {
                $("input#no_of_child").focus();
                $("input#no_of_child").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true
                });
                return false;
            }
            else if(payment_method == '' || payment_method == null || payment_method == 'undefined'){
                alert("Payment method required");
            }
            else{
                // Open Checkout with further options
                totalPrice = $('#totalprice').val() * 100;
                description = 'Trip for '+$('#firstname').val() +' '+ $('#lastname').val();
                siteName = $('#siteName').val();
                if(payment_method == 'card' || payment_method == 'secured_card_payment'){
                    handler.open({
                        name: siteName,
                        description: description,
                        currency: "gbp",
                        amount: totalPrice
                    });
                    e.preventDefault();
                    //$('#payment_with_stripe').modal();
                    //$("#booking_submit").submit();
                    return true;
                }
                else{
                        //=== submit form
                        $("#booking_submit").submit();
                }
            }

        });
        <!--   /final booking   -->


        function priceChange(selectedDate){
            var totalPriceSpecialDate = $('#totalPriceSpecialDate').val();
            var totalpriceregular = $('#totalpriceregular').val();

            if($.inArray(selectedDate, specialDate) < 0){
                $('#totalprice').val(totalpriceregular);
                var formattedPrice = parseFloat(totalpriceregular).toFixed(2);
                $('#price_show').html(formattedPrice);
                $('.specialcharge').hide();
                $('#special_date').val(0);
            }
            else{
                $('#totalprice').val(totalPriceSpecialDate);
                var formattedRegularPrice = parseFloat(totalpriceregular).toFixed(2);
                var formattedPrice = parseFloat(totalPriceSpecialDate).toFixed(2);
                var additionalPrice = formattedPrice - formattedRegularPrice;
                $('#price_show').html(formattedPrice);
                $('#additionalPrice').html(additionalPrice.toFixed(2));
                $('#specialPrice').modal();
                $('.specialcharge').show();
                $('#special_date').val(1);
            }
        }

        $('#tourdate').change(function(){
            var tourdate = $(this).val();
            $('#re-tourdate').val('');
            var selectedDate = tourdate.substr(0, 5);
            priceChange(selectedDate);
        });

        $('#re-tourdate').change(function(){
            var tourdate = $('#tourdate').val();
            var re_tourdate = $(this).val();
            var selectedDate = tourdate.substr(0, 5);
            var selectedReDate = re_tourdate.substr(0, 5);
            if($.inArray(selectedDate, specialDate) < 0){
                priceChange(selectedReDate);
            }
        });

        var tourdate = $('#tourdate').val().substr(0, 5);
        var re_tourdate = $('#re-tourdate').val().substr(0, 5);
        if($.inArray(tourdate, specialDate) >= 0){
            priceChange(tourdate);
        }else if($.inArray(re_tourdate, specialDate) >= 0){
            priceChange(re_tourdate);
        }
    });
</script>
@endif
@if(Request::segment(1) == 'editOrderCustomerDeails')
<script>
    var specialDate = ['25/12','26/12','31/12','01/01'];
    jQuery(document).ready(function($){
        function priceChange(selectedDate){
            var totalPriceSpecialDate = $('#totalPriceSpecialDate').val();
            var totalpriceregular = $('#totalpriceregular').val();

            if($.inArray(selectedDate, specialDate) < 0){
                $('#totalprice').val(totalpriceregular);
                var formattedPrice = parseFloat(totalpriceregular).toFixed(2);
                $('#price_show').html(formattedPrice);
                $('.specialcharge').hide();
                $('#special_date').val(0);
            }
            else{
                $('#totalprice').val(totalPriceSpecialDate);
                var formattedRegularPrice = parseFloat(totalpriceregular).toFixed(2);
                var formattedPrice = parseFloat(totalPriceSpecialDate).toFixed(2);
                var additionalPrice = formattedPrice - formattedRegularPrice;
                $('#price_show').html(formattedPrice);
                $('#additionalPrice').html(additionalPrice.toFixed(2));
                $('#specialPrice').modal();
                $('.specialcharge').show();
                $('#special_date').val(1);
            }
        }

        $('#tourdate').change(function(){
            var tourdate = $(this).val();
            $('#re-tourdate').val('');
            var selectedDate = tourdate.substr(0, 5);
            priceChange(selectedDate);
        });

        $('#re-tourdate').change(function(){
            var tourdate = $('#tourdate').val();
            var re_tourdate = $(this).val();
            var selectedDate = tourdate.substr(0, 5);
            var selectedReDate = re_tourdate.substr(0, 5);
            if($.inArray(selectedDate, specialDate) < 0){
                priceChange(selectedReDate);
            }
        });

        var tourdate = $('#tourdate').val().substr(0, 5);
        var re_tourdate = $('#re-tourdate').val().substr(0, 5);
        if($.inArray(tourdate, specialDate) >= 0){
            priceChange(tourdate);
        }else if($.inArray(re_tourdate, specialDate) >= 0){
            priceChange(re_tourdate);
        }





    });
</script>
@endif