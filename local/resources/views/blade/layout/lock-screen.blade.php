<!doctype html>
<html class="fixed">
@include('vascorx.common.head')
<body>
    @yield('content')

@include('vascorx.common.js')
</body>
</html>