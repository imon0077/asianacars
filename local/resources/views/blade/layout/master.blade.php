<!doctype html>
<html lang="en">
@include('blade.common.head')
<body>
<?php include(app_path().'/includes/analyticstracking.php'); ?>
<!-- start: header -->
@include('blade.common.header')
<!-- end: header -->

<!-- Booking now form wrapper html start -->

    @yield('content')

<!-- ================ footer html start ================ -->
<div class="footer custom-footer">
    <div class="">
        <div class="col-sm-4">
            <div class="link-journey">
            <a href="{!!url()!!}/index/Gatwick&ndash;Heathrow">Gatwick &ndash; Heathrow,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Stanstead">Gatwick &ndash; Stanstead,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Birmingham">Gatwick &ndash; Birmingham,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Luton">Gatwick &ndash; Luton,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Manchester">Gatwick &ndash; Manchester,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Brighton">Gatwick &ndash; Brighton,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Hove">Gatwick &ndash; Hove,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Worthing">Gatwick &ndash; Worthing,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Epsom">Gatwick &ndash; Epsom,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Woking">Gatwick &ndash; Woking,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Dorking">Gatwick &ndash; Dorking,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Maidstone">Gatwick &ndash; Maidstone,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Chatham">Gatwick &ndash; Chatham,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Canterbury">Gatwick &ndash; Canterbury,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Hastings">Gatwick &ndash; Hastings,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Bexhill">Gatwick &ndash; Bexhill,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Turnbridge">Gatwick &ndash; Turnbridge,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Turnbridge&ndash;Wells">Gatwick &ndash; Turnbridge Wells,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Ashford">Gatwick &ndash; Ashford,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Dover">Gatwick &ndash; Dover,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Folkestone">Gatwick &ndash; Folkestone.
            </a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="row text-center" style="margin-top: 20px;">
                <img src="{{ asset('assets/images/big-rapidsslsiteseal.jpg') }}" alt="Secured By RapidSSL"/>
            </div>
            <div class="row">
                <div class="text-center col-xs-12">
                    <!--a href="#" class="img-circle">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="img-circle">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="#" class="img-circle">
                        <i class="fa fa-tumblr"></i>
                    </a-->
                    <a href="{!!url()!!}/about" class="footer-menu">About Us</a>|
                    <a href="{!!url()!!}/contact" class="footer-menu">Contact Us</a>|
                    <a href="{!!url()!!}/tnc" class="footer-menu">T&C</a>
                </div>
                <div class="copy-right">
                    <p><span>ASIANA</span> &copy; Copyright 2014-<?=date('Y')?> | All Rights Rerved  | Developed By <a href="http://www.ris10.com/">RIS10</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="link-journey">
            <a href="{!!url()!!}/index/Gatwick&ndash;Savenoaks">Gatwick &ndash; Savenoaks,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Rochester">Gatwick &ndash; Rochester,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Sittingbourne">Gatwick &ndash; Sittingbourne,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Rainham">Gatwick &ndash; Rainham,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Sheerness">Gatwick &ndash; Sheerness,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Grays">Gatwick &ndash; Grays,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Dartford">Gatwick &ndash; Dartford,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Gravesend">Gatwick &ndash; Gravesend,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Sidcup">Gatwick &ndash; Sidcup,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Orpington">Gatwick &ndash; Orpington,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Bromley">Gatwick &ndash; Bromley,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Southampton">Gatwick &ndash; Southampton,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Portsmouth">Gatwick &ndash; Portsmouth,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Bognor-Rigis">Gatwick &ndash; Bognor Rigis,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Rye">Gatwick &ndash; Rye,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Godstone">Gatwick &ndash; Godstone,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;South-Godstone">Gatwick &ndash; South Godstone,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Tadworth">Gatwick &ndash; Tadworth,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Ramsgate">Gatwick &ndash; Ramsgate,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Colchester">Gatwick &ndash; Colchester,
            </a><a href="{!!url()!!}/index/Gatwick&ndash;Oxford">Gatwick &ndash; Oxford</a>
            </div>
        </div>
    </div>
</div>



<!-- ================ footer html Exit ================ -->


@include('blade.common.js')

<script type="text/javascript">
$(document).ready(function(){
		@yield('javascript')
})
</script>

</body>
</html>