@extends('blade.layout.master')

@section('title','| Booking')
@section('description','Complete the booking by providing your details information.')

@section('content')

@include('flash::message')
@include('blade.common.error-message')

<!-- start: page -->

<div class="container">
    <div class="row page25-container page25-container-custom">
        <div class="col-sm-12  col-xs-12">
            <div class="col-sm-6 col-xs-12">
                <form  method="post" action="{!!url()!!}/booking_submit" id="booking_submit" class="form-horizontal">
                    <div class="row booking">
                        <div class="form-group col-xs-12" style="display: hide">
                            {{--<div class="short-banner">--}}
                                {{--<div class="short-banner-img"><img class="img-responsive" src="{{ asset('assets/images/short-banner.jpg') }}" alt="booking details"/></div>--}}
                                {{--<div class="short-banner-text"><a href=""><h4>I'm In The City <br/> For A Few Days & I Need A Car</h4></a></div>--}}
                            {{--</div>--}}
                            <h3>Your Details</h3>

                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                            <input type="hidden" name="distances" value="{{$distances}}"  />
                            <input type="hidden" name="pickup" value="{{$pickup}}"  />
                            <input type="hidden" name="dropoff" value="{{$dropoff}}"  />
                            <input type="hidden" name="via_points" value="{{$via_points}}"  />
                            <input type="hidden" name="carname" value="{{$carname}}"  />
                            <input type="hidden" name="tourType" id="tourType" value="{{$tripType}}"  />
                            <input type="hidden" name="totalpricespecialdate" id="totalPriceSpecialDate" value="{{ $totalPriceSpecialDate }}"  />
                            <input type="hidden" name="special_date" id="special_date" value="0"  />
                            <input type="hidden" name="totalpriceregular" id="totalpriceregular" value="{{ $totalprice }}"  />
                            <input type="hidden" name="totalprice" id="totalprice" value="{{ $totalprice }}"  />
                            <input type="hidden" id="meetngreet" name="meetngreet" value="{{ $meetngreet }}"  />
                            <input type="hidden" id="stripeCustomerID" name="stripeCustomerID" value=""  />
                            <input type="hidden" id="stripeChargeID" name="stripeChargeID" value=""  />
                            <input type="hidden" id="baseURL" name="baseURL" value="{!!url()!!}" />
                            <input type="hidden" id="stripePublishableKey" name="stripePublishableKey" value="{{$stripe['publishable_key']}}"  />
                            <input type="hidden" id="siteName" name="siteName" value="Asiana Cars Ltd."  />


                            <input type="hidden" id="minimum_booking_time" name="minimum_booking_time" value="{{ $minimumBookingtime->minimum_booking_time }}"  />

                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="firstname" id="firstname" placeholder="First Name*">
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last Name*" >
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="Email" class="form-control" name="email" id="email" placeholder="Email Address*" required>
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number*" required>
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <textarea class="form-control" name="comments" rows="3" placeholder="Comments"></textarea>
                            </div>
                        </div>
                    </div>

                    <!-- Location or Address details         -->
                    <div class="row booking">

                        <div class="form-group col-xs-12">
                            <h3>Pickup Address</h3>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" value="{{ str_replace('+', ' ', $pickup) }}" name="pickup_address" id="pickup_address" disabled="disabled">
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="pickup_address1" id="pickup_address1" placeholder="Address line 1 *">
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="pickup_address2" id="pickup_address2" placeholder="Address line 2" >
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="pickup_postcode" id="pickup_postcode" placeholder="Postcode" >
                            </div>
                        </div>
                    </div>

                    <div class="row booking">

                        <div class="form-group col-xs-12">
                            <h3>Dropoff Address</h3>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" value="{{ str_replace('+', ' ', $dropoff) }}" name="dropoff_address" id="dropoff_address" disabled="disabled">
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="dropoff_address1" id="dropoff_address1" placeholder="Address line 1 *">
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="dropoff_address2" id="dropoff_address2" placeholder="Address line 2" >
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="dropoff_postcode" id="dropoff_postcode" placeholder="Postcode" >
                            </div>
                        </div>
                    </div>
                    <!-- /Location or Address details         -->

                    <div class="row booking">
                        <div class="form-group col-xs-12">
                            <h3>Journey Date & Time</h3>
                        </div>
                        <div class="form-group col-sm-12">
                            <div class="">
                                <div class="col-sm-6 custom-select-box">
                                    <div class="row">
                                        <input id="tourdate" readonly class="form-control custom-select-box tec-domain-cat5 datepicker" type="text" name="tourdate" placeholder="Journey date *" />
                                    </div>
                                </div>

                                <div class="col-sm-3 custom-select-box">
                                    <div class="row">
                                        <select class="selectpicker" id="tourtime" name="tourtime" data-size="5" data-live-search="false" >
                                            <option class="time1" value="01"> 01 hr</option>
                                            <option class="time1" value="02"> 02 hr</option>
                                            <option class="time1" value="03"> 03 hr</option>
                                            <option class="time1" value="04"> 04 hr</option>
                                            <option class="time1" value="05"> 05 hr</option>
                                            <option class="time1" value="06"> 06 hr</option>
                                            <option class="time1" value="07"> 07 hr</option>
                                            <option class="time1" value="08"> 08 hr</option>
                                            <option class="time1" value="09"> 09 hr</option>
                                            <option class="time1" value="10"> 10 hr</option>
                                            <option class="time1" value="11"> 11 hr</option>
                                            <option class="time1" value="12"> 12 hr</option>
                                            <option class="time1" value="13"> 13 hr</option>
                                            <option class="time1" value="14"> 14 hr</option>
                                            <option class="time1" value="15"> 15 hr</option>
                                            <option class="time1" value="16"> 16 hr</option>
                                            <option class="time1" value="17"> 17 hr</option>
                                            <option class="time1" value="18"> 18 hr</option>
                                            <option class="time1" value="19"> 19 hr</option>
                                            <option class="time1" value="20"> 20 hr</option>
                                            <option class="time1" value="21"> 21 hr</option>
                                            <option class="time1" value="22"> 22 hr</option>
                                            <option class="time1" value="23"> 23 hr</option>
                                            <option class="time1" value="00"> 00 hr</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-3 custom-select-box tec-domain-cat6">
                                    <div class="row">
                                        <select class="selectpicker" id="tourmin" name="tourmin" data-live-search="false" >
                                            <option class="time1" value="00"> 00 min</option>
                                            <option class="time1" value="15"> 15 min</option>
                                            <option class="time1" value="30"> 30 min</option>
                                            <option class="time1" value="45"> 45 min</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if($tripType == 'round')
                    <div class="row booking">
                        <div class="form-group col-xs-12">
                            <h3>Return Date & Time</h3>
                        </div>
                        <div class="form-group col-sm-12">
                            <div class="">
                                <div class="col-sm-6 custom-select-box">
                                    <div class="row">
                                        <input id="re-tourdate" readonly class="form-control custom-select-box tec-domain-cat5 datepicker" type="text" name="returnDate"  placeholder="Return Journey date *"/>
                                    </div>
                                </div>

                                <div class="col-sm-3 custom-select-box">
                                    <div class="row">
                                        <select class="selectpicker" id="re-tourtime" name="returnTime" data-size="5" data-live-search="false" >
                                            <option class="time1" value="01"> 01 hr</option>
                                            <option class="time1" value="02"> 02 hr</option>
                                            <option class="time1" value="03"> 03 hr</option>
                                            <option class="time1" value="04"> 04 hr</option>
                                            <option class="time1" value="05"> 05 hr</option>
                                            <option class="time1" value="06"> 06 hr</option>
                                            <option class="time1" value="07"> 07 hr</option>
                                            <option class="time1" value="08"> 08 hr</option>
                                            <option class="time1" value="09"> 09 hr</option>
                                            <option class="time1" value="10"> 10 hr</option>
                                            <option class="time1" value="11"> 11 hr</option>
                                            <option class="time1" value="12"> 12 hr</option>
                                            <option class="time1" value="13"> 13 hr</option>
                                            <option class="time1" value="14"> 14 hr</option>
                                            <option class="time1" value="15"> 15 hr</option>
                                            <option class="time1" value="16"> 16 hr</option>
                                            <option class="time1" value="17"> 17 hr</option>
                                            <option class="time1" value="18"> 18 hr</option>
                                            <option class="time1" value="19"> 19 hr</option>
                                            <option class="time1" value="20"> 20 hr</option>
                                            <option class="time1" value="21"> 21 hr</option>
                                            <option class="time1" value="22"> 22 hr</option>
                                            <option class="time1" value="23"> 23 hr</option>
                                            <option class="time1" value="00"> 00 hr</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-3 custom-select-box tec-domain-cat6">
                                    <div class="row">
                                        <select class="selectpicker" id="re-tourmin" name="returnMin" data-live-search="false" >
                                            <option class="time1" value="00"> 00 min</option>
                                            <option class="time1" value="15"> 15 min</option>
                                            <option class="time1" value="30"> 30 min</option>
                                            <option class="time1" value="45"> 45 min</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="row booking">

                        <div class="form-group col-xs-12">
                            <h3>Airport Details <span style="font-size: 13px">(optional)</span></h3>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="arrival_time" id="arrival_time" placeholder="Flight Arrival Time" required>
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="airline_name" id="airline_name" placeholder="Airline Name" >
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="flight_number" id="flight_number" placeholder="Flight Number" required>
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="text" class="form-control" name="departure_city" id="departure_city" placeholder="Departure Cty" required>
                            </div>
                        </div>

                        <div class="form-group col-sm-12">
                            <div >
                                <input type="number" class="form-control" name="pickup_after" id="pickup_after" placeholder="When should the driver pick you up?(Minutes after landing)" max="10" min="0" required>
                            </div>
                        </div>
                    </div>


                    <div class="row booking">

                        <div class="form-group col-xs-12">
                            <h3>Passenger Details</h3>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="number" class="form-control" name="no_of_person" max="10" min="0" id="no_of_person" placeholder="Number of Person *" required>
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="number" class="form-control" name="no_of_suitcase" max="10" min="0" id="no_of_suitcase" placeholder="Number of Suitcase *" required>
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <div >
                                <input type="number" class="form-control" name="no_of_child" max="10" min="0" id="no_of_child" placeholder="Number of Child *" required>
                            </div>
                        </div>

                    </div>
<!--                </form>-->
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="row">
                    <div class="contact-details-wrap">
                        <div class="contact-details-wrap-top">
                            <div class="col-xs-12" style="background: white none repeat scroll 0% 0%; text-align: center;"><a href=""><img class="minicar4" src="{!! asset('assets/images/cars/') !!}/{!! $car_pic !!}" alt="mini car"/></a></div>
                            <h4>{{$carfullname}}</h4>
                        </div>
<!--                        <div class="contact-details-wrap-middle">-->
<!--                            <div class="col-xs-2">-->
<!--                                <div class="item-details-man5 item-details-man3 "><a href="#" class="img-circle total-passenger"><img src="{{ asset('assets/images/profile.png') }}" alt=""></a></div>-->
<!--                            </div>-->
<!--                            <div class="col-xs-2">-->
<!--                                <div class="clipart5 clipart3 "><a href="#" class="">6</a></div>-->
<!--                            </div>-->
<!--                            <div class="col-xs-3">-->
<!--                                <div class=" item-details-man55 item-details-man3"><a href="#" class="img-circle total-luggage"><img src="{{ asset('assets/images/bag2.png') }}" alt=""></a></div>-->
<!--                            </div>-->
<!--                            <div class="col-xs-3">-->
<!--                                <div class="clipart55 clipart3 "><a href="#" class="">6</a></div>-->
<!--                            </div>-->
<!--                        </div>-->

                        <div class="contact-details-wrap-bottom Car-Type-wrap-margin">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Car Type:</span></div>
                                    <div class="Private-Car"><span>{{$carfullname}} </span></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>From:</span></div>
                                    <div class="Private-Car"><span id="from_final">{{ str_replace('+', ' ', $pickup) }}</span></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Destination: </span></div>
                                    <div class="Private-Car"><span id="drop_final"> {{ str_replace('+', ' ', $dropoff) }}</span></div>
                                </div>
                            </div>
                            @if($via_points)
                                @foreach($via_points_seperate as $k=>$v)
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="Car-Type"><span>Way Point {{ $k+1 }}: </span></div>
                                            <div class="Private-Car"><span> {{ str_replace('+', ' ', $v) }}</span></div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
<!--                            <div class="row">-->
<!--                                <div class="col-sm-12">-->
<!--                                    <div class="Car-Type"><span>Pickup Date:</span></div>-->
<!--                                    <div class="Private-Car"><span>{{ $tourdate }}</span></div>-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Booking Type:</span></div>
                                    <div class="Private-Car"><span>{!! ucfirst($tripType) !!} Trip</span></div>
                                </div>
                            </div>

<!--                            <div class="row">-->
<!--                                <div class="col-sm-12">-->
<!--                                    <div class="Car-Type"><span>Pickup Time:</span></div>-->
<!--                                    <div class="Private-Car"><span>{{ $tourtime }}</span></div>-->
<!--                                </div>-->
<!--                            </div> <br/>-->

                            @if($distances != 0)
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Distance:</span></div>
                                    <div class="Private-Car">
                                        <span>{{ ceil($distances) }} miles</span> <br/>
<!--                                        <span>Per Km charge = $ 5.00</span> <br/>-->
<!--                                        <span>No. of days = 12 day(s)</span> <br/>-->
<!--                                        <span>Chauffeur charge = $.20 X 12</span> <br/>-->
<!--                                        <span>Minimum billable kms per day = 250 kms</span> <br/>-->
<!--                                        <span>Service Tax</span>-->

                                    </div>
                                </div>
                            </div>
                            @endif

                            @if($meetngreet)
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="Car-Type"><span>Meet & Greet:</span></div>
                                    <div class="Private-Car">
                                        <span>£ {{$meetngreet}} (Included)</span>
                                    </div>
                                </div>
                            </div>
                            @endif

                        </div>

                        <div class="contact-details-wrap-footer">
                            <span>Total Fare:</span>
                            <h2 id="price_show">&pound;{{number_format($totalprice,2)}}</h2>
                            <p class="tollcharge">(Toll, Congestion Charge Not Included.)</p>
                            <p class="specialcharge" style="display: none; text-align: center; margin-top: 2px;">(Special pricing has been applied due to the date.)</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-offset-0 col-sm-12">

                        <h3 class="payment-method-h2">Payment Method</h3>
                        @if(in_array('Pay via phone call',$pm))<label style="color: red; font-size:20px" class="">Call us to pay cash</label>@endif


                        <div class="form-group">

                             @foreach($pm as $payment_method)
                             <?php $payment_method_label = str_replace('_',' ',$payment_method); ?>

                                <div class="form-group">
                                    <input class="" type="radio" id="{!! $payment_method !!}" name="payment_method" value="{!! $payment_method !!}" />
                                    <label style="display: initial; font-weight: normal; margin-bottom: 0;" for="{!! $payment_method !!}">&nbsp;&nbsp;{!! ucwords($payment_method_label) !!}</label>
                                    @if($payment_method == 'card') <img src="{{ asset('assets/images/big-rapidsslsiteseal.jpg') }}" style="height: 20px;" alt="Secured By RapidSSL"/><img src="{{ asset('assets/images/cards-normal.png') }}" style="height: 20px;" /> @endif
                                    @if($payment_method == 'paypal') <img src="{{ asset('assets/images/paypal.jpg') }}" style="height: 20px;" /> @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="completing-form-btnwrap completing-form-btnwrap2">
                            <button type="button" id="booking_submit_save" class="btn form-btn  btn-block">Pay for Booking</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-offset-4 col-sm-8 cards-image-booking-page">
<!--                        <img src="{{ asset('assets/images/paypal.jpg') }}" />-->
<!--                        <img src="{{ asset('assets/images/cards-normal.png') }}" />-->
                    </div>
                </div>
                <div style="text-align: right">
                    <a href="Javascript:history.go(-1);" class="btn btn-danger btn-sm">Back</a>
                    <button type="reset" id="reset" class="btn btn-danger btn-sm">Reset</button>
                </div>

            </div>
        </div>
    </div>
</div>

</form>
<!-- Errors Modal -->

<div class="modal fade" id="payment_with_stripe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-danger text-center">Payment Status</h4>
            </div>
            <div class="modal-body" style="text-align:center; min-height: 150px; height: 150px;" id="card_payment_status">

            </div>
<!--            <div class="modal-footer">-->
<!--                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
<!--            </div>-->
        </div>

    </div>

</div>

<!-- /Errors Modal -->

<!-- Special Modal -->

<div class="modal fade" id="specialPrice" tabindex="-1" role="dialog" aria-labelledby="Special Price" aria-hidden="true">

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-danger text-center">Special Charge!</h4>
            </div>
            <div class="modal-body" style="text-align:center; min-height: 150px; height: 150px; font-size: 25px;" id="">
                There will be an additional charge of <span id="additionalPrice" style="color: red; font-weight: bolder;"></span> GBP for the journey on the selected date.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
            </div>
        </div>

    </div>

</div>

<!-- /Special Modal -->


<!-- end: page -->
@stop