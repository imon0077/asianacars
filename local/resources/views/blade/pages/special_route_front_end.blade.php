@extends('blade.layout.master')

@section('title','| Contact')
@section('description','Asiana cars ltd. is a taxi booking system. Here you can contact with the administrator and let us know your query.')

@section('content')

@include('flash::message')
@include('blade.common.error-message')

<!-- start: page -->

<div class="container">
    <div class="row page25-container page25-container-custom">
        <div class="col-sm-12">
            <div class="row">

                <img src="{{ asset('assets/images/contact.png') }}" style="width: 100%;" alt="Banner">

                <div class="" style="margin-top: 0px">

                    <div class="col-sm-12">
                        @foreach($special_routes as $sr)
                            <div class="row"><a href="sr_select_car/{!! $sr->id !!}">From {!! $sr->pickup_address !!} To {!! $sr->dropoff_address !!}</a></div>
                        @endforeach
                    </div>


                </div>
                <!--/new div-->

            </div>


        </div>
    </div>
</div>

<!-- end: page -->
@stop