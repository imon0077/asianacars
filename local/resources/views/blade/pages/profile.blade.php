@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Profile</h2>

</header>
@include('flash::message')
@include('vascorx.common.error-message')
@if($profile->status == 0)
<div class="flash-message">
    <p class="alert alert-success ">

        Status is pending. An admin will approve your request
    </p>

</div>
@elseif($profile->status == 2)
<div class="flash-message">
    <p class="alert alert-danger ">

        An admin decline your request.
    </p>

</div>
@endif
<!-- start: page -->
<div class="row">
    <div class="col-lg-8">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">

                            <div id="collapse1One" class="accordion-body collapse in">

                                <!-- -->
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-default" >
                                        <tbody>
                                        <tr>
                                            <th>First name</th>
                                            <td>{!! $profile->first_name !!} </td>
                                        </tr>

                                        <tr>
                                            <th>Last name</th>
                                            <td>{!! $profile->last_name !!} </td>
                                        </tr>
                                        @if(Auth::user()->hasRole('admin'))
                                        <tr>
                                            <th>Pharmacy name</th>
                                            <td>{!! $profile->pharmacy_name !!} </td>
                                        </tr>
                                        @else
                                        <tr>
                                            <th>Contact name</th>
                                            <td>{!! $profile->contact_name !!} </td>
                                        </tr>
                                        @endif

                                        <tr>
                                            <th>Email</th>
                                            <td>{!! $profile->email !!} </td>
                                        </tr>
                                        @if(Auth::user()->hasRole('admin'))
                                        <tr>
                                            <th>Phone number</th>
                                            <td>{!! $profile->phone_number !!} </td>
                                        </tr>
                                        @else
                                        <tr>
                                            <th>Phone number</th>
                                            <td>{!! $profile->phone_number !!} </td>
                                        </tr>

                                        <tr>
                                            <th>Fax number</th>
                                            <td>{!! $profile->fax_number !!} </td>
                                        </tr>

                                        <tr>
                                            <th>NPI number</th>
                                            <td>{!! $profile->npi_number !!} </td>
                                        </tr>
                                        <tr>
                                            <th>DEA number</th>
                                            <td>{!! $profile->dea_number !!} </td>
                                        </tr>
                                        @endif

                                        </tbody>
                                    </table>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">&nbsp;</label>
                                        <div class="col-md-6">
                                            <a href="editprofile/{!! $profile->user_id !!}" class="mb-xs mt-xs mr-xs btn btn-success">Edit Profile</a>
                                        </div>
                                    </div>


                                </div>
                                <!-- -->
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- end: page -->
@stop

