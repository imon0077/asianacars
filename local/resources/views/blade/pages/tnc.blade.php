@extends('blade.layout.master')

@section('title','| Terms and Conditions')
@section('description','Asiana cars ltd. is a taxi booking system. Here you can see the terms and conditions to use our system and contents.')

@section('content')

@include('flash::message')
@include('blade.common.error-message')

<!-- start: page -->

<div class="container">
    <div class="row page25-container page25-container-custom">
        <div class="col-sm-12">
            <div class="row">

                <img src="{{ asset('assets/images/tnc.png') }}" style="width: 100%;" class="hide-phone" alt="Banner">

                <div class="" style="margin-top: 0px">
                    <!--div class="page25-content-header"></div-->

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2>Terms & Conditions</h2>
                                <p class="" style="font-size: 20px;" align="justify">
                                    <strong>1. GENERAL</strong><br/>
                                    1.1 The terms and conditions set out herein shall apply between the Company and the party whose name and address is set out in the Account Application Form ("the Customer") and shall apply to the provision of any and all carriage, courier or delivery services ("the Services") undertaken by the Company for the Customer during the continuance of this Agreement and any and all other terms, warranties and/or conditions implied by statute and/or common law and hereby expressly excluded to the fullest extent permitted by law.    The Company requests a minimum of 10 hours notice for any online bookings.  Whilst we do our utmost to ensure our drivers are punctual, you will understand that we cannot accept responsibility for delays caused by circumstances out of our control.    The driver will drive at safe and sensible speeds in accordance with road conditions, traffic and the legal speed limits. Clients are responsible for any damage they cause to the interior and or exterior of a vehicle on hire to them and will be billed accordingly for any repair or valeting required in order to reinstate a vehicle to working order.   We may provide subcontracted vehicles occasionally.    By supplying your email address you permit The Company to contact you via email, in return we promise never to supply your email to any third party. If there are any changes or variations including extra mileage on the journeys other than what were agreed at the time of booking, the client will be charged extras in accordance with the pricing structure on our website.  We reserve the right to change your vehicle or chauffeur at any time if necessary.  Every effort will be made by The Company to ensure that our vehicle(s) or Sub-Contractors vehicle(s) arrive on time.  Our Minicab / Chauffeur drivers will travel by the most appropriate route on the day, unless instructed otherwise by the Customer at the time of booking.  Airports Cars Service vehicle(s) and sub-contracted vehicles are fully insured for passenger and third party claims. However, customer's properties are carried entirely at their own risk and The Company shall not be held responsible/liable for any loss/damage to such property.  The Company will keep a lost property book at their office, and will endeavour to return any lost goods left in our vehicle(s) or subcontracted vehicle(s) to the customer.  The Company and its Minicab / Chauffeur drivers have the right to refuse to carry any passenger who is thought to be under the influence of alcohol or drugs and whose behavior poses a threat either to the minicab / Chauffeur, the vehicle or any other passenger(s).  The Company maintains a strict non-smoking policy in all its vehicles.  Nothing contained in these terms and conditions can affect the Client's statutory rights
                                    <br/><br/>

                                    <strong>2. INVOICE LIMITATIONS</strong><br/>
                                      Account Customers with the Company may be subject to a set limit on the total amount which may be outstanding as unpaid on such account at any one time. The company may in its discretion refuse to provide the Services in the event of this limit being exceeded.<br/><br/>

                                    <strong>3. SECURITY ACCOUNT NUMBER</strong> <br/>  
                                    The customer will be issued with a security account number which must be quoted on all bookings. Notwithstanding the aforesaid, the Company does not accept any responsibility whatsoever when security account numbers are used by unauthorized personnel and/or for unauthorized purposes.  <br/><br/>

                                    <strong> 4. PAYMENT AND DEPOSITS</strong><br/>
                                     4.1 It is a condition of this agreement that invoices shall be paid in full within 30 days of issue thereof. If any invoices remain unpaid within 30 days such outstanding invoices shall immediately become due and payable.<br/><br/>

                                      4.2 Without prejudice to the Company's rights hereunder all monies due to the company in respect of provision of the Services which are not paid by the due date for payment shall bear interest on the balance of such monies due from time to time at the rate of 4% per month until payment is received by the Company in respect thereof.<br/><br/>

                                      4.3 The Customer shall not be entitled for any reason to withhold payment of monies due to the Company and in particular shall not be entitled to do so in circumstances where the Customer is in dispute with the Company and/or claims money or compensation from the Company in respect of the Services. Card payments are subject to a 4% surcharge.<br/><br/>

                                     
                                    •	All prices are inclusive of VAT charged at 20%.<br/>
                                    •	Our payment terms are strictly 7 days prior to departure.<br/>
                                    •	Your booking may be subject to additional waiting time and car park charges.<br/>
                                    •	The charge on your credit card statement will be shown as Jewel Cars.<br/><br/>


                                    4.4 The charges payable by the Customer for the Services ("the Courier Charges") shall be at the rate specified in the Company's schedule of charges as in force.<br/><br/>

                                    WE DO NOT STORE CREDIT CARD DETAILS NOR DO WE SHARE CUSTOMER DETAILS WITH ANY 3RD PARTIES<br/><br/>

                                    <strong>5. CANCELLATION AND REFUND POLICY</strong><br/>

                                    5.1 If a booking is cancelled by the Customer within 24 hours from the start of the period of hire, 40% of all monies paid will be non-refundable.<br/><br/>

                                    5.1.2 If a booking is cancelled by the Customer within 8 hours from the start of the period of hire, 100% of all monies paid will be non-refundable.<br/><br/>

                                    5.1.3 If the customer does not appear at the time and place designated as the pickup point, all monies paid will be non-refundable. It takes normally 7 to 10 working days to credit your account.<br/><br/>

                                    <strong>6. INSURANCE</strong><br/>

                                    The Company does not have insurance for goods or property (of whatsoever nature) in transit (in transit for other purposes of this clause being from the time the goods or property are collected by the Company up to and including delivery thereof), and the Customer is advised to effect such insurance as the Customer deems necessary for the carriage of goods and/or property by the Company.<br/><br/>

                                    <strong>7. ALTERATIONS TO THE TERMS AND CONDITIONS</strong><br/>

                                    The Company reserves the right to alter or vary these terms and conditions at its absolute discretion upon giving reasonable notice to the Customer and without prejudice to the generality or the aforesaid the Company reserves the right to change the Courier Charges. No representations made or variations in or additions to these terms and conditions or warranty given by any person acting or purporting to act on behalf of the Company shall have any force or effect whatsoever unless confirmed in writing by an authorized representative of the Company.<br/><br/>

                                    <strong>8. LIMITATIONS AND EXCLUSIONS</strong><br/>

                                    8.1 The Company shall not undertake the carriage or delivery of:-<br/><br/>

                                    8.1.1 Money or securities (whether cash, cheques, bankers drafts, bonds, share certificates or in any other form), antiques, precious metals, furs, or jewellery (in any form whatsoever) of whatever amount or value.<br/><br/>

                                    8.1.2 Any goods or property (of whatsoever nature) of an intrinsic value of more than 100<br/><br/>

                                    8.1.3 Any goods or property of a hazardous, dangerous, inflammable, explosive or noxious nature, or are illegal to possess under existing English Law, and/or<br/><br/>

                                    8.1.4 Any goods or property (of whatsoever nature) which may deteriorate in transit. UNLESS the Customer has prior to the commencement of the Service in respect of such goods or property expressly notified the Company as to the nature and value of the same and a Director of the Company has expressly agreed in writing that the Company shall carry and deliver the same on such terms and conditions as the Company may reasonably require AND in the event that the Company undertakes the Service in respect of such goods or property without first having expressly agreed to do so as aforesaid, the Company shall have no liability whatsoever for loss or damage to the same however arising.<br/><br/>

                                    8.2 The Company shall be entitled to destroy or dispose of goods or property referred to in clauses 8.1.3 and 8.1.4 in such manner as the Company thinks fit if in the Company's opinion it is proper to do so and the Company shall account to the Customer for money it receives (if any) on such destruction or disposal in excess of the costs incurred by the Company in so disposing of or destroying the goods or property.<br/><br/>

                                    8.3 Without prejudice to the provisions of clause 8.1 the Company shall not in any event be liable directly or indirectly for:-<br/><br/>

                                    8.3.1 Consequential loss (whether for loss or profit or otherwise) and/or<br/><br/>

                                    8.3.2 Loss, damage and/or breakage to china, glass ceramics or other breakables whether arising from the acts, omissions or negligence of the Company and/or its employees and/or agents or arising otherwise howsoever.<br/><br/>

                                    8.4 Without prejudice to the generality of clauses 8.1 and 8.3 in particular the Company shall not be liable for any loss and/or damage arising directly or indirectly from:-<br/><br/>

                                    8.4.1 Breakdown, accident, adverse weather conditions.<br/><br/>

                                    8.4.2 Any act or omission on the part of the Customer.<br/><br/>

                                    8.4.3 any clause, act or circumstance beyond the control of the Company (including, without limitation, any strike, (official or not) lock-out or other form of industrial action or labour dispute, governmental regulations, legal restrictions, embargoes, fire, flood, Act of God, any consequence of riot, war, invasion, act of foreign enemy, hostilities (whether war be declared or not) civil war, acts of terrorism, rebellion, military or usurped power, confiscation, requisition or destruction of or damage to property by or upon the order of or in the name of any Government or public local authority).<br/><br/>

                                    8.4.4 Inadequate or inappropriate packaging of goods, or incorrect or inadequate labelling or instructions received from the customer and/or<br/><br/>

                                    8.4.5 The Company being prevented or hindered from delivering the goods or property<br/><br/>

                                    8.5 Without prejudice to the generality and effect of the foregoing provisions of this clause 8 the liability of the Company for each delivery or courier service undertaken by the Company howsoever arising and whether direct or indirect and including but not limited to liability arising from the acts, omissions or negligence of the Company and/or its employees and/or agents or arising otherwise howsoever shall in any event be limited to the lesser of:-<br/><br/>

                                    8.5.1 150 or<br/><br/>

                                    8.5.2 The intrinsic value of the goods or property comprised in such delivery or courier service<br/><br/>

                                    8.6 The provision of clauses 8.3, 8.4, 8.5 and 10.1 apply to liability for loss or damage to goods or property and do not apply to liability for death or personal injury.<br/><br/>

                                    <strong>9. DELIVERY</strong><br/>

                                    9.1 The Company shall use reasonable measures to deliver the Customer and the Customer's goods or property on time, however the time for delivery shall not in any event be of the essence and the Company makes no warranty that the Customer or Customer's goods or property shall be delivered within the Customers stipulated time period (if any) and/or within any time period stated by the Company unless expressly agreed in writing by a Director of a Company.<br/><br/>

                                    9.2 In the event that the Company is unable (for whatever reason) to deliver the Customer or the Customer's goods or property then the Company reserves the right to charge the Customer for any and all costs and expenses incurred in doing so and/or for any costs or storage of the goods or property.<br/><br/>

                                    <strong>10. CLAIMS</strong><br/>

                                    10.1 Without prejudice to the foregoing provisions of this Agreement the Company shall not in any event be liable for any loss and/or damage howsoever arising including but not limited to liability arising from the acts, omissions or negligence of the Company and/or its employees and/or agents and arising otherwise howsoever unless the Customer has notified the Company (with reasonable particularity) as to the nature and extent of such loss or damage within 15 working days of the date upon which the same occurred.<br/><br/>

                                    <strong>11. LIEN</strong><br/>

                                    11.1 Without prejudice to the Company's rights hereunder or arising otherwise howsoever, the Company reserves the right to exercise a lien over the Customer's goods and/or property pending payment in full or outstanding invoices.<br/><br/>

                                    <strong>12 TERMINATIONS</strong><br/>

                                    12.1 This Agreement may be terminated by either party by giving one months notice in writing.<br/><br/>

                                    12.2 In the event of the Customer being in breach of any of the terms and/or conditions of this Agreement the Company shall have the right (without prejudice to any other rights it may have) to terminate this Agreement or suspend provision of the Services, or suspend the Customer's account facility, forthwith and without notice.<br/><br/>

                                    13. RESOLUTION of DISPUTES and GOVERNING LAW<br/><br/>

                                    13.1 The parties hereto submit to the exclusive jurisdiction of the Courts of England and Wales.<br/><br/>

                                    <strong>14. ENTIRE AGREEMENT</strong><br/>

                                    14.1 This Agreement contains all the terms agreed by the parties regarding the subject matter hereof and supersedes any prior agreements, understandings or arrangements between them, whether oral or in writing, and no representation undertaking or promise shall be taken to have been given or be implied from anything said or written prior to this Agreement except as expressly set out in this Agreement.<br/><br/>

                                    <strong>15. NOTICES /REFUND</strong><br/>

                                    15.1 Any notice to be given by any party to the other under this Agreement shall be sufficiently served if left at, or sent by prepaid registered post or recorded delivery service or telefax or telex to the party to be served at. Its address as set out in this Agreement or such other address as it may notify for such purpose and shall be deemed to have been served when so left or sent by telefax or telex or in the case of posting 24 hours after the same was posted. In proving service by post it shall only be necessary to prove that the communication was contained in an envelope which was duly posted in accordance with this clause<br/><br/>

                                    25% cancellation charge if cancellation made more than 24 hours before delivery time;  50% cancellation charge if cancellation made between 12 and 24 hours before delivery time; 100% cancellation charge if cancellation made within 12 hours of delivery time.<br/><br/>

                                    <strong>16. WAIVER</strong><br/>

                                    16.1 No forbearance, indulgence or failure by the Company to enforce or to exercise, at any time or for any period of time, any term of or any right arising pursuant to this Agreement shall constitute, and shall not be construed as, a waiver of such term or right and shall in no way affect the Company's right later to enforce or exercise it.<br/><br/>

                                    <strong>17. SEVERABILITY</strong><br/>

                                    17.1 The invalidity or unenforceability of any term of or any right arising pursuant to this Agreement shall not in any way affect the remaining terms or rights.

                                </p>

                            </div>
                        </div>
                    </div>


                </div>
                <!--/new div-->

            </div>


        </div>
    </div>
</div>

<!-- end: page -->
@stop