@extends('blade.layout.master')

@section('content')

@include('flash::message')
@include('blade.common.error-message')

<!-- start: page -->

<div class="container">
    <div class="row page25-container page25-container-custom">
        <div class="col-sm-12">
            <div class="row">

                <img src="{{ asset('assets/images/Banner-inner-page.png') }}" style="width: 100%;" alt="Banner">

                <div class="" style="margin-top: 0px">
                    <!--div class="page25-content-header"></div-->

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2>404 Page Not Found</h2>
                                <p class="">Ooopss!! Please recheck the url and reload.</p>
                                <h6>Thank You</h6>
                                <a href="{!!url()!!}/index"><span>Back to home</span></a>
                            </div>
                        </div>
                    </div>


                </div>
                <!--/new div-->


<!--                <h3>Booking Completed Successfully.</h3>-->
<!--                <a href="{!!url()!!}/index"><h6>Back to home</h6></a>-->




            </div>


        </div>
    </div>
</div>

<!-- end: page -->
@stop