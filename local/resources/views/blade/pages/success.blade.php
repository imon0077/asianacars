@extends('blade.layout.master')

@section('content')

@include('flash::message')
@include('blade.common.error-message')

<!-- start: page -->

<div class="container">
    <div class="row page25-container page25-container-custom">
        <div class="col-sm-12">
            <div class="row">

                <img src="{{ asset('assets/images/Banner-inner-page.png') }}" style="width: 100%;" alt="Banner">

                <div class="" style="margin-top: 0px">
                    <!--div class="page25-content-header"></div-->

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2>Booking Completed Successfully.</h2>
                                <p class="">An email has sent to you. Please review your booking details. Let us know if any change required as soon as possible.</p>

                                <h4 style="color:red; font-weight: bold;">Note: Your booking is not confirmed until you receive a confirmation email from us</h4>

                                <h6>Thank You</h6>
                                <a href="{!!url()!!}/BackToHome"><span>Back to home</span></a>
                            </div>
                        </div>
                    </div>


                </div>
                <!--/new div-->


<!--                <h3>Booking Completed Successfully.</h3>-->
<!--                <a href="{!!url()!!}/index"><h6>Back to home</h6></a>-->




            </div>


        </div>
    </div>
</div>

<!-- end: page -->
@stop