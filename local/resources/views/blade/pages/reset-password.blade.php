@extends('vascorx.layout.master-nobar')

@section('content')
<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">
        <a href="../" class="logo">
            <img src="{{asset('assets/images/logo.png')}}" height="35" alt="VascoRx" />
        </a>

        <div class="panel panel-sign">
            <div class="panel-title-sign mt-xl text-right">
                <h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Recover Password</h2>
            </div>
            <div class="panel-body">

                @include('vascorx.common.error-message')

                <form method="POST" action="password/reset">
                    <div class="form-group mb-none">
                        <div class="input-group">
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group mb-lg">
                                <div class="input-group input-group-icon">
                                    <input class="form-control input-lg"  type="email" name="email" value="{{ old('email') }}" placeholder="E-mail">
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
                                </div>
                            </div>
                            <div class="form-group mb-lg">
                                <div class="input-group input-group-icon">
                                    <input class="form-control input-lg"  type="password" name="password" placeholder="New Password">
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
                                </div>
                            </div>
                            <div class="form-group mb-lg">
                                <div class="input-group input-group-icon">
                                    <input class="form-control input-lg"  type="password" name="password_confirmation" placeholder="Re-type Password">
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
                                </div>
                            </div>

                            <div>
                                <span class="input-group-btn">
                                    <button class="btn btn-primary btn-lg" type="submit">Reset!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <p class="text-center text-muted mt-md mb-md">&copy; Copyright 2014. All Rights Reserved.</p>
    </div>
</section>
<!-- end: page -->

@stop