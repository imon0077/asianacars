@extends('blade.layout.master')

@section('title','| Home')
@section('keywords','heathrow taxi, heathrow to gatwick, taxi to gatwick, gatwick taxi, heathrow taxis, gatwick taxis, taxi to gatwick airport, airport taxi gatwick, taxi from gatwick, taxis to gatwick, taxi gatwick airport, gatwick taxi quote, heathrow gatwick, taxi from gatwick to heathrow, taxi from heathrow to gatwick, taxi in gatwick, taxi gatwick to heathrow, taxi heathrow to gatwick, taxis from gatwick, taxis to gatwick airport, from heathrow to gatwick')
@section('description','Asiana Cars Ltd. is an online airport taxi booking system based in United Kingdom, UK, which is operating mainly in London. People can book taxi to travel from different airports like London, Heathrow, Gatwick to their desire destinations all over the UK. Different types of vehicles are available to serve different kinds of demands with affordable cost. The booking procedure is quite simple and card payment is available. The site is secured by SSL certificate therefore your payment transactions are encrypted are safe with us.')

@section('content')

@include('flash::message')
@include('blade.common.error-message')
<style>
    .pac-container {
        z-index: 99999999;
    }

    .modal-dialog {
        padding-top: 75px;
    }
</style>
<!-- start: page -->


<!--slider {{ asset('assets/images/lock.png') }}-->
<div class="google-image">
    <div class="bg-ground">
        <div class="main">
            <ul id="cbp-bislideshow" class="cbp-bislideshow">
                <li>
                    <div class="slider-img-wrap"><img src="{{ asset('assets/images/1.jpg') }}" alt="Anytime Anywhere"/>
                    </div>
                    <div class="slider-text-content">
                        <h1>ANYTIME, <br/>ANYWHERE!</h1>

                        <div class="anytime-text">
                            <p><i class="fa fa-custom fa-circle-o"></i>Well trained & Professional Drivers</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Affordable Cost</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Friendly and Communicative</p>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="slider-img-wrap"><img src="{{ asset('assets/images/2.jpg') }}" alt="Book your taxi"/>
                    </div>
                    <div class="slider-text-content">
                        <h1>book your taxi<br/>online now</h1>

                        <div class="anytime-text">
                            <p><i class="fa fa-custom fa-circle-o"></i>Well trained & Professional Drivers</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Affordable Cost</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Friendly and Communicative</p>
                        </div>

                    </div>
                </li>

                <li>
                    <div class="slider-img-wrap"><img src="{{ asset('assets/images/3.jpg') }}" alt="System with Love"/>
                    </div>
                    <div class="slider-text-content">
                        <h1>a system that<br/>made with <span style="color:red">love</span></h1>

                        <div class="anytime-text">
                            <p><i class="fa fa-custom fa-circle-o"></i>Get quickest quote</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Easy going web interface</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Customized booking</p>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="slider-img-wrap"><img src="{{ asset('assets/images/4.jpg') }}" alt="Book Online"/></div>
                    <div class="slider-text-content">
                        <h1>book your taxi<br/>online now</h1>

                        <div class="anytime-text">
                            <p><i class="fa fa-custom fa-circle-o"></i>Well trained & Professional Drivers</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Affordable Cost</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Friendly and Communicative</p>
                        </div>

                    </div>
                </li>


            </ul>
        </div>
    </div>
</div>
<!--/slider-->

<!-- Booking now form wrapper html start -->
<div class="booking-form-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="form-wrap ">
                        <div class="form-headr"></div>
                        <h2>Get Instant Quote Online</h2>

                        <div class="form-select">
                            <form method="post" action="{!!url()!!}/result" id="booking">
                                <div class="col-md-2">
                                    <div class="row">
                                        <span style="font-size: 15px" class="normal-screen">Pickup</span>
                                    </div>
                                </div>

                                <div class="col-md-10 custom-select-box tec-domain-cat3">
                                    <div class="row">
                                        <div id="panel">
                                            <input type="text" id="start" name="pickup"
                                                   value="<?= ($sessData == NULL) ? '' : $sessData['pickup'] ?>"
                                                   class="form-control tec-domain-cat16"/>
                                            <!--                                            <span id='check' class="check" style='display:none;'></span>-->
                                        </div>

                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="row custom-responsive-add-remove-waypoints">
                                        <div style="text-align: right">
                                            <!-- Button trigger modal -->
                                            <span style="font-weight: bold" id="way_point_view">0</span> way point(s) is
                                            added
                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                                    data-target="#myModal">
                                                Add/Remove waypoints
                                            </button>
                                        </div>
                                        <br/>

                                        <!-- Modal -->
                                        <div class="modal fade" id="myModal" tabindex="-1"
                                             aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Add up to 4
                                                            waypoints</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="custom-select-box tec-domain-cat4">
                                                                    <div class="row">
                                                                        <input
                                                                            value="<?= ($sessData == NULL) ? '' : $sessData['waypoint_1'] ?>"
                                                                            type="text" id="waypoint_1"
                                                                            name="waypoint_1"
                                                                            class="col-xs-10 tec-domain-cat16 col-md-10 col-lg-10 waypoint_geocode"/>
                                                                        <a href="javascript:void(0);"
                                                                           class="waypoint_del" id="waypoint_1__del"
                                                                           style="font-size: 15px;">Clear</a>
                                                                    </div>
                                                                </div>
                                                                <div class="custom-select-box tec-domain-cat4">
                                                                    <div class="row">
                                                                        <input
                                                                            value="<?= ($sessData == NULL) ? '' : $sessData['waypoint_2'] ?>"
                                                                            type="text" id="waypoint_2"
                                                                            name="waypoint_2"
                                                                            class="col-xs-10 tec-domain-cat16 col-md-10 col-lg-10 waypoint_geocode"/>
                                                                        <a href="javascript:void(0);"
                                                                           class="waypoint_del" id="waypoint_2__del"
                                                                           style="font-size: 15px;">Clear</a>
                                                                    </div>
                                                                </div>
                                                                <div class="custom-select-box tec-domain-cat4">
                                                                    <div class="row">
                                                                        <input
                                                                            value="<?= ($sessData == NULL) ? '' : $sessData['waypoint_3'] ?>"
                                                                            type="text" id="waypoint_3"
                                                                            name="waypoint_3"
                                                                            class="col-xs-10 tec-domain-cat16 col-md-10 col-lg-10 waypoint_geocode"/>
                                                                        <a href="javascript:void(0);"
                                                                           class="waypoint_del" id="waypoint_3__del"
                                                                           style="font-size: 15px;">Clear</a>
                                                                    </div>
                                                                </div>
                                                                <div class="custom-select-box tec-domain-cat4">
                                                                    <div class="row">
                                                                        <input
                                                                            value="<?= ($sessData == NULL) ? '' : $sessData['waypoint_4'] ?>"
                                                                            type="text" id="waypoint_4"
                                                                            name="waypoint_4"
                                                                            class="col-xs-10 tec-domain-cat16 col-md-10 col-lg-10 waypoint_geocode"/>
                                                                        <a href="javascript:void(0);"
                                                                           class="waypoint_del" id="waypoint_4__del"
                                                                           style="font-size: 15px;">Clear</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary" id="add_waypoints"
                                                                data-dismiss="modal">Done
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="row">
                                        <span style="font-size: 15px" class="normal-screen">Dropoff</span>
                                    </div>
                                </div>
                                <div class="col-md-10 custom-select-box tec-domain-cat4">
                                    <div class="row">
                                        <div>
                                            <input value="<?= ($sessData == NULL) ? '' : $sessData['dropoff'] ?>"
                                                   type="text" id="end" name="dropoff"
                                                   class="form-control tec-domain-cat16"/>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <input type="hidden" name="pickup_postcode" id="pickup_postcode"/>
                                    <input type="hidden" name="dropoff_postcode" id="dropoff_postcode"/>
                                    <input type="hidden" name="distances" id="distances"/>
                                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                                </div>

                                <!--/new section for pickup time-->

                                <div style="float: left; clear:right; opacity: .75"></div>

                                <div style="float: right; clear:right; text-align: right">
                                    <button type="button" id="reset_index" class="btn btn-danger btn-sm">Reset</button>
                                </div>
                                <div class="form-button" style="clear: both">
                                    <button type="button" id="save" class="btn form-btn btn-lg btn-block">
                                        <span class="ssl-logo-small" style="float:left; padding-left: 10px"><img
                                                src="{{ asset('assets/images/big-rapidsslsiteseal.jpg') }}"
                                                alt="Secured By RapidSSL"/></span>
                                        <span style="margin-left: -55px;">Quote Now</span>
                                    </button>

                                </div>
                                <div class="col-md-12 cards-image-home-page" style="clear: both; text-align: center">
                                    <img src="{{ asset('assets/images/cards-normal.png') }}" />
                                </div>

                            </form>
                            <!-- Errors Modal -->


                            <!-- /Errors Modal -->


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Booking now form wrapper html Exit -->

<!-- anytime-anywhere html start -->
<!--<div class="anytime-anywhere">-->
<!--    <div class="row">-->
<!--        <div class="anytime-wrap">-->
<!--            <h1>ANYTIME, <br/>ANYWHERE!</h1>-->
<!--            <div class="anytime-text">-->
<!--                <p><i class="fa fa-custom fa-circle-o"></i>Proin gravida nibh vel velit auctor aliquet sollicitudin.</p>-->
<!--                <p><i class="fa fa-custom fa-circle-o"></i>Qnec sagittis bibendum auctor sem nibh id.</p>-->
<!--                <p><i class="fa fa-custom fa-circle-o"></i>Rit amet nibh vulputate cursus nisi elit.</p>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- anytime-anywhere html Exit -->

<!-- label white html start -->
<div class="label-white white-lable-m">
    <div class="container">
        <div class="row">
            <div class="col-sm-6" data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row">
                    <div class="label-item">
                        <div class="containt-font">
                            <a href="#" class="img-circle"><img src="{{ asset('assets/images/lock.png') }}"
                                                                alt="Secure Booking"/></a>
                        </div>
                        <div class="containt-text">
                            <h3>Secure Booking</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6" data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row">
                    <div class="label-item">
                        <div class="containt-font">
                            <a href="#" class="img-circle"><img src="{{ asset('assets/images/reliable.png') }}"
                                                                alt="Reliable Service"/></a>
                        </div>
                        <div class="containt-text">
                            <h3>Reliable Service</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6" data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row">
                    <div class="label-item">
                        <div class="containt-font">
                            <a href="#" class="img-circle"><img src="{{ asset('assets/images/customer.png') }}"
                                                                alt="Customer Service"/></a>
                        </div>
                        <div class="containt-text">
                            <h3>Customer Service</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6" data-uk-scrollspy="{cls:'uk-animation-fade', delay:300, repeat: true}">
                <div class="row">
                    <div class="label-item">
                        <div class="containt-font">
                            <a href="#" class="img-circle"><img src="{{ asset('assets/images/hidden.png') }}"
                                                                alt="No hidden charge"/></a>
                        </div>
                        <div class="containt-text">
                            <h3>No Hidden Charges</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- label white html exit -->

<!-- label yellow html start -->
<div class="yellow-label-wrapper2" style="display: none">
<div class="label-yellow stellar" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="">
<div class="container">
<div class="row">
<div class="destination">
    <h2>Destinations You'd Love</h2>
    <h4>Look at the wonderful places</h4>
</div>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
<div class="slider-btn">
    <a class="right-cursor1" href="#carousel-example-generic" data-slide="prev"></a>
    <a class="left-cursor1" href="#carousel-example-generic" data-slide="next"></a>
</div>
<div class="carousel-inner">
<div class="item active">
    <div class="col-sm-4">
        <div class="row">
            <div class="slider-item ">
                <div class="slider-img"><img class="img-responsive" alt="Orange Skies"
                                             src="{{ asset('assets/images/slider/slider1.jpg') }}"/></div>
                <div class="slider-text-hover">
                    <div class="slider-hover-content"></div>
                    <div class="Orange">
                        <div class="slider-hover-content2">
                            <h4>Orange Skies</h4>

                            <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3">
                            <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                        </div>
                    </div>
                </div>
                <div class="slider-text">
                    <div class="slider-text1">
                        <h4>Orange Skies</h4>

                        <p>Save upto 50%</p>
                    </div>
                    <div class="slider-text2">
                        <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="row">
            <div class="slider-item ">
                <div class="slider-img"><img class="img-responsive" alt="First slide"
                                             src="{{ asset('assets/images/slider/slider2.jpg') }}"/></div>

                <div class="slider-text-hover">
                    <div class="slider-hover-content"></div>
                    <div class="Orange">
                        <div class="slider-hover-content2">
                            <h4>Orange Skies</h4>

                            <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3">
                            <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                        </div>
                    </div>
                </div>


                <div class="slider-text">
                    <div class="slider-text1">
                        <h4>Orange Skies</h4>

                        <p>Save upto 50%</p>
                    </div>
                    <div class="slider-text2">
                        <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="row">
            <div class="slider-item homepage-sllider-m">
                <div class="slider-img"><img class="img-responsive" alt="First slide"
                                             src="{{ asset('assets/images/slider/slider3.jpg') }}"/></div>

                <div class="slider-text-hover">
                    <div class="slider-hover-content"></div>
                    <div class="Orange">
                        <div class="slider-hover-content2">
                            <h4>Orange Skies</h4>

                            <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3">
                            <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                        </div>
                    </div>
                </div>


                <div class="slider-text">
                    <div class="slider-text1">
                        <h4>Orange Skies</h4>

                        <p>Save upto 50%</p>
                    </div>
                    <div class="slider-text2">
                        <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="item">
    <div class="col-sm-4">
        <div class="row">
            <div class="slider-item ">
                <div class="slider-img"><img class="img-responsive" alt="First slide"
                                             src="{{ asset('assets/images/slider/slider4.jpg') }}"/></div>
                <div class="slider-text-hover">
                    <div class="slider-hover-content"></div>
                    <div class="Orange">
                        <div class="slider-hover-content2">
                            <h4>Orange Skies</h4>

                            <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3">
                            <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                        </div>
                    </div>
                </div>

                <div class="slider-text">
                    <div class="slider-text1">
                        <h4>Orange Skies</h4>

                        <p>Save upto 50%</p>
                    </div>
                    <div class="slider-text2">
                        <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="row">
            <div class="slider-item ">
                <div class="slider-img"><img class="img-responsive" alt="First slide"
                                             src="{{ asset('assets/images/slider/slider5.jpg') }}"/></div>
                <div class="slider-text-hover">
                    <div class="slider-hover-content"></div>
                    <div class="Orange">
                        <div class="slider-hover-content2">
                            <h4>Orange Skies</h4>

                            <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3">
                            <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                        </div>
                    </div>
                </div>
                <div class="slider-text">
                    <div class="slider-text1">
                        <h4>Orange Skies</h4>

                        <p>Save upto 50%</p>
                    </div>
                    <div class="slider-text2">
                        <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="row">
            <div class="slider-item homepage-sllider-m">
                <div class="slider-img"><img class="img-responsive" alt="First slide"
                                             src="{{ asset('assets/images/slider/slider6.jpg') }}"/></div>

                <div class="slider-text-hover">
                    <div class="slider-hover-content"></div>
                    <div class="Orange">
                        <div class="slider-hover-content2">
                            <h4>Orange Skies</h4>

                            <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3">
                            <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                        </div>
                    </div>
                </div>


                <div class="slider-text">
                    <div class="slider-text1">
                        <h4>Orange Skies</h4>

                        <p>Save upto 50%</p>
                    </div>
                    <div class="slider-text2">
                        <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="item">
    <div class="col-sm-4">
        <div class="row">
            <div class="slider-item ">
                <div class="slider-img"><img class="img-responsive" alt="First slide"
                                             src="{{ asset('assets/images/slider/slider7.jpg') }}"/></div>
                <div class="slider-text-hover">
                    <div class="slider-hover-content"></div>
                    <div class="Orange">
                        <div class="slider-hover-content2">
                            <h4>Orange Skies</h4>

                            <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3">
                            <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                        </div>
                    </div>
                </div>


                <div class="slider-text">
                    <div class="slider-text1">
                        <h4>Orange Skies</h4>

                        <p>Save upto 50%</p>
                    </div>
                    <div class="slider-text2">
                        <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="row">
            <div class="slider-item ">
                <div class="slider-img"><img class="img-responsive" alt="First slide"
                                             src="{{ asset('assets/images/slider/slider8.jpg') }}"/></div>

                <div class="slider-text-hover">
                    <div class="slider-hover-content"></div>
                    <div class="Orange">
                        <div class="slider-hover-content2">
                            <h4>Orange Skies</h4>

                            <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3">
                            <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                        </div>
                    </div>
                </div>


                <div class="slider-text">
                    <div class="slider-text1">
                        <h4>Orange Skies</h4>

                        <p>Save upto 50%</p>
                    </div>
                    <div class="slider-text2">
                        <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="row">
            <div class="slider-item homepage-sllider-m">
                <div class="slider-img"><img class="img-responsive" alt="First slide"
                                             src="{{ asset('assets/images/slider/slider9.jpg') }}"/></div>
                <div class="slider-text-hover">
                    <div class="slider-hover-content"></div>
                    <div class="Orange">
                        <div class="slider-hover-content2">
                            <h4>Orange Skies</h4>

                            <p>Save upto 50%</p>
                        </div>
                        <div class="slider-hover-content3">
                            <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                        </div>
                    </div>
                </div>
                <div class="slider-text">
                    <div class="slider-text1">
                        <h4>Orange Skies</h4>

                        <p>Save upto 50%</p>
                    </div>
                    <div class="slider-text2">
                        <a href="#" class="btn slide-btn btn-lg">Avail Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
<!-- label yellow html exit -->

<!-- label white2 html start -->
<div class="label-white2" style="display: none">
    <div class="container">
        <div class="row">
            <div class="car-item-wrap">
                <div class="car-type">
                    <div class="car-wrap"><img class="private-car" src="{{ asset('assets/images/private-car.png') }}"
                                               alt=""/></div>
                    <h5>Private Car</h5>

                    <div class="car-type-btn">
                        <a href="Results_1.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"><img class="morotbike-car" src="{{ asset('assets/images/motorbike.png') }}"
                                               alt=""/></div>
                    <h5>Motorbike</h5>

                    <div class="car-type-btn">
                        <a href="Results_1.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"><img class="minicar-car" src="{{ asset('assets/images/minicar.png') }}"
                                               alt=""/></div>
                    <h5>Minicar</h5>

                    <div class="car-type-btn">
                        <a href="Results_1.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"><img class="mini-track-car" src="{{ asset('assets/images/mini-track.png') }}"
                                               alt=""/></div>
                    <h5>Mini Truck</h5>

                    <div class="car-type-btn">
                        <a href="Results_1.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"><img class="boat-car" src="{{ asset('assets/images/boat.png') }}" alt=""/>
                    </div>
                    <h5>Boat</h5>

                    <div class="car-type-btn">
                        <a href="Results_1.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"><img class="snow-car" src="{{ asset('assets/images/snow-bike.png') }}"
                                               alt=""/></div>
                    <h5>Snow Bike</h5>

                    <div class="car-type-btn">
                        <a href="Results_1.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"><img class="tractor-car" src="{{ asset('assets/images/tractor.png') }}"
                                               alt=""/></div>
                    <h5>Tractor</h5>

                    <div class="car-type-btn">
                        <a href="Results_1.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"><img class="vihicel-car" src="{{ asset('assets/images/vihicel.png') }}"
                                               alt=""/></div>
                    <h5>Large Vehicle</h5>

                    <div class="car-type-btn">
                        <a href="Results_1.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"><img class="morotbike-car" src="{{ asset('assets/images/motorbike.png') }}"
                                               alt=""/></div>
                    <h5>Motorbike</h5>

                    <div class="car-type-btn">
                        <a href="Results_1.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

                <div class="car-type">
                    <div class="car-wrap"><img class="big-track-car" src="{{ asset('assets/images/big-track.png') }}"
                                               alt=""/></div>
                    <h5>Big Truck</h5>

                    <div class="car-type-btn">
                        <a href="Results_1.html" class="btn car-btn btn-lg">BOOK NOW</a>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- label white2 html Exit -->

<!-- label yellow-2 html start -->
<div class="yellow-label-wrapper hide-phone">
    <div class="label-yellow2 stellar" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="">
        <div class="container">
            <div class="row">
                <h2>Airports You May Love</h2>
                <h4>Look at our beautiful journey</h4>

                <div class="col-sm-3">
                    <div class="page7-j0urney-wrap">
                        <div class="smialy"><a href=""><img src="{{ asset('assets/images/airports/1.png') }}"
                                                            height="150" alt=""/></a></div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="page7-j0urney-wrap">
                        <div class="smialy"><a href=""><img src="{{ asset('assets/images/airports/2.png') }}"
                                                            height="150" alt=""/></a></div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="page7-j0urney-wrap">
                        <div class="smialy"><a href=""><img src="{{ asset('assets/images/airports/3.png') }}"
                                                            height="150" alt=""/></a></div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="page7-j0urney-wrap">
                        <div class="smialy"><a href=""><img src="{{ asset('assets/images/airports/4.png') }}"
                                                            height="150" alt=""/></a></div>
                    </div>
                </div>

                <!--                <div class="col-sm-3">-->
                <!--                    <div class="page7-j0urney-wrap">-->
                <!--                        <div class="smialy"><a href=""><img src="{{ asset('assets/images/airports/5.png') }}" height="50" alt=""/></a></div>-->
                <!--                    </div>-->
                <!--                </div>-->

            </div>
        </div>
    </div>
</div>
<!-- label yellow-2 html exit -->


<!-- end: page -->
@stop

@section('javascript')

<!--    $("#start").change(function (e) {-->
<!--    var pickup = $(this).val();-->
<!---->
<!--    if (pickup == '') {-->
<!--    $("#check").html(' This field is empty.').addClass('messageboxerror').fadeTo(300,1).fadeOut(4000);-->
<!--    alert(' This field is empty');-->
<!--    $(this).focus();-->
<!---->
<!--    return false;-->
<!--    }-->
<!---->
<!--    });-->

<!--    jQuery(document).ready(function($) {-->
<!---->
<!--        // hide messages-->
<!--        $("#error").hide();-->
<!--        $("#success").hide();-->
<!---->
<!--        // on submit...-->
<!--        $("#save").click(function() {-->
<!--        $("#error").hide();-->
<!---->
<!--        //required:-->
<!---->
<!--        var pickup = $("input#start").val();-->
<!--        if (pickup == '') {-->
<!--        $("#error").fadeIn().text("Please provide pickup address.");-->
<!--        alert("Please provide pickup address.");-->
<!--        $("input#start").focus();-->
<!--        return false;-->
<!--        }-->
<!---->
<!---->
<!---->
<!--        if (confirm("Are you sure you want to submit the form?")) {-->
<!--        form.submit();-->
<!--        return true;-->
<!--        }-->
<!--        else {-->
<!--        return false; //form will not be sumitted and page will not reload-->
<!--        }-->
<!---->
<!--        });-->
<!---->
<!--        return false;-->
<!--        });-->
<!---->
<!---->
<!---->
<!---->
<!---->
<!--    });-->
@stop