@extends('blade.layout.master')

@section('title','| Home')
@section('keywords','heathrow taxi, heathrow to gatwick, taxi to gatwick, gatwick taxi, heathrow taxis, gatwick taxis, taxi to gatwick airport, airport taxi gatwick, taxi from gatwick, taxis to gatwick, taxi gatwick airport, gatwick taxi quote, heathrow gatwick, taxi from gatwick to heathrow, taxi from heathrow to gatwick, taxi in gatwick, taxi gatwick to heathrow, taxi heathrow to gatwick, taxis from gatwick, taxis to gatwick airport, from heathrow to gatwick')
@section('description','Asiana Cars Ltd. is an online airport taxi booking system based in United Kingdom, UK, which is operating mainly in London. People can book taxi to travel from different airports like London, Heathrow, Gatwick to their desire destinations all over the UK. Different types of vehicles are available to serve different kinds of demands with affordable cost. The booking procedure is quite simple and card payment is available. The site is secured by SSL certificate therefore your payment transactions are encrypted are safe with us.')

@section('content')

@include('flash::message')
@include('blade.common.error-message')
<style>
    .pac-container {
        z-index: 99999999;
    }

    .modal-dialog {
        padding-top: 75px;
    }
    .form-wrap{
        background-color: #F0C516;
    }
    .sr-wrap{
        background-color: #FFFFFF;
    }
    .link-desc{
        padding: 0px;
        text-align: justify;
    }
    .sr-wrap h2{
        font-family: SourceSansPro Light;
        font-size: 26px;
        padding: 17px;
        text-align: center;
        margin-top: 0px;
    }
    .sr-wrap > ul {
        height: 200px;
        line-height: 17px;
        overflow-y: scroll;
    }
    tr.route-head > td{
        background-color: #F09616;
        font-weight: bolder;
    }
    .table tbody tr.route-body > td{
        border-top: 1px solid #F0C516 !important;
        padding: 1px !important;
    }

    .route-table{
        height: 300px !important;
        overflow-y: scroll !important;
    }
</style>
<!-- start: page -->


<!--slider {{ asset('assets/images/lock.png') }}-->
<div class="google-image">
    <div class="bg-ground">
        <div class="main">
            <ul id="cbp-bislideshow" class="cbp-bislideshow hide">
                <li>
                    <div class="slider-img-wrap"><img src="{{ asset('assets/images/1.jpg') }}" alt="Anytime Anywhere"/>
                    </div>
                    <div class="slider-text-content">
                        <h1>ANYTIME, <br/>ANYWHERE!</h1>

                        <div class="anytime-text">
                            <p><i class="fa fa-custom fa-circle-o"></i>Well trained & Professional Drivers</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Affordable Cost</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Friendly and Communicative</p>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="slider-img-wrap"><img src="{{ asset('assets/images/2.jpg') }}" alt="Book your taxi"/>
                    </div>
                    <div class="slider-text-content">
                        <h1>book your taxi<br/>online now</h1>

                        <div class="anytime-text">
                            <p><i class="fa fa-custom fa-circle-o"></i>Well trained & Professional Drivers</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Affordable Cost</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Friendly and Communicative</p>
                        </div>

                    </div>
                </li>

                <li>
                    <div class="slider-img-wrap"><img src="{{ asset('assets/images/3.jpg') }}" alt="System with Love"/>
                    </div>
                    <div class="slider-text-content">
                        <h1>a system that<br/>made with <span style="color:red">love</span></h1>

                        <div class="anytime-text">
                            <p><i class="fa fa-custom fa-circle-o"></i>Get quickest quote</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Easy going web interface</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Customized booking</p>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="slider-img-wrap"><img src="{{ asset('assets/images/4.jpg') }}" alt="Book Online"/></div>
                    <div class="slider-text-content">
                        <h1>book your taxi<br/>online now</h1>

                        <div class="anytime-text">
                            <p><i class="fa fa-custom fa-circle-o"></i>Well trained & Professional Drivers</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Affordable Cost</p>

                            <p><i class="fa fa-custom fa-circle-o"></i>Friendly and Communicative</p>
                        </div>

                    </div>
                </li>


            </ul>
        </div>
    </div>
</div>
<!--/slider-->
<div class="welcome-message">
    <div class="container">
        <div class="row text-center">
            <h2>Excellent Gatwick Airport Taxi Service</h2>
        </div>
        <div class="row text-center">
            <h4>Travelling To/From Gatwick Airport? Check Our Price And Book Online / By Phone</h4>
        </div>
    </div>
</div>

<!-- Booking now form wrapper html start -->
<div class="booking-form-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="form-wrap">
                        <div class="form-headr"></div>
                        <h2>Get Instant Quote Online</h2>

                        <div class="form-select">
                            <form method="post" action="{!!url()!!}/result" id="booking">
                                <div class="col-md-2">
                                    <div class="row">
                                        <span style="font-size: 15px" class="normal-screen">Pickup</span>
                                    </div>
                                </div>

                                <div class="col-md-10 custom-select-box tec-domain-cat3">
                                    <div class="row">
                                        <div id="panel">
                                            <input type="text" id="start" name="pickup"
                                                   value="<?= ($sessData == NULL) ? '' : $sessData['pickup'] ?>"
                                                   class="form-control tec-domain-cat16"/>
                                            <!--                                            <span id='check' class="check" style='display:none;'></span>-->
                                        </div>

                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="row custom-responsive-add-remove-waypoints">
                                        <div style="text-align: right">
                                            <!-- Button trigger modal -->
                                            <span style="font-weight: bold" id="way_point_view">0</span> way point(s) is
                                            added
                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                                    data-target="#myModal">
                                                Add/Remove waypoints
                                            </button>
                                        </div>
                                        <br/>

                                        <!-- Modal -->
                                        <div class="modal fade" id="myModal" tabindex="-1"
                                             aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Add up to 4
                                                            waypoints</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="custom-select-box tec-domain-cat4">
                                                                    <div class="row">
                                                                        <input
                                                                            value="<?= ($sessData == NULL) ? '' : $sessData['waypoint_1'] ?>"
                                                                            type="text" id="waypoint_1"
                                                                            name="waypoint_1"
                                                                            class="col-xs-10 tec-domain-cat16 col-md-10 col-lg-10 waypoint_geocode"/>
                                                                        <a href="javascript:void(0);"
                                                                           class="waypoint_del" id="waypoint_1__del"
                                                                           style="font-size: 15px;">Clear</a>
                                                                    </div>
                                                                </div>
                                                                <div class="custom-select-box tec-domain-cat4">
                                                                    <div class="row">
                                                                        <input
                                                                            value="<?= ($sessData == NULL) ? '' : $sessData['waypoint_2'] ?>"
                                                                            type="text" id="waypoint_2"
                                                                            name="waypoint_2"
                                                                            class="col-xs-10 tec-domain-cat16 col-md-10 col-lg-10 waypoint_geocode"/>
                                                                        <a href="javascript:void(0);"
                                                                           class="waypoint_del" id="waypoint_2__del"
                                                                           style="font-size: 15px;">Clear</a>
                                                                    </div>
                                                                </div>
                                                                <div class="custom-select-box tec-domain-cat4">
                                                                    <div class="row">
                                                                        <input
                                                                            value="<?= ($sessData == NULL) ? '' : $sessData['waypoint_3'] ?>"
                                                                            type="text" id="waypoint_3"
                                                                            name="waypoint_3"
                                                                            class="col-xs-10 tec-domain-cat16 col-md-10 col-lg-10 waypoint_geocode"/>
                                                                        <a href="javascript:void(0);"
                                                                           class="waypoint_del" id="waypoint_3__del"
                                                                           style="font-size: 15px;">Clear</a>
                                                                    </div>
                                                                </div>
                                                                <div class="custom-select-box tec-domain-cat4">
                                                                    <div class="row">
                                                                        <input
                                                                            value="<?= ($sessData == NULL) ? '' : $sessData['waypoint_4'] ?>"
                                                                            type="text" id="waypoint_4"
                                                                            name="waypoint_4"
                                                                            class="col-xs-10 tec-domain-cat16 col-md-10 col-lg-10 waypoint_geocode"/>
                                                                        <a href="javascript:void(0);"
                                                                           class="waypoint_del" id="waypoint_4__del"
                                                                           style="font-size: 15px;">Clear</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary" id="add_waypoints"
                                                                data-dismiss="modal">Done
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="row">
                                        <span style="font-size: 15px" class="normal-screen">Dropoff</span>
                                    </div>
                                </div>
                                <div class="col-md-10 custom-select-box tec-domain-cat4">
                                    <div class="row">
                                        <div>
                                            <input value="<?= ($sessData == NULL) ? '' : $sessData['dropoff'] ?>"
                                                   type="text" id="end" name="dropoff"
                                                   class="form-control tec-domain-cat16"/>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <input type="hidden" name="pickup_postcode" id="pickup_postcode"/>
                                    <input type="hidden" name="dropoff_postcode" id="dropoff_postcode"/>
                                    <input type="hidden" name="distances" id="distances"/>
                                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                                </div>

                                <!--/new section for pickup time-->

                                <div style="float: left; clear:right; opacity: .75"></div>

                                <div style="float: right; clear:right; text-align: right">
                                    <button type="button" id="reset_index" class="btn btn-danger btn-sm">Reset</button>
                                </div>
                                <div class="form-button" style="clear: both">
                                    <button type="button" id="save" class="btn form-btn btn-lg btn-block">
                                        <span class="ssl-logo-small" style="float:left; padding-left: 10px"><img
                                                src="{{ asset('assets/images/big-rapidsslsiteseal.jpg') }}"
                                                alt="Secured By RapidSSL"/></span>
                                        <span style="margin-left: -55px;">Quote Now</span>
                                    </button>

                                </div>
                                <div class="col-md-12 cards-image-home-page" style="clear: both; text-align: center">
                                    <img src="{{ asset('assets/images/paypal.jpg') }}" />
                                    <img src="{{ asset('assets/images/cards-normal.png') }}" />
                                </div>

                            </form>
                            <!-- Errors Modal -->


                            <!-- /Errors Modal -->


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                @if(count($special_routes) > 0)
                <div class="row">
                    <div class="sr-wrap">
                        <div class="special-offer-header active special-offer-bg" >
                            <h2 class="special-offer-h2">SPECIAL OFFERS</h2>
                        </div>
                        <div class="text-center special-offer-text">
                            <h4>To get the special offer find your route below</h4>
                        </div>
                        <div class="special-offer-routes">
                        <table class="table table-hover">
                            <thead>
                                <tr class="route-head">
                                    <td>Journeys</td>
                                    <td>Price</td>
                                    <td>Click</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($special_routes as $sr)
                                <tr class="route-body">
                                    <td><a href="{!! url() !!}/sr_select_car/{!! $sr->id !!}" class="journey-td">{!! $sr->pickup_address !!} - {!! $sr->dropoff_address !!}</a></td>
                                    <td>&pound;{!! number_format($sr->single_1,2) !!} (From)</td>
                                    <td><a type="button" class="btn btn-xs btn-primary-route" href="{!! url() !!}/sr_select_car/{!! $sr->id !!}"">Select</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="anytime-text" style="font-size: 28px;">
                        <p><i class="fa fa-custom fa-circle-o"></i>Get quickest quote</p>

                        <p><i class="fa fa-custom fa-circle-o"></i>Easy going web interface</p>

                        <p><i class="fa fa-custom fa-circle-o"></i>Customized booking</p>

                        <p><i class="fa fa-custom fa-circle-o"></i>Well trained & Professional Drivers</p>

                        <p><i class="fa fa-custom fa-circle-o"></i>Affordable Cost</p>

                        <p><i class="fa fa-custom fa-circle-o"></i>Friendly and Communicative</p>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="label-white white-lable-m" id="" role="tablist" aria-multiselectable="true">
    <div class="container features">
        <div class="row">
            <div class="col-md-4"><i class="fa fa-plane"></i> Instant Online Quote</div>
            <div class="col-md-4"><i class="fa fa-plane"></i> Easy 3 Steps Booking</div>
            <div class="col-md-4"><i class="fa fa-plane"></i> Book Advance Online or Phone</div>
        </div>
        <div class="row">
            <div class="col-md-4"><i class="fa fa-plane"></i> Free Child/Booster Seat</div>
            <div class="col-md-4"><i class="fa fa-plane"></i> No Extra Charge For Delayed Flight</div>
            <div class="col-md-4"><i class="fa fa-plane"></i> Pay by Debit, Credit Card or Paypal</div>
        </div>
        <div class="row">
            <div class="col-md-4"><i class="fa fa-plane"></i> Discount on Many Destinations</div>
            <div class="col-md-4"><i class="fa fa-plane"></i> 8 Seaters Minibus Available</div>
            <div class="col-md-4"><i class="fa fa-plane"></i> Based Near Gatwick</div>
        </div>
    </div>
</div>

<!-- label white html start -->
<div class="label-white white-lable-m-features hide" id="" role="tablist" aria-multiselectable="true">
    <div class="container description">
        @if(isset($routing))
            <div class="row" id="">
                <p>{!! $routing->description !!}</p>
            </div>
        @else
            <div class="row" id="">
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
            </div>
        @endif
    </div>
</div>
<!-- label white html exit -->

<!-- label yellow-2 html start -->
<div class="yellow-label-wrapper hide-phone">
    <div class="label-yellow2 stellar" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="">
        <div class="container">
            <div class="row">
                <h2 class="route-heading">Excellent Gatwick Airport Taxi Service</h2>
                @if(isset($routing))
                    <h4 class="route-description">{!! $routing->description !!}</h4>
                @else
                    <h4 class="route-description">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore.</h4>
                @endif

                <div class="col-sm-2">
                    <div class="page7-j0urney-wrap">
                        <div class="smialy">
                            <a href=""><img class="img-responsive" src="{{ asset('assets/images/airports/1.png') }}"
                                                            alt=""/></a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="page7-j0urney-wrap">
                        <div class="smialy"><a href=""><img class="img-responsive" src="{{ asset('assets/images/airports/2.png') }}"
                                                            alt=""/></a></div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="page7-j0urney-wrap">
                        <div class="smialy"><a href=""><img class="img-responsive" src="{{ asset('assets/images/airports/3.png') }}"
                                                            alt=""/></a></div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="page7-j0urney-wrap">
                        <div class="smialy"><a href=""><img class="img-responsive" src="{{ asset('assets/images/airports/4.png') }}"
                                                            alt=""/></a></div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="page7-j0urney-wrap">
                        <div class="smialy"><a href=""><img class="img-responsive" src="{{ asset('assets/images/airports/5.png') }}"
                                                            alt=""/></a></div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="page7-j0urney-wrap">
                        <div class="smialy"><a href=""><img class="img-responsive" src="{{ asset('assets/images/airports/6.png') }}"
                                                            alt=""/></a></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- label yellow-2 html exit -->


<!-- end: page -->
@stop
