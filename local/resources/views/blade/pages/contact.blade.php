@extends('blade.layout.master')

@section('title','| Contact')
@section('description','Asiana cars ltd. is a taxi booking system. Here you can contact with the administrator and let us know your query.')

@section('content')

@include('flash::message')
@include('blade.common.error-message')

<!-- start: page -->

<div class="container">
    <div class="row page25-container page25-container-custom">
        <div class="col-sm-12">
            <div class="row">

                <img src="{{ asset('assets/images/contact.png') }}" style="width: 100%;" alt="Banner">

                <div class="" style="margin-top: 0px">

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <h2>Contact Us</h2>
                                <p class="" style="font-size: 20px;" align="justify">
                                    <span>Call Us</span><br/>
                                    UK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 01737 216947<br/>
                                    International&nbsp;&nbsp;: 0044 1737 216947<br/>
                                    Mobile&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 07834 372847<br/>
                                    <span>Email Us</span><br/>
                                    info@asianacars.com<br/><br/>

                                    <span>Send Us a Meassage</span>
                                </p>
                                <form  method="post" action="{!!url()!!}/contact_submit" id="contact_submit" class="form-horizontal">
                                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                                <div class="form-group col-sm-12">
                                    <div >
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Name*">
                                    </div>
                                </div>

                                <div class="form-group col-sm-12">
                                    <div >
                                        <input type="Email" class="form-control" name="email" id="email" placeholder="Email Address*" required>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <div >
                                        <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number*" >
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <div >
                                        <textarea class="form-control" name="message" id="message" rows="3" placeholder="Message"></textarea>
<!--                                        <input type="text" class="form-control" name="message" id="message" placeholder="Message*" >-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-offset-2 col-sm-5">
                                        <div class="completing-form-btnwrap completing-form-btnwrap2">
                                            <button type="button" id="contact_button" class="btn form-btn  btn-block">Submit</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
                <!--/new div-->

            </div>


        </div>
    </div>
</div>

<!-- end: page -->
@stop