@extends('blade.layout.master')

@section('title','| Result')
@section('description','Asiana Cars Ltd. is an online airport taxi booking system. This page shows the details information of your routes and available vehicles. You can choose one of the cars that can serve your need. You can also make booking for return trip. Here you are booking taxi from '.$pickup.' to '.$dropoff.' total distance '.$routesArray['distance_total_miles_text'])

@section('content')

@include('flash::message')
@include('blade.common.error-message')

<!-- start: page -->

<div class="container result_container">
<div class="row page25-container">
<div class="col-sm-12">
<div class="row top-start-responsive">

    <div class="col-xs-12">
        <div class="row" style="text-align: right; margin-right: 10px">
            <a href="{{URL::to('/')}}" id="back_btn_top" style="width:80px" class="btn btn-sm btn-primary back_btn">Back</a>
        </div>
    </div>

<!--new div-->
<div class="result-top-desc" style="">
    <div class="page25-content-header"></div>



    <div class="col-sm-3">
        <div class="row">
            <div class="Proin-text-wrap">
                <a href="#"><h6>Pickup Address</h6></a>
                <p>{{$pickup}}</p>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="row">
            <div class="Proin-text-wrap">
                <a href="#"><h6>Drop off Address</h6></a>
                <p>{{$dropoff}}</p>
            </div>
        </div>
    </div>

    <div class="col-sm-2">
        <div class="row">
            <div class="Proin-text-wrap">
                @if(Request::segment(1) != 'sr_select_car')
                <a href="#"><h6>Est. Distance</h6></a>
                <p>{{$routesArray['distance_total_miles_text']}}</p>
                @endif
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="row" style="float:right">
            <img class="img-responsive" style="width:100%" src="{{ $mapImage }}" alt="map image"/>
        </div>
    </div>

</div>
<!--/new div-->

@foreach($carList as $key => $cardetails)

<div class="page25-content">
    <div class="page25-content-header"></div>
    <div class="col-sm-3 col-xs-12">
        <div class="row">
            <div class="p25-minicar-wrap">
                <div class="p25-minicar"><a href=""><img src="{!! asset('assets/images/cars/') !!}/{!! $cardetails->car_pic !!}" alt="car image" width="132px" /></a></div>
            </div>
        </div>
    </div>
    <div class="col-sm-3 col-xs-12">
        <div class="row result-main">
            <div class="Proin-text-wrap pa25-btn-wrap">
                <a href=""><h6>{{ $cardetails->name }}</h6></a>
                <p class="discount"> @if(Request::segment(1) != 'sr_select_car')5% Discount Included on Return Journey. @endif</p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="row">
            <div class="">
                <form  method="post" action="{!!url()!!}/booking" id="booking_result" name="booking_result">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <input type="hidden" name="distances" value="{{$routesArray['distance_total_miles']}}"  />
                    <input type="hidden" name="pickup" value="{{$pickup}}"  />
                    <input type="hidden" name="dropoff" value="{{$dropoff}}"  />
                    <input type="hidden" name="via_points" value="{{$via_points}}"  />
                    <input type="hidden" name="carname" value="{{$cardetails->id}}"  />
                    <input type="hidden" name="carfullname" value="{{$cardetails->name}}"  />
                    <input type="hidden" name="car_pic" value="{{$cardetails->car_pic}}"  />

                    <input type="hidden" name="tourdate" value="{{ $tourdate }}"  />
                    <input type="hidden" name="tourtime" value="{{ $tourtime }}"  />

                    <input type="hidden" name="singleTrip" value="{{ $routesArray['carPrices'][$cardetails->id]['price'] }}"  />
                    <input type="hidden" name="roundTrip" value="{{ $routesArray['carPrices'][$cardetails->id]['priceReturn'] }}"  />
                    <input type="hidden" name="singleTripSpecialDate" value="{{ $routesArray['carPrices'][$cardetails->id]['priceSpecialDate'] }}"  />
                    <input type="hidden" name="roundTripSpecialDate" value="{{ $routesArray['carPrices'][$cardetails->id]['priceSpecialDateReturn'] }}"  />

                    <div class="col-md-6 col-xs-12 result-block">
                        <div class="row">
                            <div class="pa25-btn-wrapper">
                                <div class="pa25-btn-wrap">
                                    <a href=""><h6></h6></a>
                                    <h5>
                                        <input type="radio" class="singleTrip bookingOption" id="single_{!! $cardetails->id !!}" name="totalprice" required="required" value="{{ $routesArray['carPrices'][$cardetails->id]['price'] }}" />
                                        <label for="single_{!! $cardetails->id !!}">Single</label>
                                        <strong>{{ $routesArray['carPrices'][$cardetails->id]['currencySymbol'] }}{{ $routesArray['carPrices'][$cardetails->id]['priceFormatted'] }}
                                        </strong>
                                    </h5>
                                    <h5>
                                        <input type="radio" class="roundTrip bookingOption" id="round_{!! $cardetails->id !!}" name="totalprice" value="{{ $routesArray['carPrices'][$cardetails->id]['priceReturn'] }}" />
                                        <label for="round_{!! $cardetails->id !!}">Return</label>
                                        <strong>{{ $routesArray['carPrices'][$cardetails->id]['currencySymbol'] }}{{ $routesArray['carPrices'][$cardetails->id]['priceReturnFormatted'] }}
                                        </strong>
                                    </h5>
<!--                                    5 discount-->
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="row">
                            <div class="pa25-btn-wrapper">
                                <div class="pa25-btn-wrap meetngreet">
                                    <input type="checkbox" class="meet" id="meet_{!! $cardetails->id !!}" name="meetngreet" value="6.00" />
                                    <label for="meet_{!! $cardetails->id !!}">Meet & Greet</label>
                                    <strong>{{ $routesArray['carPrices'][$cardetails->id]['currencySymbol'] }}6.00
                                    </strong>
                                </div>
                                <div class="pa25-btn-wrap">
                                    <button type="button" id="resultsave" class="btn form-btn btn-lg btn-block resultsave">Book Now</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



</div>

@endforeach

    <div class="result-bottom-desc" style="margin-top: 15px;">
        <div class="page25-content-header"></div>



        <div class="col-sm-3">
            <div class="row">
                <div class="Proin-text-wrap">
                    <a href="#"><h6>Pickup Address</h6></a>
                    <p>{{$pickup}}</p>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="row">
                <div class="Proin-text-wrap">
                    <a href="#"><h6>Drop off Address</h6></a>
                    <p>{{$dropoff}}</p>
                </div>
            </div>
        </div>

        <div class="col-sm-2">
            <div class="row">
                <div class="Proin-text-wrap">
                    <a href="#"><h6>Est. Distance</h6></a>
                    <p>{{$routesArray['distance_total_miles_text']}}</p>
                </div>
            </div>
        </div>

        <!--    <div class="col-sm-1">-->
        <!--        <div class="row">-->
        <!--            <div class="pa28-text-wrap">-->
        <!--                <h2>$64.99</h2>-->
        <!--                <p>Fare Details</p>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->

        <div class="col-sm-4">
            <div class="row" style="float:right">
                <img class="img-responsive" style="width:100%" src="{{ $mapImage }}" alt="map image"/>
            </div>
        </div>

    </div>
</div>


</div>
</div>
</div>
<!-- end: page -->
@stop
