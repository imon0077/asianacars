@extends('blade.layout.master')

@section('title','| About')
@section('description','Asiana cars ltd. is a taxi booking system. Here you can know details about this system.')

@section('content')

@include('flash::message')
@include('blade.common.error-message')

<!-- start: page -->

<div class="container">
    <div class="row page25-container page25-container-custom">
        <div class="col-sm-12">
            <div class="row">

                <img src="{{ asset('assets/images/about.png') }}" style="width: 100%;" class="hide-phone" alt="About">

                <div class="" style="margin-top: 0px">
                    <!--div class="page25-content-header"></div-->

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2>About Us</h2>
                                <p class="" style="font-size: 20px;" align="justify">Asiana Cars Ltd. is a private hire company based in Surrey and around Gatwick/Heathrow fully licensed by Reigate & Banstead carriage office. We have a wide range of services which are flexible to your transport needs including local and national private hire vehicles, airport transfers, meet & greet.<br/><br/>

We operate 24 hours a day, 7 days a week and are available to take bookings, pre­paid and return journeys. For your convenience we accept a wide range of payments options including credit/debit cards. Our online booking system is fully secure with 128 Bit SSL, as we understand the sensitivity of your privacy.
<br/><br/>

We have got a mixed of cars including of Saloon (Sedan), Estate (Station Wagon) and also MPV (Passenger Minivans). It indicates whatever your need, i. e. a journey to the airport/station, the theatre/cinema, family outing, can be catered with the most appropriate type of taxi.
<br/><br/>

We provide service with qualified professionals, always available to deal with your transport needs. Cover all major airports and cruise ports along with all destinations in United Kingdom</p>


                            </div>
                        </div>
                    </div>


                </div>
                <!--/new div-->

            </div>


        </div>
    </div>
</div>

<!-- end: page -->
@stop