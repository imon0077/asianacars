<!-- js -->
<!--<script src="{{ asset('assets/js/jquery.js') }}"></script>-->
<script src="{{ asset('assets/js/menu/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/menu/modernizr.custom.js') }}"></script>

{{--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>--}}
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<script src="{{ asset('assets/js/geocomplete/jquery.geocomplete.js') }}"></script>
{{--<script src="{{ asset('assets/js/g-map/map.js') }}"></script>--}}
<script src="{{ asset('assets/js/bg-slider/jquery.imagesloaded.min.js') }}"></script>
<script src="{{ asset('assets/js/bg-slider/cbpBGSlideshow.js') }}"></script>
<script src="{{ asset('assets/js/uikit.js') }}"></script>
<script src="{{ asset('assets/js/jquery.stellar.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/js/jquery.bootstrap-growl.min.js') }}"></script>
<script src="{{ asset('assets/js/custom.js') }}"></script>
<script src="{{ asset('assets/js/custom2.js') }}"></script>
<script src="{{ asset('assets/js/counter.js') }}"></script>
<script src="{{ asset('assets/js/waypoints.min.js') }}"></script>
<script src="{{ asset('assets/js/wow.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('assets/js/menu/jquery.dlmenu.js') }}"></script>
<script src="{{ asset('assets/js/menu/custom-menu.js') }}"></script>
<script src="https://checkout.stripe.com/checkout.js"></script>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    })
</script>


<script language="javascript">
    jQuery(document).ready(function($) {
        $('.datepicker').datepicker({
            autoclose: true
        });
        // hide messages
        $("#error").hide();
        $("#success").hide();

        // on submit...


        $("#contact_button").click(function() {
            $("#error").hide();

            //required:
            var name = $("input#name").val();
            var email = $("input#email").val();
            var phone_number = $("input#phone_number").val();
            var message = $("textarea#message").val();

            if (name == '' || name == null) {
                $("input#name").focus();
                $("input#name").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (email == '' || email == null) {
                $("input#email").focus();
                $("input#email").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (phone_number == '' || phone_number == null) {
                $("input#phone_number").focus();
                $("input#phone_number").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if(typeof phone_number != "number" || phone_number.length < 10){

                $("input#phone_number").focus();
                $("input#phone_number").addClass('has-error');
                $.bootstrapGrowl('Please enter a valid phone number! It should be at least 10 digit number', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (message == '' || message == null) {
                $("input#message").focus();
                $("input#message").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else{
                $("#contact_submit").submit();
                return true;
            }

        });



        $("#save").click(function() {
            $("#error").hide();

            //required:
            var pickup = $("input#start").val();
            var dropoff = $("input#end").val();

            if (pickup == '' || pickup == null) {
                $("input#start").focus();
                $("input#start").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (dropoff == '' || dropoff == null) {
                $("input#end").focus();
                $("input#end").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (dropoff == pickup) {
                $("input#end").focus();
                $("input#end").addClass('has-error');
                $.bootstrapGrowl('Pickup & Dropoff address never be same', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else{
                $("#booking").submit();
                    return true;
            }

//            var tourdate = $("input#tourdate").val();
//            var tourtime = $("select#tourtime").val();
//            var tourmin = $("select#tourmin").val();
//
//
//            var startDateTime = tourdate+' '+tourtime+':'+tourmin;
//
//            var currentTime = new Date();
//            var orderTime = moment(currentTime).add(1,'d').format('MM/DD/YYYY HH:mm');
//
//
//            if (tourdate == '' || tourdate == null || tourtime == '' || tourtime == null) {
//                $("input#tourdate").focus();
//                $("input#tourdate").addClass('has-error');
//                $.bootstrapGrowl('Please enter the required fields!', {
//                    type: 'danger',
//                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
//                });
//                return false;
//            }
//            else{
//                //console.log(startDateTime+' '+orderTime);
//                if(moment(startDateTime).isAfter(orderTime)){
//                    $("#booking").submit();
//                    return true;
//                }
//                else{
//                    $("input#tourdate").addClass('has-error');
//                    $.bootstrapGrowl('Please allow at least 24 hour(s) for online bookings.', {
//                        type: 'danger',
//                        width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
//                    });
//                    return false;
//                }
//            }

        });

        $('.resultsave').click(function(){
            var thisForm = $(this).closest('form');
            //var tripType = $("input[type='radio'][name='totalprice']:checked").val();
            var tripType = thisForm.find("input[type='radio'][name='totalprice']:checked").length;
            console.log(tripType);
            if(tripType == '0' || tripType == '' || tripType == null || tripType == 'undefined'){
                $.bootstrapGrowl('Please select a trip!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
            }
            else{
                thisForm.submit();
            }
        });




        //=== stripe
        var handler = StripeCheckout.configure({
            key: $('#stripePublishableKey').val(), //'pk_test_5ELZbytCg4I78QOqQPZdcnPj',
            image: $("#baseURL").val()+'/assets/images/logo.png',
            locale: 'auto',
            token: function(token) {
                //alert(token.id)
                // Use the token to create the charge with a server-side script.
                // You can access the token ID with `token.id`
                $('#card_payment_status').html("Please wait while processing....");
                $('#payment_with_stripe').modal();
                $.ajax({
                    type     : "POST",
                    // url      : $(this).attr('action') + '/store',
                    url      : $("#baseURL").val()+"/stripeCharge",
                    data     : "tokenID="+token.id+"&tokenEmail="+token.email+'&description='+siteName+' ### '+description+'&currency=GBP&amount='+totalPrice,
                    cache    : false,

                    success  : function(data) {

                        //$('#payment_with_stripe').modal();
                        //$("#booking_submit").submit();

                        //console.log(data);
                        //=== check for any error
                        if(data == 'error_card' || data == 'error_info'){
                            if(data == 'error_card') $('#card_payment_status').html("<b style='color: red'>Unfortunately your card has been declined.</b>");
                            if(data == 'error_info') $('#card_payment_status').html("<b style='color: red'>Unfortunately your card has been declined or information is incorrect..</b>");
                        }
                        else {
                            //=== show success payment method
                            $('#card_payment_status').html("<b style='color:green'>Payment done, please wait though...</b>");

                            //===get stripe data
                            var payData = data.split('^^__^^');
                            var stripeCustomerID = payData[0];
                            var stripeChargeID = payData[1];

                            //===set stripe data as hidden form input
                            $('#stripeCustomerID').val(stripeCustomerID);
                            $('#stripeChargeID').val(stripeChargeID);

                            //=== submit form
                            $("#booking_submit").submit();

                        }
                    }
                })
            }
        });

//        $('#booking_submit_save').on('click', function(e) {
//            // Open Checkout with further options
//            handler.open({
//                name: 'Asiana Cars Ltd.',
//                description: '2 widgets',
//                currency: "gbp",
//                amount: 2000
//            });
//            e.preventDefault();
//        });

        // Close Checkout on page navigation
        $(window).on('popstate', function() {
            handler.close();
        });


        <!--   final booking     -->
        var totalPrice;
        var description;
        var siteName;

        $("#booking_submit_save").on('click',function(e) {
            $("#error").hide();

            //required:
            var firstname = $("input#firstname").val();
            var lastname = $("input#lastname").val();
            var email = $("input#email").val();
            var mobile = $("input#mobile").val();
            var reTourdate = $("input#re-tourdate").val();
            var redate = moment(reTourdate, 'DD/MM/YYYY').format('MM/DD/YYYY');
            var reTourtime = $("select#re-tourtime").val();
            var reTourmin = $("select#re-tourmin").val();
            var tourType = $("input#tourType").val();

            var no_of_person = $("input#no_of_person").val();
            var no_of_suitcase = $("input#no_of_suitcase").val();
            var no_of_child = $("input#no_of_child").val();

            var pickup_address1 = $("input#pickup_address1").val();
            var pickup_postcode = $("input#pickup_postcode").val();
            var dropoff_address1 = $("input#dropoff_address1").val();
            var dropoff_postcode = $("input#dropoff_postcode").val();

            var minimum_booking_time = $("input#minimum_booking_time").val();
            minimum_booking_time = parseInt(minimum_booking_time);
            console.log(minimum_booking_time);
            var tourdate = $("input#tourdate").val();
            var date = moment(tourdate, 'DD/MM/YYYY').format('MM/DD/YYYY');
            var tourtime = $("select#tourtime").val();
            var tourmin = $("select#tourmin").val();
            var startTime = date+' '+tourtime+':'+tourmin;
            var returnDateTime = redate+' '+reTourtime+':'+reTourmin;

            var thisForm = $(this).closest('form');
            var payment_method = $("input[type='radio'][name='payment_method']:checked").val();

            var currentTime = new Date();
            var orderTime = moment(currentTime).add(minimum_booking_time,'h').format('MM/DD/YYYY HH:mm');
            //console.log(date);
            //console.log(startTime);
            //console.log(orderTime);
            //console.log(moment(startTime).isBefore(orderTime));
            //var date = moment('15/07/2011', 'DD/MM/YYYY').format('MM/DD/YYYY');
            //console.log(date);
            //14/09/2015 20:56
            if (firstname == '' || firstname == null) {
                $("input#firstname").focus();
                $("input#firstname").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (lastname == '' || lastname == null) {
                $("input#lastname").focus();
                $("input#lastname").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (email == '' || email == null) {
                $("input#email").focus();
                $("input#email").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (mobile == '' || mobile == null) {
                $("input#mobile").focus();
                $("input#mobile").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if(isNaN(mobile) || mobile.length < 10){

                $("input#mobile").focus();
                $("input#mobile").addClass('has-error');
                $.bootstrapGrowl('Please enter a valid phone number! It should be at least 10 digit number', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (pickup_address1 == '' || pickup_address1 == null) {
                $("input#pickup_address1").focus();
                $("input#pickup_address1").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            /*
            else if (pickup_postcode == '' || pickup_postcode == null) {
                $("input#pickup_postcode").focus();
                $("input#pickup_postcode").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            */
            else if (dropoff_address1 == '' || dropoff_address1 == null) {
                $("input#dropoff_address1").focus();
                $("input#dropoff_address1").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            /*
            else if (dropoff_postcode == '' || dropoff_postcode == null) {
                $("input#dropoff_postcode").focus();
                $("input#dropoff_postcode").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            */
            else if (tourdate == '' || tourdate == null || tourtime == '' || tourtime == null) {
                $("input#tourdate").focus();
                $("input#tourdate").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields datetime!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (tourType == 'round' && (reTourdate == '' || reTourdate == null || reTourtime == '' || reTourtime == null)) {
                $("input#re-tourdate").focus();
                $("input#re-tourdate").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if(moment(orderTime).isAfter(startTime)){
                $("input#tourdate").addClass('has-error');
                $.bootstrapGrowl('Please Allow ' +minimum_booking_time+ ' Hour(s) For Online Booking, Please Call For Urgent Booking.', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if(tourType == 'round' && moment(startTime).isAfter(returnDateTime)){
                $("input#re-tourdate").addClass('has-error');
                $.bootstrapGrowl('Return schedule should be after Start schedule.', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (no_of_person == '' || no_of_person == null) {
                $("input#no_of_person").focus();
                $("input#no_of_person").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (no_of_suitcase == '' || no_of_suitcase == null) {
                $("input#no_of_suitcase").focus();
                $("input#no_of_suitcase").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if (no_of_child == '' || no_of_child == null) {
                $("input#no_of_child").focus();
                $("input#no_of_child").addClass('has-error');
                $.bootstrapGrowl('Please enter the required fields!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
                return false;
            }
            else if(payment_method == '' || payment_method == null || payment_method == 'undefined'){
                $.bootstrapGrowl('Please select payment method!', {
                    type: 'danger',
                    width: 'auto', align: 'center', allow_dismiss: true,  offset: {from: 'top', amount: 200}, delay: 8000
                });
            }
            else if(payment_method == 'paypal'){
                //=== submit form
                $("#booking_submit").submit();
            }
            else{
                // Open Checkout with further options
                totalPrice = $('#totalprice').val() * 100;
                description = 'Trip for '+$('#firstname').val() +' '+ $('#lastname').val();
                siteName = $('#siteName').val();

                handler.open({
                    name: siteName,
                    description: description,
                    currency: "gbp",
                    amount: totalPrice
                });
                e.preventDefault();
                //$('#payment_with_stripe').modal();
                //$("#booking_submit").submit();
                return true;
            }

        });
        <!--   /final booking   -->




        return false;
    });


</script>
@if(Request::segment(1) == 'booking')
<script>
    var specialDate = ['25/12','26/12','31/12','01/01'];
    jQuery(document).ready(function($){
        function priceChange(selectedDate){
            var totalPriceSpecialDate = $('#totalPriceSpecialDate').val();
            var totalpriceregular = $('#totalpriceregular').val();

            if($.inArray(selectedDate, specialDate) < 0){
                $('#totalprice').val(totalpriceregular);
                var formattedPrice = parseFloat(totalpriceregular).toFixed(2);
                $('#price_show').html('&pound;'+formattedPrice);
                $('.specialcharge').hide();
                $('#special_date').val(0);
            }
            else{
                $('#totalprice').val(totalPriceSpecialDate);
                var formattedRegularPrice = parseFloat(totalpriceregular).toFixed(2);
                var formattedPrice = parseFloat(totalPriceSpecialDate).toFixed(2);
                var additionalPrice = formattedPrice - formattedRegularPrice;
                $('#price_show').html('&pound;'+formattedPrice);
                $('#additionalPrice').html(additionalPrice.toFixed(2));
                $('#specialPrice').modal();
                $('.specialcharge').show();
                $('#special_date').val(1);
            }
        }

        $('#tourdate').change(function(){
            var tourdate = $(this).val();
            $('#re-tourdate').val('');
            var selectedDate = tourdate.substr(0, 5);
            priceChange(selectedDate);
        });

        $('#re-tourdate').change(function(){
            var tourdate = $('#tourdate').val();
            var re_tourdate = $(this).val();
            var selectedDate = tourdate.substr(0, 5);
            var selectedReDate = re_tourdate.substr(0, 5);
            if($.inArray(selectedDate, specialDate) < 0){
                priceChange(selectedReDate);
            }
        });

        var tourdate = $('#tourdate').val().substr(0, 5);
        var re_tourdate = $('#re-tourdate').val().substr(0, 5);
        if($.inArray(tourdate, specialDate) >= 0){
            priceChange(tourdate);
        }else if($.inArray(re_tourdate, specialDate) >= 0){
            priceChange(re_tourdate);
        }
    });
</script>
@endif