<div id="preloader">
    <div class="preloader-container">
        <img src="{{ asset('assets/images/preloader.gif') }}" class="preload-gif" alt="preload-image">
    </div>
</div>

<!--map-wrappaer-opacity start-->
<div class="map-wapper-opacity">

  <div class="yellow-bg-header">
        <span class="phone_numbers">
                                <span class="phone_numbers_title">UK</span> : 01737 216947,
                                <span class="phone_numbers_title">International</span> : 0044 1737 216947,
                                <span class="phone_numbers_title">Mobile</span> : 07834 372847
                            </span>
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-sm-4 top-call-us">
                    <div class="call-us">
                        <!--span class="img-circle"><i class="fa fa-phone"></i></span>
                        <!--h5>+44 1737668060</h5-->
                    </div>
                    <div class="call-us book-journey">
                        <div class="col-xs-12">

                        </div>
                        <!--div class="col-xs-6">
                            <a href="index" style="color: #C31A31;">To/From Gatwick</a> <br>
                            <a href="index" style="color: #C31A31;">To/From Heathrow</a>
                        </div-->
                    </div>
                </div>
                <div class="col-sm-3 top-logo">
                    <div class="logo-wraper">
                        <div class="logo">
                            <a href="{!!url()!!}">
                                <img src="{{ asset('assets/images/logo.png') }}" alt="Asiana Cars Ltd.">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 top-social">
                    <div id="languages" class="resister-social">


                        <!--div class="social-icon">
<!--                            <a href="#" class="img-circle">-->
<!--                                <i class="fa fa-home"></i>-->
<!--                            </a>
                            <a href="#" class="img-circle">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="#" class="img-circle">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="#" class="img-circle">
                                <i class="fa fa-tumblr"></i>
                            </a>
                        </div-->

                        <div class="login-register">
                            <a href="{!!url()!!}/index">Home</a>
                            <a href="{!!url()!!}/about">About Us</a>
                            <a href="{!!url()!!}/corporate">Corporate</a>
                            <a href="{!!url()!!}/contact">Contact Us</a>
                            <a href="{!!url()!!}/tnc">T&C</a>
                        </div>

                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="header-menu-wrap" style="display: none">
                        <nav class="navbar navbar-default" role="navigation">
                            <div class="main  collapse navbar-collapse">
                                <div class="column">
                                    <div id="dl-menu" class="dl-menuwrapper">
                                        <a href="#" class="dl-trigger" data-toggle="dropdown"><i class="fa fa-align-justify"></i></a>
                                        <ul class="dl-menu">
                                            <li><a href="index">Home Page</a></li>
                                            <li><a href="">Result Page</a></li>
                                            <li><a href="">Booking Page</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

  </div>

</div>


<!-- end -->


<div class="map-wapper-opacity-responsive">
    <div class="container">
        <div class="row col-xs-12">
            <div class="col-xs-12 screen-320">
                <a href="index">
                    <img class="site_logo" src="{{ asset('assets/images/logo.png') }}" alt="">
                </a>
            </div>
            <div style="margin-top: -25px; margin-bottom: 7px; text-align: center; padding-left: 50px" class="col-xs-12">
                <a href="{!!url()!!}/index" class="res_link"><i class="fa fa-home fa-lg"></i></a>&nbsp;&nbsp;
                <a href="{!!url()!!}/about" class="res_link"><i class="fa fa-users"></i></a>&nbsp;&nbsp;
                <a href="{!!url()!!}/contact" class="res_link"><i class="fa fa-envelope"></i></a>&nbsp;&nbsp;
                <a href="{!!url()!!}/tnc" class="res_link"><i class="fa fa-certificate fa-lg"></i></a>
            </div>
            <div class="call-us call-us-responsive text-center">
                UK: 01737 216947<br/>
                International: 0044 1737 216947<br/>
                Mobile: 07834 372847<br/>
            </div>
            {{--<div class="call-us call-us-responsive">--}}
                {{--<span class="img-circle"><i class="fa fa-plane"></i></span>--}}
                {{--<h5>Airport Taxi</h5>--}}
            {{--</div>--}}
        </div>
    </div>

</div>

<!--map-wrappaer-opacity end-->

<!--<div class="google-image">-->
<!--    <div id="directions-panel"></div>-->
<!--    <div id="map-canvas"></div>-->
<!--</div>-->