<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>@if(Auth::guest()) Gatwick Taxi Service @yield('title') @else Gatwick Taxi Service | User Panel @endif</title>
    <meta name="keywords" content="@yield('keywords')" />
    <meta name="description" content="@yield('description')">
    <meta name="author" content="ris10.com">
    <meta name="csrf-token" content="{{{ csrf_token() }}}" />

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/images/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/images/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/images/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/images/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/images/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/images/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/images/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/images/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/images/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/images/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/images/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('assets/images/favicon/manifest.json') }}">

    <!-- Mobile Metas -->
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <!-- Web Fonts  -->

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/color.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/custom-responsive.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/component.css') }}" />

    <link rel="stylesheet" href="{{ asset('assets/css/datepicker3.css') }}" />

    <link rel="stylesheet" href="{{ asset('assets/css/slide-component.css') }}" />

    <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}" />
    <!-- font awesome this template -->
    <link rel="stylesheet" href="{{ asset('assets/fonts/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/fonts/css/font-awesome.min.css') }}" />

    <style type="text/css">
        .auto-style1 {
            color: #FF0000;
        }

        .has-error{
            border:1px solid #CC0000;
            background:#F7CBCA;
            }
    </style>

</head>

