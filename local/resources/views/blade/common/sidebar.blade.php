<aside id="sidebar-left" class="sidebar-left">

<div class="sidebar-header">
    <div class="sidebar-title">
        Navigation
    </div>
    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html"
         data-fire-event="sidebar-left-toggle">
        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
    </div>
</div>

<div class="nano">
<div class="nano-content">
<nav id="menu" class="nav-main" role="navigation">
<ul class="nav nav-main">
@if(Auth::user()->status == 1)

<li class="@if(Request::segment(1)== 'dashboard') nav-active @endif">
    <a href="{!!url()!!}/dashboard">
        <i class="fa fa-home" aria-hidden="true"></i>
        <span>Dashboard</span>
    </a>

</li>

@endif
    @if(Auth::user()->status == 1)
@if(!Auth::user()->hasRole('admin'))
    <li class="@if(Request::segment(1)== 'create-patient') nav-active @endif">
        <a href="{!!url()!!}/create-patient">
            <i class="fa fa-wheelchair" aria-hidden="true"></i>
            <span>Create Patient</span>
        </a>
    </li>
    <li class="@if(Request::segment(1)== 'create-order') nav-active @endif">
        <a href="{!!url()!!}/create-order">
            <i class="fa fa-medkit" aria-hidden="true"></i>
            <span>Create Order</span>
        </a>
    </li>
    <li class="@if(Request::segment(1)== 'patientlist') nav-active @endif">
        <a href="{!!url()!!}/patientlist">
            <i class="fa fa-indent" aria-hidden="true"></i>
            <span>Patient list</span>
        </a>
    </li>
@endif
    <li class="@if(Request::segment(1)== 'orderlist') nav-active @endif">
        <a href="{!!url()!!}/orderlist">
            <i class="fa fa-file-text" aria-hidden="true"></i>
            <span>Order list</span>
        </a>
    </li>
    @endif
@if(Auth::user()->hasRole('admin'))
<!--    <li class="@if(Request::segment(1)== 'newDoctors') nav-active @endif">-->
<!--        <a href="{!!url()!!}/newDoctors">-->
<!--            <i class="fa fa-user-md" aria-hidden="true"></i>-->
<!--            <span>Signup Requests</span>-->
<!--        </a>-->
<!--    </li>-->

    <li class="@if(Request::segment(1)== 'CreateDoctors') nav-active @endif">
        <a href="{!!url()!!}/CreateDoctors">
            <i class="fa fa-user-md" aria-hidden="true"></i>
            <span>Add Doctors</span>
        </a>
    </li>

    <li class="@if(Request::segment(1)== 'Doctors') nav-active @endif">
        <a href="{!!url()!!}/Doctors">
            <i class="fa fa-user-md" aria-hidden="true"></i>
            <span>Doctors List</span>
        </a>
    </li>

@endif

    <li class="@if(Request::segment(1)== 'profile') nav-active @endif">
        <a href="{!!url()!!}/profile">
            <i class="fa fa-user" aria-hidden="true"></i>
            <span>Profile</span>
        </a>
    </li>

@if(!Auth::user()->hasRole('admin'))
    <li class="@if(Request::segment(1)== 'contact') nav-active @endif">
        <a href="{!!url()!!}/contact">
            <i class="fa fa-mobile-phone" aria-hidden="true"></i>
            <span>Contact</span>
        </a>
    </li>
@endif
</div>

</aside>
