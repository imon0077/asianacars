<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'secret' => 'sk_test_qHO8adhMPBN5hV9CQYa2f38o',
        "publishable_key" => "pk_test_5ELZbytCg4I78QOqQPZdcnPj"
	],

//    'stripe' => [
//        'secret' => 'sk_live_Dt83WiD6L61eyO6opgEY622l',
//        "publishable_key" => "pk_live_8G4C5mV3m0riFSv3g8ufSiCn"
//    ],
];
