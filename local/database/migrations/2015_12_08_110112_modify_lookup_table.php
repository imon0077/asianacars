<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyLookupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement("ALTER TABLE `order_edit_history` CHANGE `total_bill` `total_bill` DOUBLE( 11, 2 ) NOT NULL,
        CHANGE `adjusted_bill` `adjusted_bill` DOUBLE( 11, 2 ) NOT NULL;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
