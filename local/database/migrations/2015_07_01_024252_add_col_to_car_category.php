<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToCarCategory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('car_category', function($table)
        {
            $table->integer('max_person')->after('cat_name');
            $table->integer('max_suitcases');
            $table->integer('max_carry_on');
            $table->string('car_pic');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
