<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLookupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('order_edit_history', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('order_id');
            $table->string('total_bill', 11,2);
            $table->string('pay_via');
            $table->string('card_customer_id');
            $table->string('card_charge_id');
            $table->string('adjusted_bill', 11,2);
            $table->string('re_card_customer_id');
            $table->string('re_card_charge_id');
            $table->integer('edited')->comment="0 = No; 1 = Yes;";
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
