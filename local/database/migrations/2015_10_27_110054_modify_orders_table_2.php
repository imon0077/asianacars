<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyOrdersTable2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement("ALTER TABLE `orders` ADD `pay_via` VARCHAR(128) NULL DEFAULT NULL AFTER `status`, ADD `card_customer_id` VARCHAR(256) NULL DEFAULT NULL AFTER `pay_via`, ADD `card_charge_id` VARCHAR(256) NULL DEFAULT NULL AFTER `card_customer_id`;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
