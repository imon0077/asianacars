<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSpecialRoute extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('special_route', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('pickup_address');
            $table->string('dropoff_address');
            $table->integer('distance');
            $table->string('price', 11,2);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('special_route');
	}

}
