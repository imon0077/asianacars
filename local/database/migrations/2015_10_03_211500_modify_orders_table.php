<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement("ALTER TABLE `orders` CHANGE `total_bill` `total_bill` DOUBLE( 11, 2 ) NOT NULL,
        CHANGE `total_distance` `total_distance` DOUBLE( 11, 2 ) NOT NULL,
        ADD `meetngreet` TINYINT(1) NOT NULL DEFAULT '0' AFTER `carry_on` ;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
