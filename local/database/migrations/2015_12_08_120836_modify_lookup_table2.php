<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyLookupTable2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('order_edit_history', function($table)
        {
            $table->dropColumn(array('pay_via', 'total_bill', 'card_customer_id', 'card_charge_id', 're_card_customer_id', 're_card_charge_id'));
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
