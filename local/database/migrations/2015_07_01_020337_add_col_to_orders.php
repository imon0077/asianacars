<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToOrders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('orders', function($table)
        {
            $table->string('route1_lat_lon');
            $table->string('route1_distance');
            $table->string('route1_time');
            $table->string('route1_address');

            $table->string('route2_lat_lon');
            $table->string('route2_distance');
            $table->string('route2_time');
            $table->string('route2_address');

            $table->integer('suitcases');
            $table->integer('carry_on');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
