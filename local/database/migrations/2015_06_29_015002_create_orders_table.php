<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('pickup_address')->comment = "pickup address through gmap";
			$table->string('pickup_details')->comment = "meeting point (e.g. house/flat no)";
			$table->string('dropoff_address')->comment = "dropoff address through gmap";
			$table->string('dropoff_details')->comment = "dropoff details (e.g. house/flat no)";
			$table->integer('no_of_person');
			$table->integer('no_of_child');
			$table->integer('no_of_infant');
			$table->integer('no_of_booster');
			$table->integer('car_details');
			$table->integer('total_bill');
			$table->integer('total_distance');
			$table->string('total_time');
			$table->longText('via_points');
			$table->longText('picking_category')->comment = "Like Instant, Prebook, Airport";
			$table->integer('return_booking')->comment = "0 = No, 1 = Yes";
			$table->integer('status')->comment = "0 = Pending, 1 = Approve, 2 = Reject";
			$table->integer('date_time');
			$table->integer('return_date_time');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
