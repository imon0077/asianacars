<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassengersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('passengers', function(Blueprint $table)
		{
            $table->integer('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('contact_name');
            $table->string('address');
            $table->string('gender');
            $table->string('city');
            $table->string('street');
            $table->integer('postcode');
            $table->integer('residence_no');
            $table->string('fax_number');
            $table->string('email')->unique();
            $table->string('phone_number');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('passengers');
	}

}
