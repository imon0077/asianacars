<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressFieldToOrders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('orders', function($table)
        {
            $table->string('pickup_address1')->after('pickup_address');
            $table->string('pickup_address2');
            $table->string('pickup_postcode');
            $table->string('dropoff_address1')->after('dropoff_address');
            $table->string('dropoff_address2');
            $table->string('dropoff_postcode');
            $table->string('comments')->after('return_date_time');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
