<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePickupFromAirportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pickup_from_airport', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('flight_arrival_time');
			$table->string('airline_name');
			$table->string('flight_no');
			$table->string('departure_city');
			$table->string('display_name');
			$table->integer('pickup_after')->comment="When should the driver pick you up? (Minutes after landing)";
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pickup_from_airport');
	}

}
