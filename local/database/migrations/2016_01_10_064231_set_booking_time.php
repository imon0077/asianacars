<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetBookingTime extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('booking_time', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('minimum_booking_time');
            $table->timestamps();
        });

        DB::table('booking_time')->insert(
            array(
                'minimum_booking_time' => '8'
            )
        );

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
