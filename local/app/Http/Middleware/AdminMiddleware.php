<?php namespace App\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use App\Role;
use Auth;

use Closure;

class AdminMiddleware {

	protected $auth;

  	public function __construct(Guard $auth) {
    	$this->auth = $auth;
  	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		
		if ($this->auth->guest()) {
	      if ($request->ajax()) {
	        return response('Unauthorized.', 401);
	      } else {
	        return redirect()->guest('auth/login');
	      }
	    } else if($request->user()->hasRole('1')) {
	      Route::any('dashboard', 'AdminsController@index');
	    }
        else{
	      Route::any('dashboard', 'UsersController@index');
	    }
		return $next($request);
	}

}
