<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateOrderRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'prescription_filename' => 'required',
            'note_filename' => 'required'
		];

        /*
         * return [
            'prescription_filename' => 'required|mimes:doc,docx,pdf',
            'note_filename' => 'required|mimes:doc,docx,pdf'
		];
         */
	}

}
