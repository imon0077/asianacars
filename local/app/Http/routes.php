<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/n', function(){
    //var_dump(Session::get('page1Values'));
    $sessData = Session::get('page1Values');
    return view('blade.pages.index',compact('sessData'));
});

Route::get('/', [
    'as' => 'index_new',
    'uses' => 'BookingController@index_new'
]);

Route::get('index',[
    'as' => 'index',
    'uses' => 'BookingController@index_new'
]);

Route::get('index-old', function(){
    $sessData = Session::get('page1Values');
    return view('blade.pages.index',compact('sessData'));
});

Route::get('index/{route}', [
    'as' => 'index_route',
    'uses' => 'BookingController@index_route'
]);

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
// Password reset link request routes...
//Route::get('password/email', 'PasswordController@getEmail');
Route::post('password/email', 'PasswordController@postEmail');
// Password reset routes...
Route::get('password/reset/{token}', 'PasswordController@getReset');
Route::post('password/reset', 'PasswordController@postReset');

//new route start

Route::get('BackToHome', function(){
    $sessData = Session::flush();
    return Redirect::to('/');
    //return view('blade.pages.index',compact('sessData'));
});

Route::get('about', function(){
    return view('blade.pages.about');
});

Route::get('contact', function(){
    return view('blade.pages.contact');
});

Route::get('corporate', function(){
    return view('blade.pages.corporate');
});

Route::post('contact_submit', [
    'as' => 'contact_submit',
    'uses' => 'BookingController@contact_submit'
]);

Route::post('stripeCharge', [
    'as' => 'stripeCharge',
    'uses' => 'BookingController@stripeCharge'
]);


Route::get('tnc', function(){
    return view('blade.pages.tnc');
});

Route::get('success', function(){
    return view('blade.pages.success');
});

Route::post('result', [
    'as' => 'result',
    'uses' => 'BookingController@result'
]);

Route::post('booking', [
    'as' => 'booking',
    'uses' => 'BookingController@booking'
]);

Route::post('charge', [
    'as' => 'charge',
    'uses' => 'BookingController@charge'
]);

Route::post('booking_submit', [
    'as' => 'booking_submit',
    'uses' => 'BookingController@booking_submit'
]);


//Admin route
Route::get('admin_login', function(){
    return view('admin_blade.pages.login');
});

Route::get('admin', function(){
    return Redirect::to('dashboard');
});

Route::get('dashboard', 'AdminsController@index');

Route::get('profile', [
    'as' => 'profile',
    'uses' => 'AdminsController@profile'
]);

/* Admin booking */
Route::get('Createorder', function(){
    $sessData = Session::get('page1Values');
    return view('admin_blade.pages.BookingForm',compact('sessData'));
});

Route::post('AdminBookingResult', [
    'as' => 'AdminBookingResult',
    'uses' => 'BookingController@AdminBookingResult'
]);

Route::get('route_description', [
    'as' => 'route_description',
    'uses' => 'SpecialController@route_description'
]);

Route::get('route_list', [
    'as' => 'route_list',
    'uses' => 'SpecialController@route_list'
]);

Route::post('addRouteDescription', [
    'as' => 'addRouteDescription',
    'uses' => 'SpecialController@addRouteDescription'
]);

Route::post('editRouteDescription', [
    'as' => 'addRouteDescription',
    'uses' => 'SpecialController@updateRouteDescription'
]);

Route::get('edit_routeDescription/{route_id}', [
    'as' => 'edit_RouteDescription',
    'uses' => 'SpecialController@editRouteDescription'
]);

Route::get('del_routeDescription/{route_id}', [
    'as' => 'del_RouteDescription',
    'uses' => 'SpecialController@delRouteDescription'
]);

Route::get('special_route', [
    'as' => 'special_route',
    'uses' => 'SpecialController@index'
]);

Route::get('special_route_list', [
    'as' => 'special_route_list',
    'uses' => 'SpecialController@special_route_list'
]);

Route::get('special_route_front_end', [
    'as' => 'special_route_front_end',
    'uses' => 'BookingController@special_route_front_end'
]);

Route::get('sr_select_car/{sr_id}', [
    'as' => 'sr_select_car',
    'uses' => 'BookingController@sr_select_car'
]);

Route::post('addSpecialRoute', [
    'as' => 'addSpecialRoute',
    'uses' => 'SpecialController@addSpecialRoute'
]);

Route::post('editSpecialRoute', [
    'as' => 'addSpecialRoute',
    'uses' => 'SpecialController@updateSpecialRoute'
]);

Route::get('edit_sr/{sr_id}', [
    'as' => 'edit_sr',
    'uses' => 'SpecialController@editSpecialRoute'
]);

Route::get('del_sr/{sr_id}', [
    'as' => 'del_sr',
    'uses' => 'SpecialController@delSpecialRoute'
]);

Route::post('AdminBookingFinal', [
    'as' => 'AdminBookingFinal',
    'uses' => 'BookingController@AdminBookingFinal'
]);

Route::post('AdminBookingSubmit', [
    'as' => 'AdminBookingSubmit',
    'uses' => 'BookingController@AdminBookingSubmit'
]);

Route::post('AdminEditBookingSubmit', [
    'as' => 'AdminEditBookingSubmit',
    'uses' => 'BookingController@AdminEditBookingSubmit'
]);

/* /Admin booking */


Route::get('setting', [
    'as' => 'setting',
    'uses' => 'AdminsController@setting'
]);

Route::post('setting', [
    'as' => 'setting',
    'uses' => 'AdminsController@setting_post'
]);

Route::get('editprofile/{user_id}', [
    'as' => 'editprofile',
    'uses' => 'AdminsController@editprofile'
]);

Route::post('editprofilesave', [
    'as' => 'editprofilesave',
    'uses' => 'AdminsController@editprofilesave'
]);

Route::get('pricelist', [
    'as' => 'pricelist',
    'uses' => 'AdminsController@pricelist'
]);

Route::get('edit_pricelist/{id}', [
    'as' => 'edit_pricelist',
    'uses' => 'AdminsController@edit_pricelist'
]);

Route::post('edit_pricelist_save', [
    'as' => 'edit_pricelist_save',
    'uses' => 'AdminsController@edit_pricelist_save'
]);

Route::get('orderlist', [
    'as' => 'orderlist',
    'uses' => 'AdminsController@orderlist'
]);

Route::get('conf_orderlist', [
    'as' => 'conf_orderlist',
    'uses' => 'AdminsController@conf_orderlist'
]);

Route::get('orderDetails/{orderID}', [
    'as' => 'orderDetails',
    'uses' => 'AdminsController@orderDetails'
]);

Route::get('editOrderCarDeails/{orderID}', [
    'as' => 'editOrderCarDeails',
    'uses' => 'AdminsController@editOrderCarDeails'
]);

Route::post('editOrderCarDeailsSubmit', [
    'as' => 'editOrderCarDeailsSubmit',
    'uses' => 'AdminsController@editOrderCarDeailsSubmit'
]);

Route::get('editOrderCustomerDeails/{orderID}', [
    'as' => 'editOrderCustomerDeails',
    'uses' => 'AdminsController@editOrderCustomerDeails'
]);

Route::post('editOrderCustomerDeailsSubmit', [
    'as' => 'editOrderCustomerDeailsSubmit',
    'uses' => 'AdminsController@editOrderCustomerDeailsSubmit'
]);

Route::get('editOrder/{orderID}', [
    'as' => 'editOrder',
    'uses' => 'AdminsController@editOrder'
]);

Route::post('payment', array(
    'as' => 'payment',
    'uses' => 'PaypalController@postPayment',
));

// this is after make the payment, PayPal redirect back to your site
Route::get('payment/status', array(
    'as' => 'payment.status',
    'uses' => 'PaypalController@getPaymentStatus',
));

Route::get('payment/failed', array(
    'as' => 'payment.fail',
    'uses' => 'PaypalController@paymentFailed',
));

Route::get('booking', array(
    'as' => 'payment.cancel',
    'uses' => 'PaypalController@redirectBack',
));

Route::get('payment/{totalFare}', array(
    'as' => 'payment',
    'uses' => 'PaypalController@postPayment',
));

Route::get('paypal', function(){
    return view('blade.pages.paypal');
});

Route::post('editOrder/{orderID}', [
    'as' => 'editOrder',
    'uses' => 'AdminsController@editOrder'
]);

Route::post('editOrderSubmit', [
    'as' => 'editOrderSubmit',
    'uses' => 'AdminsController@editOrderSubmit'
]);

Route::get('delOrder/{orderID}', [
    'as' => 'delOrder',
    'uses' => 'AdminsController@delOrder'
]);

Route::get('dataForEmail/{orderID}', [
    'as' => 'delOrder',
    'uses' => 'PaypalController@dataForEmail'
]);

Route::get('accept/{orderID}', [
    'as' => 'accept_doctors',
    'uses' => 'AdminsController@accept'
]);

Route::get('decline/{orderID}', [
    'as' => 'decline_doctors',
    'uses' => 'AdminsController@decline'
]);

Route::get('AddCar', [
    'as' => 'AddCar',
    'uses' => 'AdminsController@AddCar'
]);

Route::post('AddCarSave', [
    'as' => 'AddCarSave',
    'uses' => 'AdminsController@AddCarSave'
]);

Route::get('AddDriver', [
    'as' => 'AddDriver',
    'uses' => 'AdminsController@AddDriver'
]);

Route::post('AddDriverSave', [
    'as' => 'AddDriverSave',
    'uses' => 'AdminsController@AddDriverSave'
]);

Route::get('carlist', [
    'as' => 'carlist',
    'uses' => 'AdminsController@carlist'
]);

Route::get('driverlist', [
    'as' => 'driverlist',
    'uses' => 'AdminsController@driverlist'
]);

Route::get('edit_car/{car_id}', [
    'as' => 'edit_car',
    'uses' => 'AdminsController@edit_car'
]);

Route::post('editCarSave', [
    'as' => 'editCarSave',
    'uses' => 'AdminsController@editCarSave'
]);

Route::get('del_car/{car_id}', [
    'as' => 'del_car',
    'uses' => 'AdminsController@del_car'
]);

Route::get('edit_driver/{driver_id}', [
    'as' => 'edit_driver',
    'uses' => 'AdminsController@edit_driver'
]);

Route::post('editDriverSave', [
    'as' => 'editDriverSave',
    'uses' => 'AdminsController@editDriverSave'
]);

Route::get('del_driver/{driver_id}', [
    'as' => 'del_driver',
    'uses' => 'AdminsController@del_driver'
]);

Route::post('changeStatusFromAdmin', 'AdminsController@changeStatusFromAdmin');

Route::post('PaymentStatus', 'AdminsController@PaymentStatus');

Route::get('bookingTime/{id}', [
    'as' => 'bookingTime',
    'uses' => 'AdminsController@bookingTime'
]);

Route::post('bookingTimesave', [
    'as' => 'bookingTimesave',
    'uses' => 'AdminsController@bookingTimesave'
]);

//new route end

Route::get('recover-password', function(){
	return view('admin_blade.pages.recover-password');
});

Route::get('signup', 'SignupController@index');
Route::post('signup', 'SignupController@signupdoctors');


Route::get('{page}', function(){
    return view('blade.pages.page404');
});