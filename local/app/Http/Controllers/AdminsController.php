<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BookingController;

use Illuminate\Http\Request;
use Session;
use Schema;

use Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Pricelist;
use App\CarsCat;
use App\CarDetails;
use App\Drivers;
use App\bookingTime;
use App\Passengers;
use App\Airports;
use App\Orders;
use App\Setting;
use App\ordereditHistory;
use App\Http\Requests\editprofilesaveRequest;
use App\Http\Requests\editcarsaveRequest;
use App\Http\Requests\editdriveresaveRequest;
use Stripe;
use Config;

class AdminsController extends Controller {

    //private $googleAPIKey = 'AIzaSyD-OnhY1_cMrzguH_dnfm8YihGd6OGRaK4'; //server based API
    private $googleAPIKey = 'AIzaSyBVxh2byXJ1aP_FOjjVIxjPsFtnj8UV1b4'; //browser based API
    //==== Price Vars
    public $londonPrice = 0;
    public $first5Price = 15; // for 0-5 miles
    //public $flatRateMoreThan30 = 1.5;
    //public $rateGreaterThan5UpTo30 = 1.5;
    public $rateGreaterThan6UpTo20 = 1.50; // for 6-20 miles; i.e. £18.00 + 1.75/mile
    public $rateGreaterThan20UpTo35 = 1.4; // for 21-35 miles; i.e. £18.00 + 1.5/mile
    public $rateGreaterThan35UpTo55 = 1.4; // for 36-55 miles; flat rate for each mile. i.e. 39 miles * £1.5 = £58.5
    public $flatRateMoreThan55 = 1.3; // for more than 55 miles; flat rate for each mile. i.e. 90 miles * £1.3 = £117
    public $firstMileageCounter = 5;
    public $secondMileageCounter = 20;
    public $thirdMileageCounter = 35;
    public $forthMileageCounter = 55;
    public $singlPrice = 0;
    public $totalPrice = 0;
    public $returnDiscountPercent = 5;
    public $currency = 'GBP';
    public $currencySymbol = '&pound;';

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if(Auth::user()->role == 1) {
            $orderlist = DB::table('orders')
                ->leftjoin('passengers', function($join)
                {
                    $join->on('orders.user_id', '=', 'passengers.id');
                })
                ->where('orders.status', '=', 0)
                ->select('orders.id','orders.user_id', 'orders.dropoff_address', 'orders.pickup_address', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.created_at', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();

            $current_date = date('Y-m-d');
            $current_month = date('m');
            $last_month = $current_month - 1;

            $last_month_of_this_year = date('Y')."-0".$last_month."-";


            $todays_confirmed = DB::table('orders')
                ->where('orders.status', '=', 1)
                ->where('orders.updated_at', 'like', $current_date.'%')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();
            $todays_confirmed_count = count($todays_confirmed);

            $todays_recieved = DB::table('orders')
                ->where('orders.updated_at', 'like', $current_date.'%')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();
            $todays_recieved_count = count($todays_recieved);

            $completed_this_month = DB::table('orders')
                ->where('orders.status', '=', 1)
                ->where('orders.payment_status', '=', 1)
                ->where('orders.updated_at', 'like', $current_date.'%')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();
            $completed_this_month_count = count($completed_this_month);


            $last_month_orders = DB::table('orders')
                ->where('orders.updated_at', 'like', $last_month_of_this_year.'%')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();
            $last_month_orders_count = count($last_month_orders);


            $approved_orderlist = DB::table('orders')
                ->leftjoin('passengers', function($join)
                {
                    $join->on('orders.user_id', '=', 'passengers.id');
                })
                ->leftjoin('drivers', function($join)
                {
                    $join->on('orders.driver_id', '=', 'drivers.id');
                })
                ->where('orders.status', '=', 1)
                ->select('orders.id','orders.user_id','orders.driver_id', 'orders.dropoff_address', 'orders.pickup_address', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.created_at', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'drivers.first_name as Dfirst_name', 'drivers.last_name as Dlast_name')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();

                return view('admin_blade.pages.dashboard-admin', compact('orderlist', 'approved_orderlist', 'todays_confirmed_count', 'todays_recieved_count', 'completed_this_month_count', 'last_month_orders_count', 'last_month'));
        }
        else{
            $driver_id = Auth::user()->getkey();
            $approved_orderlist = DB::table('orders')
                ->leftjoin('passengers', function($join)
                {
                    $join->on('orders.user_id', '=', 'passengers.id');
                })
                ->leftjoin('drivers', function($join)
                {
                    $join->on('orders.driver_id', '=', 'drivers.id');
                })
                ->where('orders.status', '=', 1)
                ->where('orders.driver_id', '=', $driver_id)
                ->select('orders.id','orders.user_id','orders.driver_id', 'orders.dropoff_address', 'orders.pickup_address', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.payment_status', 'orders.created_at', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'drivers.first_name as Dfirst_name', 'drivers.last_name as Dlast_name')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();

                return view('admin_blade.pages.user', compact('approved_orderlist'));
            }
    }



    /* Price calculation */
    public function calculation(Request $request)
    {
        $pickup = str_replace(' ','+',$request->get('pickup'));
        $dropoff = str_replace(' ','+',$request->get('dropoff'));

        //=== waypoints
        $waypoint_1 = $request->get('waypoint_1');
        $waypoint_2 = $request->get('waypoint_2');
        $waypoint_3 = $request->get('waypoint_3');
        $waypoint_4 = $request->get('waypoint_4');

        //=== checking waypoints
        $waypoint_1 = (isset($waypoint_1) && $waypoint_1 != '') ?  str_replace(' ','+',$waypoint_1) : '';
        $waypoint_2 = (isset($waypoint_2) && $waypoint_2 != '') ?  str_replace(' ','+',$waypoint_2) : '';
        $waypoint_3 = (isset($waypoint_3) && $waypoint_3 != '') ?  str_replace(' ','+',$waypoint_3) : '';
        $waypoint_4 = (isset($waypoint_4) && $waypoint_4 != '') ?  str_replace(' ','+',$waypoint_4) : '';

        //=== generate waypoint string
        $waypoints = 'waypoints=optimize:false|';
        //$waypoints = 'waypoints=optimize';
        if($waypoint_1 == '' && $waypoint_2 == '' && $waypoint_3 == '' && $waypoint_4 == '') $waypoints = '';
        $via_pointsArray = array_filter(array($waypoint_1,$waypoint_2,$waypoint_3,$waypoint_4));
        $waypoints .= implode("|",$via_pointsArray);

        $via_points = implode("|",$via_pointsArray);

        $apiKey = $this->googleAPIKey;
        $params = file_get_contents("https://maps.googleapis.com/maps/api/directions/json?region=uk&origin=$pickup&destination=$dropoff&units=imperial&$waypoints&key=$apiKey&alternatives=true");
        $directions = json_decode($params,true);
//        echo '<pre>';
//        print_r($_POST);
//        print_r($directions);
//        echo '</pre>';

        if($directions['status']!='ZERO_RESULTS'){
//            echo '<pre>';
//            print_r($directions['routes'][0]['legs']);
//            echo '</pre>';

            $pickup = str_replace('+',' ',$pickup);
            $dropoff = str_replace('+',' ',$dropoff);
            $routes = $directions['routes'][0]['legs'];

            $distance = 0;
            $duration = 0;
            $routesArray = array();

            $locationsLatLongData = array();

            foreach($routes as $k=>$route) {
                //==== check whether route has any London address
                $distance = $distance + $route['distance']['value'];
                $duration = $duration + $route['duration']['value'];
                $routesArray[$k]['start'] = str_replace('+',' ',$route['start_address']);
                $routesArray[$k]['end'] = str_replace('+',' ',$route['end_address']);
                $routesArray[$k]['distance'] = $route['distance']['value'];
                $routesArray[$k]['duration'] = $route['duration']['value'];
                $routesArray[$k]['end_location'] = $route['end_location'];
                $routesArray[$k]['start_location'] = $route['start_location'];
                $locationsLatLongData[] = $route['start_location']['lat'].','.$route['start_location']['lng'];
                $locationsLatLongData[] = $route['end_location']['lat'].','.$route['end_location']['lng'];

            }
            $distance = BookingController::shortest_distance($directions);
            $routesArray['distance_total'] = $distance;
            $routesArray['distance_total_miles'] = ($distance/1000) * 0.621371;
            //$routesArray['distance_total_miles'] = 90;
            $routesArray['distance_total_text'] = number_format(ceil($distance/1000),0) .' km';
            $routesArray['distance_total_miles_text'] = number_format(ceil(($distance/1000) * 0.621371),0) .' mi';
            $routesArray['duration_total'] = $duration;
            $routesArray['duration_total_text'] = number_format($duration/3600,0) . ' hr';

            //==== get mid value of the location data for center location of the map
            $midCounter = ceil(count($locationsLatLongData)/2);
            $centerLocation = $locationsLatLongData[$midCounter-1];

            //=== if total distance is 0-5 miles
            if($routesArray['distance_total_miles'] <= $this->firstMileageCounter)
                $this->totalPrice =  $this->first5Price;

            //=== if total distance is greater than 5 miles and less than or equal to 20 miles
            else if($routesArray['distance_total_miles'] > $this->firstMileageCounter && $routesArray['distance_total_miles'] <= $this->secondMileageCounter)
                $this->totalPrice =  $this->first5Price + ($routesArray['distance_total_miles'] - $this->firstMileageCounter) * $this->rateGreaterThan6UpTo20;

            //=== if total distance is greater than 20 miles and less than or equal to 35 miles
            else if($routesArray['distance_total_miles'] > $this->secondMileageCounter && $routesArray['distance_total_miles'] <= $this->thirdMileageCounter)
                $this->totalPrice =  $this->first5Price + ($routesArray['distance_total_miles'] - $this->firstMileageCounter) * $this->rateGreaterThan20UpTo35;

            //=== if total distance is greater than 35 miles and less than or equal to 55 miles
            else if($routesArray['distance_total_miles'] > $this->thirdMileageCounter && $routesArray['distance_total_miles'] <= $this->forthMileageCounter)
                $this->totalPrice =  $routesArray['distance_total_miles'] * $this->rateGreaterThan35UpTo55;

            //=== if total distance is greater than 55 miles
            else if($routesArray['distance_total_miles'] > $this->forthMileageCounter)
                $this->totalPrice =  $routesArray['distance_total_miles'] * $this->flatRateMoreThan55;
            else
                $this->totalPrice = 0;


            if($this->totalPrice <=0 || $this->totalPrice == NULL) {
                $locationUnrealistic = true;
                return view('blade.pages.NoResult');
            }

            $locationsLatLongDataImploded = implode("|",$locationsLatLongData);
            $locationsLatLongDataImplodedComma = implode(",",$locationsLatLongData);


        }
        else {
            //return error message
            }

    }
    /* /Price calculation */


    /**** Edit Order ****/

    public function editOrderSave(Request $request)
    {
        $orderID = $request->get('orderId');
        $pickup = $request->get('pickup_address');
        $pickup_address1 = $request->get('pickup_address1');
        $pickup_address2 = $request->get('pickup_address2');
        $pickup_postcode = $request->get('pickup_postcode');

        $dropoff = $request->get('dropoff_address');
        $dropoff_address1 = $request->get('dropoff_address1');
        $dropoff_address2 = $request->get('dropoff_address2');
        $dropoff_postcode = $request->get('dropoff_postcode');

        $via_points = $request->get('via_points');
        $distances = $request->get('distances');
        $carname = $request->get('carname');
        $tourType = $request->get('tourType');
        $meetngreet = $request->get('meetngreet');

        $tourdate = $request->get('tourdate');
        $tourdate = str_replace('/', '-', $tourdate);
        $tourtime = $request->get('tourtime');
        $tourmin = $request->get('tourmin');
        $tourdates = strtotime($tourdate." ".$tourtime.":".$tourmin);

        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');
        $email = $request->get('email');
        $mobile = $request->get('mobile');
        $comments = $request->get('comments');

        $no_of_person = $request->get('no_of_person');
        $no_of_suitcase = $request->get('no_of_suitcase');
        $no_of_child = $request->get('no_of_child');

        $arrival_time = $request->get('arrival_time');
        $airline_name = $request->get('airline_name');
        $flight_number = $request->get('flight_number');
        $departure_city = $request->get('departure_city');
        $pickup_after = $request->get('pickup_after');

        $totalprice = $request->get('total_bill');

//        $stripe_customer_id = $request->get('stripeCustomerID');
//        $stripe_charge_id = $request->get('stripeChargeID');
//        $payment_method = $request->get('payment_method');

        $driver = DB::table('car_details')
            ->where('id', $carname)
            ->first();

        $passenger = DB::table('passengers')
            ->where('email', $email)
            ->first();

            $id =$passenger->id;

        //$Orders = new Orders();
        $Orders = Orders::find($orderID);
        $Orders->user_id = $id;
        $Orders->pickup_address = $pickup;
        $Orders->pickup_address1 = $pickup_address1;
        $Orders->pickup_address2 = $pickup_address2;
        $Orders->pickup_postcode = $pickup_postcode;
        $Orders->dropoff_address = $dropoff;
        $Orders->dropoff_address1 = $dropoff_address1;
        $Orders->dropoff_address2 = $dropoff_address2;
        $Orders->dropoff_postcode = $dropoff_postcode;
        $Orders->via_points = $via_points;
        $Orders->car_details = $carname;
        $Orders->total_bill = $totalprice;
        $Orders->total_distance = $distances;

        $Orders->date_time = $tourdates;
        $Orders->no_of_child = $no_of_child;
        $Orders->no_of_person = $no_of_person;
        $Orders->suitcases = $no_of_suitcase;
        $Orders->comments = $comments;
        if((int)$meetngreet > 0) $Orders->meetngreet = $meetngreet;

        if($tourType == 'round'){
            $returndate = $request->get('returnDate');
            $returndate = str_replace('/', '-', $returndate);
            $returntime = $request->get('returnTime');
            $returnmin = $request->get('returnMin');
            $return = strtotime($returndate." ".$returntime.":".$returnmin);

            $Orders->return_date_time = $return;
            $Orders->return_booking = 1;
        }
        else {
            $returndate = '';
            $returntime = '';
            $returnmin = '';
        }

        //===card info to Order
//        $Orders->card_customer_id = $stripe_customer_id;
//        $Orders->card_charge_id = $stripe_charge_id;
//        $Orders->pay_via = $payment_method;

        $Orders->save();

        $order_id = $Orders->id;


//        $pickup_from = Airports::find($order_id);
//
//        $pickup_from->flight_arrival_time = $arrival_time;
//        $pickup_from->airline_name = $airline_name;
//        $pickup_from->flight_no = $flight_number;
//        $pickup_from->departure_city = $departure_city;
//        $pickup_from->pickup_after = $pickup_after;
//        $pickup_from->save();

        flash()->success('Order edited successfully !!');

        return redirect('orderlist');
    }

    /**** /Edit Order ****/


    /** pricelist */
    public function pricelist()
    {
        if(Auth::user()->role == 1) {
            $Pricelist = DB::table('pricelist')
                ->get();
            return view('admin_blade.pages.set_price', compact('Pricelist'));
        }
    }
    /** /pricelist */

    /* edit pricelist */
    public function edit_pricelist($id)
    {
        $pricelist = new Pricelist();
        $pricelistupdate = $pricelist->where('id', $id)->first();
        return view('admin_blade.pages.edit_price', compact('pricelistupdate'));
    }

    public function edit_pricelist_save(Request $request)
    {
        $id = $request->get('id');
        $unit_price = $request->get('unit_price');
        Pricelist::where('id', $id)
            ->update(
                array("unit_price" => $unit_price
                )
            );
        flash()->success('Updated successfully !!');

        return redirect('edit_pricelist/'.$id);
    }
    /* /edit pricelist */


    /**show profile**/
    public function profile()
    {
        if(Auth::user()->role == 1) {
            $admin_id = Auth::user()->getkey();
            $profile = DB::table('users')
                ->where('users.id', '=', $admin_id)
                ->first();
        }
        else{
            $driver_id = Auth::user()->getkey();
            $profile = DB::table('users')
                ->join('drivers', function($join)
                {
                    $join->on('users.id', '=', 'drivers.user_id');
                })
                ->where('users.id', '=', $driver_id)
                ->first();
        }

        return view('admin_blade.pages.profile', compact('profile'));
    }

    /**settings**/
    public function setting(){
        $setting = DB::table('setting')->where('property','=','payment_method')->first();
        $pm = explode(",", $setting->value);
        //var_dump($pm);die;
        return view('admin_blade.pages.setting',compact('pm'));
    }
    public function setting_post(Request $getSettings){
        $pm = $getSettings->get('pm');
        $payment_method = implode(",",$pm);

        Setting::where('property','payment_method')->update(array('value'=>$payment_method));
        return redirect('setting');
    }


    /** edit profile **/
    public function editprofile($user_id){
        if(Auth::user()->role == 1) {
            $user = new User();
            $profiledetails = $user->where('id', $user_id)->first();
        }
        else{
            $user = new Drivers();
            $profiledetails = $user->where('user_id', $user_id)->first();
        }
        return view('admin_blade.pages.editprofile', compact('profiledetails'));
    }

    public function editprofilesave(editprofilesaveRequest $request)
    {
        if(Auth::user()->role == 1) {
            $user_id = $request->get('id');
            $full_name = $request->get('full_name');
            $email = $request->get('email');
            $password = $request->get('password');
            $passwordhash = bcrypt($password);

            User::where('id', $user_id)
                ->update(
                    array("full_name" => $full_name,
                        "email" => $email
                    )
                );
            if($password){
            User::where('id', $user_id)
                ->update(
                    array("password" => $passwordhash )
                );
            }
        }
        else{
            $user_id = $request->get('id');
            $first_name = $request->get('first_name');
            $last_name = $request->get('last_name');
            $contact_name = $request->get('contact_name');
            $phone_number = $request->get('phn_number');
            $email = $request->get('email');
            $password = $request->get('password');
            $passwordhash = bcrypt($password);

            User::where('id', $user_id)
                ->update(
                    array("full_name" => $first_name." ".$last_name,
                        "email" => $email
                    )
                );
            if($password){
                User::where('id', $user_id)
                    ->update(
                        array("password" => $passwordhash )
                    );
            }

            Drivers::where('user_id', $user_id)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "contact_name" => $contact_name,
                        "phone_number" => $phone_number,
                        "email" => $email
                    )
                );
        }

        flash()->success('Profile updated successfully !!');

        return redirect('profile');
    }

    /** /edit profile **/


    /** orderlist */
    public function orderlist()
    {
        if(Auth::user()->role == 1) {
            $orderlist = DB::table('orders')
                ->leftjoin('passengers', function($join)
                {
                    $join->on('orders.user_id', '=', 'passengers.id');
                })
                ->select('orders.id','orders.user_id', 'orders.dropoff_address', 'orders.pickup_address', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.status', 'orders.created_at', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();
            return view('admin_blade.pages.orderlist', compact('orderlist'));
        }
    }
    /** /orderlist */


    /** Confirmed orderlist */
    public function conf_orderlist()
    {
        if(Auth::user()->role == 1) {


            $conf_orderlist = DB::table('orders')
                ->leftjoin('passengers', function($join)
                {
                    $join->on('orders.user_id', '=', 'passengers.id');
                })
                ->leftjoin('drivers', function($join)
                {
                    $join->on('orders.driver_id', '=', 'drivers.id');
                })
                ->where('orders.status', '=', 1)
                ->select('orders.id','orders.user_id','orders.driver_id', 'orders.dropoff_address', 'orders.pickup_address', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.created_at', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'drivers.first_name as Dfirst_name', 'drivers.last_name as Dlast_name')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();


//            $orderlist = DB::table('orders')
//                ->leftjoin('passengers', function($join)
//                {
//                    $join->on('orders.user_id', '=', 'passengers.id');
//                })
//                ->select('orders.id','orders.user_id', 'orders.dropoff_address', 'orders.pickup_address', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.status', 'orders.created_at', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number')
//                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
//                ->get();

            return view('admin_blade.pages.conf_orderlist', compact('conf_orderlist'));
        }
    }
    /** /Confirmed orderlist */


    /**show Order Details**/
    public function OrderDetails($orderID)
    {
        $order = DB::table('orders')
            ->leftjoin('passengers', function($join)
            {
                $join->on('orders.user_id', '=', 'passengers.id');
            })
            ->leftjoin('car_details', function($join)
            {
                $join->on('orders.car_details', '=', 'car_details.id');
            })

            ->leftjoin('pickup_from_airport', function($join)
            {
                $join->on('orders.id', '=', 'pickup_from_airport.order_id');
            })

            ->leftjoin('drivers', function($join)
            {
                $join->on('orders.driver_id', '=', 'drivers.id');
            })

            ->where('orders.id', $orderID)
            ->select('orders.id','orders.user_id', 'orders.dropoff_address', 'orders.dropoff_address1','orders.dropoff_address2', 'orders.dropoff_postcode', 'orders.pickup_address', 'orders.pickup_address1', 'orders.pickup_address2', 'orders.pickup_postcode', 'orders.no_of_person', 'orders.no_of_child', 'orders.suitcases', 'orders.via_points','orders.meetngreet', 'orders.comments', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.status', 'orders.pay_via','orders.card_customer_id','orders.card_charge_id', 'orders.payment_status', 'orders.date_time', 'orders.return_booking', 'orders.special_date', 'orders.return_date_time', 'orders.driver_id', 'orders.created_at', 'drivers.first_name as dFname', 'drivers.last_name as dLname', 'drivers.email as dEmail', 'drivers.phone_number as dPhone', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'car_details.name as carname', 'car_details.total_seats', 'pickup_from_airport.flight_arrival_time', 'pickup_from_airport.airline_name', 'pickup_from_airport.flight_no', 'pickup_from_airport.departure_city', 'pickup_from_airport.pickup_after')
            ->first();

        $via_points = $order->via_points;
        $via_points_all = explode("|",$via_points);
        $via_points_seperate = $via_points_all;

        $driverlist = DB::table('drivers')
            ->orderBy('drivers.updated_at', 'drivers.id', 'DESC')
            ->get();

        return view('admin_blade.pages.orderDetails', compact('order', 'driverlist', 'via_points_seperate', 'via_points'));
    }
    /** /show Order Details**/

    /**Edit Order**/
    public function editOrder($orderID)
    {
        $sessData1stStage = Session::get('1stStageValues');

//        echo '<pre>';
//        print_r(Session::get('1stStageValues'));
//        echo '</pre>';die();

        $order = DB::table('orders')
            ->leftjoin('passengers', function($join)
            {
                $join->on('orders.user_id', '=', 'passengers.id');
            })
            ->leftjoin('car_details', function($join)
            {
                $join->on('orders.car_details', '=', 'car_details.id');
            })

            ->leftjoin('pickup_from_airport', function($join)
            {
                $join->on('orders.id', '=', 'pickup_from_airport.order_id');
            })

            ->leftjoin('drivers', function($join)
            {
                $join->on('orders.driver_id', '=', 'drivers.id');
            })

            ->where('orders.id', $orderID)
            ->select('orders.id','orders.user_id', 'orders.dropoff_address', 'orders.dropoff_address1','orders.dropoff_address2', 'orders.dropoff_postcode', 'orders.pickup_address', 'orders.pickup_address1', 'orders.pickup_address2', 'orders.pickup_postcode', 'orders.no_of_person', 'orders.no_of_child', 'orders.suitcases', 'orders.via_points','orders.meetngreet', 'orders.comments', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.status', 'orders.pay_via','orders.card_customer_id','orders.card_charge_id', 'orders.payment_status', 'orders.date_time', 'orders.return_booking', 'orders.return_date_time', 'orders.driver_id', 'orders.created_at', 'drivers.first_name as dFname', 'drivers.last_name as dLname', 'drivers.email as dEmail', 'drivers.phone_number as dPhone', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'car_details.name as carname', 'car_details.total_seats', 'pickup_from_airport.flight_arrival_time', 'pickup_from_airport.airline_name', 'pickup_from_airport.flight_no', 'pickup_from_airport.departure_city', 'pickup_from_airport.pickup_after')
            ->first();

        $via_points = $order->via_points;
        $tourType = $order->return_booking;
        $via_points = str_replace('+',' ',$via_points);
        $via_points_all = explode("|",$via_points);
        $via_points_seperate = $via_points_all;
        $viapointsCount = count($via_points_seperate);
        $totalDistance = $order->total_distance;
        $carId = $order->car_details;

        //echo $viapointsCount;die();

        return view('admin_blade.pages.editForm', compact('order', 'via_points_seperate', 'viapointsCount', 'via_points', 'totalprice', 'tourType', 'sessData1stStage'));
    }
    /** /Edit Order**/

    /* editOrderSubmit */
    public function editOrderSubmit(Request $request)
    {
        Session::put('1stStageValues',$_POST);
        //        echo '<pre>';
        //        print_r($_POST);
        //        print_r(Session::get('1stStageValues'));
        //        echo '</pre>';

        $pickup = str_replace(' ','+',$request->get('pickup'));
        $dropoff = str_replace(' ','+',$request->get('dropoff'));

        $carId = $request->get('car_id');
        $tourType = $request->get('tourType');
        //echo $carId; die();
        //=== waypoints
        $waypoint_1 = $request->get('waypoint_1');
        $waypoint_2 = $request->get('waypoint_2');
        $waypoint_3 = $request->get('waypoint_3');
        $waypoint_4 = $request->get('waypoint_4');

        //=== checking waypoints
        $waypoint_1 = (isset($waypoint_1) && $waypoint_1 != '') ?  str_replace(' ','+',$waypoint_1) : '';
        $waypoint_2 = (isset($waypoint_2) && $waypoint_2 != '') ?  str_replace(' ','+',$waypoint_2) : '';
        $waypoint_3 = (isset($waypoint_3) && $waypoint_3 != '') ?  str_replace(' ','+',$waypoint_3) : '';
        $waypoint_4 = (isset($waypoint_4) && $waypoint_4 != '') ?  str_replace(' ','+',$waypoint_4) : '';

        //=== generate waypoint string
        $waypoints = 'waypoints=optimize:false|';
        //$waypoints = 'waypoints=optimize';
        if($waypoint_1 == '' && $waypoint_2 == '' && $waypoint_3 == '' && $waypoint_4 == '') $waypoints = '';
        $via_pointsArray = array_filter(array($waypoint_1,$waypoint_2,$waypoint_3,$waypoint_4));
        $waypoints .= implode("|",$via_pointsArray);

        $via_points = implode("|",$via_pointsArray);

        $apiKey = $this->googleAPIKey;
        $params = file_get_contents("https://maps.googleapis.com/maps/api/directions/json?region=uk&origin=$pickup&destination=$dropoff&units=imperial&$waypoints&key=$apiKey&alternatives=true");
        $directions = json_decode($params,true);


        if($directions['status']!='ZERO_RESULTS'){

            $pickup = str_replace('+',' ',$pickup);
            $dropoff = str_replace('+',' ',$dropoff);
            $routes = $directions['routes'][0]['legs'];

            $distance = 0;
            $duration = 0;
            $routesArray = array();

            $locationsLatLongData = array();

            foreach($routes as $k=>$route) {
                //==== check whether route has any London address
                //if(strstr($route['end_address'],'London')) $isLondon = true;
                $distance = $distance + $route['distance']['value'];
                $duration = $duration + $route['duration']['value'];
                $routesArray[$k]['start'] = str_replace('+',' ',$route['start_address']);
                $routesArray[$k]['end'] = str_replace('+',' ',$route['end_address']);
                $routesArray[$k]['distance'] = $route['distance']['value'];
                $routesArray[$k]['duration'] = $route['duration']['value'];
                $routesArray[$k]['end_location'] = $route['end_location'];
                $routesArray[$k]['start_location'] = $route['start_location'];
                $locationsLatLongData[] = $route['start_location']['lat'].','.$route['start_location']['lng'];
                $locationsLatLongData[] = $route['end_location']['lat'].','.$route['end_location']['lng'];

            }
            $distance = BookingController::shortest_distance($directions);
            $routesArray['distance_total'] = $distance;
            $routesArray['distance_total_miles'] = ($distance/1000) * 0.621371;
            //$routesArray['distance_total_miles'] = 90;
            $routesArray['distance_total_text'] = number_format(ceil($distance/1000),0) .' km';
            $routesArray['distance_total_miles_text'] = number_format(ceil(($distance/1000) * 0.621371),0) .' mi';
            $routesArray['duration_total'] = $duration;
            $routesArray['duration_total_text'] = number_format($duration/3600,0) . ' hr';

            $totalDistance = $routesArray['distance_total_miles'];


        //$totalprice = $request->get('totalprice'); echo $totalprice; die();




            //=== if total distance is 0-5 miles
            if($totalDistance <= $this->firstMileageCounter)
                $this->totalPrice =  $this->first5Price;

            //=== if total distance is greater than 5 miles and less than or equal to 20 miles
            else if($totalDistance > $this->firstMileageCounter && $totalDistance <= $this->secondMileageCounter)
                $this->totalPrice =  $this->first5Price + ($totalDistance - $this->firstMileageCounter) * $this->rateGreaterThan6UpTo20;

            //=== if total distance is greater than 20 miles and less than or equal to 35 miles
            else if($totalDistance > $this->secondMileageCounter && $totalDistance <= $this->thirdMileageCounter)
                $this->totalPrice =  $this->first5Price + ($totalDistance - $this->firstMileageCounter) * $this->rateGreaterThan20UpTo35;

            //=== if total distance is greater than 35 miles and less than or equal to 55 miles
            else if($totalDistance > $this->thirdMileageCounter && $totalDistance <= $this->forthMileageCounter)
                $this->totalPrice =  $totalDistance * $this->rateGreaterThan35UpTo55;

            //=== if total distance is greater than 55 miles
            else if($totalDistance > $this->forthMileageCounter)
                $this->totalPrice =  $totalDistance * $this->flatRateMoreThan55;
            else
                $this->totalPrice = 0;


            if($this->totalPrice <=0 || $this->totalPrice == NULL) {
                $locationUnrealistic = true;
                return view('blade.pages.NoResult');
            }

            //=== get list of cars
            $carDetails = DB::table('car_details')
                ->leftjoin('car_category', function($join)
                {
                    $join->on('car_details.category', '=', 'car_category.id');
                })
                ->select('car_details.id','car_details.name', 'car_details.total_seats', 'car_category.cat_name','car_category.details', 'car_category.max_person', 'car_category.max_carry_on', 'car_category.price_ratio', 'car_category.car_pic', 'car_category.max_suitcases')
                ->where('car_details.id', $carId)
                ->first();

            $routesArray['carPrices'][$carDetails->id]['price'] = ceil($this->totalPrice + ($this->totalPrice * $carDetails->price_ratio)/100);
            $routesArray['carPrices'][$carDetails->id]['priceFormatted'] = number_format($routesArray['carPrices'][$carDetails->id]['price'],2);
            $routesArray['carPrices'][$carDetails->id]['priceReturn'] =  2*$routesArray['carPrices'][$carDetails->id]['price'] - $routesArray['carPrices'][$carDetails->id]['price']*($this->returnDiscountPercent/100);

            $routesArray['carPrices'][$carDetails->id]['priceReturnFormatted'] = number_format($routesArray['carPrices'][$carDetails->id]['priceReturn'],2);
            $routesArray['carPrices'][$carDetails->id]['totalIfReturnSelected'] = $routesArray['carPrices'][$carDetails->id]['price'] + $routesArray['carPrices'][$carDetails->id]['priceReturn'];
            $routesArray['carPrices'][$carDetails->id]['totalIfReturnSelectedFormatted'] = number_format($routesArray['carPrices'][$carDetails->id]['totalIfReturnSelected'],2);
            $routesArray['carPrices'][$carDetails->id]['carName'] = $carDetails->cat_name;
            $routesArray['carPrices'][$carDetails->id]['priceRatio'] = $carDetails->price_ratio;
            $routesArray['carPrices'][$carDetails->id]['discountRate'] = $this->returnDiscountPercent;
            $routesArray['carPrices'][$carDetails->id]['currency'] = $this->currency;
            $routesArray['carPrices'][$carDetails->id]['currencySymbol'] = $this->currencySymbol;


            $orderID = $request->get('order_id');
            $meetngreet = $request->get('meetngreet');
            $clickedPage = $request->get('clickedPage');

            if($tourType == 1){
                $totalprice =  $routesArray['carPrices'][$carDetails->id]['priceReturn'];
                $totalpricespecialdate = ($totalprice * 1.5) + $meetngreet;
            }
            else{
                $totalprice = $routesArray['carPrices'][$carDetails->id]['price'];
                $totalpricespecialdate = ($totalprice * 1.5) + $meetngreet;
            }

            //echo "Price = ".$totalprice." Distance = ".$totalDistance;die();

            $totalprice = $totalprice + $meetngreet;

//        $UserId = Auth::user()->getkey();
//        $orderID = $request->get('order_id');
//
//        $Orders = Orders::find($orderID);
//
//        $Orders->via_points = $via_points;
//        $Orders->total_bill = $totalprice;
//        $Orders->total_distance = $totalDistance;
//
//        $Orders->editedBy = $UserId;
//
//        $Orders->save();


            Session::put('totalprice',$totalprice);
            Session::put('totalpricespecialdate',$totalpricespecialdate);
            Session::put('totalDistance',$totalDistance);
            //Session::put('meetngreet','N');
            Session::put('meetngreet',$meetngreet);

//            echo '<pre>';
//            print_r(Session::get('totalprice'));echo "<br/>";
//            print_r(Session::get('totalDistance'));echo "<br/>";
//            print_r(Session::get('1stStageValues'));
//            echo '</pre>';die();


        //flash()->success('Order edited successfully !!');

            if($clickedPage == 'customerDeails')
                return redirect('editOrderCustomerDeails/'.$orderID);
            else
                return redirect('editOrderCarDeails/'.$orderID);

        }
    }

    /* /editOrderSubmit */

    /*** editOrderCarDeails ****/
    public function editOrderCarDeails($orderID){

        $sessData1stStage = Session::get('1stStageValues');
        $sessData2ndStage = Session::get('2ndStageValues');
        $sessDatatotalDistance = Session::get('totalDistance');
        $sessDatatotalPrice = Session::get('totalprice');
        $sessData3rdStage = Session::get('3rdStageValues');
        $sessMeetngreet = Session::get('meetngreet');

//            echo '<pre>';
//            print_r(Session::get('totalprice'));echo "<br/>";
//            print_r(Session::get('totalDistance'));echo "<br/>";
//            print_r(Session::get('1stStageValues'));
//            echo '</pre>'; //die();
//
//        echo '<pre>';
//        print_r(Session::get('2ndStageValues'));
//        echo '</pre>'; //die();

        $order = DB::table('orders')
            ->leftjoin('passengers', function($join)
            {
                $join->on('orders.user_id', '=', 'passengers.id');
            })
            ->leftjoin('car_details', function($join)
            {
                $join->on('orders.car_details', '=', 'car_details.id');
            })
            ->leftjoin('pickup_from_airport', function($join)
            {
                $join->on('orders.id', '=', 'pickup_from_airport.order_id');
            })
            ->leftjoin('drivers', function($join)
            {
                $join->on('orders.driver_id', '=', 'drivers.id');
            })
            ->where('orders.id', $orderID)
            ->select('orders.id','orders.user_id', 'orders.dropoff_address', 'orders.dropoff_address1','orders.dropoff_address2', 'orders.dropoff_postcode', 'orders.pickup_address', 'orders.pickup_address1', 'orders.pickup_address2', 'orders.pickup_postcode', 'orders.no_of_person', 'orders.no_of_child', 'orders.suitcases', 'orders.via_points','orders.meetngreet', 'orders.comments', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.status', 'orders.pay_via','orders.card_customer_id','orders.card_charge_id', 'orders.payment_status', 'orders.date_time', 'orders.return_booking', 'orders.return_date_time', 'orders.driver_id', 'orders.created_at', 'drivers.first_name as dFname', 'drivers.last_name as dLname', 'drivers.email as dEmail', 'drivers.phone_number as dPhone', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'car_details.id as carId', 'car_details.name as carname', 'car_details.total_seats', 'pickup_from_airport.flight_arrival_time', 'pickup_from_airport.airline_name', 'pickup_from_airport.flight_no', 'pickup_from_airport.departure_city', 'pickup_from_airport.pickup_after')
            ->first();
        if($sessDatatotalDistance == NULL)
            $totalDistance = $order->total_distance;
        else
            $totalDistance = $sessDatatotalDistance;


            //=== if total distance is 0-5 miles
            if($totalDistance <= $this->firstMileageCounter)
                $this->totalPrice =  $this->first5Price;

            //=== if total distance is greater than 5 miles and less than or equal to 20 miles
            else if($totalDistance > $this->firstMileageCounter && $totalDistance <= $this->secondMileageCounter)
                $this->totalPrice =  $this->first5Price + ($totalDistance - $this->firstMileageCounter) * $this->rateGreaterThan6UpTo20;

            //=== if total distance is greater than 20 miles and less than or equal to 35 miles
            else if($totalDistance > $this->secondMileageCounter && $totalDistance <= $this->thirdMileageCounter)
                $this->totalPrice =  $this->first5Price + ($totalDistance - $this->firstMileageCounter) * $this->rateGreaterThan20UpTo35;

            //=== if total distance is greater than 35 miles and less than or equal to 55 miles
            else if($totalDistance > $this->thirdMileageCounter && $totalDistance <= $this->forthMileageCounter)
                $this->totalPrice =  $totalDistance * $this->rateGreaterThan35UpTo55;

            //=== if total distance is greater than 55 miles
            else if($totalDistance > $this->forthMileageCounter)
                $this->totalPrice =  $totalDistance * $this->flatRateMoreThan55;
            else
                $this->totalPrice = 0;


            if($this->totalPrice <=0 || $this->totalPrice == NULL) {
                $locationUnrealistic = true;
                return view('blade.pages.NoResult');
            }

            //=== get list of cars
            $carList = DB::table('car_details')
                ->leftjoin('car_category', function($join)
                {
                    $join->on('car_details.category', '=', 'car_category.id');
                })
                ->select('car_details.id','car_details.name', 'car_details.total_seats', 'car_category.cat_name','car_category.details', 'car_category.max_person', 'car_category.max_carry_on', 'car_category.price_ratio', 'car_category.car_pic', 'car_category.max_suitcases')
                ->orderBy('car_details.id', 'ASC')
                ->get();


            foreach($carList as $carDetails) {
                $routesArray['carPrices'][$carDetails->id]['price'] = ceil($this->totalPrice + ($this->totalPrice * $carDetails->price_ratio)/100);
                $routesArray['carPrices'][$carDetails->id]['priceFormatted'] = number_format($routesArray['carPrices'][$carDetails->id]['price'],2);
                $routesArray['carPrices'][$carDetails->id]['priceReturn'] =  2*$routesArray['carPrices'][$carDetails->id]['price'] - $routesArray['carPrices'][$carDetails->id]['price']*($this->returnDiscountPercent/100);

                $routesArray['carPrices'][$carDetails->id]['priceReturnFormatted'] = number_format($routesArray['carPrices'][$carDetails->id]['priceReturn'],2);
                $routesArray['carPrices'][$carDetails->id]['totalIfReturnSelected'] = $routesArray['carPrices'][$carDetails->id]['price'] + $routesArray['carPrices'][$carDetails->id]['priceReturn'];
                $routesArray['carPrices'][$carDetails->id]['totalIfReturnSelectedFormatted'] = number_format($routesArray['carPrices'][$carDetails->id]['totalIfReturnSelected'],2);
                $routesArray['carPrices'][$carDetails->id]['carName'] = $carDetails->cat_name;
                $routesArray['carPrices'][$carDetails->id]['priceRatio'] = $carDetails->price_ratio;
                $routesArray['carPrices'][$carDetails->id]['discountRate'] = $this->returnDiscountPercent;
                $routesArray['carPrices'][$carDetails->id]['currency'] = $this->currency;
                $routesArray['carPrices'][$carDetails->id]['currencySymbol'] = $this->currencySymbol;
            }

            return view('admin_blade.pages.editBookingResult', compact('pickup', 'dropoff', 'routesArray', 'carList', 'tourdate', 'tourtime','mapImage', 'via_points', 'order', 'sessData1stStage', 'sessData2ndStage', 'sessData3rdStage', 'sessDatatotalDistance', 'sessDatatotalPrice', 'sessMeetngreet'));

    }
    /*** /editOrderCarDeails ****/


    /* editOrderCarDeailsSubmit */
    public function editOrderCarDeailsSubmit(Request $request)
    {
        Session::put('2ndStageValues',$_POST);

//        echo '<pre>';
//        print_r(Session::get('totalprice'));echo "<br/>";
//        print_r(Session::get('totalDistance'));echo "<br/>";
//        print_r(Session::get('1stStageValues'));
//        print_r(Session::get('2ndStageValues'));
//        print_r(Session::get('3rdStageValues'));
//        echo '</pre>';die();

        $sessData1stStage = Session::get('1stStageValues');
        $sessData2ndStage = Session::get('2ndStageValues');

        $sessData3rdStage = Session::get('3rdStageValues');

        $distances = $request->get('distances');
        $carname = $request->get('carname');
        $carfullname = $request->get('carfullname');
        $car_pic = $request->get('car_pic');
        $totalprice = $request->get('totalprice');
        $singleTrip = $request->get('singleTrip');
        $roundTrip = $request->get('roundTrip');
        $meetngreet = $request->get('meetngreet');

        $passengerId = $request->get('userId');

        $tripType = 'single';
        if($totalprice == $singleTrip){
            $tripType = 'single';
            $totalprice = $totalprice + $meetngreet;
            $totalpricespecialdate = ($totalprice*1.5) + $meetngreet;
        }
        else if($totalprice == $roundTrip){
            $tripType = 'round';
            $totalprice = $totalprice + $meetngreet;
            $totalpricespecialdate = ($totalprice*1.5) + $meetngreet;
            Session::put('returnDate','');
            Session::put('returnTime','');
            Session::put('returnMin','');
        }

        Session::put('totalprice',$totalprice);
        Session::put('totalprice',$totalprice);
        Session::put('totalpricespecialdate',$totalpricespecialdate);
        Session::put('totalDistance',$distances);
        Session::put('meetngreet',$meetngreet);

        $sessDatatotalDistance = Session::get('totalDistance');
        $sessDatatotalPrice = Session::get('totalprice');
        $sessReturnDate = Session::get('returnDate');
        $sessReturnTime = Session::get('returnTime');
        $sessReturnMin = Session::get('returnMin');
        $sessMeetngreet = Session::get('meetngreet');

        $UserId = Auth::user()->getkey();
        $orderID = $request->get('order_id');
        $clickedPage = $request->get('clickedPage');
        //echo $clickedPage;die();
        $updateFlag = $request->get('updateFlag');

        $pickup_from_airport = DB::table('pickup_from_airport')
            ->where('order_id', $orderID)
            ->first();

//        $Orders = Orders::find($orderID);
//
//        $Orders->car_details = $carname;
//        $Orders->total_bill = $totalprice;
//        $Orders->total_distance = $distances;
//
//        $Orders->editedBy = $UserId;
//        if((int)$meetngreet > 0) $Orders->meetngreet = $meetngreet;
//
//        if($tripType == 'round'){
//            $Orders->return_booking = 1;
//        }
//        else {
//            $Orders->return_date_time = 0;
//            $Orders->return_booking = 0;
//        }
//
//        $Orders->save();
//echo Session::get('totalDistance');die();

        if($updateFlag == 1){
            $this->saveAllfromCarpage($orderID, $sessData1stStage, $sessData2ndStage, $sessDatatotalDistance, $sessDatatotalPrice, $sessData3rdStage, $carname, $tripType, $totalprice, $meetngreet, $UserId, $passengerId, $pickup_from_airport);

            Session::forget('1stStageValues');
            Session::forget('2ndStageValues');
            Session::forget('3rdStageValues');
            Session::forget('totalDistance');
            Session::forget('totalprice');
            Session::forget('returnDate');
            Session::forget('returnTime');
            Session::forget('returnMin');
            Session::forget('meetngreet');


            flash()->success('Order edited successfully !!');
            return redirect('editOrderCustomerDeails/'.$orderID);
        }
        elseif($updateFlag == 2){
            return redirect('editOrderCustomerDeails/'.$orderID);
        }
        else{
            if($clickedPage == 'customerDeails')
                return redirect('editOrderCustomerDeails/'.$orderID);
            else
                return redirect('editOrder/'.$orderID);
        }

    }

    /* /editOrderCarDeailsSubmit */

    /*
         * Stripe Charge
         */
    public function stripeCharge(Request $request){
        $tokenID = $request->get('tokenID');
        $tokenEmail = $request->get('tokenEmail');
        $description = $request->get('description');
        $amount = $request->get('amount');
        $currency = $request->get('currency');

        //====stripe config
        $stripe = Config::get('services.stripe');

        //=== set stripe API
        \Stripe\Stripe::setApiKey($stripe['secret']);

        //=== create a customer

        try{
            $customer = \Stripe\Customer::create([
                'email' => $tokenEmail,
                'card' => $tokenID,
                'description' => $description
            ]);

            $customerID = $customer->id;

        }catch (\Stripe\Error\Card $e){
            echo 'error_info';
            die;
        }

        try{
            $charge = \Stripe\Charge::create([
                'customer' => $customerID,
                'currency' => $currency,
                'amount'   => $amount,
                'description' => $description
            ]);

            echo $customerID.'^^__^^'.$charge['id'];

        }
        catch(\Stripe\Error\Card $e) {
            echo 'error_card';
            die;
        }


    }

    /*** editOrderCustomerDeails ****/
    public function editOrderCustomerDeails($orderID){

        $sessData1stStage = Session::get('1stStageValues');
        $sessData2ndStage = Session::get('2ndStageValues');
        $sessDatatotalDistance = Session::get('totalDistance');
        $sessDatatotalPrice = Session::get('totalprice');
        $sessDataSpecialPrice = Session::get('totalpricespecialdate');
        $sessData3rdStage = Session::get('3rdStageValues');

        $sessReturnDate = Session::get('returnDate');
        $sessReturnTime = Session::get('returnTime');
        $sessReturnMin = Session::get('returnMin');

        $sessMeetngreet = Session::get('meetngreet');
        //$sessDatatotalPrice = $sessDatatotalPrice + $sessMeetngreet;

//        echo "<pre>";
//        print_r($sessData1stStage);
//        print_r($sessData2ndStage);
//        print_r($sessData3rdStage);
//        print_r($sessDatatotalDistance);echo"<br/>";
//        print_r($sessReturnDate);echo"<br/>";
//        print_r($sessReturnTime);echo"<br/>";
//        print_r($sessReturnMin);echo"<br/>";
//        print_r($sessMeetngreet);echo" GBP<br/>";
//        print_r($sessDatatotalPrice);
//        die();
//        echo "</pre>";
        $order = DB::table('orders')
            ->leftjoin('passengers', function($join)
            {
                $join->on('orders.user_id', '=', 'passengers.id');
            })
            ->leftjoin('car_details', function($join)
            {
                $join->on('orders.car_details', '=', 'car_details.id');
            })
            ->leftjoin('pickup_from_airport', function($join)
            {
                $join->on('orders.id', '=', 'pickup_from_airport.order_id');
            })
            ->leftjoin('drivers', function($join)
            {
                $join->on('orders.driver_id', '=', 'drivers.id');
            })
            ->where('orders.id', $orderID)
            ->select('orders.id','orders.user_id', 'orders.dropoff_address', 'orders.dropoff_address1','orders.dropoff_address2', 'orders.dropoff_postcode', 'orders.pickup_address', 'orders.pickup_address1', 'orders.pickup_address2', 'orders.pickup_postcode', 'orders.no_of_person', 'orders.no_of_child', 'orders.suitcases', 'orders.via_points','orders.meetngreet', 'orders.comments', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.status', 'orders.pay_via','orders.card_customer_id','orders.card_charge_id', 'orders.payment_status', 'orders.date_time', 'orders.return_booking', 'orders.special_date', 'orders.return_date_time', 'orders.driver_id', 'orders.created_at', 'drivers.first_name as dFname', 'drivers.last_name as dLname', 'drivers.email as dEmail', 'drivers.phone_number as dPhone', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'car_details.id as carId', 'car_details.name as carname', 'car_details.category', 'car_details.total_seats', 'pickup_from_airport.flight_arrival_time', 'pickup_from_airport.airline_name', 'pickup_from_airport.flight_no', 'pickup_from_airport.departure_city', 'pickup_from_airport.pickup_after')
            ->first();

        $carCategory = $order->category;

        $carList = DB::table('car_details')
            ->leftjoin('car_category', function($join)
            {
                $join->on('car_details.category', '=', 'car_category.id');
            })
            ->select('car_details.id','car_details.name', 'car_details.total_seats', 'car_category.cat_name','car_category.details', 'car_category.max_person', 'car_category.max_carry_on', 'car_category.price_ratio', 'car_category.car_pic', 'car_category.max_suitcases')
            ->where('car_category.id', $carCategory)
            ->first();

        $setting = DB::table('setting')->where('property','=','payment_method')->first();
        $pm = explode(",", $setting->value);

        $via_points = $order->via_points;
        $via_points = str_replace('+',' ',$via_points);
        $via_points_all = explode("|",$via_points);
        $via_points_seperate = $via_points_all;

        //====stripe config
        $stripe = Config::get('services.stripe');

        return view('admin_blade.pages.editBookingFinal', compact('order', 'carList', 'via_points_seperate', 'sessData1stStage', 'sessData2ndStage', 'sessDatatotalDistance', 'sessDatatotalPrice', 'sessReturnDate', 'sessReturnTime', 'sessReturnMin', 'sessMeetngreet', 'sessData3rdStage', 'sessDataSpecialPrice', 'pm', 'stripe'));

    }
    /*** /editOrderCustomerDetails ****/


    /* editOrderCustomerDeailsSubmit */
    public function editOrderCustomerDeailsSubmit(Request $request)
    {
        Session::put('3rdStageValues',$_POST);

        $sessData2ndStage = Session::get('2ndStageValues');

        $tourType = $request->get('tourType');
        //echo $tourType;die();

        $stripe_customer_id = $request->get('stripeCustomerID');
        $stripe_charge_id = $request->get('stripeChargeID');
        $payment_method = $request->get('payment_method');

        if($sessData2ndStage['tourType'] == 1){
            Session::put('returnDate',$_POST['returnDate']);
            Session::put('returnTime',$_POST['returnTime']);
            Session::put('returnMin',$_POST['returnMin']);
        }
        if($sessData2ndStage == NULL AND $tourType == 1){
            Session::put('returnDate',$_POST['returnDate']);
            Session::put('returnTime',$_POST['returnTime']);
            Session::put('returnMin',$_POST['returnMin']);
        }

//        echo '<pre>';
//        print_r(Session::get('totalprice'));echo "<br/>";
//        print_r(Session::get('totalDistance'));echo "<br/>";
//        print_r(Session::get('returnDate'));echo "<br/>";
//        print_r(Session::get('returnTime'));echo "<br/>";
//        print_r(Session::get('returnMin'));echo "<br/>";
//        print_r(Session::get('meetngreet'));echo "<br/>";
//        print_r(Session::get('1stStageValues'));
//        print_r(Session::get('2ndStageValues'));
//        print_r(Session::get('3rdStageValues'));
//        echo '</pre>'; die();

        $sessData1stStage = Session::get('1stStageValues');

        $sessDatatotalDistance = Session::get('totalDistance');
        $sessDatatotalPrice = Session::get('totalprice');
        $sessData3rdStage = Session::get('3rdStageValues');

        $sessReturnDate = Session::get('returnDate');
        $sessReturnTime = Session::get('returnTime');
        $sessReturnMin = Session::get('returnMin');

        $sessMeetngreet = Session::get('meetngreet');

        $order_id = $request->get('order_id');
        $passengerId = $request->get('userId');
        $clickedPage = $request->get('clickedPage');
        $updateFlag = $request->get('updateFlag');


        $pickup_address1 = $request->get('pickup_address1');
        $pickup_address2 = $request->get('pickup_address2');
        $pickup_postcode = $request->get('pickup_postcode');


        $dropoff_address1 = $request->get('dropoff_address1');
        $dropoff_address2 = $request->get('dropoff_address2');
        $dropoff_postcode = $request->get('dropoff_postcode');



        //$meetngreet = $request->get('meetngreet');

        $tourdate = $request->get('tourdate');
        $tourdate = str_replace('/', '-', $tourdate);
        $tourtime = $request->get('tourtime');
        $tourmin = $request->get('tourmin');
        $tourdates = strtotime($tourdate." ".$tourtime.":".$tourmin);


        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');
        $email = $request->get('email');
        $mobile = $request->get('mobile');
        $comments = $request->get('comments');

        $no_of_person = $request->get('no_of_person');
        $no_of_suitcase = $request->get('no_of_suitcase');
        $no_of_child = $request->get('no_of_child');

        $arrival_time = $request->get('arrival_time');
        $airline_name = $request->get('airline_name');
        $flight_number = $request->get('flight_number');
        $departure_city = $request->get('departure_city');
        $pickup_after = $request->get('pickup_after');

        $returndate = $request->get('returnDate');
        $returndate = str_replace('/', '-', $returndate);
        $returntime = $request->get('returnTime');
        $returnmin = $request->get('returnMin');


        $UserId = Auth::user()->getkey();

        $passenger = DB::table('passengers')
            ->where('id', $passengerId)
            ->first();

        //$id =$passenger->id;

//        $passenger = Passengers::find($id);
//        $passenger->first_name = $firstname;
//        $passenger->last_name = $lastname;
//        $passenger->email = $email;
//        $passenger->phone_number = $mobile;
//        $passenger->save();
//
//        $Orders = Orders::find($order_id);
//        $Orders->user_id = $id;
//
//        $Orders->pickup_address1 = $pickup_address1;
//        $Orders->pickup_address2 = $pickup_address2;
//        $Orders->pickup_postcode = $pickup_postcode;
//
//        $Orders->dropoff_address1 = $dropoff_address1;
//        $Orders->dropoff_address2 = $dropoff_address2;
//        $Orders->dropoff_postcode = $dropoff_postcode;
//
//        $Orders->date_time = $tourdates;
//        $Orders->no_of_child = $no_of_child;
//        $Orders->no_of_person = $no_of_person;
//        $Orders->suitcases = $no_of_suitcase;
//        $Orders->comments = $comments;
//        $Orders->editedBy = $UserId;
        //if((int)$meetngreet > 0) $Orders->meetngreet = $meetngreet;

//        if($tourType == 1){
//            $returndate = $request->get('returnDate');
//            $returndate = str_replace('/', '-', $returndate);
//            $returntime = $request->get('returnTime');
//            $returnmin = $request->get('returnMin');
//            $return = strtotime($returndate." ".$returntime.":".$returnmin);
//
//            $Orders->return_date_time = $return;
//            $Orders->return_booking = 1;
//        }
//        else {
//            $returndate = '';
//            $returntime = '';
//            $returnmin = '';
//            $Orders->return_date_time = 0;
//            $Orders->return_booking = 0;
//        }
//
//        $Orders->save();

        $pickup_from_airport = DB::table('pickup_from_airport')
            ->where('order_id', $order_id)
            ->first();

        $totalPriceDB = DB::table('orders')
            ->where('id', $order_id)
            ->first();

        $totalPrice = $request->get('totalprice');
        $totalPricefromDB = $totalPriceDB->total_bill;
        //echo $totalPricefromDB." ".$totalPrice;die();
        $pickup_from_airport_id =$pickup_from_airport->id;

//        $pickup_from = Airports::find($pickup_from_airport_id);
//        $pickup_from->flight_arrival_time = $arrival_time;
//        $pickup_from->airline_name = $airline_name;
//        $pickup_from->flight_no = $flight_number;
//        $pickup_from->departure_city = $departure_city;
//        $pickup_from->pickup_after = $pickup_after;
//        $pickup_from->save();
//
//        $edited = 1;



        //flash()->success('Order edited successfully !!');
        if($updateFlag == 1){
            $this->saveAll($order_id, $sessData1stStage, $sessData2ndStage, $sessDatatotalDistance, $sessDatatotalPrice, $totalPrice, $totalPricefromDB, $sessData3rdStage, $sessReturnDate, $sessReturnTime, $sessReturnMin, $sessMeetngreet, $pickup_address1, $pickup_address2,$pickup_postcode, $dropoff_address1, $dropoff_address2, $dropoff_postcode, $tourType, $tourdates, $firstname, $lastname, $email, $mobile, $comments, $no_of_person, $no_of_suitcase, $no_of_child, $arrival_time, $airline_name, $flight_number, $departure_city, $pickup_after, $UserId, $passenger, $returndate, $returntime, $returnmin, $pickup_from_airport, $tourType, $stripe_customer_id, $stripe_charge_id, $payment_method);

            Session::forget('1stStageValues');
            Session::forget('2ndStageValues');
            Session::forget('3rdStageValues');
            Session::forget('totalDistance');
            Session::forget('totalprice');
            Session::forget('returnDate');
            Session::forget('returnTime');
            Session::forget('returnMin');
            Session::forget('meetngreet');

            flash()->success('Order edited successfully !!');

            return redirect('editOrderCustomerDeails/'.$order_id);
        }
        else{
            if($clickedPage == 'carSelection')
                return redirect('editOrderCarDeails/'.$order_id);
            else
                return redirect('editOrder/'.$order_id);
        }

    }

    /* /editOrderCustomerDeailsSubmit */

    /* SaveallValues - Edit order from Customer details page */
    public function saveAll($order_id, $sessData1stStage, $sessData2ndStage, $sessDatatotalDistance, $sessDatatotalPrice, $totalPrice, $totalPricefromDB, $sessData3rdStage, $sessReturnDate, $sessReturnTime, $sessReturnMin, $sessMeetngreet, $pickup_address1, $pickup_address2,$pickup_postcode, $dropoff_address1, $dropoff_address2, $dropoff_postcode, $tourType, $tourdates, $firstname, $lastname, $email, $mobile, $comments, $no_of_person, $no_of_suitcase, $no_of_child, $arrival_time, $airline_name, $flight_number, $departure_city, $pickup_after, $UserId, $passenger, $returndate, $returntime, $returnmin, $pickup_from_airport, $tourType, $stripe_customer_id, $stripe_charge_id, $payment_method){


        $id =$passenger->id;

        $passenger = Passengers::find($id);
        $passenger->first_name = $firstname;
        $passenger->last_name = $lastname;
        $passenger->email = $email;
        $passenger->phone_number = $mobile;
        $passenger->save();

        $Orders = Orders::find($order_id);
        $Orders->user_id = $id;


        if($sessData1stStage['waypoint_1'] == '' && $sessData1stStage['waypoint_2'] == '' && $sessData1stStage['waypoint_3'] == '' && $sessData1stStage['waypoint_4'] == '') $waypoints = '';

        $via_pointsArray = array_filter(array($sessData1stStage['waypoint_1'],$sessData1stStage['waypoint_2'],$sessData1stStage['waypoint_3'],$sessData1stStage['waypoint_4']));

        $via_points = implode("|",$via_pointsArray);
        //echo $via_points;die();

        $Orders->pickup_address = $sessData1stStage['pickup'];
        $Orders->dropoff_address = $sessData1stStage['dropoff'];
        $Orders->via_points = $via_points;


        $Orders->pickup_address1 = $pickup_address1;
        $Orders->pickup_address2 = $pickup_address2;
        $Orders->pickup_postcode = $pickup_postcode;

        $Orders->dropoff_address1 = $dropoff_address1;
        $Orders->dropoff_address2 = $dropoff_address2;
        $Orders->dropoff_postcode = $dropoff_postcode;

        if($sessData2ndStage)
            $Orders->car_details = $sessData2ndStage['carname'];

        //if($sessDatatotalPrice)
            $Orders->total_bill = $totalPrice;


        if($sessDatatotalDistance)
            $Orders->total_distance = $sessDatatotalDistance;

        $Orders->date_time = $tourdates;
        $Orders->no_of_child = $no_of_child;
        $Orders->no_of_person = $no_of_person;
        $Orders->suitcases = $no_of_suitcase;
        $Orders->comments = $comments;
        $Orders->editedBy = $UserId;


        if((int)$sessMeetngreet > 0) $Orders->meetngreet = $sessMeetngreet;

        if($tourType == 1){

            $return = strtotime($returndate." ".$returntime.":".$returnmin);

            $Orders->return_date_time = $return;
            $Orders->return_booking = 1;
        }
        else {
            $returndate = '';
            $returntime = '';
            $returnmin = '';
            $Orders->return_date_time = 0;
            $Orders->return_booking = 0;
        }

        //===card info to Order
        $Orders->card_customer_id = $stripe_customer_id;
        $Orders->card_charge_id = $stripe_charge_id;
        $Orders->pay_via = $payment_method;

        $Orders->save();

        $pickup_from_airport_id =$pickup_from_airport->id;

        $pickup_from = Airports::find($pickup_from_airport_id);
        $pickup_from->flight_arrival_time = $arrival_time;
        $pickup_from->airline_name = $airline_name;
        $pickup_from->flight_no = $flight_number;
        $pickup_from->departure_city = $departure_city;
        $pickup_from->pickup_after = $pickup_after;
        $pickup_from->save();

        $adjusted_bill = ($totalPrice - $totalPricefromDB);
        //echo $totalPricefromDB." ".$totalPrice;die();

        $lookupTable = new ordereditHistory();
        $lookupTable->order_id = $order_id;
        $lookupTable->adjusted_bill = $adjusted_bill;
        $lookupTable->save();

    }
    /* /SaveallValues - Edit order from Customer details page */


    /* SaveallValues - Edit order from Car choosing page */
    public function saveAllfromCarpage($order_id, $sessData1stStage, $sessData2ndStage, $sessDatatotalDistance, $sessDatatotalPrice, $sessData3rdStage, $carname, $tripType, $totalprice, $meetngreet, $UserId, $passengerId, $pickup_from_airport){

        //echo $sessDatatotalDistance;die();
        $id =$passengerId;

        if($sessData3rdStage){
            $passenger = Passengers::find($id);
            $passenger->first_name = $sessData3rdStage['firstname'];
            $passenger->last_name = $sessData3rdStage['lastname'];
            $passenger->email = $sessData3rdStage['email'];
            $passenger->phone_number = $sessData3rdStage['mobile'];
            $passenger->save();
        }

        $Orders = Orders::find($order_id);

        if($sessData1stStage){
            if($sessData1stStage['waypoint_1'] == '' && $sessData1stStage['waypoint_2'] == '' && $sessData1stStage['waypoint_3'] == '' && $sessData1stStage['waypoint_4'] == '') $waypoints = '';

            $via_pointsArray = array_filter(array($sessData1stStage['waypoint_1'],$sessData1stStage['waypoint_2'],$sessData1stStage['waypoint_3'],$sessData1stStage['waypoint_4']));

            $via_points = implode("|",$via_pointsArray);

            $Orders->pickup_address = $sessData1stStage['pickup'];
            $Orders->dropoff_address = $sessData1stStage['dropoff'];
            $Orders->via_points = $via_points;
        }

        if($sessData3rdStage){
            $Orders->pickup_address1 = $sessData3rdStage['pickup_address1'];
            $Orders->pickup_address2 = $sessData3rdStage['pickup_address2'];
            $Orders->pickup_postcode = $sessData3rdStage['pickup_postcode'];

            $Orders->dropoff_address1 = $sessData3rdStage['dropoff_address1'];
            $Orders->dropoff_address2 = $sessData3rdStage['dropoff_address2'];
            $Orders->dropoff_postcode = $sessData3rdStage['dropoff_postcode'];
        }

        $Orders->car_details = $carname;

        $Orders->total_bill = $sessDatatotalPrice;

//        if($sessData2ndStage['distances'] == NULL)
//            $Orders->total_distance = $sessDatatotalDistance;
//        else
            $Orders->total_distance = $sessDatatotalDistance;

        if($sessData3rdStage){
            $tourdates = strtotime($sessData3rdStage['tourdate']." ".$sessData3rdStage['tourtime'].":".$sessData3rdStage['tourmin']);

            $Orders->date_time = $tourdates;
            $Orders->no_of_child = $sessData3rdStage['no_of_child'];
            $Orders->no_of_person = $sessData3rdStage['no_of_person'];
            $Orders->suitcases = $sessData3rdStage['no_of_suitcase'];
            $Orders->comments = $sessData3rdStage['comments'];
        }
        $Orders->editedBy = $UserId;


        if((int)$meetngreet > 0) $Orders->meetngreet = $meetngreet;

        if($sessData3rdStage){
            if($tripType == 'round'){

                $return = strtotime($sessData3rdStage['returnDate']." ".$sessData3rdStage['returnTime'].":".$sessData3rdStage['returnMin']);

                $Orders->return_date_time = $return;
                $Orders->return_booking = 1;
            }
            else {
                $returndate = '';
                $returntime = '';
                $returnmin = '';
                $Orders->return_date_time = 0;
                $Orders->return_booking = 0;
            }
        }

        $Orders->save();

        if($sessData3rdStage){
            $pickup_from_airport_id =$pickup_from_airport->id;

            $pickup_from = Airports::find($pickup_from_airport_id);
            $pickup_from->flight_arrival_time = $sessData3rdStage['arrival_time'];
            $pickup_from->airline_name = $sessData3rdStage['airline_name'];
            $pickup_from->flight_no = $sessData3rdStage['flight_number'];
            $pickup_from->departure_city = $sessData3rdStage['departure_city'];
            $pickup_from->pickup_after = $sessData3rdStage['pickup_after'];
            $pickup_from->save();
        }


    }
    /* /SaveallValues - Edit order from Car choosing page */


    /** Change order status & add driver corresponding to order */
    public function changeStatusFromAdmin(Request $getvalue){
        $order_id = $getvalue->get('order_id');
        $status_id = $getvalue->get('status_change');
        $driver_id = $getvalue->get('driver');
        $order = Orders::find($order_id);
        $order->status = $status_id;
        $order->driver_id = $driver_id;
        $order->save();

        $this->sendCustomMailOrderApprove($order_id, $status_id);
        flash()->success('Order request has been updated !!');

        return redirect('orderlist');
    }
    /** /Change order status & add driver corresponding to order */

    /** Set Payment Status */
    public function PaymentStatus(Request $getvalue){
        $order_id = $getvalue->get('order_id');
        $status_id = $getvalue->get('PaymentStatus');
        //$driver_id = $getvalue->get('driver');
        $order = Orders::find($order_id);
        $order->payment_status = $status_id;
        $order->save();

        $this->sendCustomMailPaid($order_id);
        flash()->success('Record has been updated successfully !!');

        return redirect('orderDetails/'.$order_id);
    }
    /** /Set Payment Status */

    /** Approve an order **/
//    public function accept($orderID){
//        DB::table('orders')
//            ->where('id', $orderID)
//            ->update(['status' => 1]);
//
//        $this->sendCustomMailOrderApprove($orderID);
//
//        flash()->success('Order request has been approved !!');
//        return redirect('orderDetails/'.$orderID);
//    }
    /** /Approve an order **/

    /** Reject an order **/
//    public function decline($orderID){
//        DB::table('orders')
//            ->where('id', $orderID)
//            ->update(['status' => 2]);
//        flash()->success('Order request has been rejected !!');
//        return redirect('orderDetails/'.$orderID);
//    }
    /** /Reject an order **/

    /** Add Cars **/
    public function AddCar()
    {
        $CarsCat = CarsCat::all();
        $Driver = Drivers::all();
        return view('admin_blade.pages.AddCar', compact('CarsCat', 'Driver'));
    }

    public function AddCarSave(Request $request)
    {
        $name = $request->get('name');
        $tot_seats = $request->get('tot_seats');
        $cars_cat = $request->get('cars_cat');
        $drivers = $request->get('drivers');

        $Cars_tbl = new CarDetails();
        $Cars_tbl->name = $name;
        $Cars_tbl->total_seats = $tot_seats;
        $Cars_tbl->category = $cars_cat;
        $Cars_tbl->driver_id = $drivers;
        $Cars_tbl->save();

        $car_id = $Cars_tbl->id;
        $pre_car_id = $car_id-1;

        $single_column = 'single_'.$car_id;
        $return_column = 'round_'.$car_id;
        $pre_return_column = 'round_'.$pre_car_id;

        $single_query = 'ALTER TABLE `special_route` ADD `'.$single_column.'` VARCHAR(10) NOT NULL AFTER `'.$pre_return_column.'`;';
        $return_query = 'ALTER TABLE `special_route` ADD `'.$return_column.'` VARCHAR(10) NOT NULL AFTER `'.$single_column.'`;';

        DB::statement($single_query);
        DB::statement($return_query);

        flash()->success('Car Added Successfully !!');

        return redirect('AddCar');
    }

    /** /Add Cars **/

    /** edit Car **/
    public function edit_car($car_id){
            $CarsCat = CarsCat::all();
            $Driver = Drivers::all();

            $cardetails = DB::table('car_details')
                ->leftjoin('drivers', function($join)
                {
                    $join->on('car_details.driver_id', '=', 'drivers.id');
                })
                ->select('car_details.id','car_details.name', 'car_details.total_seats', 'car_details.category', 'car_details.created_at', 'drivers.first_name', 'drivers.last_name', 'drivers.id as driver_id')
                ->where('car_details.id', $car_id)
                ->first();

        return view('admin_blade.pages.editcar', compact('cardetails', 'CarsCat', 'Driver'));
    }

    public function editCarSave(editcarsaveRequest $request)
    {
            $car_id = $request->get('id');
            $name = $request->get('name');
            $tot_seats = $request->get('tot_seats');
            $cars_cat = $request->get('cars_cat');
            $drivers = $request->get('drivers');
            CarDetails::where('id', $car_id)
                ->update(
                    array("name" => $name,
                        "total_seats" => $tot_seats,
                        "category" => $cars_cat,
                        "driver_id" => $drivers
                    )
                );

        flash()->success('Car updated successfully !!');

        return redirect('edit_car/'.$car_id);
    }

    /** /edit Car **/

    /** carlist */
    public function carlist()
    {
        if(Auth::user()->role == 1) {
            $carlist = DB::table('car_details')
                ->leftjoin('drivers', function($join)
                {
                    $join->on('car_details.driver_id', '=', 'drivers.id');
                })
                ->select('car_details.id','car_details.name', 'car_details.total_seats', 'car_details.category', 'car_details.created_at', 'drivers.first_name', 'drivers.last_name')
                ->orderBy('car_details.updated_at', 'car_details.id', 'DESC')
                ->get();
            return view('admin_blade.pages.carlist', compact('carlist'));
        }
    }
    /** /carlist */

    /** Delete a car **/
    public function del_car($car_id){
        DB::table('car_details')
            ->where('id', $car_id)
            ->delete();

        flash()->success('One Car has been deleted !!');
        return redirect('carlist');
    }
    /** /Delete a car **/

    /** Add Driver **/
    public function AddDriver()
    {
        $Driver = Drivers::all();
        return view('admin_blade.pages.AddDriver', compact('Driver'));
    }

    public function AddDriverSave(Request $request)
    {
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $contact_name = $request->get('contact_name');
        $gender = $request->get('gender');
        $address = $request->get('address');
        $city = $request->get('city');
        $street = $request->get('street');
        $postcode = $request->get('postcode');
        $phone = $request->get('phone');
        $email = $request->get('email');

        $password = $request->get('password');
        $role= "2";
        $passwordhash = bcrypt($password);

        $user = new User();
        $id = $user::insertGetId(array('email'=>$email, 'password'=>$passwordhash, 'role'=>$role));

        $Driver_tbl = new Drivers();
        $Driver_tbl->user_id = $id;
        $Driver_tbl->first_name = $first_name;
        $Driver_tbl->last_name = $last_name;
        $Driver_tbl->contact_name = $contact_name;
        $Driver_tbl->gender = $gender;
        $Driver_tbl->address = $address;
        $Driver_tbl->city = $city;
        $Driver_tbl->street = $street;
        $Driver_tbl->postcode = $postcode;
        $Driver_tbl->phone_number = $phone;
        $Driver_tbl->email = $email;
        $Driver_tbl->save();

        flash()->success('Driver Added Successfully !!');

        return redirect('AddDriver');
    }
    /** /Add Driver **/

    /** edit Driver **/
    public function edit_driver($driver_id){
       $driverdetails = DB::table('drivers')
            ->where('id', $driver_id)
            ->first();

        return view('admin_blade.pages.editdriver', compact('driverdetails'));
    }

    public function editDriverSave(editdriveresaveRequest $request)
    {
        $driver_id = $request->get('id');
        $user_id = $request->get('user_id');
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $contact_name = $request->get('contact_name');
        $gender = $request->get('gender');
        $address = $request->get('address');
        $city = $request->get('city');
        $street = $request->get('street');
        $postcode = $request->get('postcode');
        $phone = $request->get('phone');
        $email = $request->get('email');
        $password = $request->get('password');
        $passwordhash = bcrypt($password);
        if($password AND $email){
            User::where('id', $user_id)
            ->update(
                array("email" => $email,
                    "password" => $passwordhash
                )
            );
        }
        elseif($password){
            User::where('id', $user_id)
                ->update(
                    array("password" => $passwordhash)
                );
        }
        elseif($email){
            User::where('id', $user_id)
                ->update(
                    array("email" => $email)
                );
        }


        Drivers::where('id', $driver_id)
            ->update(
                array("first_name" => $first_name,
                    "last_name" => $last_name,
                    "contact_name" => $contact_name,
                    "gender" => $gender,
                    "address" => $address,
                    "city" => $city,
                    "street" => $street,
                    "postcode" => $postcode,
                    "phone_number" => $phone,
                    "street" => $street,
                    "email" => $email
                )
            );

        flash()->success('Driver updated successfully !!');

        return redirect('edit_driver/'.$driver_id);
    }

    /** /edit Driver **/

    /** driverlist */
    public function driverlist()
    {
        if(Auth::user()->role == 1) {
            $driverlist = DB::table('drivers')
                ->orderBy('drivers.updated_at', 'drivers.id', 'DESC')
                ->get();
            return view('admin_blade.pages.driverlist', compact('driverlist'));
        }
    }
    /** /driverlist */

    /** Delete a driver **/
    public function del_driver($driver_id){
        DB::table('drivers')
            ->where('user_id', $driver_id)
            ->delete();

        DB::table('users')
            ->where('id', $driver_id)
            ->delete();

        flash()->success('One Driver has been deleted !!');
        return redirect('driverlist');
    }
    /** /Delete a driver **/

    /** del an order **/
    public function delOrder($orderID){
        Orders::where('id', '=', $orderID)->delete();
        flash()->success('Order has been deleted successfully !!');
        return redirect('orderlist');
    }
    /** /del an order **/

    /** edit minimum booking time **/
    public function bookingTime($id){
        $minimumBookingtime = DB::table('booking_time')
            ->where('id', $id)
            ->first();
        return view('admin_blade.pages.bookingTime', compact('minimumBookingtime'));
    }

    public function bookingTimesave(Request $request)
    {
        $id = $request->get('id');
        $booking_time = $request->get('booking_time');

        bookingTime::where('id', $id)
            ->update(
                array("minimum_booking_time" => $booking_time
                )
            );

        flash()->success('Updated successfully !!');

        return redirect('bookingTime/'.$id);
    }

    /** /edit minimum booking time **/

    /* send email during order Approve */
    public function sendCustomMailOrderApprove($orderID, $status_id)
    {
        $order_info = DB::table('orders')
            ->leftjoin('passengers', function($join)
            {
                $join->on('orders.user_id', '=', 'passengers.id');
            })
            ->leftjoin('car_details', function($join)
            {
                $join->on('orders.car_details', '=', 'car_details.id');
            })

            ->leftjoin('pickup_from_airport', function($join)
            {
                $join->on('orders.id', '=', 'pickup_from_airport.order_id');
            })

            ->leftjoin('drivers', function($join)
            {
                $join->on('orders.driver_id', '=', 'drivers.id');
            })

            ->select('orders.id','orders.user_id', 'orders.dropoff_address', 'orders.dropoff_address1','orders.dropoff_address2', 'orders.dropoff_postcode', 'orders.pickup_address', 'orders.pickup_address1', 'orders.pickup_address2', 'orders.pickup_postcode', 'orders.no_of_person', 'orders.no_of_child', 'orders.suitcases', 'orders.via_points', 'orders.meetngreet', 'orders.comments', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.status', 'orders.payment_status', 'orders.date_time', 'orders.return_booking', 'orders.return_date_time', 'orders.driver_id', 'orders.created_at', 'drivers.first_name as dFname', 'drivers.last_name as dLname', 'drivers.email as dEmail', 'drivers.phone_number as dPhone', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'car_details.name as carname', 'car_details.total_seats', 'pickup_from_airport.flight_arrival_time', 'pickup_from_airport.airline_name', 'pickup_from_airport.flight_no', 'pickup_from_airport.departure_city', 'pickup_from_airport.pickup_after')

            ->where('orders.id',$orderID)
            ->first();

        $tourType = $order_info->return_booking;
        $via_points = $order_info->via_points;

        if($via_points){
            $via_points_all = explode("|",$via_points);
            $ViaPointscount = count($via_points_all);

            $via_point_details = '
            <tr>
                <td colspan="2" style="height:10px; padding-top: 10px; padding-left:30px;">
                    <strong>WayPoints:</strong>
                </td>
            </tr>
            ';
            $via_points_seperate = '';

            for($i = 1; $i <= $ViaPointscount; $i++){
                $via_points_seperate .= '
                <tr>
                    <td colspan="2" style="height:10px; padding-left:40px;">
                        '.$i.'. '.str_replace('+',' ',$via_points_all[$i-1]).'
                    </td>
                </tr>
                ';

                //$via_points_seperate .= ''.$i.'. '.str_replace('+',' ',$via_points_all[$i-1]).'<br/>';
            }
        }
        else{
            $via_point_details = '';
            $via_points_seperate = '';
        }

        if($tourType == 1){
            $return_details = '
            <tr>
                <td colspan="2" style="height:10px; padding-top: 10px; padding-left:30px;">
                    <strong>Roundtrip Time: </strong> '.date('l j M Y, H:i', $order_info->return_date_time).' ('.date('h:i a', $order_info->return_date_time).')
                </td>
            </tr>
            ';

            //$return_details = '<strong>Roundtrip Time</strong>
            //'.date('l j M Y, H:i', $order_info->return_date_time).' ('.date('h:i a', $order_info->return_date_time).')<br/>';
        }
        else{
            $return_details = '';
        }

        $headers = "From: Asiana Cars <" . strip_tags("info@asianacars.com") . ">\r\n";
        $headers .= "Reply-To: Asiana Cars <" . strip_tags("info@asianacars.com") . ">\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


        if($status_id == 1){
            $status = 'Approved';
            $body = '


                    <tr>
                        <td colspan="2" style="height:10px; padding-left:30px; padding-bottom:10;">
                            We are delighted to confirm your booking request. Please, check the journey details below.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Order ID : </strong> Ref-'.$orderID.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Desired Vehicle</strong>: '.$order_info->carname.' ('.$order_info->total_seats.' Persons)
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Luggage</strong>:'.$order_info->suitcases.' suitcases
                        </td>
                   </tr>

                    <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Head Passenger:</strong>
            '.$order_info->first_name.' '.$order_info->last_name.'</span>
                        </td>
                   </tr>
                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile</strong>: '.$order_info->phone_number.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Email</strong>: '.$order_info->email.'</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>No. of Person</strong>: '.$order_info->no_of_person.' Nos.</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Child</strong>: '.$order_info->no_of_child.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Suitcase</strong>:'.$order_info->suitcases.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Comments</strong>: '.$order_info->comments.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup Time:</strong>:
            '.date('l j M Y, H:i', $order_info->date_time).' ('.date('h:i a', $order_info->date_time).')
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Contact Person</strong>: '.$order_info->dFname.' '.$order_info->dLname.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile Number</strong>: '.$order_info->dPhone.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup</strong>: '.$order_info->pickup_address.', '.$order_info->pickup_address1.', '.$order_info->pickup_address2.', '.$order_info->pickup_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Drop-off</strong>: '.$order_info->dropoff_address.', '.$order_info->dropoff_address1.', '.$order_info->dropoff_address2.', '.$order_info->dropoff_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Total Distance</strong>: '.ceil($order_info->total_distance).' miles
                        </td>
                   </tr>

                          '.$via_point_details.'
                          '.$via_points_seperate.'

                          '.$return_details.'
        ';

            if((int)$order_info->meetngreet > 0) $body .= '<tr>
                        <td colspan="2" style="height:10px; padding-top:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Special Requirement: </strong>: + Meet & Greet  '.$this->currencySymbol.$order_info->meetngreet.'.00 (Included)
                        </td>
                   </tr>';

            //$body .= '<strong>Special Requirement: </strong>: + Meet & Greet  '.$this->currencySymbol.$order_info->meetngreet.'.00 (Included)<br>';

            $body .='

            <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-left:30px;">
                    <strong>Transfer Information</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Arrival</strong>: '.$order_info->flight_arrival_time.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Airline</strong>: '.$order_info->airline_name.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Flight</strong>: '.$order_info->flight_no.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Departure City</strong>: '.$order_info->departure_city.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                    <strong>Pickup after landing</strong>: '.$order_info->pickup_after.' minutes
                </td>
           </tr>


        ';
        }
        elseif($status_id == 3){
            $status = 'Confirm LGW North';
            $body = '
            <tr>
                <td colspan="2" style="height:10px; padding-left:30px; padding-bottom:10;">
                    We are delighted to confirm your booking request. Please, check the journey details below.
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding-left:30px; padding-bottom:10; font-weight:bold; color:red;">
                    Note: The price not included Toll / Congestion Charge. Customers can pay Toll / Congestion charge in cash to the driver.
                </td>
            </tr>

            <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Order ID : </strong> Ref-'.$orderID.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Desired Vehicle</strong>: '.$order_info->carname.' ('.$order_info->total_seats.' Persons)
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Luggage</strong>:'.$order_info->suitcases.' suitcases
                        </td>
                   </tr>

                    <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Head Passenger:</strong>
            '.$order_info->first_name.' '.$order_info->last_name.'</span>
                        </td>
                   </tr>
                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile</strong>: '.$order_info->phone_number.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Email</strong>: '.$order_info->email.'</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>No. of Person</strong>: '.$order_info->no_of_person.' Nos.</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Child</strong>: '.$order_info->no_of_child.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Suitcase</strong>:'.$order_info->suitcases.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Comments</strong>: '.$order_info->comments.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup Time:</strong>:
            '.date('l j M Y, H:i', $order_info->date_time).' ('.date('h:i a', $order_info->date_time).')
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Contact Person</strong>: '.$order_info->dFname.' '.$order_info->dLname.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile Number</strong>: '.$order_info->dPhone.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup</strong>: '.$order_info->pickup_address.', '.$order_info->pickup_address1.', '.$order_info->pickup_address2.', '.$order_info->pickup_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Drop-off</strong>: '.$order_info->dropoff_address.', '.$order_info->dropoff_address1.', '.$order_info->dropoff_address2.', '.$order_info->dropoff_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Total Distance</strong>: '.ceil($order_info->total_distance).' miles
                        </td>
                   </tr>

                          '.$via_point_details.'
                          '.$via_points_seperate.'

                          '.$return_details.'

           <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-top:10px; padding-left:30px;">
                    <strong>Instructions For Customers</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="padding-left:30px; text-align:justify; padding-right:10px;">
                    On arrival at the North Terminal, please go outside of the airport and make your way towards the Sofitel Hotel or Premier Inn. Your driver will meet you here. Please, call your driver on the supplied number below to arrange pickup as soon as possible. It is important that you make contact with the representative / driver within 1 hour after landing so that we know you are at the airport.
                </td>
           </tr>


        ';

            if((int)$order_info->meetngreet > 0) $body .= '<tr>
                        <td colspan="2" style="height:10px; padding-top:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Special Requirement: </strong>: + Meet & Greet  '.$this->currencySymbol.$order_info->meetngreet.'.00 (Included)
                        </td>
                   </tr>';

            $body .='

            <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-left:30px;">
                    <strong>Transfer Information</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Arrival</strong>: '.$order_info->flight_arrival_time.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Airline</strong>: '.$order_info->airline_name.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Flight</strong>: '.$order_info->flight_no.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Departure City</strong>: '.$order_info->departure_city.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                    <strong>Pickup after landing</strong>: '.$order_info->pickup_after.' minutes
                </td>
           </tr>

        ';
        }
        elseif($status_id == 4){
            $status = 'Confirm LGW South';
            $body = '

            <tr>
                <td colspan="2" style="height:10px; padding-left:30px; padding-bottom:10;">
                    We are delighted to confirm your booking request. Please, check the journey details below.
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding-left:30px; padding-bottom:10; font-weight:bold; color:red;">
                    Note: The price not included Toll / Congestion Charge. Customers can pay Toll / Congestion charge in cash to the driver.
                </td>
            </tr>

            <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Order ID : </strong> Ref-'.$orderID.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Desired Vehicle</strong>: '.$order_info->carname.' ('.$order_info->total_seats.' Persons)
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Luggage</strong>:'.$order_info->suitcases.' suitcases
                        </td>
                   </tr>

                    <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Head Passenger:</strong>
            '.$order_info->first_name.' '.$order_info->last_name.'</span>
                        </td>
                   </tr>
                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile</strong>: '.$order_info->phone_number.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Email</strong>: '.$order_info->email.'</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>No. of Person</strong>: '.$order_info->no_of_person.' Nos.</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Child</strong>: '.$order_info->no_of_child.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Suitcase</strong>:'.$order_info->suitcases.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Comments</strong>: '.$order_info->comments.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup Time:</strong>:
            '.date('l j M Y, H:i', $order_info->date_time).' ('.date('h:i a', $order_info->date_time).')
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Contact Person</strong>: '.$order_info->dFname.' '.$order_info->dLname.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile Number</strong>: '.$order_info->dPhone.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup</strong>: '.$order_info->pickup_address.', '.$order_info->pickup_address1.', '.$order_info->pickup_address2.', '.$order_info->pickup_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Drop-off</strong>: '.$order_info->dropoff_address.', '.$order_info->dropoff_address1.', '.$order_info->dropoff_address2.', '.$order_info->dropoff_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Total Distance</strong>: '.ceil($order_info->total_distance).' miles
                        </td>
                   </tr>

                          '.$via_point_details.'
                          '.$via_points_seperate.'

                          '.$return_details.'

           <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-top:10px; padding-left:30px;">
                    <strong>Instructions For Customers</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="padding-left:30px; text-align:justify; padding-right:10px;">
                    On arrival at the South Terminal, please go downstairs and make your way towards the drop off point. Once outside, turn left, your driver will meet you here. Please, call your driver on the supplied number below to arrange pickup as soon as possible. It is important that you make contact with the representative / driver within 1 hour after landing so that we know you are at the airport.
                </td>
           </tr>


        ';

            if((int)$order_info->meetngreet > 0) $body .= '<tr>
                        <td colspan="2" style="height:10px; padding-top:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Special Requirement: </strong>: + Meet & Greet  '.$this->currencySymbol.$order_info->meetngreet.'.00 (Included)
                        </td>
                   </tr>';

            $body .='

            <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-left:30px;">
                    <strong>Transfer Information</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Arrival</strong>: '.$order_info->flight_arrival_time.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Airline</strong>: '.$order_info->airline_name.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Flight</strong>: '.$order_info->flight_no.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Departure City</strong>: '.$order_info->departure_city.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                    <strong>Pickup after landing</strong>: '.$order_info->pickup_after.' minutes
                </td>
           </tr>
        ';
        }
        elseif($status_id == 5){
            $status = 'Confirm LHR T3 M&G';
            $body = '
            <tr>
                <td colspan="2" style="height:10px; padding-left:30px; padding-bottom:10;">
                    We are delighted to confirm your booking request. Please, check the journey details below.
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding-left:30px; padding-bottom:10; font-weight:bold; color:red;">
                    Note: The price not included Toll / Congestion Charge. Customers can pay Toll / Congestion charge in cash to the driver.
                </td>
            </tr>

            <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Order ID : </strong> Ref-'.$orderID.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Desired Vehicle</strong>: '.$order_info->carname.' ('.$order_info->total_seats.' Persons)
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Luggage</strong>:'.$order_info->suitcases.' suitcases
                        </td>
                   </tr>

                    <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Head Passenger:</strong>
            '.$order_info->first_name.' '.$order_info->last_name.'</span>
                        </td>
                   </tr>
                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile</strong>: '.$order_info->phone_number.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Email</strong>: '.$order_info->email.'</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>No. of Person</strong>: '.$order_info->no_of_person.' Nos.</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Child</strong>: '.$order_info->no_of_child.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Suitcase</strong>:'.$order_info->suitcases.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Comments</strong>: '.$order_info->comments.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup Time:</strong>:
            '.date('l j M Y, H:i', $order_info->date_time).' ('.date('h:i a', $order_info->date_time).')
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Contact Person</strong>: '.$order_info->dFname.' '.$order_info->dLname.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile Number</strong>: '.$order_info->dPhone.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup</strong>: '.$order_info->pickup_address.', '.$order_info->pickup_address1.', '.$order_info->pickup_address2.', '.$order_info->pickup_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Drop-off</strong>: '.$order_info->dropoff_address.', '.$order_info->dropoff_address1.', '.$order_info->dropoff_address2.', '.$order_info->dropoff_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Total Distance</strong>: '.ceil($order_info->total_distance).' miles
                        </td>
                   </tr>

                          '.$via_point_details.'
                          '.$via_points_seperate.'

                          '.$return_details.'

           <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-top:10px; padding-left:30px;">
                    <strong>Instructions For Customers</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="padding-left:30px; text-align:justify; padding-right:10px;">
                    When you enter the Arrivals hall please walk forward and then take a right turn. Your driver will be waiting with your name on a board in front of the WHSmiths next to the Boots. If can not locate him please call on the number supplied below. It is important that you make contact with the driver/representative within 1 hour after landing so we know you are at the airport.
                </td>
           </tr>


        ';

            if((int)$order_info->meetngreet > 0) $body .= '<tr>
                        <td colspan="2" style="height:10px; padding-top:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Special Requirement: </strong>: + Meet & Greet  '.$this->currencySymbol.$order_info->meetngreet.'.00 (Included)
                        </td>
                   </tr>';

            $body .='

            <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-left:30px;">
                    <strong>Transfer Information</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Arrival</strong>: '.$order_info->flight_arrival_time.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Airline</strong>: '.$order_info->airline_name.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Flight</strong>: '.$order_info->flight_no.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Departure City</strong>: '.$order_info->departure_city.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                    <strong>Pickup after landing</strong>: '.$order_info->pickup_after.' minutes
                </td>
           </tr>
        ';
        }
        elseif($status_id == 6){
            $status = 'Confirm M&G';
            $body = '
            <tr>
                <td colspan="2" style="height:10px; padding-left:30px; padding-bottom:10;">
                    We are delighted to confirm your booking request. Please, check the journey details below.
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding-left:30px; padding-bottom:10; font-weight:bold; color:red;">
                    Note: The price not included Toll / Congestion Charge. Customers can pay Toll / Congestion charge in cash to the driver.
                </td>
            </tr>

            <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Order ID : </strong> Ref-'.$orderID.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Desired Vehicle</strong>: '.$order_info->carname.' ('.$order_info->total_seats.' Persons)
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Luggage</strong>:'.$order_info->suitcases.' suitcases
                        </td>
                   </tr>

                    <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Head Passenger:</strong>
            '.$order_info->first_name.' '.$order_info->last_name.'</span>
                        </td>
                   </tr>
                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile</strong>: '.$order_info->phone_number.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Email</strong>: '.$order_info->email.'</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>No. of Person</strong>: '.$order_info->no_of_person.' Nos.</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Child</strong>: '.$order_info->no_of_child.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Suitcase</strong>:'.$order_info->suitcases.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Comments</strong>: '.$order_info->comments.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup Time:</strong>:
            '.date('l j M Y, H:i', $order_info->date_time).' ('.date('h:i a', $order_info->date_time).')
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Contact Person</strong>: '.$order_info->dFname.' '.$order_info->dLname.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile Number</strong>: '.$order_info->dPhone.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup</strong>: '.$order_info->pickup_address.', '.$order_info->pickup_address1.', '.$order_info->pickup_address2.', '.$order_info->pickup_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Drop-off</strong>: '.$order_info->dropoff_address.', '.$order_info->dropoff_address1.', '.$order_info->dropoff_address2.', '.$order_info->dropoff_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Total Distance</strong>: '.ceil($order_info->total_distance).' miles
                        </td>
                   </tr>

                          '.$via_point_details.'
                          '.$via_points_seperate.'

                          '.$return_details.'

           <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-top:10px; padding-left:30px;">
                    <strong>Instructions For Customers</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="padding-left:30px; text-align:justify; padding-right:10px;">
                    On your way out through Arrivals your driver/ representative will be standing in the centre with your name. If you can not locate him please call on the number supplied below. It is important that you make contact with the representative / driver on the number supplied within 1 hour of landing so that we know you are at the airport.
                </td>
           </tr>


        ';

            if((int)$order_info->meetngreet > 0) $body .= '<tr>
                        <td colspan="2" style="height:10px; padding-top:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Special Requirement: </strong>: + Meet & Greet  '.$this->currencySymbol.$order_info->meetngreet.'.00 (Included)
                        </td>
                   </tr>';

            $body .='

            <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-left:30px;">
                    <strong>Transfer Information</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Arrival</strong>: '.$order_info->flight_arrival_time.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Airline</strong>: '.$order_info->airline_name.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Flight</strong>: '.$order_info->flight_no.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Departure City</strong>: '.$order_info->departure_city.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                    <strong>Pickup after landing</strong>: '.$order_info->pickup_after.' minutes
                </td>
           </tr>
        ';
        }
        elseif($status_id == 7){
            $status = 'Confirm Non Airport';
            $body = '
            <tr>
                <td colspan="2" style="height:10px; padding-left:30px; padding-bottom:10;">
                    We are delighted to confirm your booking request. Please, check the journey details below.
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding-left:30px; padding-bottom:10; font-weight:bold; color:red;">
                    Note: The price not included Toll / Congestion Charge. Customers can pay Toll / Congestion charge in cash to the driver.
                </td>
            </tr>

            <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Order ID : </strong> Ref-'.$orderID.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Desired Vehicle</strong>: '.$order_info->carname.' ('.$order_info->total_seats.' Persons)
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Luggage</strong>:'.$order_info->suitcases.' suitcases
                        </td>
                   </tr>

                    <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Head Passenger:</strong>
            '.$order_info->first_name.' '.$order_info->last_name.'</span>
                        </td>
                   </tr>
                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile</strong>: '.$order_info->phone_number.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Email</strong>: '.$order_info->email.'</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>No. of Person</strong>: '.$order_info->no_of_person.' Nos.</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Child</strong>: '.$order_info->no_of_child.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Suitcase</strong>:'.$order_info->suitcases.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Comments</strong>: '.$order_info->comments.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup Time:</strong>:
            '.date('l j M Y, H:i', $order_info->date_time).' ('.date('h:i a', $order_info->date_time).')
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Contact Person</strong>: '.$order_info->dFname.' '.$order_info->dLname.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile Number</strong>: '.$order_info->dPhone.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup</strong>: '.$order_info->pickup_address.', '.$order_info->pickup_address1.', '.$order_info->pickup_address2.', '.$order_info->pickup_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Drop-off</strong>: '.$order_info->dropoff_address.', '.$order_info->dropoff_address1.', '.$order_info->dropoff_address2.', '.$order_info->dropoff_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Total Distance</strong>: '.ceil($order_info->total_distance).' miles
                        </td>
                   </tr>

                          '.$via_point_details.'
                          '.$via_points_seperate.'

                          '.$return_details.'

           <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-top:10px; padding-left:30px;">
                    <strong>Instructions For Customers</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="padding-left:30px; text-align:justify; padding-right:10px;">
                    Please contact the number below should you have difficulties to meet with the representative / driver.
                </td>
           </tr>


        ';

            if((int)$order_info->meetngreet > 0) $body .= '<tr>
                        <td colspan="2" style="height:10px; padding-top:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Special Requirement: </strong>: + Meet & Greet  '.$this->currencySymbol.$order_info->meetngreet.'.00 (Included)
                        </td>
                   </tr>';

            $body .='

            <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-left:30px;">
                    <strong>Transfer Information</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Arrival</strong>: '.$order_info->flight_arrival_time.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Airline</strong>: '.$order_info->airline_name.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Flight</strong>: '.$order_info->flight_no.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Departure City</strong>: '.$order_info->departure_city.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                    <strong>Pickup after landing</strong>: '.$order_info->pickup_after.' minutes
                </td>
           </tr>
        ';
        }
        else{
            $status = 'Rejected';
            $body = '
            <tr>
                <td colspan="2" style="height:10px; padding-left:30px; padding-bottom:10;">
                    We are sorry. Your order has been rejected.
                </td>
            </tr>

            <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Order ID : </strong> Ref-'.$orderID.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Desired Vehicle</strong>: '.$order_info->carname.' ('.$order_info->total_seats.' Persons)
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Luggage</strong>:'.$order_info->suitcases.' suitcases
                        </td>
                   </tr>

                    <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Head Passenger:</strong>
            '.$order_info->first_name.' '.$order_info->last_name.'</span>
                        </td>
                   </tr>
                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile</strong>: '.$order_info->phone_number.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>Email</strong>: '.$order_info->email.'</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <span><strong>No. of Person</strong>: '.$order_info->no_of_person.' Nos.</span>
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Child</strong>: '.$order_info->no_of_child.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>No. of Suitcase</strong>:'.$order_info->suitcases.' Nos.
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Comments</strong>: '.$order_info->comments.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup Time:</strong>:
            '.date('l j M Y, H:i', $order_info->date_time).' ('.date('h:i a', $order_info->date_time).')
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Contact Person</strong>: '.$order_info->dFname.' '.$order_info->dLname.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Mobile Number</strong>: '.$order_info->dPhone.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Pickup</strong>: '.$order_info->pickup_address.', '.$order_info->pickup_address1.', '.$order_info->pickup_address2.', '.$order_info->pickup_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Drop-off</strong>: '.$order_info->dropoff_address.', '.$order_info->dropoff_address1.', '.$order_info->dropoff_address2.', '.$order_info->dropoff_postcode.'
                        </td>
                   </tr>

                   <tr>
                        <td colspan="2" style="height:10px; padding-left:30px;">
                            <strong>Total Distance</strong>: '.ceil($order_info->total_distance).' miles
                        </td>
                   </tr>

                          '.$via_point_details.'
                          '.$via_points_seperate.'

                          '.$return_details.'

           <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-top:10px; padding-left:30px;">
                    <strong>Instructions For Customers</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="padding-left:30px; text-align:justify; padding-right:10px;">
                    Please contact the number below should you have difficulties to meet with the representative / driver.
                </td>
           </tr>


        ';

            if((int)$order_info->meetngreet > 0) $body .= '<tr>
                        <td colspan="2" style="height:10px; padding-top:10px; padding-bottom:10px; padding-left:30px;">
                            <strong>Special Requirement: </strong>: + Meet & Greet  '.$this->currencySymbol.$order_info->meetngreet.'.00 (Included)
                        </td>
                   </tr>';

            $body .='

            <tr>
                <td colspan="2" style="height:10px; font-size:18px; padding-left:30px;">
                    <strong>Transfer Information</strong>
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Arrival</strong>: '.$order_info->flight_arrival_time.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Airline</strong>: '.$order_info->airline_name.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Flight</strong>: '.$order_info->flight_no.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-left:30px;">
                    <strong>Departure City</strong>: '.$order_info->departure_city.'
                </td>
           </tr>

           <tr>
                <td colspan="2" style="height:10px; padding-bottom:10px; padding-left:30px;">
                    <strong>Pickup after landing</strong>: '.$order_info->pickup_after.' minutes
                </td>
           </tr>

        ';
        }


    $email_temp = '<html>
    <head>
        <title>Taxi Booking</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body bgcolor="#d4d2d2" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

    <table width="600" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
      <tr>
        <td align="center">
            <img src="http://asianacars.com/assets/images/logo2.jpg" alt="Logo">
        </td>
      </tr>

      <tr>
          <td width="600" height="100%" valign="top">

              <table cellpadding="0" cellspacing="0" style="color:#000; border:1px solid #EBEBEB; font-family:Arial, sans-serif">

                   <tr>
                        <td colspan="2">
                            <img src="http://asianacars.com/assets/images/Banner-600.png" alt="Banner">
                        </td>
                   </tr>

                    <tr>
                      <td colspan="2" style="padding:10px 30px;" style="font-family:Arial, sans-serif;">

                          Dear '.$order_info->first_name.' '.$order_info->last_name.',
                      </td>
                    </tr>

                         '.$body.'

                    <tr>
                      <td colspan="2" style="padding-left:30px; padding-bottom:10px;" style="font-family:Arial, sans-serif;">
                          <strong>You have accepted this job for </strong>'.$this->currencySymbol. number_format($order_info->total_bill,2).'
                      </td>
                    </tr>

                    <tr>
                      <td colspan="2" style="padding-left:30px;" style="font-family:Arial, sans-serif;">
                          <strong>Regards</strong>
                      </td>
                    </tr>

                    <tr>
                      <td colspan="2" style="padding-left:30px;" style="font-family:Arial, sans-serif;">
                          Asiana Cars Ltd
                      </td>
                    </tr>

                    <tr>
                      <td colspan="2" style="padding-left:30px; padding-bottom:20px;" style="font-family:Arial, sans-serif;">
                          www.asianacars.com
                      </td>
                    </tr>


                    <tr style="border-top:1px solid #EBEBEB;">
                        <td width="300" height="40" align="left" style="color:#000; padding-left:30px; padding-bottom:15px; font-family:Arial, sans-serif">
                              <strong>Contact Information</strong><br>
                              Tel : +44 1737668060<br>
                              <a href="mailto:info@asianacars.com" style="color:#000;">info@asianacars.com</a><br>
                        </td>
                        <td width="300" height="20" align="right" style="color:#fff; padding-right:30px; font-family:Arial, sans-serif">
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/fb.png" alt="fb">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/twitter.png" alt="twitter">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/linkedin.png" alt="linkedin">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/youtube.png" alt="youtube">
                            </a>
                        </td>
                    </tr>

                    <tr>
                    <td colspan="2" width="600">
                        <img src="http://asianacars.com/assets/images/line.jpg" alt="Line">
                    </td>
                  </tr>

              </table>
          </td>

      </tr>

    </table>

    </body>
    </html>';

        if($status_id == 2){
            $subUser = "Asiana Cars: Booking Rejected: Ref Number: ".$order_info->id;
        }
        else{
            $subUser = "Asiana Cars: Booking Confirmation: Ref Number: ".$order_info->id;
            $subDriver = "Asiana Cars: One Order Approved: ".$order_info->id;
        }
        $subAdmin = "Asiana Cars: One Order ".$status.": ".$order_info->id;
//        if($status_id == 1){
//            $subDriver = "Asiana Cars: One Order Approved: ".$order_info->id;
//        }
        //echo $email_temp;die();
        /*This is email Body Content for sending an email when approve an order*/
        mail($order_info->email, $subUser, $email_temp, $headers);
        if($status_id == 1){
            mail($order_info->dEmail, $subDriver, $email_temp, $headers);
        }

        /*For All Admin Email Send*/
        $allAdminRoleID = User::where('role','1')->get();

        foreach($allAdminRoleID as $adminID){
            mail($adminID->email, $subAdmin, $email_temp, $headers);
        }

    }

    /* /send email order Approve */



    /* send email When payment status is paid */
    public function sendCustomMailPaid($orderID)
    {
        $order_info = DB::table('orders')
            ->leftjoin('passengers', function($join)
            {
                $join->on('orders.user_id', '=', 'passengers.id');
            })
            ->leftjoin('car_details', function($join)
            {
                $join->on('orders.car_details', '=', 'car_details.id');
            })

            ->leftjoin('pickup_from_airport', function($join)
            {
                $join->on('orders.id', '=', 'pickup_from_airport.order_id');
            })

            ->leftjoin('drivers', function($join)
            {
                $join->on('orders.driver_id', '=', 'drivers.id');
            })

            ->select('orders.id','orders.user_id', 'orders.dropoff_address', 'orders.dropoff_address1','orders.dropoff_address2', 'orders.dropoff_postcode', 'orders.pickup_address', 'orders.pickup_address1', 'orders.pickup_address2', 'orders.pickup_postcode', 'orders.no_of_person', 'orders.no_of_child', 'orders.suitcases', 'orders.via_points', 'orders.comments', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.status', 'orders.payment_status', 'orders.date_time', 'orders.return_booking', 'orders.return_date_time', 'orders.driver_id', 'orders.created_at', 'drivers.first_name as dFname', 'drivers.last_name as dLname', 'drivers.email as dEmail', 'drivers.phone_number as dPhone', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'car_details.name as carname', 'car_details.total_seats', 'pickup_from_airport.flight_arrival_time', 'pickup_from_airport.airline_name', 'pickup_from_airport.flight_no', 'pickup_from_airport.departure_city', 'pickup_from_airport.pickup_after')

            ->where('orders.id',$orderID)
            ->first();

        $tourType = $order_info->return_booking;
        $via_points = $order_info->via_points;

        $headers = "From: Asiana Cars <" . strip_tags("info@asianacars.com") . ">\r\n";
        $headers .= "Reply-To: Asiana Cars <" . strip_tags("info@asianacars.com") . ">\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";



        $body = '<p>Bill paid for Reference Id : '.$orderID.'.  </p>

        <strong>Order information:</strong><br><br>


        <strong>Passenger Details:</strong><br><br/>

        <strong>Passenger Name</strong>: '.$order_info->first_name.' '.$order_info->last_name.'<br>
        <strong>Mobile</strong>: '.$order_info->mobile.'<br>
        <strong>Email Address</strong>: '.$order_info->email.'<br>
        <strong>Comments</strong>: '.$order_info->comments.'<br><br/>


        <strong>Pickup Details:</strong><br><br/>

        <strong>Pickup Address</strong>: '.$order_info->pickup_address.'<br>
        <strong>Pickup Address1</strong>: '.$order_info->pickup_address1.'<br>
        <strong>Pickup Address2</strong>: '.$order_info->pickup_address2.'<br>
        <strong>Pickup Postcode</strong>: '.$order_info->pickup_postcode.'<br><br>


        <strong>Drop-off Details:</strong><br><br/>

        <strong>Drop-off Address</strong>: '.$order_info->dropoff_address.'<br>
        <strong>Drop-off Address1</strong>: '.$order_info->dropoff_address1.'<br>
        <strong>Drop-off Address2</strong>: '.$order_info->dropoff_address2.'<br>
        <strong>Drop-off Postcode</strong>: '.$order_info->dropoff_postcode.'<br><br>


        <strong>Journey Details:</strong><br><br/>

        <strong>Total Distance</strong>: '.ceil($order_info->total_distance).' miles<br>
        <strong>Total Cost</strong>: '. $this->currencySymbol .number_format($order_info->total_bill,2).'<br><br>


        <strong>Journey Date &amp; Time:</strong><br><br/>

        <strong>Journey Date</strong>: '.date('m-d-Y', $order_info->date_time).'<br>
        <strong>Journey Time</strong>: '.date('H:i', $order_info->date_time).'<br><br>


        <strong>Passenger Details:</strong><br><br/>

        <strong>No. of Person</strong>: '.$order_info->no_of_person.'<br>
        <strong>No. of Child</strong>: '.$order_info->no_of_child.'<br>
        <strong>No. of Suitcase</strong>: '.$order_info->suitcases.'<br><br>


        <strong>Airport Details:</strong><br><br/>

        <strong>Arrival Time</strong>: '.$order_info->arrival_time.'<br>
        <strong>Airline Name</strong>: '.$order_info->airline_name.'<br>
        <strong>Flight Number</strong>: '.$order_info->flight_number.'<br>
        <strong>Departure City</strong>: '.$order_info->departure_city.'<br>
        <strong>Pickup after landing</strong>: '.$order_info->pickup_after.' minutes<br>
        <strong>Reference ID</strong>: '.$orderID.'<br>';


        if($tourType == 1){
            $return_details = '<br/> <strong>Roundtrip information:</strong><br><br>

                <strong>Return Date</strong>: '.date('m-d-Y', $order_info->return_date_time).'<br>
                <strong>Return Time</strong>: '.date('H:i', $order_info->return_date_time).'<br>
            ';
        }
        else{
            $return_details = '';
        }

        if($via_points){

            $via_points_all = explode("|",$via_points);
            $ViaPointscount = count($via_points_all);

            $via_point_details = '<br/> <strong>Way Point Details:</strong><br><br>';
            $via_points_seperate = '';

            for($i = 1; $i <= $ViaPointscount; $i++){
                $via_points_seperate .= 'Waypoint '.$i.' : '.$via_points_all[$i-1].'<br/>';
            }


        }
        else{
            $via_point_details = '';
            $via_points_seperate = '';
        }

        $email_temp = '<html>
    <head>
        <title>Taxi Booking</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body bgcolor="#d4d2d2" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

    <table width="600" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
      <tr>
        <td align="center">
            <img src="http://asianacars.com/assets/images/logo2.jpg" alt="Logo">
        </td>
      </tr>

      <tr>
          <td width="600" height="100%" valign="top">

              <table cellpadding="0" cellspacing="0" style="color:#000; border:1px solid #EBEBEB; font-family:Arial, sans-serif">

                   <tr>
                        <td colspan="2">
                            <img src="http://asianacars.com/assets/images/Banner-600.png" alt="Banner">
                        </td>
                   </tr>

                    <tr>
                      <td colspan="2" style="padding:30px;" style="font-family:Arial, sans-serif;">
                          Dear User,<br>
                          <br>

                         '.$body.'

                          <br>
                          <br>

                          '.$via_point_details.'
                          '.$via_points_seperate.'

                          '.$return_details.'

                          <br>
                          <br>
                          <strong>Regards</strong><br>
                          Asiana Cars Ltd<br>
                          www.asianacars.com<br/>
                      </td>
                    </tr>


                    <tr style="border-top:1px solid #EBEBEB;">
                        <td width="300" height="125" align="left" style="color:#000; padding-left:30px; font-family:Arial, sans-serif">
                              <strong>Contact Information</strong><br>
                              Tel : +44 1737668060<br>
                              <a href="mailto:info@asianacars.com" style="color:#000;">info@asianacars.com</a><br>
                        </td>
                        <td width="300" height="125" align="right" style="color:#fff; padding-right:30px; font-family:Arial, sans-serif">
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/fb.png" alt="fb">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/twitter.png" alt="twitter">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/linkedin.png" alt="linkedin">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/youtube.png" alt="youtube">
                            </a>
                        </td>
                    </tr>

                    <tr>
                    <td colspan="2" width="600">
                        <img src="http://asianacars.com/assets/images/line.jpg" alt="Line">
                    </td>
                  </tr>

              </table>
          </td>

      </tr>

    </table>

    </body>
    </html>';

//        $email_temp = '<html>
//        <head>
//            <title>Taxi Booking</title>
//            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
//        </head>
//        <body bgcolor="#d4d2d2" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
//            <table id="Table_01" width="600" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
//              <tr>
//                <td colspan="3" style="border-bottom:3px solid #730606;">
//                    <img src="http://asianacars.com/assets/images/logo.png" width="300" height="47" alt="">
//                </td>
//              </tr>
//              <tr>
//
//              <td width="23" height="2" bgcolor="#ffffff"></td>
//              <td rowspan="2" width="540" height="100%" bgcolor="#ffffff" valign="top">
//
//                  <table cellpadding="0" cellspacing="0">
//                    <tr>
//                      <td style="padding-top:30px; padding-bottom:30px;" style="font-family:Arial, sans-serif;">
//                      Dear User,<br>
//                      <br>
//
//                     '.$body.'
//
//                      <br>
//                      <br>
//                      <strong>Regards</strong><br>
//                      Taxi Booking Team<br>
//                    </td>
//
//                    </tr>
//
//                  </table>
//              </td>
//
//              <td width="37" height="2" bgcolor="#ffffff"></td>
//              </tr>
//              <tr>
//                <td></td>
//                <td></td>
//              </tr>
//              <tr>
//                <td colspan="3" width="600" height="175" bgcolor="#730606" align="center" style="color:#fff;font-family:Arial, sans-serif"><strong>Contact Information</strong><br>
//                  London<br>
//                  <a href="mailto:info@taxi-booking.com" style="color:#ffffff;">info@taxi-booking.com</a><br></td>
//              </tr>
//            </table>
//        </body>
//        </html>';

        $subUser = "Asiana Cars: Your payment received successfully: ".$order_info->id;
        $subAdmin = "Asiana Cars: Bill paid for order id : ".$order_info->id;
        $subDriver = "Asiana Cars: Bill paid for order id: ".$order_info->id;

        /*This is email Body Content for sending an email when approve an order*/

        mail($order_info->email, $subUser, $email_temp, $headers);
        mail($order_info->dEmail, $subDriver, $email_temp, $headers);

        /*For All Admin Email Send*/
        $allAdminRoleID = User::where('role','1')->get();

        foreach($allAdminRoleID as $adminID){
            mail($adminID->email, $subAdmin, $email_temp, $headers);
        }

    }

    /* /send email When payment status is paid */



    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
