<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;

use Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Pricelist;
use App\CarsCat;
use App\CarDetails;
use App\Drivers;
use App\Passengers;
use App\Airports;
use App\Orders;
use App\Setting;
use App\Http\Requests\editprofilesaveRequest;
use App\Http\Requests\editcarsaveRequest;
use App\Http\Requests\editdriveresaveRequest;

class EditController extends Controller {

    //private $googleAPIKey = 'AIzaSyD-OnhY1_cMrzguH_dnfm8YihGd6OGRaK4'; //server based API
    private $googleAPIKey = 'AIzaSyBVxh2byXJ1aP_FOjjVIxjPsFtnj8UV1b4'; //browser based API
    //==== Price Vars
    public $londonPrice = 0;
    public $first5Price = 18; // for 0-5 miles
    //public $flatRateMoreThan30 = 1.5;
    //public $rateGreaterThan5UpTo30 = 1.5;
    public $rateGreaterThan6UpTo20 = 1.75; // for 6-20 miles; i.e. £18.00 + 1.75/mile
    public $rateGreaterThan20UpTo35 = 1.5; // for 21-35 miles; i.e. £18.00 + 1.5/mile
    public $rateGreaterThan35UpTo55 = 1.5; // for 36-55 miles; flat rate for each mile. i.e. 39 miles * £1.5 = £58.5
    public $flatRateMoreThan55 = 1.3; // for more than 55 miles; flat rate for each mile. i.e. 90 miles * £1.3 = £117
    public $firstMileageCounter = 5;
    public $secondMileageCounter = 20;
    public $thirdMileageCounter = 35;
    public $forthMileageCounter = 55;
    public $singlPrice = 0;
    public $totalPrice = 0;
    public $returnDiscountPercent = 5;
    public $currency = 'GBP';
    public $currencySymbol = '&pound;';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editOrder($orderID){
        if(Session::has('order')){
            $order = Session::get('order');
            //$order->picking_category = 'abcd';
            //var_dump($order);die;
        }
        else{
            $orderDB = DB::table('orders')
                ->leftjoin('passengers', function($join)
                {
                    $join->on('orders.user_id', '=', 'passengers.id');
                })
                ->leftjoin('car_details', function($join)
                {
                    $join->on('orders.car_details', '=', 'car_details.id');
                })

                ->leftjoin('pickup_from_airport', function($join)
                {
                    $join->on('orders.id', '=', 'pickup_from_airport.order_id');
                })

                ->leftjoin('drivers', function($join)
                {
                    $join->on('orders.driver_id', '=', 'drivers.id');
                })

                ->where('orders.id', $orderID)
                ->select('orders.*', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'car_details.name as carname', 'car_details.total_seats', 'pickup_from_airport.flight_arrival_time', 'pickup_from_airport.airline_name', 'pickup_from_airport.flight_no', 'pickup_from_airport.departure_city', 'pickup_from_airport.pickup_after')
                ->first();

            Session::put('order',$orderDB);

            //Session::push('order.no_of_infant',3);
            $order = Session::get('order');
        }
        //echo $total_bill->email;die;
        //var_dump($total_bill);die;

        $via_points = $order->via_points;
        $tourType = $order->return_booking;
        $via_points = str_replace('+',' ',$via_points);
        $via_points_all = explode("|",$via_points);
        $via_points_seperate = $via_points_all;
        $viapointsCount = count($via_points_seperate);
        $totalDistance = $order->total_distance;
        $carId = $order->car_details;

        //echo $viapointsCount;die();

        return view('admin_blade.pages.editOrderAdmin', compact('order', 'via_points_seperate', 'viapointsCount', 'via_points', 'totalprice', 'tourType', 'sessData1stStage'));

    }

}