<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Session;
use Input;
use DB;

use App\User;
use App\Pricelist;
use App\Passengers;
use App\Airports;
use App\Drivers;
use App\Orders;
use App\bookingTime;
use App\CarsCat;
use App\CarDetails;
use App\ordereditHistory;


class PaypalController extends Controller
{
    private $_api_context;

    public function __construct()
    {
        // setup PayPal api context
        $paypal_conf = config('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function postPayment($totalFare)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        //echo $totalFare;die;
        //$totalFare = number_format(floatval($totalFare), 2, '.', '');

        $item_1 = new Item();
        $item_1->setName('Total Fare') // item name
            ->setCurrency('GBP')
            ->setQuantity(1)
            ->setPrice($totalFare); // unit price

        // add item to list
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('GBP')
            ->setTotal($totalFare);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('payment.status'))
            ->setCancelUrl(route('payment.cancel'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (config('app.debug')) {
                echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getData(), true);
                exit;
            } else {
                die('Some error occur, sorry for inconvenient');
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        // add payment ID to session
        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            // redirect to paypal
            return redirect($redirect_url);
        }

        return redirect(route('original.route'))
            ->with('error', 'Unknown error occurred');
    }

    public function getPaymentStatus()
    {
        // Get the payment ID before session clear
        $payment_id = Session::get('paypal_payment_id');
        $payerID = Input::get('PayerID');
        $token = Input::get('token');

        if (empty($payerID) || empty($token)) {
            return redirect(route('payment.cancel'))
                ->with('error', 'Payment failed');
        }

        $payment = Payment::get($payment_id, $this->_api_context);

        // PaymentExecution object includes information necessary
        // to execute a PayPal account payment.
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        //Execute the payment
        $result = $payment->execute($execution, $this->_api_context);

        $payment_status = array();

        //echo '<pre>';print_r($result);echo '</pre>';exit; // DEBUG RESULT, remove it later

        if ($result->getState() == 'approved') { // payment made
            $payment_status['card_charge_id'] = $result->id;
            $payment_status['card_customer_id'] = $result->payer->payer_info->payer_id;
            $payment_status['payment_status'] = 1;
            //$order_id = Session::get('asiana_order_id');
            //$payent_done = Order::where('id',$order_id)->update($payment_status);
            // clear the session payment ID
            $order_id = $this->saveOrder($payment_status);
            if($payment_id)Session::forget('paypal_payment_id');

            $this->emailPaypalPayment($order_id);

            return redirect('success');
        }
        else{
            return redirect(route('payment.fail'));
        }

    }

    function redirectBack(){
        /*echo "<script>
            history.go(-2);
        </script>";*/
        $order_id = Session::get('asiana_order_id');
        $payent_done = Order::where('id',$order_id)->delete();
        return redirect(route('index_new'));
    }

    function paymentFailed(){
        /*echo "<script>
            alert(Your Paypal Payment have failed. We are redirecting you to Booking page.);
            setTimeout(function(){ history.go(-2); }, 3000);
        </script>";*/
        //return redirect('failed');
        $order_id = Session::get('asiana_order_id');
        $payent_done = Order::where('id',$order_id)->delete();
        return redirect(route('index_new'));
    }

    function saveOrder($payment_status){
        $order_data = Session::get('bookingPageValues');
        $payment_method = $order_data['payment_method'];
        $totalprice = $order_data['totalprice'];

        $pickup = $order_data['pickup'];
        $pickup_address1 = $order_data['pickup_address1'];
        $pickup_address2 = $order_data['pickup_address2'];
        $pickup_postcode = $order_data['pickup_postcode'];

        //$pickup_details = $pickup_address1.", ".$pickup_address2." Postcode : ".$pickup_postcode;

        $dropoff = $order_data['dropoff'];
        $dropoff_address1 = $order_data['dropoff_address1'];
        $dropoff_address2 = $order_data['dropoff_address2'];
        $dropoff_postcode = $order_data['dropoff_postcode'];

        //$dropoff_details = $dropoff_address1.", ".$dropoff_address2." Postcode : ".$dropoff_postcode;

        $via_points = $order_data['via_points'];
        $distances = $order_data['distances'];
        $carname = $order_data['carname'];
        $tourType = $order_data['tourType'];
        $meetngreet = $order_data['meetngreet'];
        //$carname = 1;
        $special_date = $order_data['special_date'];
        $tourdate = $order_data['tourdate'];
        $tourdate = str_replace('/', '-', $tourdate);
        $tourtime = $order_data['tourtime'];
        $tourmin = $order_data['tourmin'];
        $tourdates = strtotime($tourdate." ".$tourtime.":".$tourmin);


        $firstname = $order_data['firstname'];
        $lastname = $order_data['lastname'];
        $email = $order_data['email'];
        $mobile = $order_data['mobile'];
        $comments = $order_data['comments'];

        $no_of_person = $order_data['no_of_person'];
        $no_of_suitcase = $order_data['no_of_suitcase'];
        $no_of_child = $order_data['no_of_child'];

        $arrival_time = $order_data['arrival_time'];
        $airline_name = $order_data['airline_name'];
        $flight_number = $order_data['flight_number'];
        $departure_city = $order_data['departure_city'];
        $pickup_after = $order_data['pickup_after'];

        $driver = DB::table('car_details')
            ->where('id', $carname)
            ->first();

        $passenger = DB::table('passengers')
            ->where('email', $email)
            ->first();
        if(count($passenger) < 1){
            $user = new Passengers();
            $id = $user::insertGetId(array('first_name'=>$firstname, 'last_name'=>$lastname, 'email'=>$email, 'phone_number'=>$mobile));
        }
        else
            $id =$passenger->id;

        $Orders = new Orders();
        $Orders->user_id = $id;
        $Orders->pickup_address = $pickup;
        $Orders->pickup_address1 = $pickup_address1;
        $Orders->pickup_address2 = $pickup_address2;
        $Orders->pickup_postcode = $pickup_postcode;
        $Orders->dropoff_address = $dropoff;
        $Orders->dropoff_address1 = $dropoff_address1;
        $Orders->dropoff_address2 = $dropoff_address2;
        $Orders->dropoff_postcode = $dropoff_postcode;
        $Orders->via_points = $via_points;
        $Orders->car_details = $carname;
        $Orders->total_bill = $totalprice;
        $Orders->total_distance = $distances;
        $Orders->driver_id = $driver->driver_id;
        $Orders->date_time = $tourdates;
        $Orders->special_date = $special_date;
        $Orders->no_of_child = $no_of_child;
        $Orders->no_of_person = $no_of_person;
        $Orders->suitcases = $no_of_suitcase;
        $Orders->comments = $comments;
        $Orders->createdBy = 'OnlineBooking';
        if((int)$meetngreet > 0) $Orders->meetngreet = $meetngreet;

        if($tourType == 'round'){
            $returndate = $order_data['returnDate'];
            $returndate = str_replace('/', '-', $returndate);
            $returntime = $order_data['returnTime'];
            $returnmin = $order_data['returnMin'];
            $return = strtotime($returndate." ".$returntime.":".$returnmin);

            $Orders->return_date_time = $return;
            $Orders->return_booking = 1;
        }
        else {
            $returndate = '';
            $returntime = '';
            $returnmin = '';
        }

        //===card info to Order

        $Orders->card_customer_id = $payment_status['card_customer_id'];
        $Orders->card_charge_id = $payment_status['card_charge_id'];
        $Orders->pay_via = $payment_method;
        $Orders->payment_status = $payment_status['payment_status'];
        $Orders->save();

        $order_id = $Orders->id;

        $pickup_from = new Airports();
        $pickup_from->order_id = $order_id;
        $pickup_from->flight_arrival_time = $arrival_time;
        $pickup_from->airline_name = $airline_name;
        $pickup_from->flight_no = $flight_number;
        $pickup_from->departure_city = $departure_city;
        $pickup_from->pickup_after = $pickup_after;
        $pickup_from->save();

        $edited = 0;
        return $order_id;
    }

    function emailPaypalPayment($order_id){
        $conf_orderlist = DB::table('orders')
            ->leftjoin('passengers', function($join)
            {
                $join->on('orders.user_id', '=', 'passengers.id');
            })
            ->leftjoin('drivers', function($join)
            {
                $join->on('orders.driver_id', '=', 'drivers.id');
            })
            ->leftjoin('car_details', function($join)
            {
                $join->on('orders.car_details', '=', 'car_details.id');
            })
            ->leftjoin('pickup_from_airport', function($join)
            {
                $join->on('pickup_from_airport.order_id', '=', 'orders.id');
            })
            ->where('orders.id', '=', $order_id)
            ->select('orders.*', 'pickup_from_airport.flight_arrival_time', 'pickup_from_airport.airline_name', 'pickup_from_airport.flight_no', 'pickup_from_airport.departure_city', 'pickup_from_airport.pickup_after', 'car_details.id as carname', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'drivers.first_name as Dfirst_name', 'drivers.last_name as Dlast_name')
            ->orderBy('orders.updated_at', 'orders.id', 'DESC')
            ->first();

        $firstname = $conf_orderlist->first_name;
        $lastname = $conf_orderlist->last_name;
        $pickup = $conf_orderlist->pickup_address;
        $pickup_address1 = $conf_orderlist->pickup_address1;
        $pickup_address2 = $conf_orderlist->pickup_address2;
        $pickup_postcode = $conf_orderlist->pickup_postcode;
        $dropoff = $conf_orderlist->dropoff_address;
        $dropoff_address1 = $conf_orderlist->dropoff_address1;
        $dropoff_address2 = $conf_orderlist->dropoff_address2;
        $dropoff_postcode = $conf_orderlist->dropoff_postcode;
        $distances = $conf_orderlist->total_distance;
        $email = $conf_orderlist->email;
        $mobile = $conf_orderlist->phone_number;
        $comments = $conf_orderlist->comments;
        $tourdate = $conf_orderlist->date_time;
        $tourtime = date('H',$tourdate);
        $tourmin = date('i',$tourdate);
        $tourdates = $tourdate;
        $return = $conf_orderlist->return_booking;
        $tourType = ($return == 1)?'round':'single';
        $returndate = $conf_orderlist->return_date_time;
        $returntime = date('H',$returndate);
        $returnmin = date('i',$returndate);
        $via_points = $conf_orderlist->via_points;
        $carname = $conf_orderlist->carname;
        $no_of_person = $conf_orderlist->no_of_person;
        $no_of_child = $conf_orderlist->no_of_child;
        $no_of_suitcase = $conf_orderlist->suitcases;
        $totalprice = $conf_orderlist->total_bill;
        $meetngreet = $conf_orderlist->meetngreet;
        $arrival_time = $conf_orderlist->flight_arrival_time;
        $airline_name = $conf_orderlist->airline_name;
        $flight_number = $conf_orderlist->flight_no;
        $departure_city = $conf_orderlist->departure_city;
        $pickup_after = $conf_orderlist->pickup_after;
        $edited = 0;

        BookingController::sendCustomMail($order_id,$firstname,$lastname,$pickup, $pickup_address1, $pickup_address2, $pickup_postcode, $dropoff, $dropoff_address1, $dropoff_address2, $dropoff_postcode, $distances, $email, $mobile, $comments, $tourdate, $tourtime, $tourmin, $tourdates, $tourType, $returndate, $return, $returntime, $returnmin, $via_points, $carname, $no_of_person, $no_of_child, $no_of_suitcase, $totalprice, $meetngreet, $arrival_time, $airline_name, $flight_number, $departure_city, $pickup_after, $edited);

    }
}
