<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PaypalController;
use Illuminate\Http\Request;
use Session;

use Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Pricelist;
use App\Passengers;
use App\Airports;
use App\Drivers;
use App\Orders;
use App\bookingTime;
use App\CarsCat;
use App\CarDetails;
use App\ordereditHistory;
use Stripe;
use Config;

class BookingController extends Controller {

    //private $googleAPIKey = 'AIzaSyD-OnhY1_cMrzguH_dnfm8YihGd6OGRaK4'; //server based API
    private $googleAPIKey = 'AIzaSyBVxh2byXJ1aP_FOjjVIxjPsFtnj8UV1b4'; //browser based API
    //==== Price Vars
    public $londonPrice = 0;
    public $first5Price = 15; // for 0-5 miles
    //public $flatRateMoreThan30 = 1.5;
    //public $rateGreaterThan5UpTo30 = 1.5;
    public $rateGreaterThan6UpTo20 = 1.50; // for 6-20 miles; i.e. £18.00 + 1.75/mile
    public $rateGreaterThan20UpTo35 = 1.6; // for 21-35 miles; i.e. £18.00 + 1.5/mile
    public $rateGreaterThan35UpTo55 = 1.5; // for 36-55 miles; flat rate for each mile. i.e. 39 miles * £1.5 = £58.5
    public $flatRateMoreThan55 = 1.2; // for more than 55 miles; flat rate for each mile. i.e. 90 miles * £1.3 = £117
    public $firstMileageCounter = 5;
    public $secondMileageCounter = 20;
    public $thirdMileageCounter = 35;
    public $forthMileageCounter = 55;
    public $singlPrice = 0;
    public $totalPrice = 0;
    public $returnDiscountPercent = 5;
    public $currency = 'GBP';
    public $currencySymbol = '&pound;';

    public function __construct()
    {
        //$this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function calcRoutes($directions){

        $routes = $directions['routes'][0]['legs'];

        $distance = 0;
        $duration = 0;
        $routesArray = array();

        $locationsLatLongData = array();

        foreach($routes as $k=>$route) {
            //==== check whether route has any London address
            //if(strstr($route['end_address'],'London')) $isLondon = true;
            $distance = $distance + $route['distance']['value'];
            $duration = $duration + $route['duration']['value'];
            $routesArray[$k]['start'] = str_replace('+',' ',$route['start_address']);
            $routesArray[$k]['end'] = str_replace('+',' ',$route['end_address']);
            $routesArray[$k]['distance'] = $route['distance']['value'];
            $routesArray[$k]['duration'] = $route['duration']['value'];
            $routesArray[$k]['end_location'] = $route['end_location'];
            $routesArray[$k]['start_location'] = $route['start_location'];
            $routesArray['locationsLatLongData'][] = $route['start_location']['lat'].','.$route['start_location']['lng'];
            $routesArray['locationsLatLongData'][] = $route['end_location']['lat'].','.$route['end_location']['lng'];

        }
        $distance = $this->shortest_distance($directions);
        $routesArray['distance_total'] = $distance;
        $routesArray['distance_total_miles'] = ($distance/1000) * 0.621371;
        //$routesArray['distance_total_miles'] = 90;
        $routesArray['distance_total_text'] = number_format(ceil($distance/1000),0) .' km';
        $routesArray['distance_total_miles_text'] = number_format(ceil(($distance/1000) * 0.621371),0) .' mi';
        $routesArray['duration_total'] = $duration;
        $routesArray['duration_total_text'] = number_format($duration/3600,0) . ' hr';


        //==== calculate price
        //if(!$isLondon){ //=== if the route does not have any london address

        //=== if total distance is 0-5 miles
        if($routesArray['distance_total_miles'] <= $this->firstMileageCounter)
            $this->totalPrice =  $this->first5Price;

        //=== if total distance is greater than 5 miles and less than or equal to 20 miles
        else if($routesArray['distance_total_miles'] > $this->firstMileageCounter && $routesArray['distance_total_miles'] <= $this->secondMileageCounter)
            $this->totalPrice =  $this->first5Price + ($routesArray['distance_total_miles'] - $this->firstMileageCounter) * $this->rateGreaterThan6UpTo20;

        //=== if total distance is greater than 20 miles and less than or equal to 35 miles
        else if($routesArray['distance_total_miles'] > $this->secondMileageCounter && $routesArray['distance_total_miles'] <= $this->thirdMileageCounter)
            $this->totalPrice =  $this->first5Price + ($routesArray['distance_total_miles'] - $this->firstMileageCounter) * $this->rateGreaterThan20UpTo35;

        //=== if total distance is greater than 35 miles and less than or equal to 55 miles
        else if($routesArray['distance_total_miles'] > $this->thirdMileageCounter && $routesArray['distance_total_miles'] <= $this->forthMileageCounter)
            $this->totalPrice =  $routesArray['distance_total_miles'] * $this->rateGreaterThan35UpTo55;

        //=== if total distance is greater than 55 miles
        else if($routesArray['distance_total_miles'] > $this->forthMileageCounter)
            $this->totalPrice =  $routesArray['distance_total_miles'] * $this->flatRateMoreThan55;
        else
            $this->totalPrice = 0;


        if($this->totalPrice <=0 || $this->totalPrice == NULL) {
            $locationUnrealistic = true;
            return view('blade.pages.NoResult');
        }


        //=== get list of cars
        $carList = DB::table('car_details')
            ->leftjoin('car_category', function($join)
            {
                $join->on('car_details.category', '=', 'car_category.id');
            })
            ->select('car_details.id','car_details.name', 'car_details.total_seats', 'car_category.cat_name','car_category.details', 'car_category.max_person', 'car_category.max_carry_on', 'car_category.price_ratio', 'car_category.car_pic', 'car_category.max_suitcases')
            ->orderBy('car_details.id', 'ASC')
            ->get();
        $routesArray['carList'] = $carList;


        foreach($carList as $carDetails) {
            $routesArray['carPrices'][$carDetails->id]['price'] = ceil($this->totalPrice + ($this->totalPrice * $carDetails->price_ratio)/100);
            $routesArray['carPrices'][$carDetails->id]['priceFormatted'] = number_format($routesArray['carPrices'][$carDetails->id]['price'],2);
            $routesArray['carPrices'][$carDetails->id]['priceReturn'] =  2*$routesArray['carPrices'][$carDetails->id]['price'] - $routesArray['carPrices'][$carDetails->id]['price']*($this->returnDiscountPercent/100);
            //echo $routesArray['carPrices'][$carDetails->id]['priceFormatted']."<br>".$routesArray['carPrices'][$carDetails->id]['priceReturn'];die;
            $routesArray['carPrices'][$carDetails->id]['priceReturnFormatted'] = number_format($routesArray['carPrices'][$carDetails->id]['priceReturn'],2);
            $routesArray['carPrices'][$carDetails->id]['totalIfReturnSelected'] = $routesArray['carPrices'][$carDetails->id]['price'] + $routesArray['carPrices'][$carDetails->id]['priceReturn'];
            $routesArray['carPrices'][$carDetails->id]['totalIfReturnSelectedFormatted'] = number_format($routesArray['carPrices'][$carDetails->id]['totalIfReturnSelected'],2);

            $routesArray['carPrices'][$carDetails->id]['priceSpecialDate'] = ceil($routesArray['carPrices'][$carDetails->id]['price'] * 1.5);
            $routesArray['carPrices'][$carDetails->id]['priceSpecialDateFormatted'] = number_format($routesArray['carPrices'][$carDetails->id]['priceSpecialDate'],2);
            $routesArray['carPrices'][$carDetails->id]['priceSpecialDateReturn'] = ceil($routesArray['carPrices'][$carDetails->id]['priceReturn'] * 1.5);
            $routesArray['carPrices'][$carDetails->id]['priceSpecialDateReturnFormatted'] = number_format($routesArray['carPrices'][$carDetails->id]['priceSpecialDateReturn'],2);

            $routesArray['carPrices'][$carDetails->id]['carName'] = $carDetails->cat_name;
            $routesArray['carPrices'][$carDetails->id]['priceRatio'] = $carDetails->price_ratio;
            $routesArray['carPrices'][$carDetails->id]['discountRate'] = $this->returnDiscountPercent;
            $routesArray['carPrices'][$carDetails->id]['currency'] = $this->currency;
            $routesArray['carPrices'][$carDetails->id]['currencySymbol'] = $this->currencySymbol;
        }

        return $routesArray;
    }

    static public function shortest_distance($directions){

        $route_options = [];

        for ($i = 0; $i < count($directions['routes']); $i++){
            $route = $directions['routes'][$i];
            $distance = 0;
            //echo var_dump($route['legs']);die;
            // Total the legs to find the overall journey distance for each route option
            for ($j = 0; $j < count($route['legs']); $j++)
            {
                $distance += $route['legs'][$j]['distance']['value']; // metres
            }

            $route_options_data = ['distance'=> $distance];

            array_push($route_options,$route_options_data);
        }

        sort($route_options,SORT_ASC);

        $shortest_distance = $route_options[0]['distance']; // meters
        //$shortest_distance = ($route_options[0]['distance'] * 0.001 * 0.621371); // convert metres to kilometres to miles
        return $shortest_distance;
    }

    /* Booking result */
    public function result(Request $request)
    {
        Session::put('page1Values',$_POST);
//        echo '<pre>';
//        print_r($_POST);
//        print_r(Session::get('page1Values'));
//        echo '</pre>';

        $pickup = str_replace(' ','+',$request->get('pickup'));
        $dropoff = str_replace(' ','+',$request->get('dropoff'));

        //=== waypoints
        $waypoint_1 = $request->get('waypoint_1');
        $waypoint_2 = $request->get('waypoint_2');
        $waypoint_3 = $request->get('waypoint_3');
        $waypoint_4 = $request->get('waypoint_4');

        //=== checking waypoints
        $waypoint_1 = (isset($waypoint_1) && $waypoint_1 != '') ?  str_replace(' ','+',$waypoint_1) : '';
        $waypoint_2 = (isset($waypoint_2) && $waypoint_2 != '') ?  str_replace(' ','+',$waypoint_2) : '';
        $waypoint_3 = (isset($waypoint_3) && $waypoint_3 != '') ?  str_replace(' ','+',$waypoint_3) : '';
        $waypoint_4 = (isset($waypoint_4) && $waypoint_4 != '') ?  str_replace(' ','+',$waypoint_4) : '';

        //=== generate waypoint string
        $waypoints = 'waypoints=optimize:false|';
        //$waypoints = 'waypoints=optimize';
        if($waypoint_1 == '' && $waypoint_2 == '' && $waypoint_3 == '' && $waypoint_4 == '') $waypoints = '';
        $via_pointsArray = array_filter(array($waypoint_1,$waypoint_2,$waypoint_3,$waypoint_4));
        $waypoints .= implode("|",$via_pointsArray);

        $via_points = implode("|",$via_pointsArray);

        $apiKey = $this->googleAPIKey;
        $params = file_get_contents("https://maps.googleapis.com/maps/api/directions/json?region=uk&origin=$pickup&destination=$dropoff&alternatives=true&sensor=false&units=imperial&$waypoints&key=$apiKey");
        $directions = json_decode($params,true);
//        echo '<pre>';
//        print_r($_POST);
//        print_r($directions);
//        echo '</pre>';

        if($directions['status']!='ZERO_RESULTS'){
//            echo '<pre>';
//            print_r($directions['routes'][0]['legs']);
//            echo '</pre>';

            $pickup = str_replace('+',' ',$pickup);
            $dropoff = str_replace('+',' ',$dropoff);

            $routesArray = $this->calcRoutes($directions);
            $carList = $routesArray['carList'];
            $locationsLatLongData = $routesArray['locationsLatLongData'];

            $tourdate = $request->get('tourdate');
            $tourtime = $request->get('tourtime');
            $tourmin = $request->get('tourmin');
            $tourtime = $tourtime.":".$tourmin;


            //echo '<pre>';
            //print_r($_POST);
            //var_dump($isLondon);
            //print_r($routesArray);
            //print_r($locationsLatLongData);
            //print_r($geoCodeData);
            //echo $centerLocation;
            //echo '</pre>';
            $locationsLatLongDataImploded = implode("|",$locationsLatLongData);
            $locationsLatLongDataImplodedComma = implode(",",$locationsLatLongData);


            $mapImage = "https://maps.googleapis.com/maps/api/staticmap?maptype=roadmap&markers=size:mid%7Ccolor:red%7C$locationsLatLongDataImploded&zoom=5&size=310x125&path=$locationsLatLongDataImploded";


            return view('blade.pages.result', compact('pickup', 'dropoff', 'routesArray', 'carList', 'tourdate', 'tourtime','mapImage', 'via_points'));
        }
        else {
            //return error message
            return view('blade.pages.NoResult');
        }




////        die;
    }
    /* /Booking result */



    /* Booking details */
    public function booking(Request $request)
    {
        $pickup = $request->get('pickup');
        $dropoff = $request->get('dropoff');
        $via_points = $request->get('via_points');
        $distances = $request->get('distances');
        $carname = $request->get('carname');
        $carfullname = $request->get('carfullname');
        $car_pic = $request->get('car_pic');
        $totalprice = $request->get('totalprice');
        $singleTrip = $request->get('singleTrip');
        $roundTrip = $request->get('roundTrip');
        $singleTripSpecialDate = $request->get('singleTripSpecialDate');
        $roundTripSpecialDate = $request->get('roundTripSpecialDate');
        $tourdate = $request->get('tourdate');
        $tourtime = $request->get('tourtime');
        $meetngreet = $request->get('meetngreet');

        $via_points_all = explode("|",$via_points);
        $ViaPointscount = count($via_points_all);
        $via_points_seperate = $via_points_all;

        $tripType = 'single';
        if($totalprice == $singleTrip){
            $tripType = 'single';
            $totalprice = $totalprice + $meetngreet;
            $totalPriceSpecialDate = $singleTripSpecialDate + $meetngreet;
        }
        else if($totalprice == $roundTrip){
            $tripType = 'round';
            $totalprice = $totalprice + $meetngreet;
            $totalPriceSpecialDate = $roundTripSpecialDate + $meetngreet;
        }

        $setting = DB::table('setting')->where('property','=','payment_method')->first();
        $pm = explode(",", $setting->value);

        $minimumBookingtime = DB::table('booking_time')
            ->where('id', 1)
            ->first();

        //====stripe config
        $stripe = Config::get('services.stripe');

        return view('blade.pages.booking', compact('pickup', 'dropoff', 'via_points', 'distances', 'carname', 'carfullname', 'car_pic', 'totalprice', 'bookedcost', 'tripType', 'tourdate', 'tourtime', 'via_points_seperate', 'meetngreet', 'pm', 'stripe', 'totalPriceSpecialDate', 'minimumBookingtime'));
    }
    /* /Booking details */

    /*
     * Stripe Charge
     */
    public function stripeCharge(Request $request){
        $tokenID = $request->get('tokenID');
        $tokenEmail = $request->get('tokenEmail');
        $description = $request->get('description');
        $amount = $request->get('amount');
        $currency = $request->get('currency');

        //====stripe config
        $stripe = Config::get('services.stripe');

        //=== set stripe API
        \Stripe\Stripe::setApiKey($stripe['secret']);

        //=== create a customer

        try{
            $customer = \Stripe\Customer::create([
                'email' => $tokenEmail,
                'card' => $tokenID,
                'description' => $description
            ]);

            $customerID = $customer->id;

        }catch (\Stripe\Error\Card $e){
            echo 'error_info';
            die;
        }

        try{
            $charge = \Stripe\Charge::create([
                'customer' => $customerID,
                'currency' => $currency,
                'amount'   => $amount,
                'description' => $description
            ]);

            echo $customerID.'^^__^^'.$charge['id'];

        }
        catch(\Stripe\Error\Card $e) {
            echo 'error_card';
            die;
        }


    }

    /* Booking complete */
    public function booking_submit(Request $request)
    {
        //        echo '<pre>';
        //        print_r($_POST);
        //        echo '</pre>';
        //        die;
                //$payment_method = 'stripe';


        $payment_method = $request->get('payment_method');
        $totalprice = $request->get('totalprice');

        if($payment_method == 'paypal'){
            Session::put('bookingPageValues',$_POST);
            //PaypalController::postpayment($totalprice);
            return redirect('payment/'.$totalprice);
        }


        $pickup = $request->get('pickup');
        $pickup_address1 = $request->get('pickup_address1');
        $pickup_address2 = $request->get('pickup_address2');
        $pickup_postcode = $request->get('pickup_postcode');

        //$pickup_details = $pickup_address1.", ".$pickup_address2." Postcode : ".$pickup_postcode;

        $dropoff = $request->get('dropoff');
        $dropoff_address1 = $request->get('dropoff_address1');
        $dropoff_address2 = $request->get('dropoff_address2');
        $dropoff_postcode = $request->get('dropoff_postcode');

        //$dropoff_details = $dropoff_address1.", ".$dropoff_address2." Postcode : ".$dropoff_postcode;

        $via_points = $request->get('via_points');
        $distances = $request->get('distances');
        $carname = $request->get('carname');
        $tourType = $request->get('tourType');
        $meetngreet = $request->get('meetngreet');
        //$carname = 1;
        $special_date = $request->get('special_date');
        $tourdate = $request->get('tourdate');
        $tourdate = str_replace('/', '-', $tourdate);
        $tourtime = $request->get('tourtime');
        $tourmin = $request->get('tourmin');
        $tourdates = strtotime($tourdate." ".$tourtime.":".$tourmin);


        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');
        $email = $request->get('email');
        $mobile = $request->get('mobile');
        $comments = $request->get('comments');

        $no_of_person = $request->get('no_of_person');
        $no_of_suitcase = $request->get('no_of_suitcase');
        $no_of_child = $request->get('no_of_child');

        $arrival_time = $request->get('arrival_time');
        $airline_name = $request->get('airline_name');
        $flight_number = $request->get('flight_number');
        $departure_city = $request->get('departure_city');
        $pickup_after = $request->get('pickup_after');

        $stripe_customer_id = $request->get('stripeCustomerID');
        $stripe_charge_id = $request->get('stripeChargeID');

//        if($payment_method == 'stripe'){
//            return redirect('http://localhost/asiana_heroku/booking');
//        }
//        else{

        $driver = DB::table('car_details')
            ->where('id', $carname)
            ->first();

        $passenger = DB::table('passengers')
            ->where('email', $email)
            ->first();
        if(count($passenger) < 1){
            $user = new Passengers();
            $id = $user::insertGetId(array('first_name'=>$firstname, 'last_name'=>$lastname, 'email'=>$email, 'phone_number'=>$mobile));
        }
        else
            $id =$passenger->id;

        $Orders = new Orders();
        $Orders->user_id = $id;
        $Orders->pickup_address = $pickup;
        $Orders->pickup_address1 = $pickup_address1;
        $Orders->pickup_address2 = $pickup_address2;
        $Orders->pickup_postcode = $pickup_postcode;
        $Orders->dropoff_address = $dropoff;
        $Orders->dropoff_address1 = $dropoff_address1;
        $Orders->dropoff_address2 = $dropoff_address2;
        $Orders->dropoff_postcode = $dropoff_postcode;
        $Orders->via_points = $via_points;
        $Orders->car_details = $carname;
        $Orders->total_bill = $totalprice;
        $Orders->total_distance = $distances;
        $Orders->driver_id = $driver->driver_id;
        $Orders->date_time = $tourdates;
        $Orders->special_date = $special_date;
        $Orders->no_of_child = $no_of_child;
        $Orders->no_of_person = $no_of_person;
        $Orders->suitcases = $no_of_suitcase;
        $Orders->comments = $comments;
        $Orders->createdBy = 'OnlineBooking';
        if((int)$meetngreet > 0) $Orders->meetngreet = $meetngreet;

        if($tourType == 'round'){
            $returndate = $request->get('returnDate');
            $returndate = str_replace('/', '-', $returndate);
            $returntime = $request->get('returnTime');
            $returnmin = $request->get('returnMin');
            $return = strtotime($returndate." ".$returntime.":".$returnmin);

            $Orders->return_date_time = $return;
            $Orders->return_booking = 1;
        }
        else {
            $returndate = '';
            $returntime = '';
            $returnmin = '';
        }

        //===card info to Order
        $Orders->card_customer_id = $stripe_customer_id;
        $Orders->card_charge_id = $stripe_charge_id;
        $Orders->pay_via = $payment_method;

        $Orders->save();

        $order_id = $Orders->id;

        $pickup_from = new Airports();
        $pickup_from->order_id = $order_id;
        $pickup_from->flight_arrival_time = $arrival_time;
        $pickup_from->airline_name = $airline_name;
        $pickup_from->flight_no = $flight_number;
        $pickup_from->departure_city = $departure_city;
        $pickup_from->pickup_after = $pickup_after;
        $pickup_from->save();

        $edited = 0;
        /*
         * $arrival_time = $request->get('arrival_time');
        $airline_name = $request->get('airline_name');
        $flight_number = $request->get('flight_number');
        $departure_city = $request->get('departure_city');
        $pickup_after = $request->get('pickup_after');
         */

        Session::put('asiana_order_id',$order_id);

        if($returndate == '')
            $this->sendCustomMail($order_id,$firstname,$lastname,$pickup, $pickup_address1, $pickup_address2, $pickup_postcode, $dropoff, $dropoff_address1, $dropoff_address2, $dropoff_postcode, $distances, $email, $mobile, $comments, $tourdate, $tourtime, $tourmin, $tourdates, $tourType, $via_points, $carname, $no_of_person, $no_of_child, $no_of_suitcase, $totalprice, $meetngreet, $arrival_time, $airline_name, $flight_number, $departure_city, $pickup_after, $edited);
        else
            $this->sendCustomMail($order_id,$firstname,$lastname,$pickup, $pickup_address1, $pickup_address2, $pickup_postcode, $dropoff, $dropoff_address1, $dropoff_address2, $dropoff_postcode, $distances, $email, $mobile, $comments, $tourdate, $tourtime, $tourmin, $tourdates, $tourType, $returndate, $return, $returntime, $returnmin, $via_points, $carname, $no_of_person, $no_of_child, $no_of_suitcase, $totalprice, $meetngreet, $arrival_time, $airline_name, $flight_number, $departure_city, $pickup_after, $edited);


        //flash()->success('Booking submitted successfully !!');

        return redirect('success');
        //} //end else
    }
    /* /Booking complete */


    /* Contact submit */
    public function contact_submit(Request $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $phone_number = $request->get('phone_number');
        $message = $request->get('message');

        $headers = "From: Asiana Cars <" . strip_tags("info@asianacars.com") . ">\r\n";
        $headers .= "Reply-To: Asiana Cars <" . strip_tags("info@asianacars.com") . ">\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $body = '<p>Contact us message details.  </p>

        <strong>Name</strong>: '.$name.'<br>
        <strong>Phone Number</strong>: '.$phone_number.'<br>
        <strong>Email</strong>: '.$email.'<br>
        <strong>Message</strong>: '.$message.'<br>';

        $email_temp = '<html>
    <head>
        <title>Taxi Booking</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body bgcolor="#d4d2d2" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

    <table width="600" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
      <tr>
        <td align="center">
            <img src="http://asianacars.com/assets/images/logo2.jpg" alt="Logo">
        </td>
      </tr>

      <tr>
          <td width="600" height="100%" valign="top">

              <table cellpadding="0" cellspacing="0" style="color:#000; border:1px solid #EBEBEB; font-family:Arial, sans-serif">

                   <tr>
                        <td colspan="2">
                            <img src="http://asianacars.com/assets/images/Banner-600.png" alt="Banner">
                        </td>
                   </tr>

                    <tr>
                      <td colspan="2" style="padding:30px;" style="font-family:Arial, sans-serif;">
                          Dear User,<br>
                          <br>

                         '.$body.'

                          <br>
                          <br>
                          <strong>Regards</strong><br>
                          Asiana Cars Team<br>
                      </td>
                    </tr>


                    <tr style="border-top:1px solid #EBEBEB;">
                        <td width="300" height="125" align="left" style="color:#000; padding-left:30px; font-family:Arial, sans-serif">
                              <strong>Contact Information</strong><br>
                              Tel : +44 1737668060<br>
                              <a href="mailto:info@asianacars.com" style="color:#000;">info@asianacars.com</a><br>
                        </td>
                        <td width="300" height="125" align="right" style="color:#fff; padding-right:30px; font-family:Arial, sans-serif">
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/fb.png" alt="fb">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/twitter.png" alt="twitter">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/linkedin.png" alt="linkedin">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/youtube.png" alt="youtube">
                            </a>
                        </td>
                    </tr>

                    <tr>
                    <td colspan="2" width="600">
                        <img src="http://asianacars.com/assets/images/line.jpg" alt="Line">
                    </td>
                  </tr>

              </table>
          </td>

      </tr>

    </table>

    </body>
    </html>';

        $subUser = "Asiana Cars: Message received";
        $subAdmin = "Asiana Cars: New Contact Message";

        /*This is email Body Content for sending an email after booking completion*/

        mail($email, $subUser, $email_temp, $headers);

        /*For All Admin Email Send*/
        $allAdminRoleID = User::where('role','1')->get();


        foreach($allAdminRoleID as $adminID){
            //$adminEmail = Admin::where('user_id',$adminID->id)->first();

            mail($adminID->email, $subAdmin, $email_temp, $headers);
        }

        //flash()->success('Booking submitted successfully !!');

        //return redirect('contactSuccess');
        return view('blade.pages.contactSuccess');

    }
    /* /Contact submit */


    /* send email during order submit */
    static public function  sendCustomMail($order_id,$firstname,$lastname,$pickup, $pickup_address1, $pickup_address2, $pickup_postcode, $dropoff, $dropoff_address1, $dropoff_address2, $dropoff_postcode, $distances, $email, $mobile, $comments, $tourdate, $tourtime, $tourmin, $tourdates, $tourType, $returndate='', $return, $returntime='', $returnmin='', $via_points, $carname, $no_of_person, $no_of_child, $no_of_suitcase, $totalprice, $meetngreet, $arrival_time, $airline_name, $flight_number, $departure_city, $pickup_after, $edited)
    {
        $currencySymbol = '&pound;';
        $headers = "From: Asiana Cars <" . strip_tags("info@asianacars.com") . ">\r\n";
        $headers .= "Reply-To: Asiana Cars <" . strip_tags("info@asianacars.com") . ">\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $carlist = DB::table('car_details')
            ->leftjoin('drivers', function($join)
            {
                $join->on('car_details.driver_id', '=', 'drivers.id');
            })
            ->select('car_details.id','car_details.name', 'car_details.total_seats', 'car_details.category', 'car_details.created_at', 'drivers.first_name as Dfname', 'drivers.last_name as Dlname', 'drivers.phone_number as Dphone', 'drivers.email as Demail')
            ->where('car_details.id', $carname)
            ->first();

        $carname = $carlist->name;
        $totalSeats = $carlist->total_seats;
        $drivername = $carlist->Dfname." ".$carlist->Dlname;
        $driverphone = $carlist->Dphone;
        $driveremail = $carlist->Demail;
        //echo $driveremail; die();

        $carinfo = '<strong>Vehicle</strong>:<br> '.$carname;

        if($edited == 1){
        $bodyHeading = 'We would like to take the opportunity to say thank you. We have received your edited booking request. Please, check your journey details below and let us know should anything required to change. We will confirm your booking by phone and /or email soon.';
        }
        else{
            $bodyHeading = 'We would like to take the opportunity to say thank you. We have received your booking request. Please, check your journey details below and let us know should anything required to change. We will confirm your booking by phone and /or email soon.';
        }

        $body = '<p> '.$bodyHeading.' </p>

        <h3 style="color:red; font-weight:bold;">Note: This is not a confirmation of your booking. We will send an email soon.</h3>

        <br/><strong>Asiana Cars Journey</strong><br><br/>
        <strong>Ref-</strong> '.$order_id.'<br>
        '.$carinfo.' ('.$totalSeats.'Persons)<br/>
        <strong>Luggage</strong>:<br> '.$no_of_suitcase.' suitcases<br><br/>


        <strong>Passenger Details:</strong><br>
        '.$firstname.' '.$lastname.'<br>
        <strong>Mobile</strong>: <br>'.$mobile.'<br>
        <strong>Email</strong>: <br>'.$email.'<br>
        <strong>No. of Person</strong>: '.$no_of_person.' Nos.<br>
        <strong>No. of Child</strong>: '.$no_of_child.' Nos.<br>
        <strong>No. of Suitcase</strong>:'.$no_of_suitcase.' Nos.<br>
        <strong>Comments</strong>: <br>'.$comments.'<br><br/>

        <strong>Pickup Time:</strong>:<br>
        '.date('l j M Y, H:i', $tourdates).' ('.date('h:i a', $tourdates).')<br/>
        <strong>Contact Person</strong>: <br/>'.$drivername.'<br>
        <strong>Mobile Number</strong>: <br/>'.$driverphone.'<br>
        <strong>Pickup</strong>: <br/>'.$pickup.', '.$pickup_address1.', '.$pickup_address2.', '.$pickup_postcode.'<br>
        <strong>Drop-off</strong>: <br/>'.$dropoff.', '.$dropoff_address1.', '.$dropoff_address2.', '.$dropoff_postcode.'<br>
        <strong>Total Distance</strong>: '.ceil($distances).' miles<br><br/>';
        if((int)$meetngreet > 0) $body .= '<strong>Special Requirement: </strong>: <br/>+ Meet & Greet '.$this->currencySymbol.$meetngreet.'.00 (Included)<br>';
        $body .='
        <br>
        <strong>Transfer Information</strong><br>
        <strong>Arrival</strong>: '.$arrival_time.'<br>
        <strong>Airline</strong>: '.$airline_name.'<br>
        <strong>Flight</strong>: '.$flight_number.'<br>
        <strong>Departure City</strong>: '.$departure_city.'<br>
        <strong>Pickup after landing</strong>: '.$pickup_after.' minutes';

        if($tourType == "round"){
            $return_details = '<br/> <strong>Roundtrip Time</strong><br>
            '.date('l j M Y, H:i', $return).' ('.date('h:i a', $return).')<br/>
               ';
        }
        else{
            $return_details = '';
        }

        if($via_points){

            $via_points_all = explode("|",$via_points);
            $ViaPointscount = count($via_points_all);

            $via_point_details = '<strong>Waypoints:</strong><br>';
            $via_points_seperate = '';

            for($i = 1; $i <= $ViaPointscount; $i++){
                $via_points_seperate .= ''.$i.'. '.str_replace('+',' ',$via_points_all[$i-1]).'<br/>';
            }


        }
        else{
            $via_point_details = '';
            $via_points_seperate = '';
        }

        $selectedTourDate = substr($tourdate,0,5);
        $specialDate = array('25/12','26/12','31/12','01/01');
        if(in_array($selectedTourDate,$specialDate)){
            $specialPriceMessage = '<p class="specialcharge" style="text-align: center; margin-top: 2px;">(Special pricing has been applied due to the date.)</p>';
        }

     $email_temp = '<html>
    <head>
        <title>Taxi Booking</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body bgcolor="#d4d2d2" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

    <table width="600" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
      <tr>
        <td align="center">
            <a href="http://www.asianacars.com"><img src="http://asianacars.com/assets/images/logo2.jpg" alt="Logo"></a>
        </td>
      </tr>

      <tr>
          <td width="600" height="100%" valign="top">

              <table cellpadding="0" cellspacing="0" style="color:#000; border:1px solid #EBEBEB; font-family:Arial, sans-serif">

                   <tr>
                        <td colspan="2">
                            <a href="http://www.asianacars.com"><img src="http://asianacars.com/assets/images/Banner-600.png" alt="Banner"></a>
                        </td>
                   </tr>

                    <tr>
                      <td colspan="2" style="padding:30px;" style="font-family:Arial, sans-serif;">

                          Dear '.$firstname.',<br>

                         '.$body.'

                          <br>
                          <br>

                          '.$via_point_details.'
                          '.$via_points_seperate.'

                          '.$return_details.'
<br/>
                          <strong>Total Cost</strong>: '.$currencySymbol. number_format($totalprice,2).'<br>

                          <br>
                          <br>
                          Wish you a safe and comfortable journey.
                          <br>
                          <br>
                          <strong>Regards</strong><br>
                          Asiana Cars Ltd<br>
                          www.asianacars.com<br/>
                      </td>
                    </tr>


                    <tr style="border-top:1px solid #EBEBEB;">
                        <td width="300" height="125" align="left" style="color:#000; padding-left:30px; font-family:Arial, sans-serif">
                              <strong>Contact Information</strong><br>
                              Tel: +44 1737668060<br/>
                              Email : <a href="mailto:info@asianacars.com" style="color:#000;">info@asianacars.com</a><br>
                        </td>
                        <td width="300" height="125" align="right" style="color:#fff; padding-right:30px; font-family:Arial, sans-serif">
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/fb.png" alt="fb">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/twitter.png" alt="twitter">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/linkedin.png" alt="linkedin">
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="#" style="text-decoration:none;">
                                <img src="http://asianacars.com/assets/images/youtube.png" alt="youtube">
                            </a>
                        </td>
                    </tr>

                    <tr>
                    <td colspan="2" width="600">
                        <img src="http://asianacars.com/assets/images/line.jpg" alt="Line">
                    </td>
                  </tr>

              </table>
          </td>

      </tr>

    </table>

    </body>
    </html>';

        if($edited == 1){
            $subUser = "Asiana Cars: Booking Edited: Ref Number: ".$order_id;
            $subAdmin = "Asiana Cars: Order Edited: Ref Number: ".$order_id;
        }
        else{
            $subUser = "Asiana Cars: Booking Recieved: Ref Number: ".$order_id;
            $subAdmin = "Asiana Cars: New Order: Ref Number: ".$order_id;
        }


        /*This is email Body Content for sending an email after booking completion*/

        //echo $email_temp; die();
        //ini_set('SMTP', 'localhost');
        mail($email, $subUser, $email_temp, $headers);

        if($edited == 1){
            $subDriver = "Asiana Cars: One Order Edited: Ref Number: ".$order_id;
            mail($driveremail, $subDriver, $email_temp, $headers);
        }

        /*For All Admin Email Send*/
        $allAdminRoleID = User::where('role','1')->get();


        foreach($allAdminRoleID as $adminID){
            //$adminEmail = Admin::where('user_id',$adminID->id)->first();

            mail($adminID->email, $subAdmin, $email_temp, $headers);
        }

    }

    /* /send email order submit */


    /* Admin Booking result */
    public function AdminBookingResult(Request $request)
    {
        Session::put('page1Values',$_POST);
//        echo '<pre>';
//        print_r($_POST);
//        print_r(Session::get('page1Values'));
//        echo '</pre>';

        $pickup = str_replace(' ','+',$request->get('pickup'));
        $dropoff = str_replace(' ','+',$request->get('dropoff'));

        //=== waypoints
        $waypoint_1 = $request->get('waypoint_1');
        $waypoint_2 = $request->get('waypoint_2');
        $waypoint_3 = $request->get('waypoint_3');
        $waypoint_4 = $request->get('waypoint_4');

        //=== checking waypoints
        $waypoint_1 = (isset($waypoint_1) && $waypoint_1 != '') ?  str_replace(' ','+',$waypoint_1) : '';
        $waypoint_2 = (isset($waypoint_2) && $waypoint_2 != '') ?  str_replace(' ','+',$waypoint_2) : '';
        $waypoint_3 = (isset($waypoint_3) && $waypoint_3 != '') ?  str_replace(' ','+',$waypoint_3) : '';
        $waypoint_4 = (isset($waypoint_4) && $waypoint_4 != '') ?  str_replace(' ','+',$waypoint_4) : '';

        //=== generate waypoint string
        $waypoints = 'waypoints=optimize:false|';
        //$waypoints = 'waypoints=optimize';
        if($waypoint_1 == '' && $waypoint_2 == '' && $waypoint_3 == '' && $waypoint_4 == '') $waypoints = '';
        $via_pointsArray = array_filter(array($waypoint_1,$waypoint_2,$waypoint_3,$waypoint_4));
        $waypoints .= implode("|",$via_pointsArray);

        $via_points = implode("|",$via_pointsArray);

        $apiKey = $this->googleAPIKey;
        $params = file_get_contents("https://maps.googleapis.com/maps/api/directions/json?region=uk&origin=$pickup&destination=$dropoff&units=imperial&$waypoints&key=$apiKey&alternatives=true");
        $directions = json_decode($params,true);
//        echo '<pre>';
//        print_r($_POST);
//        print_r($directions);
//        echo '</pre>';

        if($directions['status']!='ZERO_RESULTS'){
//            echo '<pre>';
//            print_r($directions['routes'][0]['legs']);
//            echo '</pre>';

            $pickup = str_replace('+',' ',$pickup);
            $dropoff = str_replace('+',' ',$dropoff);
            $routes = $directions['routes'][0]['legs'];

            $routesArray = $this->calcRoutes($directions);
            $carList = $routesArray['carList'];
            $locationsLatLongData = $routesArray['locationsLatLongData'];

            $tourdate = $request->get('tourdate');
            $tourtime = $request->get('tourtime');
            $tourmin = $request->get('tourmin');
            $tourtime = $tourtime.":".$tourmin;

            $locationsLatLongDataImploded = implode("|",$locationsLatLongData);
            $locationsLatLongDataImplodedComma = implode(",",$locationsLatLongData);


            $mapImage = "https://maps.googleapis.com/maps/api/staticmap?maptype=roadmap&markers=size:mid%7Ccolor:red%7C$locationsLatLongDataImploded&zoom=5&size=310x125&path=$locationsLatLongDataImploded";

            $orderID = $request->get('order_id');
            if($orderID > 0){
                $order = DB::table('orders')
                    ->leftjoin('passengers', function($join)
                    {
                        $join->on('orders.user_id', '=', 'passengers.id');
                    })
                    ->leftjoin('car_details', function($join)
                    {
                        $join->on('orders.car_details', '=', 'car_details.id');
                    })
                    ->leftjoin('pickup_from_airport', function($join)
                    {
                        $join->on('orders.id', '=', 'pickup_from_airport.order_id');
                    })
                    ->leftjoin('drivers', function($join)
                    {
                        $join->on('orders.driver_id', '=', 'drivers.id');
                    })
                    ->where('orders.id', $orderID)
                    ->select('orders.id','orders.user_id', 'orders.dropoff_address', 'orders.dropoff_address1','orders.dropoff_address2', 'orders.dropoff_postcode', 'orders.pickup_address', 'orders.pickup_address1', 'orders.pickup_address2', 'orders.pickup_postcode', 'orders.no_of_person', 'orders.no_of_child', 'orders.suitcases', 'orders.via_points','orders.meetngreet', 'orders.comments', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.status', 'orders.pay_via','orders.card_customer_id','orders.card_charge_id', 'orders.payment_status', 'orders.date_time', 'orders.return_booking', 'orders.return_date_time', 'orders.driver_id', 'orders.created_at', 'drivers.first_name as dFname', 'drivers.last_name as dLname', 'drivers.email as dEmail', 'drivers.phone_number as dPhone', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'car_details.id as carId', 'car_details.name as carname', 'car_details.total_seats', 'pickup_from_airport.flight_arrival_time', 'pickup_from_airport.airline_name', 'pickup_from_airport.flight_no', 'pickup_from_airport.departure_city', 'pickup_from_airport.pickup_after')
                    ->first();
                return view('admin_blade.pages.editBookingResult', compact('pickup', 'dropoff', 'routesArray', 'carList', 'tourdate', 'tourtime','mapImage', 'via_points', 'order'));
            }

            return view('admin_blade.pages.AdminBookingResult', compact('pickup', 'dropoff', 'routesArray', 'carList', 'tourdate', 'tourtime','mapImage', 'via_points'));
        }
        else {
            //return error message
            return view('blade.pages.NoResult');
        }

    }
    /* /Admin Booking result */


    /* Admin Booking details */
    public function AdminBookingFinal(Request $request)
    {
        $pickup = $request->get('pickup');
        $dropoff = $request->get('dropoff');
        $via_points = $request->get('via_points');
        $distances = $request->get('distances');
        $carname = $request->get('carname');
        $carfullname = $request->get('carfullname');
        $car_pic = $request->get('car_pic');
        $totalprice = $request->get('totalprice');
        $singleTrip = $request->get('singleTrip');
        $roundTrip = $request->get('roundTrip');
        $tourdate = $request->get('tourdate');
        $tourtime = $request->get('tourtime');
        $meetngreet = $request->get('meetngreet');

        $via_points_all = explode("|",$via_points);
        $ViaPointscount = count($via_points_all);
        $via_points_seperate = $via_points_all;

        $tripType = 'single';
        if($totalprice == $singleTrip){
            $tripType = 'single';
            $totalSpecialPrice = ($totalprice*1.5)+$meetngreet;
            $totalprice = $totalprice + $meetngreet;
        }
        else if($totalprice == $roundTrip){
            $tripType = 'round';
            $totalSpecialPrice = ($totalprice*1.5)+$meetngreet;
            $totalprice = $totalprice + $meetngreet;
        }

        $setting = DB::table('setting')->where('property','=','payment_method')->first();
        $pm = explode(",", $setting->value);

        $orderID = $request->get('order_id');
        if($orderID > 0){
            $order = DB::table('orders')
                ->leftjoin('passengers', function($join)
                {
                    $join->on('orders.user_id', '=', 'passengers.id');
                })
                ->leftjoin('car_details', function($join)
                {
                    $join->on('orders.car_details', '=', 'car_details.id');
                })
                ->leftjoin('pickup_from_airport', function($join)
                {
                    $join->on('orders.id', '=', 'pickup_from_airport.order_id');
                })
                ->leftjoin('drivers', function($join)
                {
                    $join->on('orders.driver_id', '=', 'drivers.id');
                })
                ->where('orders.id', $orderID)
                ->select('orders.id','orders.user_id', 'orders.dropoff_address', 'orders.dropoff_address1','orders.dropoff_address2', 'orders.dropoff_postcode', 'orders.pickup_address', 'orders.pickup_address1', 'orders.pickup_address2', 'orders.pickup_postcode', 'orders.no_of_person', 'orders.no_of_child', 'orders.suitcases', 'orders.via_points','orders.meetngreet', 'orders.comments', 'orders.car_details', 'orders.total_bill', 'orders.total_distance', 'orders.status', 'orders.pay_via','orders.card_customer_id','orders.card_charge_id', 'orders.payment_status', 'orders.date_time', 'orders.return_booking', 'orders.return_date_time', 'orders.driver_id', 'orders.created_at', 'drivers.first_name as dFname', 'drivers.last_name as dLname', 'drivers.email as dEmail', 'drivers.phone_number as dPhone', 'passengers.id as P_id', 'passengers.first_name', 'passengers.last_name', 'passengers.email', 'passengers.phone_number', 'car_details.name as carname', 'car_details.total_seats', 'pickup_from_airport.flight_arrival_time', 'pickup_from_airport.airline_name', 'pickup_from_airport.flight_no', 'pickup_from_airport.departure_city', 'pickup_from_airport.pickup_after')
                ->first();
            return view('admin_blade.pages.editBookingFinal', compact('pickup', 'dropoff', 'via_points', 'distances', 'carname', 'carfullname', 'car_pic', 'totalprice', 'bookedcost', 'tripType', 'tourdate', 'tourtime', 'via_points_seperate', 'meetngreet', 'pm', 'order'));
        }
        //====stripe config
        $stripe = Config::get('services.stripe');

        return view('admin_blade.pages.AdminBookingFinal', compact('pickup', 'dropoff', 'via_points', 'distances', 'carname', 'carfullname', 'car_pic', 'totalprice', 'bookedcost', 'tripType', 'tourdate', 'tourtime', 'via_points_seperate', 'meetngreet', 'pm', 'totalSpecialPrice', 'stripe'));
    }

    /* /Admin Booking details */

    /* Admin Booking complete */
    public function AdminBookingSubmit(Request $request)
    {
//        echo '<pre>';
//        print_r($_POST);
//        echo '</pre>';
//        die;
        //$payment_method = 'stripe';
        $returndate='';
        $return='';
        $returntime='';
        $returnmin='';

        $pickup = $request->get('pickup');
        $pickup_address1 = $request->get('pickup_address1');
        $pickup_address2 = $request->get('pickup_address2');
        $pickup_postcode = $request->get('pickup_postcode');

        $dropoff = $request->get('dropoff');
        $dropoff_address1 = $request->get('dropoff_address1');
        $dropoff_address2 = $request->get('dropoff_address2');
        $dropoff_postcode = $request->get('dropoff_postcode');

        $via_points = $request->get('via_points');
        $distances = $request->get('distances');
        $carname = $request->get('carname');
        $tourType = $request->get('tourType');
        $meetngreet = $request->get('meetngreet');
        //$carname = 1;
        $special_date = $request->get('special_date');
        $tourdate = $request->get('tourdate');
        $tourdate = str_replace('/', '-', $tourdate);
        $tourtime = $request->get('tourtime');
        $tourmin = $request->get('tourmin');
        $tourdates = strtotime($tourdate." ".$tourtime.":".$tourmin);


        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');
        $email = $request->get('email');
        $mobile = $request->get('mobile');
        $comments = $request->get('comments');

        $no_of_person = $request->get('no_of_person');
        $no_of_suitcase = $request->get('no_of_suitcase');
        $no_of_child = $request->get('no_of_child');

        $arrival_time = $request->get('arrival_time');
        $airline_name = $request->get('airline_name');
        $flight_number = $request->get('flight_number');
        $departure_city = $request->get('departure_city');
        $pickup_after = $request->get('pickup_after');

        $totalprice = $request->get('totalprice');

        $stripe_customer_id = $request->get('stripeCustomerID');
        $stripe_charge_id = $request->get('stripeChargeID');
        $payment_method = $request->get('payment_method');

//        if($payment_method == 'stripe'){
//            return redirect('http://localhost/asiana_heroku/booking');
//        }
//        else{

        $UserId = Auth::user()->getkey();

        $driver = DB::table('car_details')
            ->where('id', $carname)
            ->first();

        $passenger = DB::table('passengers')
            ->where('email', $email)
            ->first();
        if(count($passenger) < 1){
            $user = new Passengers();
            $id = $user::insertGetId(array('first_name'=>$firstname, 'last_name'=>$lastname, 'email'=>$email, 'phone_number'=>$mobile));
        }
        else
            $id =$passenger->id;

        $Orders = new Orders();
        $Orders->user_id = $id;
        $Orders->pickup_address = $pickup;
        $Orders->pickup_address1 = $pickup_address1;
        $Orders->pickup_address2 = $pickup_address2;
        $Orders->pickup_postcode = $pickup_postcode;
        $Orders->dropoff_address = $dropoff;
        $Orders->dropoff_address1 = $dropoff_address1;
        $Orders->dropoff_address2 = $dropoff_address2;
        $Orders->dropoff_postcode = $dropoff_postcode;
        $Orders->via_points = $via_points;
        $Orders->car_details = $carname;
        $Orders->total_bill = $totalprice;
        $Orders->total_distance = $distances;
        $Orders->driver_id = $driver->driver_id;
        $Orders->date_time = $tourdates;
        $Orders->special_date = $special_date;
        $Orders->no_of_child = $no_of_child;
        $Orders->no_of_person = $no_of_person;
        $Orders->suitcases = $no_of_suitcase;
        $Orders->comments = $comments;
        $Orders->createdBy = $UserId;
        if((int)$meetngreet > 0) $Orders->meetngreet = $meetngreet;

        if($tourType == 'round'){
            $returndate = $request->get('returnDate');
            $returndate = str_replace('/', '-', $returndate);
            $returntime = $request->get('returnTime');
            $returnmin = $request->get('returnMin');
            $return = strtotime($returndate." ".$returntime.":".$returnmin);

            $Orders->return_date_time = $return;
            $Orders->return_booking = 1;
        }
        else {
            $returndate = '';
            $returntime = '';
            $returnmin = '';
        }

        //===card info to Order
        $Orders->card_customer_id = $stripe_customer_id;
        $Orders->card_charge_id = $stripe_charge_id;
        $Orders->pay_via = $payment_method;

        //===card info to Order
//        $Orders->card_customer_id = $stripe_customer_id;
//        $Orders->card_charge_id = $stripe_charge_id;
        //$Orders->pay_via = 'Cash';

        $Orders->save();

        $order_id = $Orders->id;

        $pickup_from = new Airports();
        $pickup_from->order_id = $order_id;
        $pickup_from->flight_arrival_time = $arrival_time;
        $pickup_from->airline_name = $airline_name;
        $pickup_from->flight_no = $flight_number;
        $pickup_from->departure_city = $departure_city;
        $pickup_from->pickup_after = $pickup_after;
        $pickup_from->save();

        $lookupTable = new ordereditHistory();
        $lookupTable->order_id = $order_id;
        $lookupTable->adjusted_bill = $totalprice;
        $lookupTable->save();

        $edited = 0;

        $this->sendCustomMail($order_id,$firstname,$lastname,$pickup, $pickup_address1, $pickup_address2, $pickup_postcode, $dropoff, $dropoff_address1, $dropoff_address2, $dropoff_postcode, $distances, $email, $mobile, $comments, $tourdate, $tourtime, $tourmin, $tourdates, $tourType, $returndate, $return, $returntime, $returnmin, $via_points, $carname, $no_of_person, $no_of_child, $no_of_suitcase, $totalprice, $meetngreet, $arrival_time, $airline_name, $flight_number, $departure_city, $pickup_after, $edited);

        Session::forget('page1Values');

        flash()->success('Order Created successfully !!');

        return redirect('orderlist');
        //} //end else
    }
    /* /Admin Booking complete */


    /* Admin Edit Booking complete */
    public function AdminEditBookingSubmit(Request $request)
    {
        $order_id = $request->get('order_id');

        $pickup = $request->get('pickup');
        $pickup_address1 = $request->get('pickup_address1');
        $pickup_address2 = $request->get('pickup_address2');
        $pickup_postcode = $request->get('pickup_postcode');

        $dropoff = $request->get('dropoff');
        $dropoff_address1 = $request->get('dropoff_address1');
        $dropoff_address2 = $request->get('dropoff_address2');
        $dropoff_postcode = $request->get('dropoff_postcode');

        $via_points = $request->get('via_points');
        $distances = $request->get('distances');
        $carname = $request->get('carname');
        $tourType = $request->get('tourType');
        $meetngreet = $request->get('meetngreet');
        //$carname = 1;
        $tourdate = $request->get('tourdate');
        $tourdate = str_replace('/', '-', $tourdate);
        $tourtime = $request->get('tourtime');
        $tourmin = $request->get('tourmin');
        $tourdates = strtotime($tourdate." ".$tourtime.":".$tourmin);


        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');
        $email = $request->get('email');
        $mobile = $request->get('mobile');
        $comments = $request->get('comments');

        $no_of_person = $request->get('no_of_person');
        $no_of_suitcase = $request->get('no_of_suitcase');
        $no_of_child = $request->get('no_of_child');

        $arrival_time = $request->get('arrival_time');
        $airline_name = $request->get('airline_name');
        $flight_number = $request->get('flight_number');
        $departure_city = $request->get('departure_city');
        $pickup_after = $request->get('pickup_after');

        $totalprice = $request->get('totalprice');

//        $stripe_customer_id = $request->get('stripeCustomerID');
//        $stripe_charge_id = $request->get('stripeChargeID');
        $payment_method = $request->get('payment_method');

//        if($payment_method == 'stripe'){
//            return redirect('http://localhost/asiana_heroku/booking');
//        }
//        else{

//        $driver = DB::table('car_details')
//            ->where('id', $carname)
//            ->first();

        $UserId = Auth::user()->getkey();

        $passenger = DB::table('passengers')
            ->where('email', $email)
            ->first();

        $id =$passenger->id;

        $passenger = Passengers::find($id);
        $passenger->first_name = $firstname;
        $passenger->last_name = $lastname;
        $passenger->email = $email;
        $passenger->phone_number = $mobile;
        $passenger->save();

        $Orders = Orders::find($order_id);
        $Orders->user_id = $id;
        $Orders->pickup_address = $pickup;
        $Orders->pickup_address1 = $pickup_address1;
        $Orders->pickup_address2 = $pickup_address2;
        $Orders->pickup_postcode = $pickup_postcode;
        $Orders->dropoff_address = $dropoff;
        $Orders->dropoff_address1 = $dropoff_address1;
        $Orders->dropoff_address2 = $dropoff_address2;
        $Orders->dropoff_postcode = $dropoff_postcode;
        $Orders->via_points = $via_points;
        $Orders->car_details = $carname;
        $Orders->total_bill = $totalprice;
        $Orders->total_distance = $distances;
        //$Orders->driver_id = $driver->driver_id;
        $Orders->date_time = $tourdates;
        $Orders->no_of_child = $no_of_child;
        $Orders->no_of_person = $no_of_person;
        $Orders->suitcases = $no_of_suitcase;
        $Orders->comments = $comments;
        $Orders->editedBy = $UserId;
        if((int)$meetngreet > 0) $Orders->meetngreet = $meetngreet;

        if($tourType == 'round'){
            $returndate = $request->get('returnDate');
            $returndate = str_replace('/', '-', $returndate);
            $returntime = $request->get('returnTime');
            $returnmin = $request->get('returnMin');
            $return = strtotime($returndate." ".$returntime.":".$returnmin);

            $Orders->return_date_time = $return;
            $Orders->return_booking = 1;
        }
        else {
            $returndate = '';
            $returntime = '';
            $returnmin = '';
            $Orders->return_date_time = 0;
            $Orders->return_booking = 0;
        }

        //===card info to Order
//        $Orders->card_customer_id = $stripe_customer_id;
//        $Orders->card_charge_id = $stripe_charge_id;
        $Orders->pay_via = $payment_method;
        $Orders->save();

        $pickup_from_airport = DB::table('pickup_from_airport')
            ->where('order_id', $order_id)
            ->first();

        $pickup_from_airport_id =$pickup_from_airport->id;

        $pickup_from = Airports::find($pickup_from_airport_id);
        $pickup_from->flight_arrival_time = $arrival_time;
        $pickup_from->airline_name = $airline_name;
        $pickup_from->flight_no = $flight_number;
        $pickup_from->departure_city = $departure_city;
        $pickup_from->pickup_after = $pickup_after;
        $pickup_from->save();

        $edited = 1;

        $this->sendCustomMail($order_id,$firstname,$lastname,$pickup, $pickup_address1, $pickup_address2, $pickup_postcode, $dropoff, $dropoff_address1, $dropoff_address2, $dropoff_postcode, $distances, $email, $mobile, $comments, $tourdate, $tourtime, $tourmin, $tourType, $returndate, $returntime, $returnmin, $via_points, $carname, $no_of_person, $no_of_child, $no_of_suitcase, $totalprice, $meetngreet, $arrival_time, $airline_name, $flight_number, $departure_city, $pickup_after, $edited);


        flash()->success('Booking edited successfully !!');

        return redirect('orderlist');
        //} //end else
    }
    /* /Admin Edit Booking complete */



    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function index_new(){
        $sessData = Session::get('page1Values');
        $special_routes = DB::table('special_route')->get();
        return view('blade.pages.index-new',compact('sessData','special_routes'));
    }

    public function index_route($route){
        $sessData = Session::get('page1Values');
        $special_routes = DB::table('special_route')->get();
        $routing = DB::table('routes')->where('route','=',$route)->first();
        return view('blade.pages.index-new',compact('sessData','special_routes','routing'));
    }

    public function special_route_front_end(){
        $special_routes = DB::table('special_route')->get();
        return view('blade.pages.special_route_front_end', compact('special_routes'));
    }

    public function sr_select_car($sr_id){
        $special_routes = DB::table('special_route')->where('id','=',$sr_id)->first();


        $pickup = str_replace(' ','+',$special_routes->pickup_address);
        $dropoff = str_replace(' ','+',$special_routes->dropoff_address);

        //=== waypoints
        $waypoint_1 = '';
        $waypoint_2 = '';
        $waypoint_3 = '';
        $waypoint_4 = '';

        //=== checking waypoints
        $waypoint_1 = (isset($waypoint_1) && $waypoint_1 != '') ?  str_replace(' ','+',$waypoint_1) : '';
        $waypoint_2 = (isset($waypoint_2) && $waypoint_2 != '') ?  str_replace(' ','+',$waypoint_2) : '';
        $waypoint_3 = (isset($waypoint_3) && $waypoint_3 != '') ?  str_replace(' ','+',$waypoint_3) : '';
        $waypoint_4 = (isset($waypoint_4) && $waypoint_4 != '') ?  str_replace(' ','+',$waypoint_4) : '';

        //=== generate waypoint string
        $waypoints = 'waypoints=optimize:false|';
        //$waypoints = 'waypoints=optimize';
        if($waypoint_1 == '' && $waypoint_2 == '' && $waypoint_3 == '' && $waypoint_4 == '') $waypoints = '';
        $via_pointsArray = array_filter(array($waypoint_1,$waypoint_2,$waypoint_3,$waypoint_4));
        $waypoints .= implode("|",$via_pointsArray);

        $via_points = implode("|",$via_pointsArray);

        $apiKey = $this->googleAPIKey;
        //$params = file_get_contents("https://maps.googleapis.com/maps/api/directions/json?region=uk&origin=$pickup&destination=$dropoff&sensor=false&units=imperial&$waypoints&key=$apiKey&alternatives=true");
        //$directions = json_decode($params,true);

        //if($directions['status']!='ZERO_RESULTS'){
            $pickup = str_replace('+',' ',$pickup);
            $dropoff = str_replace('+',' ',$dropoff);
            //$routes = $directions['routes'][0]['legs'];

            //$routesArray = $this->calcRoutes($directions);
            //=== get list of cars
            $carList = DB::table('car_details')
                ->leftjoin('car_category', function($join)
                {
                    $join->on('car_details.category', '=', 'car_category.id');
                })
                ->select('car_details.id','car_details.name', 'car_details.total_seats', 'car_category.cat_name','car_category.details', 'car_category.max_person', 'car_category.max_carry_on', 'car_category.price_ratio', 'car_category.car_pic', 'car_category.max_suitcases')
                ->orderBy('car_details.id', 'ASC')
                ->get();

            //$locationsLatLongData = $routesArray['locationsLatLongData'];

            foreach($carList as $car){
                $car_id = $car->id;
                $single = 'single_'.$car_id;
                $round = 'round_'.$car_id;
                $routesArray['carPrices'][$car_id]['price'] = (int)$special_routes->$single;
                $routesArray['carPrices'][$car_id]['priceSpecialDate'] = (int)$special_routes->$single;
                $routesArray['carPrices'][$car_id]['priceReturn'] = (int)$special_routes->$round;
                $routesArray['carPrices'][$car_id]['priceSpecialDateReturn'] = (int)$special_routes->$round;

                $routesArray['carPrices'][$car_id]['priceFormatted'] = number_format($routesArray['carPrices'][$car_id]['price'],2);
                $routesArray['carPrices'][$car_id]['priceReturnFormatted'] = number_format($routesArray['carPrices'][$car_id]['priceReturn'],2);

                $routesArray['carPrices'][$car_id]['currencySymbol'] = '&pound;';
            }
            $routesArray['distance_total_miles_text'] = $special_routes->distance.' mi.';
            $routesArray['distance_total_miles'] = $special_routes->distance;

            //echo '<pre>';
            //print_r($routesArray);
            //echo '</pre>';die;

            $tourdate = '';
            $tourtime = '';
            $tourmin = '';
            $tourtime = '';

            //$locationsLatLongDataImploded = implode("|",$locationsLatLongData);
            //$locationsLatLongDataImplodedComma = implode(",",$locationsLatLongData);


            $mapImage = "";


            return view('blade.pages.result', compact('pickup', 'dropoff', 'routesArray', 'carList', 'tourdate', 'tourtime','mapImage', 'via_points'));
        /*}
        else {
            //return error message
            return view('blade.pages.NoResult');
        }*/
    }

}
