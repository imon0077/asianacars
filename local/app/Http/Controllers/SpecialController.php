<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;

use Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Pricelist;
use App\CarsCat;
use App\CarDetails;
use App\Drivers;
use App\Passengers;
use App\Airports;
use App\Orders;
use App\SpecialRoute;
use App\Routes;
use App\Setting;
use App\ordereditHistory;
use App\Http\Requests\editprofilesaveRequest;
use App\Http\Requests\editcarsaveRequest;
use App\Http\Requests\editdriveresaveRequest;
use Stripe;
use Config;

class SpecialController extends Controller {

    //private $googleAPIKey = 'AIzaSyD-OnhY1_cMrzguH_dnfm8YihGd6OGRaK4'; //server based API
    private $googleAPIKey = 'AIzaSyBVxh2byXJ1aP_FOjjVIxjPsFtnj8UV1b4'; //browser based API
    //==== Price Vars
    public $londonPrice = 0;
    public $first5Price = 15; // for 0-5 miles
    //public $flatRateMoreThan30 = 1.5;
    //public $rateGreaterThan5UpTo30 = 1.5;
    public $rateGreaterThan6UpTo20 = 1.50; // for 6-20 miles; i.e. £18.00 + 1.75/mile
    public $rateGreaterThan20UpTo35 = 1.4; // for 21-35 miles; i.e. £18.00 + 1.5/mile
    public $rateGreaterThan35UpTo55 = 1.4; // for 36-55 miles; flat rate for each mile. i.e. 39 miles * £1.5 = £58.5
    public $flatRateMoreThan55 = 1.3; // for more than 55 miles; flat rate for each mile. i.e. 90 miles * £1.3 = £117
    public $firstMileageCounter = 5;
    public $secondMileageCounter = 20;
    public $thirdMileageCounter = 35;
    public $forthMileageCounter = 55;
    public $singlPrice = 0;
    public $totalPrice = 0;
    public $returnDiscountPercent = 5;
    public $currency = 'GBP';
    public $currencySymbol = '&pound;';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $cars = DB::table('car_details')->get();
        return view('admin_blade.pages.specialRoute', compact('cars'));
    }

    public function addSpecialRoute(Request $aSR){
        $pickup_address = $aSR->get('pickup');
        $dropoff_address = $aSR->get('dropoff');
        $distance = $aSR->get('distance');
        $price = $aSR->get('price');

        $sr = new SpecialRoute();
        $sr->pickup_address = $pickup_address;
        $sr->dropoff_address = $dropoff_address;
        $sr->distance = $distance;
        $sr->price = $price;

        $cars = DB::table('car_details')->get();
        foreach($cars as $car){
            $sr['single_'.$car->id] = $aSR->get('single_'.$car->id);
            $sr['round_'.$car->id] = $aSR->get('round_'.$car->id);
        }

        $sr_done = $sr->save();
        if($sr_done)
            flash()->success('Special Route creation successful!!');
        else
            flash()->error('Special Route creation failed!!');

        return redirect('special_route');
    }

    public function special_route_list(){
        $cars = DB::table('car_details')->get();
        $special_routes = DB::table('special_route')->get();
        return view('admin_blade.pages.special_route_list', compact('cars','special_routes'));
    }

    public function delSpecialRoute($sr_id){
        $sr = DB::table('special_route')->where('id','=',$sr_id)->delete();
        if($sr)
            flash()->success('Special Route deleted successfully!!');
        else
            flash()->error('Special Route delete failed!!');

        return redirect('special_route_list');
    }

    public function editSpecialRoute($sr_id){
        $cars = DB::table('car_details')->get();
        $sr = DB::table('special_route')->where('id','=',$sr_id)->first();

        return view('admin_blade.pages.specialRouteEdit', compact('cars','sr'));
    }

    public function updateSpecialRoute(Request $aSR){
        $sr_id = $aSR->get('sr_id');
        $pickup_address = $aSR->get('pickup');
        $dropoff_address = $aSR->get('dropoff');
        $distance = $aSR->get('distance');
        $price = $aSR->get('price');

        $sr = array();
        $sr['pickup_address'] = $pickup_address;
        $sr['dropoff_address'] = $dropoff_address;
        $sr['distance'] = $distance;
        $sr['price'] = $price;

        $cars = DB::table('car_details')->get();
        foreach($cars as $car){
            $sr['single_'.$car->id] = $aSR->get('single_'.$car->id);
            $sr['round_'.$car->id] = $aSR->get('round_'.$car->id);
        }

        $sr_done = DB::table('special_route')->where('id','=',$sr_id)->update($sr);
        if($sr_done)
            flash()->success('Special Route updated successfully!!');
        else
            flash()->error('Special Route update failed!!');

        return redirect('edit_sr/'.$sr_id);
    }

    public function route_description(){
        return view('admin_blade.pages.routeDescription');
    }

    public function addRouteDescription(Request $routes){
        $route = $routes->get('route');
        $description = $routes->get('description');

        $dbRoutes = new Routes();
        $dbRoutes->route = $route;
        $dbRoutes->description = $description;
        $route_create = $dbRoutes->save();

        if($route_create)
            flash()->success('Route creation successful!!');
        else
            flash()->error('Route creation failed!!');

        return redirect('route_description');
    }

    public function route_list(){
        $routes = DB::table('routes')->get();
        return view('admin_blade.pages.route_list', compact('routes'));
    }

    public function editRouteDescription($route_id){
        $sr = DB::table('routes')->where('id','=',$route_id)->first();

        return view('admin_blade.pages.routeEdit', compact('sr'));
    }

    public function updateRouteDescription(Request $aSR){
        $sr_id = $aSR->get('route_id');
        $route = $aSR->get('route');
        $description = $aSR->get('description');

        $sr = array();
        $sr['route'] = $route;
        $sr['description'] = $description;

        $sr_done = DB::table('routes')->where('id','=',$sr_id)->update($sr);
        if($sr_done)
            flash()->success('Route updated successfully!!');
        else
            flash()->error('Route update failed!!');

        return redirect('edit_routeDescription/'.$sr_id);
    }

    public function delRouteDescription($route_id){
        $sr = DB::table('routes')->where('id','=',$route_id)->delete();
        if($sr)
            flash()->success('Special Route deleted successfully!!');
        else
            flash()->error('Special Route delete failed!!');

        return redirect('route_list');
    }

}
