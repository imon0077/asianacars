-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 03, 2015 at 09:46 AM
-- Server version: 5.6.23
-- PHP Version: 5.6.8RC1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `taxi_booking`
--

-- --------------------------------------------------------

--
-- Table structure for table `airports`
--

CREATE TABLE IF NOT EXISTS `airports` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `car_category`
--

CREATE TABLE IF NOT EXISTS `car_category` (
  `id` int(10) unsigned NOT NULL,
  `cat_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `max_person` int(11) NOT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_ratio` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `max_suitcases` int(11) NOT NULL,
  `max_carry_on` int(11) NOT NULL,
  `car_pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `car_category`
--

INSERT INTO `car_category` (`id`, `cat_name`, `max_person`, `details`, `price_ratio`, `created_at`, `updated_at`, `max_suitcases`, `max_carry_on`, `car_pic`) VALUES
(1, 'Saloon', 4, 'Its a mini car', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 6, 'saloon.jpg'),
(2, 'Estate', 3, 'Its a large car', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 5, 'estate.jpg'),
(3, 'MPV', 3, 'Its a large car', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 5, 'mpv.jpg'),
(4, 'Executive', 3, 'Its a large car', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 5, 'executive.jpg'),
(5, '8 Seater Minibus', 3, 'Its a large car', 150, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 5, 'miniBus.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `car_details`
--

CREATE TABLE IF NOT EXISTS `car_details` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `total_seats` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `car_details`
--

INSERT INTO `car_details` (`id`, `name`, `category`, `total_seats`, `driver_id`, `created_at`, `updated_at`) VALUES
(1, 'Saloon', 1, 6, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Estate', 2, 6, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'MPV', 3, 6, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Executive', 4, 6, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '8 Seaters Mini Bus', 5, 12, 1, '2015-08-04 17:10:46', '2015-08-04 17:10:46');

-- --------------------------------------------------------

--
-- Table structure for table `cruiseports`
--

CREATE TABLE IF NOT EXISTS `cruiseports` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE IF NOT EXISTS `drivers` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` int(11) NOT NULL,
  `residence_no` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`id`, `user_id`, `first_name`, `last_name`, `contact_name`, `address`, `gender`, `city`, `street`, `postcode`, `residence_no`, `email`, `phone_number`, `created_at`, `updated_at`) VALUES
(2, 2, 'Mr. ABC', 'Khondokar', 'XYZ', 'abcd', 'Male', 'Ctg', 'CTG', 4000, 234, 'abc@gmail.com', '123', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 3, 'imon', 'islam', 'imon', 'ctg', 'Male', 'Chittagong', 'LA', 4000, 0, 'me@imonislam.com', '343443', '2015-09-02 20:43:45', '2015-09-02 20:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_06_28_225802_create_users_table', 1),
('2015_06_28_233453_create_pricelist_table', 1),
('2015_06_29_002414_update_pricelist', 1),
('2015_06_29_015002_create_orders_table', 1),
('2015_06_29_030635_create_passengers_table', 1),
('2015_06_29_033558_create_car_details_table', 1),
('2015_06_29_034412_create_car_category_table', 1),
('2015_06_29_034606_create_drivers_table', 1),
('2015_06_29_040600_create_pickup_from_airport_table', 1),
('2015_06_30_222412_create_airports_table', 1),
('2015_06_30_222647_create_cruiseports_table', 1),
('2015_07_01_020337_add_col_to_orders', 1),
('2015_07_01_024252_add_col_to_car_category', 1),
('2015_07_01_025134_modify_car_category', 1),
('2015_07_08_025331_create_roles_table', 1),
('2015_07_08_025414_create_role_user_table', 1),
('2015_08_13_041106_add_payment_status_to_orders', 1),
('2015_08_15_213752_modify_tbl_orders', 1),
('2015_08_22_060334_cost_ratio', 1),
('2015_06_28_225802_create_users_table', 1),
('2015_06_28_233453_create_pricelist_table', 1),
('2015_06_29_002414_update_pricelist', 1),
('2015_06_29_015002_create_orders_table', 1),
('2015_06_29_030635_create_passengers_table', 1),
('2015_06_29_033558_create_car_details_table', 1),
('2015_06_29_034412_create_car_category_table', 1),
('2015_06_29_034606_create_drivers_table', 1),
('2015_06_29_040600_create_pickup_from_airport_table', 1),
('2015_06_30_222412_create_airports_table', 1),
('2015_06_30_222647_create_cruiseports_table', 1),
('2015_07_01_020337_add_col_to_orders', 1),
('2015_07_01_024252_add_col_to_car_category', 1),
('2015_07_01_025134_modify_car_category', 1),
('2015_07_08_025331_create_roles_table', 1),
('2015_07_08_025414_create_role_user_table', 1),
('2015_08_13_041106_add_payment_status_to_orders', 1),
('2015_08_15_213752_modify_tbl_orders', 1),
('2015_08_22_060334_cost_ratio', 1),
('2015_06_28_225802_create_users_table', 1),
('2015_06_28_233453_create_pricelist_table', 2),
('2015_06_29_002414_update_pricelist', 3),
('2015_06_29_015002_create_orders_table', 4),
('2015_06_29_030635_create_passengers_table', 5),
('2015_06_29_033558_create_car_details_table', 6),
('2015_06_29_034412_create_car_category_table', 7),
('2015_06_29_034606_create_drivers_table', 8),
('2015_06_29_040600_create_pickup_from_airport_table', 9),
('2015_06_30_222412_create_airports_table', 10),
('2015_06_30_222647_create_cruiseports_table', 10),
('2015_07_01_020337_add_col_to_orders', 11),
('2015_07_01_024252_add_col_to_car_category', 12),
('2015_07_01_025134_modify_car_category', 13),
('2015_07_08_025331_create_roles_table', 14),
('2015_07_08_025414_create_role_user_table', 14),
('2015_08_24_024237_modify_table_passengers', 15),
('2015_08_24_030833_modify_table_pickup_from_airport', 15),
('2015_09_01_224454_add_address_field_to_orders', 16),
('2015_09_03_023933_modify_tbl_drivers', 17);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `pickup_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pickup address through gmap',
  `pickup_address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pickup_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'meeting point (e.g. house/flat no)',
  `dropoff_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'dropoff address through gmap',
  `dropoff_address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dropoff_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'dropoff details (e.g. house/flat no)',
  `no_of_person` int(11) NOT NULL,
  `no_of_child` int(11) NOT NULL,
  `no_of_infant` int(11) NOT NULL,
  `no_of_booster` int(11) NOT NULL,
  `car_details` int(11) NOT NULL,
  `total_bill` int(11) NOT NULL,
  `total_distance` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `via_points` longtext COLLATE utf8_unicode_ci NOT NULL,
  `picking_category` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'Like Instant, Prebook, Airport',
  `return_booking` int(11) NOT NULL COMMENT '0 = No, 1 = Yes',
  `status` int(11) NOT NULL COMMENT '0 = Pending, 1 = Approve, 2 = Reject',
  `payment_status` int(11) NOT NULL COMMENT '0 = Unpaid; 1 = Paid; 2 = Partially paid',
  `date_time` int(11) NOT NULL,
  `return_date_time` int(11) NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `route1_lat_lon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route1_distance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route1_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route1_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route2_lat_lon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route2_distance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route2_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route2_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suitcases` int(11) NOT NULL,
  `carry_on` int(11) NOT NULL,
  `pickup_address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pickup_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dropoff_address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dropoff_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `pickup_address`, `pickup_address1`, `pickup_details`, `dropoff_address`, `dropoff_address1`, `dropoff_details`, `no_of_person`, `no_of_child`, `no_of_infant`, `no_of_booster`, `car_details`, `total_bill`, `total_distance`, `driver_id`, `total_time`, `via_points`, `picking_category`, `return_booking`, `status`, `payment_status`, `date_time`, `return_date_time`, `comments`, `created_at`, `updated_at`, `route1_lat_lon`, `route1_distance`, `route1_time`, `route1_address`, `route2_lat_lon`, `route2_distance`, `route2_time`, `route2_address`, `suitcases`, `carry_on`, `pickup_address2`, `pickup_postcode`, `dropoff_address2`, `dropoff_postcode`) VALUES
(1, 1, 'st louis, mo', '', '', 'oklahoma city, ok', '', '', 0, 0, 0, 0, 1, 4015, 803, 0, '', '', '', 0, 0, 0, 2015, 0, '', '2015-07-27 22:30:05', '2015-07-27 22:30:05', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(2, 2, 'st louis, mo', '', '', 'oklahoma city, ok', '', '', 0, 0, 0, 0, 1, 4015, 803, 0, '', '', '', 0, 1, 1, 2015, 0, '', '2015-08-26 22:50:01', '2015-08-26 22:50:01', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(3, 1, 'st louis, mo', '', '', 'oklahoma city, ok', '', '', 0, 0, 0, 0, 1, 4015, 803, 0, '', '', '', 0, 1, 1, 2015, 0, '', '2015-08-26 22:30:05', '2015-08-26 22:30:05', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(4, 1, 'st louis, mo', '', '', 'oklahoma city, ok', '', '', 0, 0, 0, 0, 1, 4015, 803, 2, '', '', '', 0, 1, 1, 2015, 0, '', '2015-07-27 22:30:05', '2015-07-27 22:30:05', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(5, 3, 'st louis, mo', '', '', 'oklahoma city, ok', '', '', 0, 0, 0, 0, 1, 2757, 802, 0, '', '', '', 0, 0, 0, 2015, 0, '', '2015-08-09 16:04:48', '2015-08-27 16:04:48', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(6, 3, 'London+Borough+of+Southwark,+United+Kingdom', '', '', 'Cardiff,+United+Kingdom', '', '', 0, 0, 0, 0, 2, 1233, 383, 2, '', 'Heathrow+Terminal+5,+Longford,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, 0, 1440867600, 1441008000, '', '2015-08-25 17:10:53', '2015-08-25 17:10:53', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(7, 4, 'London,+United+Kingdom', '', '', 'Barming,+Maidstone+District,+United+Kingdom', '', '', 0, 0, 0, 0, 2, 283, 48, 2, '', '', '', 1, 0, 0, 1440748800, 1440921600, '', '2015-08-25 23:50:15', '2015-08-25 23:50:15', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(8, 3, 'London, United Kingdom', '', '', 'Heathrow Terminal 5, Longford, United Kingdom', '', '', 0, 0, 0, 0, 1, 104, 19, 1, '', '', '', 1, 0, 0, 0, 1441094400, '', '2015-08-29 22:30:41', '2015-08-29 22:30:41', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(9, 3, 'London, United Kingdom', '', '', 'Heathrow Terminal 5, Longford, United Kingdom', '', '', 0, 0, 0, 0, 1, 104, 19, 1, '', '', '', 1, 0, 0, 0, 1441267200, '', '2015-08-29 22:43:19', '2015-08-29 22:43:19', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(10, 4, 'London, United Kingdom', '', '', 'Barmouth, United Kingdom', '', '', 0, 0, 0, 0, 2, 1440, 244, 2, '', 'London+Borough+of+Hillingdon,+United+Kingdom|London+Borough+of+Southwark,+United+Kingdom', '', 1, 0, 0, 0, 1441267200, '', '2015-08-30 17:05:39', '2015-08-30 17:05:39', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(11, 4, 'London, United Kingdom', '', '', 'London Borough of Hillingdon, United Kingdom', '', '', 0, 0, 0, 0, 2, 3189, 540, 2, '', 'Barming,+Maidstone+District,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, 0, 0, 1441180800, '', '2015-08-30 18:06:47', '2015-08-30 18:06:47', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(12, 4, 'London, United Kingdom', '', '', 'London Borough of Hillingdon, United Kingdom', '', '', 0, 0, 0, 0, 2, 3189, 540, 2, '', 'Barming,+Maidstone+District,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, 0, 0, 1441180800, '', '2015-08-30 18:07:29', '2015-08-30 18:07:29', '', '', '', '', '', '', '', '', 0, 0, '', '', '', ''),
(13, 1, 'London, United Kingdom', '', '', 'London Borough of Hillingdon, United Kingdom', '', '', 2, 2, 0, 0, 2, 3189, 540, 2, '', 'Barming,+Maidstone+District,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, 0, 0, 1440835200, '', '2015-08-30 18:09:42', '2015-08-30 18:09:42', '', '', '', '', '', '', '', '', 2, 0, '', '', '', ''),
(14, 4, 'London, United Kingdom', '', '', 'London Borough of Hillingdon, United Kingdom', '', '', 2, 2, 0, 0, 2, 3189, 540, 2, '', 'Barming,+Maidstone+District,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, 0, 0, 1441008000, '', '2015-08-30 19:29:40', '2015-08-30 19:29:40', '', '', '', '', '', '', '', '', 2, 0, '', '', '', ''),
(15, 4, 'London, United Kingdom', '', '', 'London Borough of Hillingdon, United Kingdom', '', '', 2, 2, 0, 0, 2, 3189, 540, 2, '', 'Barming,+Maidstone+District,+United+Kingdom|Barmouth,+United+Kingdom', '', 1, 0, 0, 0, 1441267200, '', '2015-08-30 19:31:56', '2015-08-30 19:31:56', '', '', '', '', '', '', '', '', 2, 0, '', '', '', ''),
(16, 5, 'London, United Kingdom', '', 'House number : 06, Rd : 3, abcd area Postcode : 4000', 'Barmouth, United Kingdom', '', 'House : 50, Rd : 04, O. R. Nizam R/A. Postcode : 4000', 2, 2, 0, 0, 2, 1385, 234, 2, '', '', '', 1, 0, 0, 0, 1441267200, '', '2015-08-30 20:53:55', '2015-08-30 20:53:55', '', '', '', '', '', '', '', '', 2, 0, '', '', '', ''),
(17, 4, 'Barmouth, United Kingdom', 'Rd: 01, H : 50', '', 'Barming, London, United Kingdom', 'Rd : 05, H : 334', '', 2, 3, 0, 0, 2, 905, 281, 2, '', '', '', 1, 0, 0, 0, 1444291200, 'testing fdfd', '2015-09-01 17:20:44', '2015-09-01 17:20:44', '', '', '', '', '', '', '', '', 3, 0, '', '4000', '', '4001'),
(18, 4, 'London, United Kingdom', '', '', 'Durham, United Kingdom', '', '', 2, 2, 0, 0, 1, 1408, 262, 1, '', '', '', 1, 0, 0, 0, 1444291200, 'fsdfdsfsd', '2015-09-02 15:46:52', '2015-09-02 15:46:52', '', '', '', '', '', '', '', '', 2, 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `passengers`
--

CREATE TABLE IF NOT EXISTS `passengers` (
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` int(11) NOT NULL,
  `residence_no` int(11) NOT NULL,
  `fax_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `passengers`
--

INSERT INTO `passengers` (`first_name`, `last_name`, `contact_name`, `address`, `gender`, `city`, `street`, `postcode`, `residence_no`, `fax_number`, `email`, `phone_number`, `created_at`, `updated_at`, `id`) VALUES
('Imon', 'Islam', '', '', '', '', '', 0, 0, '', 'imon0077@gmail.com', '+8801616010000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
('Imon', 'Islam', '', '', '', '', '', 0, 0, '', 'imon0077@yahoo.com', '44444444', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2),
('Md.', 'Anisuzzaman', '', '', '', '', '', 0, 0, '', 'tasneemanis4@gmail.com', '01672053707', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3),
('Mr.', 'Islam', '', '', '', '', '', 0, 0, '', 'me@imonislam.com', '01616010000', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4),
('Mr.', 'Arif', '', '', '', '', '', 0, 0, '', 'imoni@ris10.com', '43434', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5);

-- --------------------------------------------------------

--
-- Table structure for table `pickup_from_airport`
--

CREATE TABLE IF NOT EXISTS `pickup_from_airport` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(11) NOT NULL,
  `flight_arrival_time` int(11) NOT NULL,
  `airline_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `flight_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `departure_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pickup_after` int(11) NOT NULL COMMENT 'When should the driver pick you up? (Minutes after landing)',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pickup_from_airport`
--

INSERT INTO `pickup_from_airport` (`id`, `order_id`, `flight_arrival_time`, `airline_name`, `flight_no`, `departure_city`, `display_name`, `pickup_after`, `created_at`, `updated_at`) VALUES
(1, 6, 12, 'Air Arabia', '12', 'Chittagong', '', 1, '2015-08-25 17:10:53', '2015-08-25 17:10:53'),
(2, 7, 0, '', '', '', '', 0, '2015-08-25 23:50:15', '2015-08-25 23:50:15'),
(3, 8, 0, '', '', '', '', 0, '2015-08-29 22:30:42', '2015-08-29 22:30:42'),
(4, 9, 0, '', '', '', '', 0, '2015-08-29 22:43:19', '2015-08-29 22:43:19'),
(5, 10, 0, '', '', '', '', 0, '2015-08-30 17:05:39', '2015-08-30 17:05:39'),
(6, 11, 0, '', '', '', '', 0, '2015-08-30 18:06:47', '2015-08-30 18:06:47'),
(7, 12, 0, '', '', '', '', 0, '2015-08-30 18:07:29', '2015-08-30 18:07:29'),
(8, 13, 0, '', '', '', '', 0, '2015-08-30 18:09:42', '2015-08-30 18:09:42'),
(9, 14, 0, '', '', '', '', 0, '2015-08-30 19:29:40', '2015-08-30 19:29:40'),
(10, 15, 0, '', '', '', '', 0, '2015-08-30 19:31:56', '2015-08-30 19:31:56'),
(11, 16, 0, '', '', '', '', 0, '2015-08-30 20:53:55', '2015-08-30 20:53:55'),
(12, 17, 0, '', '', '', '', 0, '2015-09-01 17:20:44', '2015-09-01 17:20:44'),
(13, 18, 0, '', '', '', '', 0, '2015-09-02 15:46:52', '2015-09-02 15:46:52');

-- --------------------------------------------------------

--
-- Table structure for table `pricelist`
--

CREATE TABLE IF NOT EXISTS `pricelist` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_price` int(10) DEFAULT NULL COMMENT 'set unit price for per km, per person, child, infant, booster etc',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pricelist`
--

INSERT INTO `pricelist` (`id`, `name`, `unit_price`, `created_at`, `updated_at`) VALUES
(1, 'Per km distance', 10, '0000-00-00 00:00:00', '2015-07-08 21:10:58'),
(2, 'Child seats (9-18kg, 1-3 years)', 5, '0000-00-00 00:00:00', '2015-07-08 21:11:17'),
(3, 'Infant seats (10-13kg, up to 1 year)', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Booster seats (15-25kg, 3-8 years)', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Stop on the way(via)', 3, '0000-00-00 00:00:00', '2015-07-08 21:11:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'imon0077@gmail.com', 1, '$2y$10$GIRme40.GDM4iUMcsTbsAOW/EpjNfiES4Y/LDVsOy3rZ/UUckW0im', '599bBmfbPSim9131k7Iia97aMHqthRMt6J25pUsCGHnJ3WRmJKgRnkK5drLV', '0000-00-00 00:00:00', '2015-07-07 21:51:51'),
(2, 'imon0077@yahoo.com', 2, '$2y$10$GIRme40.GDM4iUMcsTbsAOW/EpjNfiES4Y/LDVsOy3rZ/UUckW0im', 'n58Oq2kXJpkIzV2yutNQqFMoxImPBbrZg2iRHNoNRYuC6D2hHjsVF4vlSE5A', '0000-00-00 00:00:00', '2015-07-07 21:54:37'),
(3, 'me@imonislam.com', 2, '$2y$10$4g1f0lDSMBqbdWPaOCjS.OPsYc.8GqCdKVdAmCJtYfDnrrxwLOGre', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `airports`
--
ALTER TABLE `airports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_category`
--
ALTER TABLE `car_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_details`
--
ALTER TABLE `car_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cruiseports`
--
ALTER TABLE `cruiseports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passengers`
--
ALTER TABLE `passengers`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `passengers_email_unique` (`email`);

--
-- Indexes for table `pickup_from_airport`
--
ALTER TABLE `pickup_from_airport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricelist`
--
ALTER TABLE `pricelist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `airports`
--
ALTER TABLE `airports`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `car_category`
--
ALTER TABLE `car_category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `car_details`
--
ALTER TABLE `car_details`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cruiseports`
--
ALTER TABLE `cruiseports`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `passengers`
--
ALTER TABLE `passengers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pickup_from_airport`
--
ALTER TABLE `pickup_from_airport`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pricelist`
--
ALTER TABLE `pricelist`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
